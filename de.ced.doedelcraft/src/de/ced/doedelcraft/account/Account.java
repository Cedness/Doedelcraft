package de.ced.doedelcraft.account;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.account.command.CommandManager;
import de.ced.doedelcraft.account.command.CommandResult;
import de.ced.doedelcraft.account.command.Executable;
import de.ced.doedelcraft.account.command.SadCommand;
import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Addable;
import de.ced.doedelcraft.ingame.chaa.Removable;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.UserData;
import de.ced.doedelcraft.menu.CharSorting;
import de.ced.doedelcraft.menu.MenuPlayer;
import de.ced.doedelcraft.util.Logger;
import de.ced.doedelcraft.util.SimpleSound;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

/**
 * Equivalent to Bukkit's {@link Player}. Represents a Minecraft account. An
 * Account-instance is created when a player joins the server and destroyed when
 * he leaves.
 * 
 * @author Ced
 */
public class Account extends AbstractUnit implements Addable, Removable {

	private final Player player;
	private final AccountManager accountManager;
	private final Doedelcraft doedelcraft;
	private final File configFile;
	private final YamlConfiguration config;
	private final String name;
	private Rank rank;
	private String languageId;
	private Language language;
	private CharSorting charSorting;
	private boolean charSortingReversed;
	private int charCreationCount;
	private List<Integer> saveFileIds;
	private final Map<Integer, UserData> saveFiles = new HashMap<>();

	private MenuPlayer menuPlayer;
	private User user;

	private final Commander commander;

	Account(Player player, AccountManager accountManager) {
		this.player = player;
		this.accountManager = accountManager;
		doedelcraft = accountManager.getDoedelcraft();
		name = player.getName();
		configFile = new File(doedelcraft.getDataFolder(), "players/" + player.getUniqueId().toString() + "/account.yml");
		boolean newPlayer = !configFile.exists();
		config = YamlConfiguration.loadConfiguration(configFile);
		config.setDefaults(doedelcraft.getDefaultAccountConfig());
		config.options().copyDefaults(true);

		if (newPlayer) {
			config.set("uuid", player.getUniqueId().toString());
			// Logger.info("Account file for new player " + name + " created");
		}

		config.set("name", name);
		setRank(Rank.valueOf(config.getString("rank", Rank.NONE.name())));
		setLanguageId(config.getString("language", "en"));

		setCharSorting(CharSorting.valueOf(config.getString("sort.type", CharSorting.getSortings().get(0).name())));
		setCharSortingReversed(config.getBoolean("sort.reversed", true));

		charCreationCount = config.getInt("char-creation-count", 0);
		config.set("char-creation-count", charCreationCount);

		saveFileIds = config.getIntegerList("chars");
		if (saveFileIds == null) {
			saveFileIds = new ArrayList<>();
		}
		for (int charId = 0; charId < saveFileIds.size(); charId++) {
			int saveFileId = saveFileIds.get(charId);
			UserData saveFile = new UserData(this, doedelcraft, saveFileId, charId);
			saveFiles.put(saveFileId, saveFile);
			saveFile.saveConfig();
		}

		// Logger.info("Account file for player " + name + " (Rank: " + rank.getName() +
		// ") loaded");

		commander = new Commander();

		saveConfig();
	}

	public void tick(int tick) {
		commander.tick(tick);
	}

	@Override
	public void add() {
	}

	@Override
	public void remove() {
		user = null;
	}

	public Player getPlayer() {
		return player;
	}

	public AccountManager getAccountManager() {
		return accountManager;
	}

	public YamlConfiguration getConfig() {
		return config;
	}

	public void saveConfig() {
		try {
			config.save(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Logger.info("Account file for player " + name + " updated");
	}

	public String getName() {
		return name;
	}

	public Rank getRank() {
		return rank;
	}

	public void setRank(Rank rank) {
		this.rank = rank;
		config.set("rank", rank.name());
	}

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
		config.set("language", this.languageId);
		this.language = accountManager.getLanguageManager().getLanguage(this.languageId);
	}

	public Language getLanguage() {
		return language;
	}

	public Dictionary getD(String key) {
		return language.getD(key);
	}

	public CharSorting getCharSorting() {
		return charSorting;
	}

	public void setCharSorting(CharSorting charSorting) {
		this.charSorting = charSorting;
		config.set("sort.type", charSorting.name());
	}

	public boolean isCharSortingReversed() {
		return charSortingReversed;
	}

	public void setCharSortingReversed(boolean charSortingReversed) {
		this.charSortingReversed = charSortingReversed;
		config.set("sort.reversed", charSortingReversed);
	}

	public int getCharCount() {
		return saveFiles.size();
	}

	public UserData addChar() {
		int saveFileId = charCreationCount++;
		saveFileIds.add(saveFileId);
		config.set("chars", saveFileIds);
		config.set("char-creation-count", charCreationCount);
		saveConfig();
		UserData chaa = new UserData(this, doedelcraft, saveFileId, saveFileIds.size() - 1);
		saveFiles.put(saveFileId, chaa);
		return chaa;
	}

	public void deleteChar(UserData chaa) {
		int charId = chaa.getCharId();
		saveFileIds.remove(charId);
		config.set("chars", saveFileIds);
		saveConfig();
		saveFiles.remove(chaa.getSaveFileId());
		for (; charId < getCharCount(); charId++) {
			getChar(charId).setCharId(charId);
		}
	}

	public UserData getChar(int charId) {
		return saveFiles.get(saveFileIds.get(charId));
	}

	public boolean hasMenuPlayer() {
		return menuPlayer != null;
	}

	public MenuPlayer getMenuPlayer() {
		return menuPlayer;
	}

	public void setMenuPlayer(MenuPlayer menuPlayer) {
		this.menuPlayer = menuPlayer;
	}

	public boolean hasUser() {
		return user != null;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void message(String message) {
		player.sendMessage(message);
	}

	public void actionbar(String message) {
		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(message));
	}

	public void kick(String message) {
		player.kickPlayer(message);
	}

	public Commander getCommander() {
		return commander;
	}

	/**
	 * Contains all ranks in the game. Permissions are given by level, so a higher
	 * level inherits all permissions given to lower levels.
	 * 
	 * @author Ced
	 */
	public enum Rank {

		// Name, PermissionLevel, Color
		NONE(0, "f"), // Players
		CONTENT_CREATOR(1, "5"), // Team members, can do some not so critical administrative stuff.
		ADMIN(2, "4"), // Team members, can do all regular administrative stuff.
		DEVELOPER(3, "6"); // Team members, can do everything. Be careful with this one!

		private final int permissionLevel;
		private final String color;

		Rank(int permissionLevel, String color) {
			this.permissionLevel = permissionLevel;
			this.color = "§" + color;
		}

		public int getPermissionLevel() {
			return permissionLevel;
		}

		public String getColor() {
			return color;
		}
	}

	public class Commander {

		private final CommandManager commandManager;
		private final Dictionary dictionary;
		private final CommandResult[] results;
		private boolean cmdMode = false;

		private String vanillaCommand = null;
		private boolean setOp = false;

		public Commander() {
			commandManager = accountManager.getCommandManager();
			dictionary = getD("command");
			results = new CommandResult[] {
					new CommandResult("invalid", '4', new SimpleSound(Sound.BLOCK_ANVIL_LAND)),
					new CommandResult("login_required", 'c', new SimpleSound(Sound.BLOCK_ANVIL_LAND)),
					new CommandResult("cmd_required", 'c', new SimpleSound(Sound.BLOCK_ANVIL_LAND))
			};
			player.setOp(false);
		}

		public void tick(int tick) {
			if (vanillaCommand != null) {
				String vanillaCommand = this.vanillaCommand;
				boolean setOp = this.setOp;
				this.vanillaCommand = null;
				this.setOp = false;
				if (setOp) {
					player.setOp(true);
				}
				try {
					player.performCommand(vanillaCommand);
				} catch (Exception e) {
					Logger.severe("Exception while executing vanilla command for player " + name + ": " + vanillaCommand);
					e.printStackTrace();
				}
				if (setOp) {
					player.setOp(false);
				}
			}
		}

		public void onCommandListSend(Collection<String> commands) {
			int permissionLevel = rank.getPermissionLevel();
			List<String> toRemove = new ArrayList<>();
			if (cmdMode) {
				return;
			}
			for (String commandName : commands) {
				if (!commandManager.isOfficialCommand(commandName)) {
					toRemove.add(commandName);
					continue;
				}
				if (commandManager.getCommand(commandName).getPermissionLevel() > permissionLevel) {
					// Logger.info(commandName);
					toRemove.add(commandName);
				}
			}
			commands.removeAll(toRemove);
		}

		public void prepareCommand(String command) {
			if (rank.getPermissionLevel() >= 1) {
				if ("cmd".equals(command)) {
					cmdMode = !cmdMode;
					player.setOp(cmdMode);
					message("§4" + dictionary.get("cmd_changed", (cmdMode ? "§a" : "§c") + dictionary.get("cmd_changed_" + (cmdMode ? "on" : "off")) + "§4"));
					return;
				}
				if ("rl".equals(command)) {
					Bukkit.reload();
					return;
				}
				if (cmdMode) {
					performVanillaCommand(command, false);
					return;
				} else if (command.startsWith("cmd ")) {
					performVanillaCommand(command.substring(4), true);
					return;
				}
			}
			performCommand(split(command));
		}

		private void performVanillaCommand(String vanillaCommand, boolean setOp) {
			String[] parts = split(vanillaCommand);
			if (doedelcraft.getCommand(parts[0]) != null) {
				performCommand(parts);
				return;
			}
			this.vanillaCommand = vanillaCommand;
			this.setOp = setOp;
		}

		private String[] split(String command) {
			return command.trim().split(" ");
		}

		private void performCommand(String[] parts) {
			SadCommand command = commandManager.getCommand(parts[0]);
			Executable executable = new Executable(Account.this, command);
			int resultId = processCommand(executable, parts);
			CommandResult result = getResult(command, resultId);
			displayResult(executable, result);
			playResult(result);
		}

		private int processCommand(Executable executable, String[] parts) {
			SadCommand command = executable.getCommand();
			int permissionLevel = rank.getPermissionLevel();
			if (command == null || command.getPermissionLevel() > permissionLevel) {
				return Integer.MIN_VALUE;
			}
			if (command.needsUser() && user == null) {
				return Integer.MIN_VALUE + 1;
			}
			if (command.needsCmd() && !cmdMode) {
				if (permissionLevel >= 1) {
					return Integer.MIN_VALUE + 2;
				}
				return Integer.MIN_VALUE; // Just backup for dump commands requiring cmd but having perm 0
			}
			try {
				return executable.getActions().execute(parts, parts.length);
			} catch (Exception e) {
				Logger.severe("Exception while performing command " + command.getName() + " for account " + name);
				e.printStackTrace();
			}
			return Integer.MIN_VALUE;
		}

		private CommandResult getResult(SadCommand command, int resultId) {
			int failId = resultId - Integer.MIN_VALUE;
			if (0 <= failId && failId < results.length) {
				return results[failId];
			}
			if (resultId < 0) {
				return null;
			}
			return command.getResult(resultId);
		}

		private void displayResult(Executable executable, CommandResult result) {
			if (result == null) {
				return;
			}
			String msg = result.getMessage();
			String color = result.getColor();
			String[] oldArgs = executable.getCommand() != null ? executable.getActions().getResultArgs() : null;
			String[] args;
			args = new String[oldArgs == null ? 1 : oldArgs.length + 1];
			for (int i = 1; i < args.length; i++) {
				args[i] = oldArgs[i - 1];
			}
			args[0] = color;
			msg = color + dictionary.get(msg, args);
			if (user != null) {
				user.getUI().getBottomLabel().setActionbarText(msg, 60);
			} else {
				actionbar(msg);
			}
		}

		private void playResult(CommandResult result) {
			if (result == null || result.getSound() == null) {
				CommandResult.FALLBACK_RESULT.getSound().playFor(player);
			} else {
				result.getSound().playFor(player);
			}
		}
	}
}
