package de.ced.doedelcraft.account;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.configuration.file.YamlConfiguration;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.util.Logger;

public class LanguageManager {

	private final Map<String, Language> languages = new LinkedHashMap<>();
	private final Language fallbackLanguage;

	public LanguageManager(AccountManager accountManager) {
		Doedelcraft doedelcraft = accountManager.getDoedelcraft();
		Language fallbackLanguage = null;
		try {
			YamlConfiguration languagesConfig = doedelcraft.loadConfig("languages");
			for (String languageId : languagesConfig.getStringList("languages")) {
				try {
					YamlConfiguration languageConfig = doedelcraft.loadConfig(languageId, "lang", "languages");
					Language language = new Language(languageId, languageConfig);
					languages.put(languageId, language);
				} catch (Exception e) {
					Logger.severe("Exception while loading language \"" + languageId + "\"");
					e.printStackTrace();
				}
			}
			fallbackLanguage = languages.values().iterator().next();
			for (Language language : languages.values()) {
				language.fillInIfAbsent(fallbackLanguage);
			}
		} catch (Exception e) {
			Logger.severe("General exception while loading languages");
			e.printStackTrace();
			if (languages.isEmpty()) {
				Logger.fatal("Fatal Exception: No language could be loaded!");
			}
		}
		this.fallbackLanguage = fallbackLanguage;

//		for (Language lanugage : languages.values()) {
//			Logger.info("Loaded language:\n" + lanugage);
//		}
	}

	public Language getLanguage(String language) {
		return languages.getOrDefault(language, fallbackLanguage);
	}

	public Set<Entry<String, Language>> getLanguageSet() {
		return languages.entrySet();
	}

	public Collection<Language> getLanguages() {
		return languages.values();
	}

	public Language getFallbackLanguage() {
		return fallbackLanguage;
	}
}
