package de.ced.doedelcraft.account.command;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.account.Account;
import de.ced.doedelcraft.account.Dictionary;
import de.ced.doedelcraft.account.Language;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.UserManager;
import de.ced.doedelcraft.ingame.chaa.combat.GenuineUserCombat;
import de.ced.doedelcraft.ingame.chaa.exhaustion.Exhaustion;
import de.ced.doedelcraft.ingame.chaa.health.Health;
import de.ced.doedelcraft.ingame.chaa.mana.Mana;
import de.ced.doedelcraft.ingame.chaa.spellinfo.SpellInfo;
import de.ced.doedelcraft.ingame.chaa.stat.StatLike;
import de.ced.doedelcraft.ingame.clazz.Clazz;
import de.ced.doedelcraft.ingame.clazz.Spell;
import de.ced.doedelcraft.ingame.world.time.LunarPhase;
import de.ced.doedelcraft.ingame.world.time.TimeManager;
import de.ced.doedelcraft.menu.MenuManager;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

public class Executable {

	private final Account account;
	private final SadCommand command;
	private final CommandManager commandManager;
	private final Doedelcraft doedelcraft;
	private final CommandActions actions;

	public Executable(Account account, SadCommand command) {
		this.account = account;
		this.command = command;
		commandManager = account.getAccountManager().getCommandManager();
		doedelcraft = account.getAccountManager().getDoedelcraft();
		CommandActions actions = null;
		if (command != null) {
			try {
				actions = (CommandActions) command.getActionsClass().getConstructor(Executable.class).newInstance(this);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		}
		this.actions = actions;
	}

	public Account getAccount() {
		return account;
	}

	public SadCommand getCommand() {
		return command;
	}

	public CommandActions getActions() {
		return actions;
	}

	public abstract class CommandActions {

		protected String[] args;

		public String[] getResultArgs() {
			return args;
		}

		public int execute(String[] parts, int count) {
			return 0;
		}

		protected Language getLanguage(Char chaa) {
			return chaa instanceof User ? ((User) chaa).getLanguage() : commandManager.getAccountManager().getLanguageManager().getFallbackLanguage();
		}

		protected Spell getSpellByName(Char chaa, String[] parts, int start, int end) {
			StringBuilder spellNameBuilder = new StringBuilder(parts[start]);
			for (int i = start + 1; i <= end; i++) {
				spellNameBuilder.append(' ').append(parts[i]);
			}
			String spellName = spellNameBuilder.toString();
			Clazz clazz = chaa.getClazz();
			Dictionary dictionary = (chaa instanceof User ? ((User) chaa).getLanguage() : commandManager.getAccountManager().getLanguageManager().getFallbackLanguage())
					.getD("spell").getD(clazz.getName());
			for (Spell spell : chaa.getClazz().getAllSpells()) {
				if (spellName.equals(dictionary.getD(spell.getName()).get("name"))) {
					return spell;
				}
			}
			return null;
		}

		protected User getUser(String name) {
			if (name.length() < 2) {
				return null;
			}
			name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
			UserManager userManager = doedelcraft.getUserManager();
			if (!userManager.hasUser(name)) {
				return null;
			}
			return userManager.getUser(name);
		}
	}

	public class CmdCommandActions extends CommandActions {

	}

	public class RlCommandActions extends CommandActions {

	}

	public class LogoutCommandActions extends CommandActions {

		@Override
		public int execute(String[] parts, int count) {
			boolean loggedIn = account.hasMenuPlayer() || account.hasUser();
			doedelcraft.getMenuManager().remove(account);
			doedelcraft.getUserManager().remove(account);
			if (loggedIn) {
				account.message("§7Do /login to log back in");
				return 0;
			}
			return 1;
		}
	}

	public class ClassCommandActions extends CommandActions {

		@Override
		public int execute(String[] parts, int count) {
			args = new String[] { "§a" };
			int result = !account.hasUser() && !account.hasMenuPlayer() ? 1 : 0;
			doedelcraft.getUserManager().remove(account);
			MenuManager menuManager = doedelcraft.getMenuManager();
			menuManager.remove(account);
			menuManager.add(account);
			menuManager.prepare(account);
			return result;
		}
	}

	public class MsgCommandActions extends CommandActions {

		@Override
		public int execute(String[] parts, int count) {
			User user = account.getUser();
			if (count < 2) {
				return 0;
			}
			String receiverName = parts[1];
			if (receiverName.length() < 2) {
				args = new String[] { receiverName };
				return 1;
			}
			UserManager userManager = doedelcraft.getUserManager();
			receiverName = receiverName.substring(0, 1).toUpperCase() + receiverName.substring(1).toLowerCase();
			if (!userManager.hasUser(receiverName)) {
				args = new String[] { receiverName };
				return 1;
			}
			User receiver = userManager.getUser(receiverName);
			if (user.equals(receiver)) {
				user.getMinecraft().message("§cIf you feel the need to talk to yourself, we nicely ask you not to use our game to satisfy this need. "
						+ "We strongly advise you to rather look for a psychiatrist. " + "Please take care of your health!");
				return -1;
			}
			if (count < 3) {
				args = new String[] { receiverName };
				return 2;
			}
			StringBuilder messageBuilder = new StringBuilder();
			for (int i = 2; i < count; i++) {
				messageBuilder.append(' ').append(parts[i]);
			}
			String message = messageBuilder.toString();
			user.getMinecraft().message("§8[§7You §8-> §7" + receiver.getIdentity().getName() + "§8]§f" + message);
			receiver.getMinecraft().message("§8[§7" + user.getIdentity().getName() + " §8-> §7You§8]§f" + message);
			return -1;
		}
	}

	public class KillCommandActions extends CommandActions {

		@Override
		public int execute(String[] parts, int count) {
			User user = account.getUser();
			User receiver;
			String name;
			if (count < 2 || account.getRank().getPermissionLevel() < 1) {
				receiver = user;
				name = "yourself";
			} else if (count > 2) {
				return 0;
			} else {
				receiver = getUser(parts[1]);
				if (receiver == null) {
					args = new String[] { parts[1] };
					return 1;
				}
				name = receiver.getIdentity().getName();
			}
			user.getMinecraft().message("§7§oDealing some vast damage to " + name + "...");
			receiver.getDefence().setLastDamager(receiver);
			receiver.getHealth().set(0);
			return -1;
		}
	}

	public abstract class ToggleCommandActions extends CommandActions {

		public ToggleCommandActions() {
			args = new String[] { "§7" };
		}

		@Override
		public int execute(String[] parts, int count) {
			boolean boo = getBoolean(account);
			if (count < 2) {
				boo = !boo;
			} else if ("on".equals(parts[1])) {
				boo = true;
			} else if ("off".equals(parts[1])) {
				boo = false;
			} else {
				return 0;
			}
			setBoolean(boo, account);
			return boo ? 2 : 1;
		}

		protected abstract boolean getBoolean(Account account);

		protected abstract void setBoolean(boolean boo, Account account);
	}

	public class CombatCommandActions extends ToggleCommandActions {

		@Override
		protected boolean getBoolean(Account account) {
			return account.getUser().getCombat().isCombatMode();
		}

		@Override
		protected void setBoolean(boolean boo, Account account) {
			account.getUser().getCombat().toggleCombatMode(boo);
		}
	}

	public class InstantCommandActions extends ToggleCommandActions {

		@Override
		protected boolean getBoolean(Account account) {
			return true;
		}

		@Override
		protected void setBoolean(boolean boo, Account account) {
		}
	}

	public class TutorialCommandActions extends ToggleCommandActions {

		@Override
		protected boolean getBoolean(Account account) {
			return account.getUser().getData().getTutorialMode();
		}

		@Override
		protected void setBoolean(boolean boo, Account account) {
			User user = account.getUser();
			user.getData().setTutorialMode(boo);
			user.getUI().getSideLabel().setShowControlObjective(boo);
		}
	}

	public class SidebarCommandActions extends ToggleCommandActions {

		@Override
		protected boolean getBoolean(Account account) {
			return account.getUser().getUI().getSideLabel().getSidebarState();
		}

		@Override
		protected void setBoolean(boolean boo, Account account) {
			account.getUser().getUI().getSideLabel().setSidebarState(boo);
		}
	}

	public class AnimatedbarsCommandActions extends ToggleCommandActions {

		@Override
		protected boolean getBoolean(Account account) {
			return account.getUser().getUI().isAnimatedbars();
		}

		@Override
		protected void setBoolean(boolean boo, Account account) {
			account.getUser().getUI().setAnimatedbars(boo);
		}
	}

	public class LanguageCommandActions extends CommandActions {

	}

	@Deprecated
	public class SpawnCommandActions extends CommandActions {

		{
			args = new String[1];
		}

		@Override
		public int execute(String[] parts, int count) {
			if (count < 2) {
				args[0] = "nothing";
				return 1;
			}
			String type = "";
			for (int i = 1; i < count; i++) {
				String part = parts[i];
				type += part.substring(0, 1) + part.substring(1);
			}
			args[0] = type;
			if ("Npc".equals(type)) {
				// NPManager npManager = doedelcraft.getNPManager();
				// npManager.spawnTestNPC(account.getUser().getMinecraft().getLocation());
				return 0;
			}
			return 1;
		}
	}

	public class TimeCommandActions extends CommandActions {

		{
			args = new String[1];
		}

		@Override
		public int execute(String[] parts, int count) {
			if (count > 4) {
				return 0;
			}

			TimeManager timeManager = doedelcraft.getWorldManager().getTimeManager();

			if (count <= 1) {
				args[0] = timeManager.getFormattedTime();
				return 1;
			}

			int hour, minute, second;

			try {
				hour = Integer.parseInt(parts[1]);
				minute = count >= 3 ? Integer.parseInt(parts[2]) : 0;
				second = count >= 4 ? Integer.parseInt(parts[3]) : 0;
			} catch (NumberFormatException sadLife) {
				return 0;
			}

			if (timeManager.setDayTime(hour, minute, second, 0)) {
				args[0] = timeManager.getFormattedTime();
				return 2;
			} else {
				return 0;
			}
		}
	}

	public class LunarphaseCommandActions extends CommandActions {

		{
			args = new String[1];
		}

		@Override
		public int execute(String[] parts, int count) {
			if (count > 2) {
				return 0;
			}

			TimeManager timeManager = doedelcraft.getWorldManager().getTimeManager();

			if (count <= 1) {
				args[0] = timeManager.getLunarPhase().name();
				return 1;
			}

			String lunarPhaseName = parts[1].toUpperCase();
			LunarPhase lunarPhase;
			try {
				lunarPhase = LunarPhase.valueOf(lunarPhaseName);
			} catch (IllegalArgumentException ex) {
				return 0;
			}
			timeManager.setLunarPhase(lunarPhase);
			args[0] = lunarPhase.name();
			return 2;
		}
	}

	public class ExpCommandActions extends CommandActions {

		@Override
		public int execute(String[] parts, int count) {
			User user = account.getUser();
			if (count != 2) {
				if (count != 3 || account.getRank().getPermissionLevel() < 2) {
					return 0;
				}
				String receiverName = parts[2];
				if (receiverName.length() < 2) {
					args = new String[] { receiverName };
					return 3;
				}
				UserManager userManager = doedelcraft.getUserManager();
				receiverName = receiverName.substring(0, 1).toUpperCase() + receiverName.substring(1).toLowerCase();
				if (!userManager.hasUser(receiverName)) {
					args = new String[] { receiverName };
					return 3;
				}
				user = userManager.getUser(receiverName);
			}
			try {
				double newValue = Double.parseDouble(parts[1]);
				if (newValue < 0) {
					return 1;
				}
				user.getProgress().giveExp(newValue);
				args = new String[] { "§e", String.valueOf(newValue), user.getIdentity().getName() };
				return 4;
			} catch (NumberFormatException ex) {
				return 0;
			}
		}
	}

	public abstract class BarCommandActions extends CommandActions {

		@Override
		public int execute(String[] parts, int count) {
			User user = account.getUser();
			if (count != 2) {
				if (count != 3 || account.getRank().getPermissionLevel() < 2) {
					return 0;
				}
				User receiver = getUser(parts[2]);
				if (receiver == null) {
					args = new String[] { parts[2] };
					return 3;
				}
				user = receiver;
			}
			try {
				int newValue = Integer.parseInt(parts[1]);
				StatLike bar = getBar(user);
				if (newValue < bar.getMin()) {
					return 1;
				}
				if (newValue > bar.getMax()) {
					return 2;
				}
				bar.set(newValue, false);
				args = new String[] { String.valueOf(bar.get()) };
				return 4;
			} catch (NumberFormatException ex) {
				return 0;
			}
		}

		protected abstract StatLike getBar(Char chaa);
	}

	public class HealthCommandActions extends BarCommandActions {

		@Override
		protected StatLike getBar(Char chaa) {
			return chaa.getHealth();
		}
	}

	public class ExhaustionCommandActions extends BarCommandActions {

		@Override
		protected StatLike getBar(Char chaa) {
			return chaa.getExhaustion();
		}
	}

	public class ManaCommandActions extends BarCommandActions {

		@Override
		protected StatLike getBar(Char chaa) {
			return chaa.getMana();
		}
	}

	public class ModifyprogressCommandActions extends BarCommandActions {

		@Override
		protected StatLike getBar(Char chaa) {
			return chaa.getProgress().getProgressBar();
		}
	}

	public class ModifyageCommandActions extends BarCommandActions {

		@Override
		protected StatLike getBar(Char chaa) {
			return chaa.getProgress().getLevelBar();
		}
	}

	public class CooldownCommandActions extends CommandActions {

		@Override
		public int execute(String[] parts, int count) {
			User receiver;
			int end = parts.length - 1;
			if (count < 2) {
				return 0;
			} else if (count < 3 || account.getRank().getPermissionLevel() < 1) {
				receiver = account.getUser();
			} else {
				receiver = getUser(parts[parts.length - 1]);
				if (receiver == null) {
					receiver = account.getUser();
				} else {
					end--;
				}
			}

			SpellInfo spellInfo = receiver.getSpellInfo();
			Spell spell = getSpellByName(receiver, parts, 1, end);
			if (spell == null || spellInfo.getSpellLevel(spell.getSpellId()) < spell.getSpellLevel()) {
				return 1;
			}
			if (!spellInfo.isCooldowningSpell(spell)) {
				return 2;
			}
			spellInfo.removeSpellCooldown(spell);
			if (spell.getSpellLevel() == spellInfo.getSelectedSpellLevel(spell.getSpellId())) {
				receiver.getMinecraft().setCooldown(((GenuineUserCombat) receiver.getCombat()).getSpellItem(spell.getSpellId()).getType(), 0);
			}
			User user = account.getUser();
			args = new String[] { getLanguage(user).getD("spell").getD(user.getClazz().getName()).getD(spell.getName()).get("name") };
			return 3;
		}
	}

	public class RefreshCommandActions extends CommandActions {

		@Override
		public int execute(String[] parts, int count) {
			User receiver = account.getUser();
			if (count > 2) {
				return 0;
			} else if (count == 2) {
				receiver = getUser(parts[1]);
				if (receiver == null) {
					args = new String[] { parts[1] };
					return 2;
				}
			} else {
				receiver = account.getUser();
			}

			Health health = receiver.getHealth();
			Exhaustion exhaustion = receiver.getExhaustion();
			Mana mana = receiver.getMana();
			SpellInfo spellInfo = receiver.getSpellInfo();

			int maxHealth = health.getMax();
			int maxExhaustion = exhaustion.getMax() - exhaustion.getLocked();
			int maxMana = mana.getMax();

			if (health.get() >= maxHealth
					&& exhaustion.get() >= maxExhaustion
					&& mana.get() >= maxMana
					&& !spellInfo.hasCooldowningSpells()) {
				return 1;
			}

			health.set(maxHealth);
			exhaustion.set(maxExhaustion);
			mana.set(maxMana);

			GenuineUserCombat combat = (GenuineUserCombat) receiver.getCombat();
			for (Iterator<Spell> iterator = spellInfo.getCooldowningSpells().iterator(); iterator.hasNext();) {
				Spell spell = iterator.next();
				iterator.remove();
				spellInfo.removeSpellCooldown(spell);
				if (spell.getSpellLevel() == spellInfo.getSelectedSpellLevel(spell.getSpellId())) {
					receiver.getMinecraft().setCooldown(combat.getSpellItem(spell.getSpellId()).getType(), 0);
				}
			}

			return 3;
		}
	}

	public abstract class GamemodeCommandActions extends CommandActions {

		protected final GameMode gamemode;

		public GamemodeCommandActions(GameMode gamemode) {
			this.gamemode = gamemode;
		}

		@Override
		public int execute(String[] parts, int count) {
			Player player = account.getPlayer();
			if (player.getGameMode() == gamemode) {
				return 1;
			}
			player.setGameMode(gamemode);
			return 0;
		}
	}

	public class CommandActions0 extends GamemodeCommandActions {

		public CommandActions0() {
			super(GameMode.SURVIVAL);
		}
	}

	public class CommandActions1 extends GamemodeCommandActions {

		public CommandActions1() {
			super(GameMode.CREATIVE);
		}
	}

	public class CommandActions2 extends GamemodeCommandActions {

		public CommandActions2() {
			super(GameMode.ADVENTURE);
		}
	}

	public class CommandActions3 extends GamemodeCommandActions {

		public CommandActions3() {
			super(GameMode.SPECTATOR);
		}
	}
}
