package de.ced.doedelcraft.account.command;

import de.ced.doedelcraft.util.SimpleSound;
import org.bukkit.Sound;

public class CommandResult {

	public static final CommandResult FALLBACK_RESULT = new CommandResult(new SimpleSound(Sound.ENTITY_ITEM_PICKUP, false));

	private String message;
	private String color;
	private SimpleSound sound;
	private boolean successful;

	public CommandResult() {
		this(null, null, null, true);
	}

	public CommandResult(String message) {
		this(message, null, null, true);
	}

	public CommandResult(SimpleSound sound) {
		this(null, null, sound, true);
	}

	public CommandResult(boolean successful) {
		this(null, null, null, successful);
	}

	public CommandResult(String message, Character color) {
		this(message, color, null, true);
	}

	public CommandResult(String message, SimpleSound sound) {
		this(message, null, sound, true);
	}

	public CommandResult(String message, boolean successful) {
		this(message, null, null, successful);
	}

	public CommandResult(SimpleSound sound, boolean successful) {
		this(null, null, sound, successful);
	}

	public CommandResult(String message, Character color, SimpleSound sound) {
		this(message, color, sound, true);
	}

	public CommandResult(String message, Character color, boolean successful) {
		this(message, color, null, successful);
	}

	public CommandResult(String message, SimpleSound sound, boolean successful) {
		this(message, null, sound, successful);
	}

	public CommandResult(String message, Character color, SimpleSound sound, boolean successful) {
		setMessage(message);
		setColor(color);
		setSound(sound);
		setSuccessful(successful);
	}

	public String getMessage() {
		return message;
	}

	public String getColor() {
		return color;
	}

	public SimpleSound getSound() {
		return sound;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public void setMessage(String message) {
		this.message = message == null ? "" : message;
	}

	public void setColor(Character color) {
		this.color = "§" + (color == null ? '7' : color);
	}

	public void setSound(SimpleSound sound) {
		this.sound = sound == null ? FALLBACK_RESULT.sound : sound;
	}

	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}
}
