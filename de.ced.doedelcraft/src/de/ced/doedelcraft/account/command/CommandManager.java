package de.ced.doedelcraft.account.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.account.AccountManager;
import de.ced.doedelcraft.util.Logger;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerCommandSendEvent;

public class CommandManager implements CommandExecutor, TabCompleter {

	private AccountManager accountManager;
	private Doedelcraft doedelcraft;
	private Map<String, SadCommand> commands = new LinkedHashMap<>();
	private List<SadCommand> commandList = new ArrayList<>();
	private List<String> officialCommands = new ArrayList<>();

	public CommandManager(AccountManager accountManager) {
		this.accountManager = accountManager;
		doedelcraft = accountManager.getDoedelcraft();

		List<String> commandNames = doedelcraft.loadConfig("commands").getStringList("commands");
		for (String commandName : commandNames) {
			// Logger.info("Loading command " + commandName + "...");
			SadCommand command = new SadCommand(this, commandName, doedelcraft.loadConfig(commandName.toLowerCase(), "cmd", "commands"));
			commands.put(commandName, command);
			commandList.add(command);
			officialCommands.add(commandName);
			// Das letzte Hemd hat keine Taschen
			try {
				PluginCommand pluginCommand = doedelcraft.getCommand(commandName);
				pluginCommand.setExecutor(this);
				for (String alias : pluginCommand.getAliases()) {
					commands.put(alias, command);
				}
			} catch (Exception ex) {
				Logger.severe("Exception while registering " + command.getClass().getSimpleName());
				ex.printStackTrace();
			}
		}
	}

	public AccountManager getAccountManager() {
		return accountManager;
	}

	public void registerCommand(SadCommand command) {
		commandList.add(command);
	}

	public boolean isOfficialCommand(String name) {
		return officialCommands.contains(name);
	}

	public boolean hasCommand(String name) {
		return commands.containsKey(name);
	}

	public SadCommand getCommand(String name) {
		return commands.get(name);
	}

	public Collection<SadCommand> getCommands() {
		return commands.values();
	}

	public void onCommandListSend(PlayerCommandSendEvent e) {
		// Logger.info(e.getEventName());

		Collection<String> commands = e.getCommands();
		Player player = e.getPlayer();
		accountManager.getAccount(player).getCommander().onCommandListSend(commands);
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		return doedelcraft.getUserManager().getUserNames();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		onCommand(sender, command.getName(), args);
		return true;
	}

	private void onCommand(CommandSender sender, String command, String[] args) {
		if (!(sender instanceof Player)) {
			if ("rl".equals(command)) {
				Bukkit.reload();
				return;
			}
			sender.sendMessage(Logger.PREFIX + "Error: This is a command made for ingame use");
			return;
		}
		StringBuilder commandBuilder = new StringBuilder(command);
		for (String arg : args) {
			commandBuilder.append(' ').append(arg);
		}
		accountManager.getAccount((Player) sender).getCommander().prepareCommand(commandBuilder.toString());
	}

	public void onPlayerCommand(PlayerCommandPreprocessEvent e) {
		e.setCancelled(true);
		Player player = e.getPlayer();
		accountManager.getAccount(player).getCommander().prepareCommand(e.getMessage().substring(1).trim());
	}
}
