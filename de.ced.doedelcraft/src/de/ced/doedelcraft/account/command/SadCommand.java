package de.ced.doedelcraft.account.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.ced.doedelcraft.util.Logger;
import de.ced.doedelcraft.util.SimpleSound;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;

public class SadCommand {

	private final String name;
	private final Class<?> actionsClass;
	private final int permissionLevel;
	private final boolean needsCmd;
	private final boolean needsUser;
	private final List<CommandResult> results;

	public SadCommand(CommandManager commandManager, String name, ConfigurationSection config) {
		this.name = name;
		String actionsClassName = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
		Class<?> actionsClass = null;
		for (Class<?> anActionsClass : Executable.class.getClasses()) {
			String className = anActionsClass.getSimpleName().replaceAll("CommandActions", "");
			if (className.equals(actionsClassName)) {
				actionsClass = anActionsClass;
				break;
			}
		}
		if (actionsClass == null) {
			Logger.severe("Could not find ActionsClass for command " + name);
		}
		this.actionsClass = actionsClass;
		int permissionLevel = config.getInt("permission_level", 0);
		needsCmd = config.getBoolean("needs_cmd", false);
		this.permissionLevel = Math.max(needsCmd ? 1 : 0, permissionLevel); // to filter out invalid combination perm 0 and cmd true
		needsUser = config.getBoolean("needs_user", true);
		results = new ArrayList<>();
		for (Map<?, ?> map : config.getMapList("results")) {
			@SuppressWarnings("unchecked")
			Map<String, Object> resultConfig = (Map<String, Object>) map;
			boolean successful = (boolean) resultConfig.getOrDefault("successful", Boolean.valueOf(true));
			String message = (String) resultConfig.getOrDefault("message", null);
			String colorString = String.valueOf(resultConfig.getOrDefault("color", ""));
			Character color = colorString.isEmpty() ? null : colorString.charAt(0);
			String soundName = (String) resultConfig.getOrDefault("sound", null);
			SimpleSound sound = null;
			if (!"".equals(soundName)) {
				double volume = (Double) resultConfig.getOrDefault("volume", 1.0);
				double pitch = (Double) resultConfig.getOrDefault("pitch", 1.0);
				Sound mcSound = null;
				for (Sound anotherMCSound : Sound.values()) {
					if (!anotherMCSound.name().equals(soundName)) {
						mcSound = anotherMCSound;
						break;
					}
				}
				sound = new SimpleSound(mcSound, volume, pitch, false);
			}
			results.add(message == null && sound == null ? CommandResult.FALLBACK_RESULT : new CommandResult(message, color, sound, successful));
		}
		if (results.isEmpty()) {
			results.add(CommandResult.FALLBACK_RESULT);
		}

		// ?
		commandManager.registerCommand(this);
	}

	public final String getName() {
		return name;
	}

	public Class<?> getActionsClass() {
		return actionsClass;
	}

	public final int getPermissionLevel() {
		return permissionLevel;
	}

	public final boolean needsCmd() {
		return needsCmd;
	}

	public final boolean needsUser() {
		return needsUser;
	}

	public final CommandResult getResult(int id) {
		return 0 <= id && id < results.size() ? results.get(id) : null;
	}
}
