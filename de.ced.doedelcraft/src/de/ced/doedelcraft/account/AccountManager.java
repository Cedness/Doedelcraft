package de.ced.doedelcraft.account;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerListPingEvent;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.account.command.CommandManager;
import de.ced.doedelcraft.util.Logger;
import de.ced.doedelcraft.util.RealTimeManager;

/**
 * Manages all {@link Account}s.
 * 
 * @author Ced
 */
public class AccountManager {

	private final Doedelcraft doedelcraft;
	private final LanguageManager languageManager;
	private CommandManager commandManager;

	private final Map<Player, Account> playerMap = new HashMap<>();

	public AccountManager(Doedelcraft doedelcraft) {
		this.doedelcraft = doedelcraft;
		languageManager = new LanguageManager(this);
	}

	public void createCommands() {
		if (commandManager != null) {
			return;
		}
		commandManager = new CommandManager(this);
	}

	public Doedelcraft getDoedelcraft() {
		return doedelcraft;
	}

	public LanguageManager getLanguageManager() {
		return languageManager;
	}

	public CommandManager getCommandManager() {
		return commandManager;
	}

	public void tick(int tick) {
		for (Account account : playerMap.values()) {
			account.tick(tick);
		}
	}

	public void onPing(ServerListPingEvent e) {
		for (Iterator<Player> iterator = e.iterator(); iterator.hasNext();) {
			iterator.next();
			iterator.remove();
		}
		e.setMaxPlayers(0);
		RealTimeManager.refresh();
		String name = "§fDödelcraft Server §8- §cInder development";
		String version = "§8Version " + doedelcraft.getDescription().getVersion();
		String now = RealTimeManager.getWeekDayName() + ", " + RealTimeManager.getDayFix() + "/" + RealTimeManager.getMonthFix() + "/" + RealTimeManager.getYear() + ", " +
				RealTimeManager.getHourFix() + ":" + RealTimeManager.getMinuteFix() + ":" + RealTimeManager.getSecondFix();
		String info = version + " | " + now;
		e.setMotd(name + "\n" + info);
	}

	public void onLogin(PlayerLoginEvent e) {
		Player player = e.getPlayer();
		if (playerMap.containsKey(player)) {
			String message = "There was an error while joining";
			try {
				message = playerMap.get(player).getD("brownscreen").get("already_online");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.disallow(PlayerLoginEvent.Result.KICK_OTHER, "§c" + message + ".\n");
		} else {
			e.allow();
		}

		login(player, false);
	}

	public void onJoin(PlayerJoinEvent e) {
		e.setJoinMessage(null);

		prepare(playerMap.get(e.getPlayer()));
	}

	public void add(Player player) {
		add(player, false);
	}

	public void add(Player player, boolean silent) {
		prepare(login(player, silent));
	}

	private Account login(Player player, boolean silent) {
		if (!silent) {
			Logger.info(player.getName() + " is now online");
		}
		Account account = new Account(player, this);
		playerMap.put(account.getPlayer(), account);
		doedelcraft.getMenuManager().add(account);
		return account;
	}

	private void prepare(Account account) {
		doedelcraft.getMenuManager().prepare(account);
	}

	public void onQuit(PlayerQuitEvent e) {
		e.setQuitMessage(null);
		remove(e.getPlayer());
	}

	public void remove(Player player) {
		remove(player, false);
	}

	public void remove(Player player, boolean silent) {
		Account account = playerMap.get(player);
		account.saveConfig();
		playerMap.remove(player);
		doedelcraft.getMenuManager().remove(account);
		doedelcraft.getUserManager().remove(account);
		player.saveData();
		if (!silent) {
			Logger.info(player.getName() + " is now offline");
		}
	}

	public boolean hasAccount(Player player) {
		return playerMap.containsKey(player);
	}

	public Account getAccount(Player player) {
		return playerMap.get(player);
	}

	public int getAccountAmount() {
		return playerMap.size();
	}
}
