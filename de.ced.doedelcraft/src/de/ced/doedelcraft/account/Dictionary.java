package de.ced.doedelcraft.account;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import de.ced.doedelcraft.util.Logger;
import org.bukkit.configuration.ConfigurationSection;

public class Dictionary {

	protected final String name;
	protected final String path;
	protected final Language language;
	protected final Dictionary parent;
	protected final int depth;
	protected final Map<String, String> contents = new LinkedHashMap<>();
	protected final Map<String, Dictionary> children = new LinkedHashMap<>();

	protected Dictionary(String name, Language language, Dictionary parent, int depth, ConfigurationSection config) {
		this(name, language, parent, depth);
		for (Entry<String, Object> entry : config.getValues(false).entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			if (value instanceof ConfigurationSection) {
				children.put(key, new Dictionary(key, this.language, this, depth + 1, (ConfigurationSection) value));
			} else {
				contents.put(key, String.valueOf(value));
			}
		}
	}

	private Dictionary(String name, Language language, Dictionary parent, int depth, Dictionary original) {
		this(name, language, parent, depth);
		contents.putAll(original.contents);
		for (Entry<String, Dictionary> entry : original.children.entrySet()) {
			String key = entry.getKey();
			children.put(key, new Dictionary(key, this.language, this, depth + 1, entry.getValue()));
		}
	}

	protected Dictionary(String name, Language language, Dictionary parent, int depth) {
		this.language = language == null ? (Language) this : language;
		this.parent = parent;
		this.name = name;
		path = (parent != null ? parent.getPath() + "." : "") + name;
		this.depth = depth;
	}

	void fillInIfAbsent(Dictionary original) {
		for (Entry<String, String> entry : original.contents.entrySet()) {
			contents.putIfAbsent(entry.getKey(), entry.getValue());
		}
		for (Entry<String, Dictionary> entry : original.children.entrySet()) {
			String key = entry.getKey();
			Dictionary value = entry.getValue();
			if (children.containsKey(key)) {
				children.get(key).fillInIfAbsent(value);
			} else {
				children.put(key, new Dictionary(key, language, this, depth + 1, entry.getValue()));
			}
		}
	}

	public String getName() {
		return name;
	}

	public String getPath() {
		return path;
	}

	public Language getLanguage() {
		return language;
	}

	public Dictionary getParent() {
		return parent;
	}

	/**
	 * Gets the child-{@link Dictionary} under the given key.
	 * 
	 * @param key the key of the dictionary
	 * @return the Dictionary under the key
	 */
	public Dictionary getD(String key) {
		if (key.contains(".")) {
			Logger.warning(path + ": Legacy d-key used: " + key);
		}
		Dictionary dictionary = children.get(key);
		if (dictionary == null) {
			// Logger.warning(path + ": Invalid d-key used: " + key);
			dictionary = language.getEmptyDictionary();
		}
		return dictionary;
	}

	/**
	 * Gets the translation for the given key.
	 * 
	 * @param key the key for the value
	 * @return the value stored under the key
	 */
	public String get(String key) {
		if (key.contains(".")) {
			Logger.warning(path + ": Legacy key used: " + key);
		}
		String value = contents.get(key);
		if (value == null) {
			// Logger.warning(path + ": Invalid key used: " + key);
			return "<" + key + ">";
		}
		return value;
	}

	/**
	 * 
	 * Gets the translation for the given key, replacing the placeholders with the
	 * given arguments.
	 * For example:
	 * The translation for the key is:
	 * {@code "Welcome, %2, have fun. Currently, there are
	 * %1 people online."}
	 * Your args are: {@code accountManager.getAccountAmount()}
	 * and {@code account.getName()}
	 * The resulting string could be:
	 * {@code "Welcome, Gunther, have fun. Currently, there are
	 * 69 people online."}
	 * So, more formally, each occurrence of {@code %i} is replaced by
	 * {@code args[i - 1]}
	 * 
	 * @param key  the key for the value
	 * @param args lol
	 * @return the value stored under the key
	 */
	public String get(String key, String... args) {
		return replace(get(key), args);
	}

	@Override
	public String toString() {
		return toString("");
	}

	private String toString(String prefix) {
		StringBuilder builder = new StringBuilder();
		builder.append(prefix).append(path).append(":\n");
		prefix += "- ";
		for (Entry<String, String> entry : contents.entrySet()) {
			builder.append(prefix).append(toString(entry)).append('\n');
		}
		for (Dictionary child : children.values()) {
			builder.append(child.toString(prefix));
		}
		return builder.toString();
	}

	private String toString(Entry<String, String> entry) {
		return entry.getKey() + " = " + entry.getValue();
	}

	@SuppressWarnings("unused")
	private String buildPathRecursively() {
		String path = name;
		if (depth > 0) {
			path = parent.buildPathRecursively() + "." + path;
		}
		return path;
	}

	public static String replace(String value, String... args) {
		for (int i = 0; i < args.length; i++) {
			value = value.replaceAll("%" + (i + 1), args[i]);
		}
		return value;
	}
}
