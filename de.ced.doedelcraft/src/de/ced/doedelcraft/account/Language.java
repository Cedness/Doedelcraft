package de.ced.doedelcraft.account;

import org.bukkit.configuration.file.YamlConfiguration;

/*
 * Ä - \u00C4
 * Ö - \u00D6
 * Ü - \u00DC
 * 
 * ä - \u00E4
 * ö - \u00F6
 * ü - \u00FC
 * 
 * ß - \u00DF
 */
/**
 * @author Ced
 */
public class Language extends Dictionary {

	private final EmptyDictionary emptyDictionary = new EmptyDictionary(this);

	Language(String languageId, YamlConfiguration config) {
		super(languageId, null, null, 0, config);
	}

	protected Dictionary getEmptyDictionary() {
		return emptyDictionary;
	}

	public static class EmptyDictionary extends Dictionary {

		private EmptyDictionary(Language language) {
			super("FALLBACK", language, null, 0);
		}

		@Override
		public Dictionary getParent() {
			return this;
		}

		@Override
		public Dictionary getD(String key) {
			return this;
		}

		@Override
		public String get(String key) {
			return key;
		}

		@Override
		public String get(String key, String... args) {
			return key;
		}
	}
}
