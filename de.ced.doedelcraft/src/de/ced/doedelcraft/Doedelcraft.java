package de.ced.doedelcraft;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.UUID;

import de.ced.doedelcraft.account.AccountManager;
import de.ced.doedelcraft.menu.MenuManager;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;

import de.ced.doedelcraft.ingame.chaa.CharManager;
import de.ced.doedelcraft.ingame.chaa.NPManager;
import de.ced.doedelcraft.ingame.chaa.UserManager;
import de.ced.doedelcraft.ingame.world.WorldManager;
import de.ced.doedelcraft.util.ListenerClass;
import de.ced.doedelcraft.util.Logger;

/**
 * Main-class, creates all top-level-managers and IS the Minecraft-plugin. So
 * reference this if Bukkit requires a {@link JavaPlugin}.
 * 
 * @author Ced
 */
public class Doedelcraft extends JavaPlugin implements Listener {

	private boolean operating = true;

	private YamlConfiguration defaultAccountConfig;
	private YamlConfiguration defaultCharConfig;
	private File namesFile;
	private YamlConfiguration namesConfig;
	private Map<String, Object> names;

	private ListenerClass listener;
	private WorldManager worldManager;
	private MenuManager menuManager;
	private CharManager charManager;
	private AccountManager accountManager;

	private static int tick = 300;

	public static int currentTick() {
		return tick;
	}

	// Start Stop

	/**
	 * Use this method instead of the constructor. It is called once the server has
	 * started.
	 */
	@Override
	public void onEnable() {
		if (!Bukkit.getPluginManager().isPluginEnabled("CrashDoctor")) {
			System.err.println("[D] Startup error, shutting down...");
			Bukkit.shutdown();
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		try {
			Bukkit.getPluginManager().registerEvents(this, this);
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this::check, 1, 1);
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this::secure, 1, 1);
		} catch (Exception ex) {
			ex.printStackTrace();
			try {
				System.err.println("[D] Startup error, shutting down...");
				Bukkit.shutdown();
			} catch (Exception ex2) {
				ex2.printStackTrace();
				operating = false;
				while (true) {
					System.err.println("[D] Tried to shutdown after startup error, but failed. SHUTDOWN MANUALLY IMMEDIATELY!");
				}
			}
			return;
		}

		defaultAccountConfig = loadConfig("defaultAccount");
		defaultCharConfig = loadConfig("defaultChar");

		namesFile = new File(getDataFolder(), "names.yml");
		namesConfig = YamlConfiguration.loadConfiguration(namesFile);
		names = namesConfig.getValues(false);

		listener = new ListenerClass(this);
		worldManager = new WorldManager(this);
		accountManager = new AccountManager(this);
		menuManager = new MenuManager(this);
		charManager = new CharManager(this);
		accountManager.createCommands();

		for (Player player : Bukkit.getOnlinePlayers()) {
			accountManager.add(player);
			// player.kickPlayer("What ya' even doin' here?");
		}

		listener.ready();

		Bukkit.getScheduler().runTaskTimer(this, () -> {
			if (tick >= Integer.MAX_VALUE - 10000) {
				Bukkit.reload();
				return;
			}
			tick++;
			try {
				accountManager.tick(tick);
			} catch (Exception ex) {
				Logger.severe("General tick-exception in AccountManager");
				ex.printStackTrace();
			}
			try {
				menuManager.tick(tick);
			} catch (Exception ex) {
				Logger.severe("General tick-exception in MenuManager");
				ex.printStackTrace();
			}
			try {
				charManager.tick(tick);
			} catch (Exception ex) {
				Logger.severe("General tick-exception in UserManager");
				ex.printStackTrace();
			}
			try {
				worldManager.tick(tick);
			} catch (Exception ex) {
				Logger.severe("General tick-exception in UserManager");
				ex.printStackTrace();
			}
		}, 1, 1);

		// Test inconvenience mode
		/*
		 * Bukkit.getScheduler().runTaskLater(this, () -> { operating = false; }, 20 *
		 * 5);
		 */

		Logger.info("Running on version " + getDescription().getVersion());
	}

	public YamlConfiguration loadConfig(String name) {
		return loadConfig(name, "yml", null);
	}

	public YamlConfiguration loadConfig(String name, String type, String folder) {
		return YamlConfiguration.loadConfiguration(loadFile(name, type, folder));
	}

	public File loadFile(String name, String type, String folder) {
		folder = folder == null ? "" : "/" + folder;
		name = name + "." + type;
		File directory = new File(getDataFolder().getPath() + folder);
		directory.mkdirs();
		File file = new File(directory, name);
		try {
			InputStream stream = getClass().getResourceAsStream("/resources" + folder + "/" + name);
			InputStreamReader streamReader = new InputStreamReader(stream);
			BufferedReader reader = new BufferedReader(streamReader);
			String line = reader.readLine();

			FileWriter fileWriter = new FileWriter(file);
			BufferedWriter writer = new BufferedWriter(fileWriter);

			if (reader.ready() && !line.isEmpty()) {
				while (true) {
					writer.write(line); // TODO oneliners crash here UPDATE: maybe now fixed
					writer.newLine();
					if (!reader.ready()) {
						break;
					}
					line = reader.readLine();
				}
			}
			writer.flush();
			reader.close();
			writer.close();
			// Logger.info("Successfully wrote file from stream: " + file.getName());
		} catch (Exception ex) {
			// Logger.warning("Exception while writing file from stream: " +
			// file.getName());
			// ex.printStackTrace();
		}
		return file;
	}

	/**
	 * This method is called when the server shuts down.
	 */
	@Override
	public void onDisable() {
		if (listener != null) {
			listener.done();
		}

		for (Player player : Bukkit.getOnlinePlayers()) {
			accountManager.remove(player);

			// player.kickPlayer("§0Y§1o§2u§3'§4v§5e §6b§7e§8e§9n §ag§bo§cn§de§e.");
		}
		charManager.getNPManager().cleanup();
	}

	// Security

	private void check() {
		if (!Bukkit.getPluginManager().isPluginEnabled("CrashDoctor")) {
			operating = false;
		}
	}

	private void secure() {
		if (operating) {
			return;
		}
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.kickPlayer("§c[D] Internal server error, we're sorry for the inconvenience");
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onLogin(PlayerLoginEvent e) {
		if (operating) {
			return;
		}
		e.disallow(PlayerLoginEvent.Result.KICK_OTHER, "§c[D] Internal server error, we're sorry for the inconvenience");
	}

	// Yamls

	public YamlConfiguration getDefaultAccountConfig() {
		return defaultAccountConfig;
	}

	public YamlConfiguration getDefaultCharConfig() {
		return defaultCharConfig;
	}

	// Names

	public void addName(String name, UUID uuid) {
		names.put(name, uuid.toString());
		namesConfig.set(name, uuid.toString());
	}

	public void removeName(String name) {
		names.remove(name);
		namesConfig.set(name, null);
	}

	public void saveNames() {
		try {
			namesConfig.save(namesFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Logger.info("Names file updated");
	}

	public boolean isNameTakenBy(String name, UUID uuid) {
		return uuid.toString().equals(names.get(name));
	}

	public boolean isNameTaken(String name) {
		return names.containsKey(name);
	}

	// Getters

	public int getTick() {
		return tick;
	}

	public WorldManager getWorldManager() {
		return worldManager;
	}

	public AccountManager getAccountManager() {
		return accountManager;
	}

	public MenuManager getMenuManager() {
		return menuManager;
	}

	public CharManager getCharManager() {
		return charManager;
	}

	public UserManager getUserManager() {
		return charManager.getUserManager();
	}

	public NPManager getNPManager() {
		return charManager.getNPManager();
	}

	public static void main(String[] args) {
		System.err.println("This is a Minecraft-plugin, it cannot be started like this. Export it as a .jar-file and put it in the plugins folder of a Minecraft-Bukkit-Server");
	}
}
