package de.ced.doedelcraft.menu;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import de.ced.doedelcraft.ingame.chaa.UserData;

public enum CharSorting {

	CREATION((saveFile) -> {
		return saveFile.getCreation();
	}),
	AGE((saveFile) -> {
		return saveFile.getLevel() + saveFile.getProgress();
	}),
	EXPERIENCE((saveFile) -> {
		return saveFile.getExperience();
	}),
	LAST_PLAYED((saveFile) -> {
		return saveFile.getLogout();
	});

	private static final List<CharSorting> SORTINGS = Arrays.asList(CharSorting.CREATION, CharSorting.LAST_PLAYED, CharSorting.AGE, CharSorting.EXPERIENCE);

	public static List<CharSorting> getSortings() {
		return SORTINGS;
	}

	private final Comparator<UserData> comparator;

	private CharSorting(Extractor extractor) {
		comparator = new CharComparator(extractor);
	}

	public Comparator<UserData> getComparator() {
		return comparator;
	}

	@FunctionalInterface
	private static interface Extractor {
		double getValue(UserData saveFile);
	}

	public static class CharComparator implements Comparator<UserData> {

		private final Extractor extractor;

		public CharComparator(Extractor extractor) {
			this.extractor = extractor;
		}

		@Override
		public int compare(UserData saveFile1, UserData saveFile2) {
			return Double.compare(extractor.getValue(saveFile1), extractor.getValue(saveFile2));
		}
	}
}
