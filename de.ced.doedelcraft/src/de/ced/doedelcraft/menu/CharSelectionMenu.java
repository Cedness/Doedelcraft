package de.ced.doedelcraft.menu;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

import de.ced.doedelcraft.account.AccountManager;
import de.ced.doedelcraft.account.Language;
import de.ced.doedelcraft.ingame.chaa.UserData;
import de.ced.doedelcraft.ingame.clazz.Clazz;
import de.ced.doedelcraft.util.RealTimeManager;
import de.ced.doedelcraft.util.SadItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.ced.doedelcraft.ingame.chaa.Char;

/**
 * Main menu. Lists all of the player's {@link Char}s and lets him select them
 * for playing. They can also be deleted from here and new chars can be created.
 * 
 * Anos di Voldigoad
 * 
 * @author Ced
 */
public class CharSelectionMenu extends Menu {

	private static final String LANG = "selection";

	private static final int MAX_CHAR_COUNT = 5 * 9;
	private static final String NEW_CHAR_IDENTIFIER = "§2[§a+§2] §7";
	private static final int SORT_SLOT = 0;
	private static final String SORT_PLACEHOLDER = "%SORT%";
	private static final int REVERSE_SLOT = 1;
	private static final String REVERSE_PLACEHOLDER = "%REVERSE%";
	private static final int LANGUAGE_SLOT_BEGIN = 2;
	private static final int LANGUAGE_SLOT_END = 7;
	private static final int MAX_LANGUAGE_COUNT = LANGUAGE_SLOT_END - LANGUAGE_SLOT_BEGIN + 1;
	private static final int QUIT_SLOT = 8;

	private final SadItem rawCharItem = new SadItem(Material.BARRIER);
	private final SadItem newCharItem = new SadItem(Material.ARMOR_STAND);
	private final SadItem sortItem = new SadItem(Material.COARSE_DIRT);
	private final SadItem reverseItem = new SadItem(Material.HOPPER_MINECART);
	private final SadItem languageItem = new SadItem(Material.BOOKSHELF);
	private final SadItem quitItem = new SadItem(Material.BARRIER);
	private final List<SadItem> languageItems = new ArrayList<>();
	private final String inventoryTitle;

	private boolean canCreateChar;
	private List<SadItem> charItems;
	private List<UserData> saveFiles;

	public CharSelectionMenu(MenuPlayer menuPlayer) {
		super(menuPlayer, LANG);

		rawCharItem.setText(
				"§6",
				"",
				"§7§o - " + genericDictionary.get("clazz") + ": §b",
				"§7§o - " + genericDictionary.get("age") + ": §b",
				"",
				"§7§o - " + dictionary.get("created") + ": §7",
				"§7§o - " + dictionary.get("last_played") + ": §7",
				"",
				"",
				"§f" + dictionary.get("play"),
				"§8" + dictionary.get("delete"));
		newCharItem.setName(NEW_CHAR_IDENTIFIER + dictionary.get("create"));
		sortItem.setText("§7" + dictionary.get("sorted") + " §b" + SORT_PLACEHOLDER, "", "§7" + dictionary.get("sort") + "...");
		reverseItem.setText("§b" + REVERSE_PLACEHOLDER, "", "§7" + dictionary.get("reverse"));
		quitItem.setName("§4[§cX§4] §7" + dictionary.get("quit"));

		for (Language language : menuPlayer.getDoedelcraft().getAccountManager().getLanguageManager().getLanguages()) {
			boolean selected = language.equals(account.getLanguage());
			SadItem item = languageItem.clone();
			String languageName = language.get("name");
			String languageInfo = language.getD(Menu.LANG).getD(LANG).get("language_" + (selected ? "set" : "not_set"), languageName);
			item.enchant(selected);
			item.setText((selected ? "§6" : "§7") + languageName, "", "§7" + languageInfo);
			languageItems.add(item);
			if (languageItems.size() >= MAX_LANGUAGE_COUNT) {
				break;
			}
		}

		String name = account.getName();
		String genitive = "s";
		switch (name.charAt(name.length() - 1)) {
		case 's':
		case 'S':
			genitive = "";
		}
		inventoryTitle = dictionary.get("title", name + "'" + genitive);
	}

	@Override
	public void create() {
		int charCount = account.getCharCount();
		if (charCount > MAX_CHAR_COUNT) {
			charCount = MAX_CHAR_COUNT;
		}
		canCreateChar = charCount < MAX_CHAR_COUNT;
		int lineCount = (charCount / 9 + 2) * 9;
		inventory = Bukkit.createInventory(null, lineCount, inventoryTitle);

		charItems = new ArrayList<>();
		saveFiles = new ArrayList<>();
		RealTimeManager.refresh();
		for (int charId = 0; charId < charCount; charId++) {
			UserData saveFile = account.getChar(charId);
			charItems.add(createClassSelectionItem(saveFile));
			saveFiles.add(saveFile);
		}

		resortChars();

		if (canCreateChar) {
			inventory.setItem(charCount, newCharItem.clone());
		}
		int inventorySize = inventory.getSize();
		for (int i = 0; i < languageItems.size(); i++) {
			inventory.setItem(inventorySize - 9 + 2 + MAX_LANGUAGE_COUNT - languageItems.size() + i, languageItems.get(i));
		}
		inventory.setItem(inventorySize - 9 + QUIT_SLOT, quitItem.clone());
	}

	public SadItem createClassSelectionItem(UserData saveFile) {
		Clazz clazz = saveFile.getClazz();
		return rawCharItem.clone().setItem(clazz.getItem()).extendText(
				saveFile.getName(),
				"",
				clazz.getColor().replaceAll("0", "8") + "§o" + genericDictionary.get(clazz.getName()),
				saveFile.getLevelColor().replaceAll("0", "8") + "§o" + saveFile.getLevel(),
				"",
				RealTimeManager.getTimespanToNow(saveFile.getCreation()).humanize(account.getLanguage()),
				RealTimeManager.getTimespanToNow(saveFile.getLogout()).humanize(account.getLanguage()));
	}

	private void resortChars() {
		CharSorting sorter = account.getCharSorting();
		Comparator<UserData> comparator = sorter.getComparator();
		boolean reversed = account.isCharSortingReversed();
		if (reversed) {
			comparator = comparator.reversed();
		}
		saveFiles.sort(comparator);
		for (int slotId = 0; slotId < account.getCharCount(); slotId++) {
			UserData saveFile = saveFiles.get(slotId);
			inventory.setItem(slotId, charItems.get(saveFile.getCharId()));
		}
		int inventorySize = inventory.getSize();
		inventory.setItem(inventorySize - 9 + SORT_SLOT, sortItem.clone().setName(
				sortItem.getName().replaceAll(SORT_PLACEHOLDER, dictionary.getD("sortings").get(sorter.name().toLowerCase()))));
		inventory.setItem(inventorySize - 9 + REVERSE_SLOT, reverseItem.clone()
				.setItem(reversed ? Material.HOPPER_MINECART : Material.FURNACE_MINECART)
				.setName(reverseItem.getName().replaceAll(REVERSE_PLACEHOLDER, dictionary.get((reversed ? "de" : "a") + "scending"))));
	}

	@Override
	protected void onClick(InventoryClickEvent e) {
		ItemStack item = e.getCurrentItem();
		if (item == null) {
			/*
			 * Account account = menuPlayer.getAccount(); remove(account);
			 * player.closeInventory(); account.message("§7Do /class to log back in");
			 */
			return;
		}

		int slot = e.getSlot();
		int row = slot / 9;
		int column = slot % 9;
		int rows = inventory.getSize() / 9;
		if (row >= rows - 1) { // Bottom row
			if (column == QUIT_SLOT) {
				player.kickPlayer("§3[§bDödelcraft Server§3]\n\n§7" + account.getD("brownscreen").get("goodbye", ".\n\n§b", " §3" + account.getName() + "§b,\n§b") + "!");
			} else if (column == SORT_SLOT) {
				List<CharSorting> sorters = CharSorting.getSortings();
				int sorterId = (sorters.indexOf(account.getCharSorting()) + 1) % sorters.size();
				account.setCharSorting(sorters.get(sorterId));
				resortChars();
			} else if (column == REVERSE_SLOT) {
				account.setCharSortingReversed(!account.isCharSortingReversed());
				resortChars();
			} else {
				if (LANGUAGE_SLOT_BEGIN > column || column > LANGUAGE_SLOT_END) {
					return;
				}
				int languageId = column - LANGUAGE_SLOT_BEGIN - MAX_LANGUAGE_COUNT + languageItems.size();
				if (0 > languageId || languageId >= languageItems.size()) {
					return;
				}
				AccountManager accountManager = menuPlayer.getDoedelcraft().getAccountManager();
				String key = null;
				int i = 0;
				for (Entry<String, Language> entry : accountManager.getLanguageManager().getLanguageSet()) {
					key = entry.getKey();
					if (languageId == i) {
						if (account.getLanguage().equals(entry.getValue())) {
							return;
						}
						break;
					}
					i++;
				}
				account.setLanguageId(key);
				accountManager.remove(player, true);
				player.closeInventory();
				accountManager.add(player, true);
			}
		} else { // Top rows
			ItemMeta meta = item.getItemMeta();
			if (meta == null || !meta.hasDisplayName()) {
				return;
			}
			String displayName = meta.getDisplayName();

			if (displayName.startsWith(NEW_CHAR_IDENTIFIER)) { // Create Char
				createChar();
			} else if (slot < charItems.size()) { // Char selection
				if (e.getClick() == ClickType.SHIFT_RIGHT) {
					deleteChar(slot);
				} else {
					selectChar(slot);
					return;
				}
			}
		}
		BUTTON_CLICK_SOUND.playFor(player);
	}

	@Override
	protected void onChat(AsyncPlayerChatEvent e) {
	}

	private void createChar() {
		Menu menu = menuPlayer.getCharCreationMenu();
		menu.reset();
		menuPlayer.openMenu(menu);
	}

	private void selectChar(int slotId) {
		MenuManager menuManager = menuPlayer.getMenuManager();
		menuManager.remove(account);
		player.closeInventory();
		menuManager.getDoedelcraft().getCharManager().getUserManager().add(saveFiles.get(slotId));
	}

	private void deleteChar(int slotId) {
		CharDeletionMenu menu = menuPlayer.getCharDeletionMenu();
		menu.reset();
		menu.setSaveFile(saveFiles.get(slotId));
		menuPlayer.openMenu(menu);
	}
}
