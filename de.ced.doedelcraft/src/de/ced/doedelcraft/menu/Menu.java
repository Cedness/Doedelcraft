package de.ced.doedelcraft.menu;

import de.ced.doedelcraft.account.Account;
import de.ced.doedelcraft.account.Dictionary;
import de.ced.doedelcraft.util.SimpleSound;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.Inventory;

/**
 * Base class for all menus.
 * 
 * @author Ced
 */
public abstract class Menu {

	protected static final String LANG = "mainmenu";
	protected static final SimpleSound BUTTON_CLICK_SOUND = new SimpleSound(Sound.UI_BUTTON_CLICK, false);
	protected static final SimpleSound BUTTON_MOVE_SOUND = new SimpleSound(Sound.ENTITY_ITEM_PICKUP, false);

	protected final MenuPlayer menuPlayer;
	protected final Account account;
	protected final Player player;
	protected final Dictionary genericDictionary;
	protected final Dictionary dictionary;
	protected boolean reset = true;

	protected Inventory inventory;

	public Menu(MenuPlayer menuPlayer, String languageKey) {
		this.menuPlayer = menuPlayer;
		account = menuPlayer.getAccount();
		player = menuPlayer.getPlayer();
		genericDictionary = account.getD("generic");
		dictionary = account.getD("mainmenu").getD(languageKey);
	}

	public Inventory getInventory() {
		return inventory;
	}

	public abstract void create();

	public void open() {
		if (reset) {
			reset = false;
			create();
		}
		player.openInventory(inventory);
	}

	public void reset() {
		reset = true;
	}

	protected abstract void onClick(InventoryClickEvent e);

	protected abstract void onChat(AsyncPlayerChatEvent e);
}
