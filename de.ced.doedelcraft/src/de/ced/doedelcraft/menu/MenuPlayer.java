package de.ced.doedelcraft.menu;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.account.Account;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.Inventory;

/**
 * Represents an {@link Account} who has the Main-menu
 * ({@link CharSelectionMenu}) or one of its submenu's opened.
 * 
 * @author Ced
 */
public class MenuPlayer {

	private final Account account;
	private final Player player;
	private final MenuManager menuManager;
	private final Doedelcraft doedelcraft;

	private final CharSelectionMenu charSelectionMenu;
	private final CharCreationMenu charCreationMenu;
	private final CharDeletionMenu charDeletionMenu;

	private Menu activeMenu;
	private boolean closed = true;
	private int menuClosedTime;
	private int countdown = 0;
	private String countdownMessage = "";

	MenuPlayer(Account account, MenuManager menuManager) {
		this.account = account;
		player = account.getPlayer();
		this.menuManager = menuManager;
		doedelcraft = menuManager.getDoedelcraft();

		account.setMenuPlayer(this);

		charSelectionMenu = new CharSelectionMenu(this);
		charCreationMenu = new CharCreationMenu(this);
		charDeletionMenu = new CharDeletionMenu(this);

		activeMenu = charSelectionMenu;
		setMenuClosedTimeToNow();
	}

	public void remove() {
		account.setMenuPlayer(null);
	}

	public Account getAccount() {
		return account;
	}

	public Player getPlayer() {
		return player;
	}

	public MenuManager getMenuManager() {
		return menuManager;
	}

	public Doedelcraft getDoedelcraft() {
		return doedelcraft;
	}

	public void onClick(InventoryClickEvent e) {
		if (!activeMenu.getInventory().equals(e.getInventory())) {
			return;
		}
		activeMenu.onClick(e);
	}

	public void onChat(AsyncPlayerChatEvent e) {
		activeMenu.onChat(e);
	}

	public void setMenuClosedTimeToNow() {
		menuClosedTime = doedelcraft.getTick() + 1;
	}

	/**
	 * Changes the countdown timing and message
	 */
	public void setCountdownData(int countdown, String countdownMessage) {
		this.countdown = countdown;
		this.countdownMessage = countdownMessage;
	}

	public void tick(int tick) {
		if (!closed) {
			return;
		}
		int ticksOver = tick - menuClosedTime;
		if (ticksOver % 20 != 0) {
			return;
		}
		int timeOver = ticksOver / 20;
		if (timeOver == countdown) {
			closed = false;
			activeMenu.open();
			return;
		}
		player.sendTitle("§6" + (countdown - timeOver), "§e" + countdownMessage, 0, countdownMessage.equals("") ? 5 : 25, 25);
	}

	public void onOpen(Inventory openedInventory) {
		if (!openedInventory.equals(activeMenu.getInventory())) {
			return;
		}
		countdown = 3;
		countdownMessage = "";
		closed = false;
	}

	public void onClose(Inventory closedInventory) {
		if (!closedInventory.equals(activeMenu.getInventory())) {
			return;
		}
		setMenuClosedTimeToNow();
		closed = true;
	}

	/**
	 * Sets the menu as active without opening it.
	 * This can be used to delay the opening with the inv-close cooldown.
	 */
	public void setActiveMenu(Menu menu) {
		activeMenu = menu;
	}

	/**
	 * Sets the menu as active and opens it
	 */
	public void openMenu(Menu menu) {
		setActiveMenu(menu);
		activeMenu.open();
	}

	public CharSelectionMenu getCharSelectionMenu() {
		return charSelectionMenu;
	}

	public CharCreationMenu getCharCreationMenu() {
		return charCreationMenu;
	}

	public CharDeletionMenu getCharDeletionMenu() {
		return charDeletionMenu;
	}
}
