package de.ced.doedelcraft.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.account.Dictionary;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.UserData;
import de.ced.doedelcraft.ingame.clazz.Clazz;
import de.ced.doedelcraft.ingame.clazz.ClazzManager;
import de.ced.doedelcraft.util.SadItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import de.ced.doedelcraft.ingame.clazz.Race;

/**
 * Lets the player create a new {@link Char}.
 * 
 * @author Ced
 */
public class CharCreationMenu extends Menu {

	private static final String LANG = "creation";

	private static final String ALLOWED_LETTERS = "abcdefghijklmnopqrstuvwxyzäöüáàéèíìóòúù";
	private static final SadItem FRAME = new SadItem(Material.BLACK_STAINED_GLASS_PANE, " ");
	private static final SadItem BORDER = new SadItem(Material.IRON_BARS, " ");
	private final Dictionary namechangeDictionary;
	private final String namePlaceholder;
	private final SadItem nameItem = new SadItem(Material.FILLED_MAP);
	private final SadItem cancel = new SadItem(Material.BARRIER);
	private final SadItem confirm = new SadItem(Material.LIME_TERRACOTTA);

	private final List<SadItem> raceItems = new ArrayList<>();
	private final Map<Race, List<SadItem>> classItems = new HashMap<>();

	private final Doedelcraft doedelcraft;
	private final ClazzManager clazzManager;

	private String charName = "";
	private Race race;
	private Clazz clazz;

	private boolean inputActive = false;

	public CharCreationMenu(MenuPlayer menuPlayer) {
		super(menuPlayer, LANG);
		namechangeDictionary = dictionary.getD("name_change");
		namePlaceholder = dictionary.get("name1");
		nameItem.setText("§f\"§6" + namePlaceholder + "§f\"", "", "§7" + dictionary.get("name2"));
		cancel.setText("§c§l" + dictionary.get("cancel1"), "", "§7" + dictionary.get("cancel2"));
		confirm.setText("§a§l" + dictionary.get("confirm1"), "", "§7" + dictionary.get("confirm2"));

		doedelcraft = menuPlayer.getDoedelcraft();
		clazzManager = doedelcraft.getCharManager().getClazzManager();

		inventory = Bukkit.createInventory(null, 9 * 6, " " + genericDictionary.get("race") + "  -  " + dictionary.get("title") + "  -  " + genericDictionary.get("clazz"));
		SadItem border = BORDER.clone();
		for (int i = 0; i < 6; i++) {
			inventory.setItem(i * 9 + 4, border);
		}
		SadItem frame = FRAME.clone();
		for (int i = 0; i < 9; i++) {
			inventory.setItem(4 * 9 + i, frame);
		}
		inventory.setItem(52, cancel.clone());
		inventory.setItem(53, confirm.clone());

		for (Race race : clazzManager.getRaces()) {
			{
				List<String> text = new ArrayList<>();
				text.add("§o" + race.getColor() + genericDictionary.get(race.getName()));
				text.add("");
				for (String line : race.getFormattedDescription(account.getLanguage())) {
					text.add("§7" + line);
				}
				raceItems.add(new SadItem(race.getItem(), text));
			}

			List<SadItem> raceClassItems = new ArrayList<>();
			classItems.put(race, raceClassItems);
			for (Clazz clazz : race.getClazzes()) {
				List<String> text = new ArrayList<>();
				text.add("§o" + clazz.getColor() + genericDictionary.get(clazz.getName()));
				text.add("");
				for (String line : clazz.getFormattedDescription(account.getLanguage())) {
					text.add("§7" + line);
				}
				raceClassItems.add(new SadItem(clazz.getItem(), text));
			}
		}
	}

	@Override
	public void create() {
		race = clazzManager.getRaceByName("human");
		clazz = clazzManager.getClazzByName("human");
		setCharName(null);
		setRace(null);
		inputActive = false;
	}

	@Override
	protected void onClick(InventoryClickEvent e) {
		if (e.getCurrentItem() == null) {
			/*
			 * Account account = menuPlayer.getAccount(); remove(account);
			 * player.closeInventory(); account.message("§7Do /class to log back in");
			 */
			return;
		}

		int slot = e.getSlot();
		int row = slot / 9;
		int column = slot % 9;

		if (row < 4) { // Top rows
			if (column <= 1) {
				setRace(clazzManager.getRaces().get(2 * row + column));
			} else if (column >= 7 && race != null) {
				setClazz(race.getClazzes().get(2 * row + column - 7));
			} else {
				return;
			}
			BUTTON_MOVE_SOUND.playFor(player);
			return;
		} else if (row > 4) { // Bottom row
			if (column < 4) {
				switch (column) {
				case 0:
					setRace(null);
					break;
				case 1:
					setClazz(null);
					break;
				default:
					return;
				}
				BUTTON_MOVE_SOUND.playFor(player);
				return;
			} else if (column > 4) {
				switch (column) {
				case 5:
					changeName();
					break;
				case 7:
					menuPlayer.openMenu(menuPlayer.getCharSelectionMenu());
					break;
				case 8:
					confirmChar();
					return;
				default:
					return;
				}
			} else {
				return;
			}
		} else {
			return;
		}
		BUTTON_CLICK_SOUND.playFor(player);
	}

	@Override
	protected void onChat(AsyncPlayerChatEvent e) {
		if (!inputActive) {
			return;
		}
		e.setCancelled(true);
		String rawName = e.getMessage().toLowerCase();
		String name = rawName.substring(0, 1).toUpperCase() + rawName.substring(1);
		String titleMsg;
		switch (checkName(rawName)) {
		case 1:
			titleMsg = "§c" + namechangeDictionary.get("cancelled");
			break;
		default:
			titleMsg = "§c" + namechangeDictionary.get("invalid");
			break;
		case -2:
			titleMsg = "§c" + namechangeDictionary.get("taken", "'§e " + name + "§c '");
			break;
		case 0:
			synchronized (this) {
				setCharName(name);
			}
			titleMsg = "§a" + namechangeDictionary.get("success") + " '§e" + name + "§a'";
		}
		synchronized (this) {
			inputActive = false;
			menuPlayer.setCountdownData(2, titleMsg);
			menuPlayer.setMenuClosedTimeToNow();
		}
	}

	private int checkName(String name) {
		if (namechangeDictionary.get("cancel").equals(name)) {
			return 1;
		}
		if (name == null || name.length() < 2 || name.length() > 16) {
			return -1;
		}
		for (char letter : name.toCharArray()) {
			if (ALLOWED_LETTERS.indexOf(letter) == -1) {
				return -1;
			}
		}
		synchronized (doedelcraft) {
			if (!isNameAvailable(name)) {
				return -2;
			}
		}
		return 0;
	}

	private boolean isNameAvailable(String name) {
		return !doedelcraft.isNameTaken(name) || doedelcraft.isNameTakenBy(name, player.getUniqueId());
	}

	public void changeName() {
		inputActive = true;
		menuPlayer.setCountdownData(30, namechangeDictionary.get("request", namechangeDictionary.get("cancel")));
		player.closeInventory();
	}

	public void confirmChar() {
		if (charName == null) {
			changeName();
			return;
		}
		if (!isNameAvailable(charName)) {
			menuPlayer.setCountdownData(3, "§c" + namechangeDictionary.get("just_taken", "' §e" + charName + "§c '"));
			player.closeInventory();
		}
		doedelcraft.addName(charName.toLowerCase(), player.getUniqueId());
		doedelcraft.saveNames();

		if (clazz == null) {
			clazz = clazzManager.getClazzByName("human");
			double random = Math.random();
			double probability = 0;
			for (Clazz clazz2 : clazzManager.getClazzes()) {
				probability += clazz2.getProbability();
				if (probability >= random) {
					clazz = clazz2;
					break;
				}
			}
			race = clazz.getRace();
		}

		// Create SaveFile
		UserData saveFile = account.addChar();
		saveFile.setName(charName);
		saveFile.setClazz(clazz);
		saveFile.saveConfig();
		String color = "§a";
		String type = genericDictionary.get(race.getName());
		switch (race.getName()) {
		case "esper":
			type = clazz.getColor() + clazz.getName() + color + " " + type;
			break;
		}
		String message = color + dictionary.get("born", type, "'§e" + saveFile.getName() + color + "'");
		MenuManager menuManager = menuPlayer.getMenuManager();
		menuManager.remove(account);
		player.closeInventory();
		menuManager.getDoedelcraft().getCharManager().getUserManager().add(saveFile);
		player.sendTitle("", message, 0, 105, 25);
	}

	public void setRace(Race race) {
		if (this.race == race) {
			return;
		}
		this.race = race;
		boolean reset = race == null;
		int newRaceId = reset ? -1 : clazzManager.getRaces().indexOf(race);
		outer: for (int raceId = 0, i = 0; i < 4; i++) {
			for (int j = 0; j < 2; j++, raceId++) {
				if (raceId >= clazzManager.getRaces().size()) {
					break outer;
				}
				inventory.setItem(9 * i + j, raceId == newRaceId ? null : raceItems.get(raceId));
			}
		}
		SadItem item = null;
		if (!reset) {
			item = raceItems.get(newRaceId).clone();
			List<String> text = item.getText();
			text.add("");
			text.add("§b§o" + dictionary.get("selected", race.getColor() + genericDictionary.get(race.getName()) + "§b§o", genericDictionary.get("race")));
			item.setText(text);
		}
		inventory.setItem(9 * 5, item);
		setClazz(null);
	}

	public void setClazz(Clazz clazz) {
		if (clazz != null && clazz.getRace() != race) {
			clazz = null;
		}
		this.clazz = clazz;
		boolean clear = race == null;
		boolean reset = clazz == null;
		List<SadItem> raceClassItems = clear ? null : classItems.get(race);
		int newClassId = reset ? -1 : race.getClazzes().indexOf(clazz);
		for (int classId = 0, i = 0; i < 4; i++) {
			for (int j = 0; j < 2; j++, classId++) {
				SadItem item = null;
				if (!clear && classId < raceClassItems.size() && classId != newClassId) {
					item = raceClassItems.get(classId);
				}
				inventory.setItem(9 * i + j + 7, item);
			}
		}
		SadItem item = null;
		if (!reset) {
			item = raceClassItems.get(newClassId).clone();
			List<String> text = item.getText();
			text.add("");
			text.add("§b§o" + dictionary.get("selected", clazz.getColor() + genericDictionary.get(clazz.getName()) + "§b§o", genericDictionary.get("clazz")));
			item.setText(text);
			item.enchant(true);
		}
		inventory.setItem(9 * 5 + 1, item);
	}

	public void setCharName(String name) {
		if (this.charName == name) {
			return;
		}
		this.charName = name;
		SadItem item = nameItem.clone();
		if (name != null) {
			item.setName(item.getName().replaceAll(namePlaceholder, name)).enchant(true);
		}
		inventory.setItem(9 * 5 + 5, item);
	}
}
