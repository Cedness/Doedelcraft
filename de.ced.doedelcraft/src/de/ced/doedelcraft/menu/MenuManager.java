package de.ced.doedelcraft.menu;

import java.util.HashMap;
import java.util.Map;

import de.ced.doedelcraft.account.Account;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftLivingEntity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.potion.PotionEffect;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.util.Logger;

/**
 * Manages all {@link MenuPlayer}s.
 * 
 * @author Ced
 */
public class MenuManager {

	private final Doedelcraft doedelcraft;

	private final Map<Player, MenuPlayer> menuPlayerMap = new HashMap<>();

	public MenuManager(Doedelcraft doedelcraft) {
		this.doedelcraft = doedelcraft;
	}

	public Doedelcraft getDoedelcraft() {
		return doedelcraft;
	}

	public void tick(int tick) {
		for (MenuPlayer menuPlayer : menuPlayerMap.values()) {
			try {
				menuPlayer.tick(tick);
			} catch (Exception e) {
				String name;
				try {
					name = menuPlayer.getAccount().getName();
				} catch (Exception e2) {
					name = "<unknown>";
					e2.printStackTrace();
				}
				Logger.severe("Tick-exception in MenuManager for Player " + name);
				e.printStackTrace();
			}
		}
	}

	public void add(Account account) {
		MenuPlayer menuPlayer = new MenuPlayer(account, this);
		Player player = account.getPlayer();
		menuPlayerMap.put(player, menuPlayer);
	}

	public void prepare(Account account) {
		Player player = account.getPlayer();

		player.setDisplayName(null);
		player.setCustomName(null);
		player.setCustomNameVisible(false);
		player.setPlayerListName(null);

		player.teleport(new Location(player.getWorld(), 0.5, 3, 0.5));
		player.setHealth(20);
		player.setFoodLevel(20);
		((CraftLivingEntity) player).setAbsorptionAmount(0);
		player.setExp(0);
		player.setLevel(0);
		player.setTotalExperience(0);
		player.getInventory().clear();
		for (PotionEffect potionEffect : player.getActivePotionEffects()) {
			player.removePotionEffect(potionEffect.getType());
		}
		player.setWalkSpeed(0.2f);
		player.setSilent(false);

		for (Sound sound : Sound.values()) {
			player.stopSound(sound);
		}
		NamespacedKey key = new NamespacedKey(doedelcraft, player.getUniqueId().toString());
		Bukkit.removeBossBar(key);
	}

	public void remove(Account account) {
		if (!account.hasMenuPlayer()) {
			return;
		}
		MenuPlayer menuPlayer = account.getMenuPlayer();
		menuPlayer.remove();
		menuPlayerMap.remove(account.getPlayer());
		account.getPlayer().closeInventory();
	}

	@EventHandler
	public void onMenuOpen(InventoryOpenEvent e) {
		HumanEntity humanEntity = e.getPlayer();
		if (!(humanEntity instanceof Player)) {
			return;
		}
		Player player = (Player) humanEntity;
		if (!menuPlayerMap.containsKey(player)) {
			return;
		}
		MenuPlayer menuPlayer = menuPlayerMap.get(player);
		menuPlayer.onOpen(e.getInventory());
	}

	@EventHandler
	public void onMenuClose(InventoryCloseEvent e) {
		HumanEntity humanEntity = e.getPlayer();
		if (!(humanEntity instanceof Player)) {
			return;
		}
		Player player = (Player) humanEntity;
		if (!menuPlayerMap.containsKey(player)) {
			return;
		}
		MenuPlayer menuPlayer = menuPlayerMap.get(player);
		menuPlayer.onClose(e.getInventory());
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		HumanEntity humanEntity = e.getWhoClicked();
		if (!(humanEntity instanceof Player)) {
			return;
		}
		Player player = (Player) humanEntity;
		if (!menuPlayerMap.containsKey(player)) {
			return;
		}
		e.setCancelled(true);
		MenuPlayer menuPlayer = menuPlayerMap.get(player);
		menuPlayer.onClick(e);
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		MenuPlayer menuPlayer;
		synchronized (menuPlayerMap) {
			if (!menuPlayerMap.containsKey(player)) {
				return;
			}
			menuPlayer = menuPlayerMap.get(player);
		}
		menuPlayer.onChat(e);
	}
}
