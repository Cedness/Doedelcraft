package de.ced.doedelcraft.menu;

import java.util.List;

import de.ced.doedelcraft.ingame.chaa.UserData;
import de.ced.doedelcraft.ingame.clazz.Clazz;
import de.ced.doedelcraft.util.SadItem;
import de.ced.doedelcraft.util.SimpleSound;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import de.ced.doedelcraft.ingame.chaa.Char;

/**
 * Using a small quiz it let's the player confirm that he really wants to delete
 * a {@link Char}.
 * 
 * @author Ced
 */
public class CharDeletionMenu extends Menu {

	private enum State {
		NOT_SET(),
		OFF(),
		ON();
		static {
			NOT_SET.nextState = OFF;
			OFF.nextState = ON;
			ON.nextState = OFF;
		}

		private State nextState;

		public State getNextState() {
			return nextState;
		}
	}

	private static final String LANG = "deletion";

	private static final SimpleSound DELETE_SOUND = new SimpleSound(Sound.ENTITY_WITHER_DEATH, 10, 0.5, false);

	private final String inventoryTitle;
	private final String info;
	private final SadItem border = new SadItem(Material.BLACK_STAINED_GLASS_PANE);
	private final SadItem cancel = new SadItem(Material.BARRIER);
	private final SadItem shouldOff = new SadItem(Material.REDSTONE_BLOCK, "§l§40");
	private final SadItem shouldOn = new SadItem(Material.EMERALD_BLOCK, "§l§a1");
	private final SadItem isNotSet = new SadItem(Material.CYAN_TERRACOTTA);
	private final SadItem IS_OFF = new SadItem(Material.RED_TERRACOTTA);
	private final SadItem IS_ON = new SadItem(Material.LIME_TERRACOTTA);

	private final Inventory INVENTORY;

	private UserData saveFile;

	private State[] solution = new State[7];
	private State[] state = new State[solution.length];

	public CharDeletionMenu(MenuPlayer menuPlayer) {
		super(menuPlayer, LANG);
		inventoryTitle = "§7" + dictionary.get("title", "§l§4", "§7") + " §o§0";
		info = "§7" + dictionary.get("info1", "§l§4") + "§7 " + dictionary.get("info2");
		border.setText(info, "", "§7" + dictionary.get("border"));
		cancel.setText(info, "", "§f" + dictionary.get("cancel", "§o§b"));
		String clickToSwitch = "§7" + dictionary.get("switch");
		isNotSet.setText("§7" + dictionary.get("not_set"), "", clickToSwitch);
		IS_OFF.setText("§40", "", clickToSwitch);
		IS_ON.setText("§a1", "", clickToSwitch);
		SadItem cancel = this.cancel.clone();
		INVENTORY = Bukkit.createInventory(null, 9 * 6, "");
		for (int x = 1; x < 8; x++) {
			INVENTORY.setItem(9 * 1 + x, cancel);
		}

		SadItem border = this.border.clone();
		for (int x = 0; x < 9; x += 8) {
			for (int y = 0; y < 6; y++) {
				INVENTORY.setItem(9 * y + x, border);
			}
		}
		for (int y = 0; y < 6; y += 5) {
			for (int x = 1; x < 8; x++) {
				INVENTORY.setItem(9 * y + x, border);
			}
		}
	}

	public void setSaveFile(UserData saveFile) {
		this.saveFile = saveFile;
	}

	@Override
	public void create() {
		if (saveFile == null) {
			menuPlayer.openMenu(menuPlayer.getCharSelectionMenu());
			return;
		}
		inventory = Bukkit.createInventory(null, 9 * 6, inventoryTitle + saveFile.getName());
		SadItem.cloneInventory(INVENTORY, inventory);

		SadItem item = menuPlayer.getCharSelectionMenu().createClassSelectionItem(saveFile);
		List<String> text = item.getText();
		text.set(0, "§7" + dictionary.get("info1", "§l§4") + "§7 " + item.getName());
		text.set(text.size() - 2, "§c" + dictionary.get("warning") + "!");
		text.remove(text.size() - 1);
		inventory.setItem(9 + 4, item.setText(text));

		SadItem shouldOff = this.shouldOff.clone();
		SadItem shouldOn = this.shouldOn.clone();
		SadItem isNotSet = this.isNotSet.clone();
		for (int i = 0; i < solution.length; i++) {
			solution[i] = Math.random() >= 0.5 ? State.ON : State.OFF;
			state[i] = State.NOT_SET;
			inventory.setItem(9 * 3 + 1 + i, solution[i] == State.ON ? shouldOn : shouldOff);
			inventory.setItem(9 * 4 + 1 + i, isNotSet);
		}
	}

	@Override
	protected void onClick(InventoryClickEvent e) {
		ItemStack item = e.getCurrentItem();
		if (item == null || !item.hasItemMeta()) {
			return;
		}

		int slot = e.getSlot();
		int row = slot / 9;
		int column = slot % 9;
		if (row == 4 && 0 < column && column < 8) {
			toggle((column - 1) % 7);
		} else if (row == 1 && column % 4 > 0) {
			menuPlayer.openMenu(menuPlayer.getCharSelectionMenu());
		} else {
			return;
		}
		BUTTON_CLICK_SOUND.playFor(player);
	}

	private void toggle(int switchId) {
		State newState = state[switchId].getNextState();
		state[switchId] = newState;
		inventory.setItem(9 * 4 + 1 + switchId, newState == State.ON ? IS_ON.clone() : IS_OFF.clone());
		check();
	}

	private void check() {
		for (int switchId = 0; switchId < solution.length; switchId++) {
			if (solution[switchId] != state[switchId]) {
				return;
			}
		}
		account.deleteChar(saveFile);
		DELETE_SOUND.playFor(player);
		Clazz clazz = saveFile.getClazz();
		String countdownMessage = "§4" + saveFile.getName() + "§4, " + saveFile.getLevelColor() + saveFile.getLevel() + "§4-yo, "
				+ clazz.getColor() + genericDictionary.get(clazz.getName()) + "§4 " + dictionary.get("done");
		menuPlayer.setCountdownData(5, countdownMessage);
		player.closeInventory();
		Menu menu = menuPlayer.getCharSelectionMenu();
		menu.reset();
		menuPlayer.setActiveMenu(menu);
	}

	@Override
	protected void onChat(AsyncPlayerChatEvent e) {
	}
}
