package de.ced.doedelcraft.game;

import de.ced.logic.vector.Vector3;
import de.ced.logic.vector.Vector5;

public interface GameVectorCreator {

    Vector3 createLocationVector();
}
