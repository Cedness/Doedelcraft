package de.ced.doedelcraft.ingame.sound;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.util.Logger;
import org.bukkit.Sound;
import org.bukkit.configuration.file.YamlConfiguration;

import de.ced.doedelcraft.ingame.chaa.CharManager;
import de.ced.logic.vector.Vector3;

public class SoundManager {

	private final Doedelcraft doedelcraft;

	private Map<SuperSoundCategory, Map<String, SuperSound>> sounds = new HashMap<>();
	private List<PlayingSound> playingSounds = new ArrayList<>();

	@SuppressWarnings("unchecked")
	public SoundManager(CharManager charManager) {
		doedelcraft = charManager.getDoedelcraft();
		for (SuperSoundCategory category : SuperSoundCategory.values()) {
			sounds.put(category, new HashMap<>());
		}
		try {
			YamlConfiguration soundConfig = doedelcraft.loadConfig("sounds");
			for (Map<?, ?> map : soundConfig.getMapList("sounds")) {
				try {
					loadSound((Map<String, Object>) map);
				} catch (Exception ex) {
					Logger.warning("Exception while loading a SoundDefintion");
					ex.printStackTrace();
				}
			}
		} catch (Exception ex) {
			Logger.severe("Exception while loading SoundDefintions");
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void loadSound(Map<String, Object> map) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException {

		String name = (String) map.get("name");
		SuperSoundCategory category = SuperSoundCategory.valueOf(((String) map.getOrDefault("category", "PLAYER")).toUpperCase());
		float volume = extractFloat(map, "volume", 1);
		float pitch = extractFloat(map, "pitch", 1);
		boolean stop = (boolean) map.getOrDefault("stop", false);

		SuperSound superSound = new SuperSound(doedelcraft, name, category, volume);

		for (Map<?, ?> rawSubMap : (List<Map<?, ?>>) map.get("sounds")) {
			Map<String, Object> subMap = (Map<String, Object>) rawSubMap;

			String id = (String) subMap.get("id");
			try {
				Sound.valueOf(id.replace('.', '_').toUpperCase());
			} catch (Exception ex) {
				Logger.warning("Id " + id + " of sound " + superSound.getName() + " is invalid");
			}
			int time = ((Number) subMap.getOrDefault("time", 0)).intValue();
			SoundDefinition definition;
			if ((boolean) subMap.getOrDefault("stop", stop)) {
				definition = new SoundStop(id, time);
			} else {
				float subVolume = extractFloat(subMap, "volume", volume);
				float subPitch = extractFloat(subMap, "pitch", pitch);
				definition = new SoundStart(id, time, subVolume, subPitch);
			}

			superSound.addDefintion(definition);
		}
		sounds.get(category).put(name, superSound);
	}

	private float extractFloat(Map<String, Object> map, String key, float defaultValue) {
		return ((Number) map.getOrDefault(key, defaultValue)).floatValue();
	}

	public void tick(int tick) {
		for (Iterator<PlayingSound> iterator = playingSounds.iterator(); iterator.hasNext();) {
			PlayingSound playingSound = iterator.next();
			int time = tick - playingSound.getStartTick();
			if (time < 0) {
				continue;
			}
			SuperSound superSound = playingSound.getSuperSound();
			if (superSound.getDuration() <= time) {
				iterator.remove();
				continue;
			}
			superSound.apply(time, playingSound.getLocation());
		}
	}

	public SuperSound getSound(SuperSoundCategory soundCategory, String id) {
		return sounds.get(soundCategory).get(id);
	}

	public SuperSound getSound(String id) {
		for (SuperSoundCategory category : SuperSoundCategory.sortedValues()) {
			SuperSound superSound = sounds.get(category).get(id);
			if (superSound == null) {
				continue;
			}
			return superSound;
		}
		return null;
	}

	public void play(SuperSound sound, Vector3 location) {
		if (sound == null) {
			return;
		}
		playingSounds.add(new PlayingSound(sound, location, doedelcraft.getTick()));
	}
}
