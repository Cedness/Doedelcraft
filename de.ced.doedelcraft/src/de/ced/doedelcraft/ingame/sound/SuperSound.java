package de.ced.doedelcraft.ingame.sound;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.World;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.minecraft.UserMinecraft;
import de.ced.logic.vector.Vector3;

public class SuperSound {

	private final Doedelcraft doedelcraft;
	private final String name;
	private final SuperSoundCategory category;
	private final float distance;
	private final ArrayList<List<SoundDefinition>> sounds = new ArrayList<>();

	public SuperSound(Doedelcraft doedelcraft, String name, SuperSoundCategory category, float volume) {
		this.doedelcraft = doedelcraft;
		this.name = name;
		this.category = category;
		distance = 256.0f * volume * volume + 25;
	}

	void addDefintion(SoundDefinition definition) {
		int time = definition.getTime();
		sounds.ensureCapacity(time);
		while (sounds.size() <= time) {
			sounds.add(null);
		}

		if (sounds.get(time) == null) {
			sounds.set(time, new ArrayList<>());
		}
		sounds.get(time).add(definition);
	}

	public String getName() {
		return name;
	}

	public SuperSoundCategory getCategory() {
		return category;
	}

	public int getDuration() {
		return sounds.size();
	}

	public void apply(int time, Char chaa) {
		apply(time, chaa.getMinecraft().getLocation());
	}

	public void apply(int time, User listener) {
		apply(time, listener.getMinecraft().getLocation(), listener);
	}

	public void apply(int time, Vector3 location) {
		apply(time, location, null);
	}

	public void apply(int time, Vector3 location, User listener) {
		List<SoundDefinition> definitions = sounds.get(time);
		if (definitions == null) {
			return;
		}
		for (SoundDefinition definition : definitions) {
			if (definition instanceof SoundStop) {
				stop((SoundStop) definition, location, listener);
			}
		}
		for (SoundDefinition definition : definitions) {
			if (definition instanceof SoundStart) {
				start((SoundStart) definition, location, listener);
			}
		}
	}

	private void start(SoundStart definition, Vector3 location, User listener) {
		if (listener == null || category.isAmbient()) {
			World world = doedelcraft.getWorldManager().getWorld(); // TODO make world access abstract
			world.playSound(location.toLocation(), definition.getId(), category.getMCCategory(), definition.getVolume(), definition.getPitch());
		} else {
			listener.getMinecraft().playSound(location, definition.getId(), category.getMCCategory(), definition.getVolume(), definition.getPitch());
		}
	}

	private void stop(SoundStop soundStop, Vector3 location, User listener) {
		if (listener == null || category.isAmbient()) {
			for (User user : doedelcraft.getUserManager().getUsers()) {
				UserMinecraft minecraft = user.getMinecraft();
				if (location.distanceSquared(minecraft.getLocation()) > distance) {
					continue;
				}
				minecraft.stopSound(soundStop.getId(), category.getMCCategory());
			}
		} else {
			listener.getMinecraft().stopSound(soundStop.getId(), category.getMCCategory());
		}
	}
}
