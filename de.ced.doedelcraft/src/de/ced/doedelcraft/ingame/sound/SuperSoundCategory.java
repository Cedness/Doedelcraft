package de.ced.doedelcraft.ingame.sound;

import org.bukkit.SoundCategory;

public enum SuperSoundCategory {

	UI(SoundCategory.VOICE, false),
	PLAYER(SoundCategory.PLAYERS, true),
	AMBIENT(SoundCategory.AMBIENT, true),
	SPELLINFO(SoundCategory.VOICE, false),
	SPELLEMIT(SoundCategory.PLAYERS, true);

	private final static SuperSoundCategory[] sortedValues = { UI, PLAYER, SPELLINFO, SPELLEMIT, AMBIENT };

	private final SoundCategory mcSoundCategory;
	private final boolean ambient;

	private SuperSoundCategory(SoundCategory mcSoundCategory, boolean ambient) {
		this.mcSoundCategory = mcSoundCategory;
		this.ambient = ambient;
	}

	public SoundCategory getMCCategory() {
		return mcSoundCategory;
	}

	public boolean isAmbient() {
		return ambient;
	}

	public static SuperSoundCategory[] sortedValues() {
		return sortedValues;
	}
}
