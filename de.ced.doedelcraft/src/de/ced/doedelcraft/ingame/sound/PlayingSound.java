package de.ced.doedelcraft.ingame.sound;

import de.ced.logic.vector.Vector3;

public class PlayingSound {

	private final SuperSound superSound;
	private final Vector3 location;
	private final int startTick;

	public PlayingSound(SuperSound superSound, Vector3 location, int startTick) {
		this.superSound = superSound;
		this.location = location;
		this.startTick = startTick;
	}

	public SuperSound getSuperSound() {
		return superSound;
	}

	public Vector3 getLocation() {
		return location;
	}

	public boolean hasLocation() {
		return location != null;
	}

	public int getStartTick() {
		return startTick;
	}
}
