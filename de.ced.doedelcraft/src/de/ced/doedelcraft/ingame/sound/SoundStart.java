package de.ced.doedelcraft.ingame.sound;

public class SoundStart extends SoundDefinition {

	private float volume;
	private float pitch;

	public SoundStart(String id, int time, float volume, float pitch) {
		super(id, time);
		this.volume = volume;
		this.pitch = pitch;
	}

	public float getVolume() {
		return volume;
	}

	public float getPitch() {
		return pitch;
	}
}
