package de.ced.doedelcraft.ingame.sound;

public abstract class SoundDefinition {

	private String id;
	private int time;

	SoundDefinition(String id, int time) {
		this.id = id;
		this.time = time;
	}

	public String getId() {
		return id;
	}

	public int getTime() {
		return time;
	}
}
