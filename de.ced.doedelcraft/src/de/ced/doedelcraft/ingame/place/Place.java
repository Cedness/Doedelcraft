package de.ced.doedelcraft.ingame.place;

import de.ced.doedelcraft.ingame.aoe.Realm;
import de.ced.logic.vector.Vector2;
import de.ced.logic.vector.Vector3;

public class Place {

	private String name;
	private Vector3 position = Vector3.empty();
	private Vector2 facing = Vector2.empty();
	private Realm realm;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Vector3 getPosition() {
		return position;
	}

	public void setPosition(Vector3 location) {
		this.position = location;
	}

	public Vector2 getFacing() {
		return facing;
	}

	public void setFacing(Vector2 facing) {
		this.facing = facing;
	}

	public Realm getRealm() {
		return realm;
	}

	public void setRealm(Realm realm) {
		this.realm = realm;
	}
}
