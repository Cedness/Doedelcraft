package de.ced.doedelcraft.ingame.place;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.ingame.chaa.CharManager;
import de.ced.doedelcraft.util.Logger;
import org.bukkit.configuration.file.YamlConfiguration;

import de.ced.doedelcraft.ingame.aoe.AbstractArea;
import de.ced.doedelcraft.ingame.aoe.Area;
import de.ced.doedelcraft.ingame.aoe.Circle;
import de.ced.doedelcraft.ingame.aoe.Cuboid;
import de.ced.doedelcraft.ingame.aoe.Cylinder;
import de.ced.doedelcraft.ingame.aoe.RealmType;
import de.ced.doedelcraft.ingame.aoe.Rectangle;
import de.ced.doedelcraft.ingame.aoe.Sphere;

public class PlaceManager {

	private final Doedelcraft doedelcraft;
	private YamlConfiguration placeConfig;
	private final List<Place> countries = new ArrayList<>();
	private final List<Place> regions = new ArrayList<>();
	private final List<Place> towns = new ArrayList<>();
	private final List<Place> places = new ArrayList<>();
	private final List<Place> locations = new ArrayList<>();

	public PlaceManager(CharManager charManager) {
		doedelcraft = charManager.getDoedelcraft();
		try {
			placeConfig = doedelcraft.loadConfig("places");
			loadPlaces("countries", countries);
			loadPlaces("regions", regions);
			loadPlaces("towns", towns);
			loadPlaces("places", places);
			loadPlaces("locations", locations);
		} catch (Exception ex) {
			Logger.severe("Exception while loading places");
			ex.printStackTrace();
		}
	}

	private void loadPlaces(String name, List<Place> list) {
		// Logger.info("Loading " + name);
		List<?> config = placeConfig.getList(name);
		if (config == null) {
			Logger.warning("No " + name + " found");
			return;
		}
		for (Object object : config) {
			if (loadPlace(object, list)) {
				continue;
			}
			Logger.severe("Failed to load a place in " + name);
		}
		int count = list.size();
		if (count <= 0) {
			Logger.warning("No " + name + " found");
			return;
		}
		// Logger.info(count + " " + name + " found");
	}

	@SuppressWarnings("unchecked")
	private boolean loadPlace(Object object, List<Place> list) {
		if (!(object instanceof Map)) {
			return false;
		}
		try {
			Place place = new Place();
			// Logger.info("Loading a place...");

			Map<String, Object> map = (Map<String, Object>) object;
			/*
			 * for (Object obj : map.values()) { Logger.info(obj.getClass().getName()); if
			 * (obj instanceof List) { for (Object obj2 : ((List<?>) obj)) {
			 * Logger.info(obj2.getClass().getName()); } } }
			 */

			place.setName((String) map.get("name"));
			Logger.info("Name: " + place.getName());

			List<Number> positionCoordinates = (List<Number>) map.get("position");
			place.getPosition().set(
					get(positionCoordinates, 0),
					get(positionCoordinates, 1),
					get(positionCoordinates, 2)
			);
			// Logger.info("Position: loc" + Arrays.toString(place.getPosition()));

			if (map.containsKey("facing")) {
				List<Number> facingCoordinates = (List<Number>) map.get("facing");
				place.getFacing().set(
						get(facingCoordinates, 0),
						get(facingCoordinates, 1)
				);
				// Logger.info("Facing: loc" + Arrays.toString(place.getFacing()));
			}

			boolean absolutePlace = (boolean) map.getOrDefault("absolute", false);
			boolean blockPlace = (boolean) map.getOrDefault("block", false);

			List<Area> areasOfEffect = new ArrayList<>();
			for (Map<String, Object> areaMap : (List<Map<String, Object>>) map.get("areas")) {
				String areaType = areaMap.containsKey("type") ? (String) areaMap.get("type") : "rectangle";
				List<Number> areaCoordinates = (List<Number>) areaMap.get("coordinates");

				boolean absolute = absolutePlace || (boolean) areaMap.getOrDefault("absolute", false);
				boolean block = blockPlace || (boolean) areaMap.getOrDefault("block", false);

				AbstractArea areaOfEffect = loadAreaOfEffect(areaType, areaCoordinates, absolute, block);
				if (areaOfEffect == null) {
					continue;
				}
				areasOfEffect.add(areaOfEffect);

				Logger.info("AreaOfEffect: " + areaOfEffect.getClass().getSimpleName() + " " + areaOfEffect.toString());
			}
			place.setRealm(doedelcraft.getWorldManager().getRealmFactory().createImmutableRealm(place.getName(), RealmType.LOCATION, areasOfEffect));

			list.add(place);

			Logger.info("Place " + place.getName() + " loaded");
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	private AbstractArea loadAreaOfEffect(String areaType, List<Number> areaCoordinates, boolean absolute, boolean block) {
		int count = areaCoordinates.size();
		switch (areaType) {
		case "rectangle": {
			if (count != 4) {
				break;
			}
			double[] location = {
					get(areaCoordinates, 0),
					get(areaCoordinates, 1)
			};
			double[] lw = {
					get(areaCoordinates, 2),
					get(areaCoordinates, 3)
			};
			processAttributes(location, lw, absolute, block);
			Rectangle rectangle = new Rectangle();
			rectangle.getLocation().set(location[0], 0, location[0]);
			rectangle.getLw().set(lw[0], lw[1]);
			return rectangle;
		}
		case "cuboid": {
			if (count != 6) {
				break;
			}
			double[] location = {
					get(areaCoordinates, 0),
					get(areaCoordinates, 1),
					get(areaCoordinates, 2)
			};
			double[] lwh = {
					get(areaCoordinates, 3),
					get(areaCoordinates, 4),
					get(areaCoordinates, 5)
			};
			processAttributes(location, lwh, absolute, block);
			Cuboid cuboid = new Cuboid();
			cuboid.getLocation().set(location[0], location[1], location[2]);
			cuboid.getLwh().set(lwh[0], lwh[1], lwh[2]);
			return cuboid;
		}
		case "circle": {
			if (count != 3) {
				break;
			}
			Circle circle = new Circle();
			circle.getLocation().set(
					get(areaCoordinates, 0),
					0.0,
					get(areaCoordinates, 1)
			);
			circle.setRadius(get(areaCoordinates, 2));
			return circle;
		}
		case "cylinder": {
			if (count != 4) {
				break;
			}
			Cylinder cylinder = new Cylinder();
			cylinder.getLocation().set(
					get(areaCoordinates, 0),
					get(areaCoordinates, 1),
					get(areaCoordinates, 2)
			);
			cylinder.setRadius(get(areaCoordinates, 3));
			return cylinder;
		}
		case "sphere": {
			if (count != 4) {
				break;
			}
			Sphere sphere = new Sphere();
			sphere.getLocation().set(
					get(areaCoordinates, 0),
					get(areaCoordinates, 1),
					get(areaCoordinates, 2)
			);
			sphere.setRadius(get(areaCoordinates, 3));
			return sphere;
		}
		}
		return null;
	}

	private double get(List<Number> list, int index) {
		return list.get(index).doubleValue();
	}

	private void processAttributes(double[] location, double[] offset, boolean absolute, boolean block) {
		if (absolute) {
			for (int i = 0; i < location.length; i++) {
				if (location[i] > offset[i]) {
					double d = location[i];
					location[i] = offset[i];
					offset[i] = d;
				}
				offset[i] -= location[i];
			}
		}
		if (block) {
			for (int i = 0; i < offset.length; i++) {
				offset[i]++;
			}
		}
	}

	public List<Place> getCountries() {
		return countries;
	}

	public List<Place> getRegions() {
		return regions;
	}

	public List<Place> getTowns() {
		return towns;
	}

	public List<Place> getPlaces() {
		return places;
	}

	public List<Place> getLocations() {
		return locations;
	}
}
