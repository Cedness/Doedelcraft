package de.ced.doedelcraft.ingame.clazz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import de.ced.doedelcraft.account.Language;
import de.ced.doedelcraft.ingame.chaa.CastedSpell;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.util.Logger;
import de.ced.doedelcraft.util.SadItem;

/**
 * Represents a spell which can be used by a {@link Char}. Each spell belongs to
 * a {@link Clazz}.
 * 
 * @author Ced
 */
public class Spell extends ConfigObject {

	protected static final int DESCRIPTION_WIDTH = 40;

	protected final Class<?> actionsClass;
	protected final Clazz clazz;
	protected final int spellId;
	protected final int spellLevel;
	protected final String color;
	protected final Material item;
	protected final Map<Language, List<String>> description;
	protected final List<SpellResult> results;

	protected final boolean switchSpell;
	protected final boolean register;
	protected final boolean interactive;
	protected final boolean adapts;
	protected final boolean needsCombatMode;

	protected final int maxStrength;
	protected final int cooldown;

	protected final IntConsumption launchMana;
	protected final DoubleConsumption constantMana;
	protected final IntConsumption disableMana;
	protected final IntConsumption exhaustion;

	public Spell(ClazzManager clazzManager, YamlConfiguration config, Clazz clazz, int spellId, int spellLevel) {
		super(clazzManager, config);
		StringBuilder builder = new StringBuilder();
		for (String part : name.split("_")) {
			builder.append(part.substring(0, 1).toUpperCase()).append(part.substring(1).toLowerCase());
		}
		String actionsClassName = builder.toString();
		Class<?> actionsClass = null;
		for (Class<?> anActionsClass : CastedSpell.class.getClasses()) {
			String className = anActionsClass.getSimpleName().replaceAll("SpellActions", "");
			if (className.equals(actionsClassName)) {
				actionsClass = anActionsClass;
				break;
			}
		}
		if (actionsClass == null) {
			Logger.severe("Could not find ActionsClass for spell " + name);
		}
		this.actionsClass = actionsClass;
		this.clazz = clazz;
		this.spellId = spellId;
		this.spellLevel = spellLevel;
		color = "§" + config.get("color", "f");
		item = Material.valueOf(config.getString("item", "BARRIER"));
		description = new HashMap<>();
		for (Language language : clazzManager.getDoedelcraft().getAccountManager().getLanguageManager().getLanguages()) {
			description.put(language, SadItem.parseText(language.getD("spell").getD(clazz.getName()).getD(name).get("description"), DESCRIPTION_WIDTH));
		}
		results = new ArrayList<>();
		for (Map<?, ?> map : config.getMapList("results")) {
			@SuppressWarnings("unchecked")
			Map<String, Object> resultConfig = (Map<String, Object>) map;
			boolean successful = (boolean) resultConfig.getOrDefault("successful", Boolean.valueOf(true));
			String message = (String) resultConfig.getOrDefault("message", null);
			String colorString = String.valueOf(resultConfig.getOrDefault("color", ""));
			Character color = colorString.isEmpty() ? null : colorString.charAt(0);
			String sound = (String) resultConfig.getOrDefault("sound", null);
			results.add(message == null && color == null && sound == null ? SpellResult.getFallbackResult() : new SpellResult(message, color, sound, successful));
		}
		if (results.isEmpty()) {
			// Logger.warning("No results defined for spell " + this);
			results.add(SpellResult.getFallbackResult());
		}
		switchSpell = config.getBoolean("switchspell", false);
		register = config.getBoolean("register", switchSpell);
		interactive = config.getBoolean("interactive", switchSpell);
		adapts = config.getBoolean("adapts", false);
		needsCombatMode = config.getBoolean("needs_combatmode", true);
		maxStrength = config.getInt("max_strength", 1);
		cooldown = config.getInt("cooldown", 1);
		launchMana = new IntConsumption(config.getConfigurationSection("mana"));
		constantMana = new DoubleConsumption(config.getConfigurationSection("mana.constant"));
		disableMana = new IntConsumption(config.getConfigurationSection("mana.disable"));
		exhaustion = new IntConsumption(config.getConfigurationSection("exhaustion"));
	}

	public class IntConsumption extends Consumption {

		public IntConsumption(ConfigurationSection config) {
			super(config);
		}

		@Override
		public Integer getValue(int multiplier) {
			return super.getValue(multiplier).intValue();
		}
	}

	public class DoubleConsumption extends Consumption {

		public DoubleConsumption(ConfigurationSection config) {
			super(config);
		}

		@Override
		public Double getValue(int multiplier) {
			return super.getValue(multiplier).doubleValue();
		}
	}

	public abstract class Consumption {

		protected final double min;
		protected final double max;
		protected final List<Double> values;

		public Consumption(ConfigurationSection config) {
			if (config == null) {
				values = null;
				min = 0.0;
				max = Double.MIN_VALUE;
				return;
			}
			this.values = config.isList("values") ? config.getDoubleList("values") : null;
			this.min = config.getDouble("min", 0.0);
			this.max = config.getDouble("max", Double.MIN_VALUE);
		}

		public Number getValue(int multiplier) {
			if (values != null) {
				return values.get(multiplier);
			}
			if (maxStrength <= 1 || multiplier <= 0 || max == Double.MIN_VALUE || max == min) {
				return min;
			}
			return (max - min) / (maxStrength - 1) * multiplier + min;
		}
	}

	public Class<?> getActionsClass() {
		return actionsClass;
	}

	public Clazz getClazz() {
		return clazz;
	}

	public int getSpellId() {
		return spellId;
	}

	public int getSpellLevel() {
		return spellLevel;
	}

	public String getColor() {
		return color;
	}

	public Material getItem() {
		return item;
	}

	public List<String> getFormattedDescription(Language language) {
		return description.get(language);
	}

	public SpellResult getResult(int id) {
		return 0 <= id && id < results.size() ? results.get(id) : null;
	}

	public boolean isSwitchSpell() {
		return switchSpell;
	}

	public boolean isRegistered() {
		return register;
	}

	public boolean isInteractive() {
		return interactive;
	}

	public boolean adapts() {
		return adapts;
	}

	public boolean needsCombatMode() {
		return needsCombatMode;
	}

	public int getMaxStrength() {
		return maxStrength;
	}

	public int getCooldown() {
		return cooldown;
	}

	public IntConsumption getLaunchMana() {
		return launchMana;
	}

	public DoubleConsumption getConstantMana() {
		return constantMana;
	}

	public IntConsumption getDisableMana() {
		return disableMana;
	}

	public IntConsumption getExhaustion() {
		return exhaustion;
	}

	@Deprecated
	public boolean hasActive(User user) {
		return user.getSpellInfo().getSpellState(this);
	}

	@Override
	public String toString() {
		return name + " (" + clazz.getName() + "|" + (spellId + 1) + "." + (spellLevel + 1) + ")";
	}
}
