package de.ced.doedelcraft.ingame.clazz;

import java.util.ArrayList;
import java.util.List;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.ingame.chaa.CharManager;

public class ClazzManager {

	private final Doedelcraft doedelcraft;

	private final List<Race> races;
	private final List<Clazz> clazzes;
	private final List<Spell> spells;

	public ClazzManager(CharManager charManager) {
		doedelcraft = charManager.getDoedelcraft();

		races = new ArrayList<>();
		{
			List<String> raceNames = doedelcraft.loadConfig("races").getStringList("races");
			for (String raceName : raceNames) {
				// Logger.info("Loading race " + raceName + "...");
				races.add(new Race(this, doedelcraft.loadConfig(raceName, "race", "races")));
			}
		}
		// Logger.info("Loaded races: " + listToString(races));

		clazzes = new ArrayList<>();
		for (Race race : races) {
			for (String clazzName : race.getRawClazzes()) {
				// Logger.info("Loading clazz " + clazzName + "...");
				clazzes.add(new Clazz(this, doedelcraft.loadConfig(clazzName, "clazz", "clazzes"), race));
			}
		}
		// Logger.info("Loaded clazzes: " + listToString(clazzes));

		spells = new ArrayList<>();
		for (Clazz clazz : clazzes) {
			for (List<String> spellNames : clazz.getRawSpells()) {
				List<Spell> someSpells = new ArrayList<>();
				for (String spellName : spellNames) {
					// Logger.info("Loading spell " + spellName + "...");
					Spell spell = new Spell(this, doedelcraft.loadConfig(spellName, "spell", "spells/" + clazz.getName()),
							clazz,
							clazz.getSpells().size(),
							someSpells.size());
					spells.add(spell);
					someSpells.add(spell);
				}
				clazz.addSpells(someSpells);
			}
		}
		// Logger.info("Loaded spells: " + listToString(spells));
	}

	@SuppressWarnings("unused")
	private String listToString(List<? extends ConfigObject> configObjects) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0;; i++) {
			builder.append(configObjects.get(i));
			if (i >= configObjects.size() - 1) {
				break;
			}
			builder.append(", ");
		}
		return builder.toString();
	}

	public Doedelcraft getDoedelcraft() {
		return doedelcraft;
	}

	public List<Race> getRaces() {
		return races;
	}

	public List<Clazz> getClazzes() {
		return clazzes;
	}

	public List<Spell> getSpells() {
		return spells;
	}

	public Race getRaceByName(String raceName) {
		for (Race race : races) {
			if (race.getName().equals(raceName)) {
				return race;
			}
		}
		return null;
	}

	public Clazz getClazzByName(String clazzName) {
		for (Clazz clazz : clazzes) {
			if (clazz.getName().equals(clazzName)) {
				return clazz;
			}
		}
		return null;
	}

	public Spell getSpellByName(String spellName) {
		for (Spell spell : spells) {
			if (spell.getName().equals(spellName)) {
				return spell;
			}
		}
		return null;
	}
}
