package de.ced.doedelcraft.ingame.clazz;

public class SpellResult {

	private static final SpellResult FALLBACK_RESULT = new SpellResult(null, "fallback");

	public static SpellResult getFallbackResult() {
		return new SpellResult(FALLBACK_RESULT);
	}

	private String message;
	private String color;
	private String sound;
	private boolean successful;

	public SpellResult(SpellResult result) {
		message = result.getMessage();
		color = result.getColor();
		sound = result.getColor();
		successful = result.isSuccessful();
	}

	public SpellResult() {
		this(null, null, null, true);
	}

	public SpellResult(String message) {
		this(message, null, null, true);
	}

	public SpellResult(String message, boolean successful) {
		this(message, null, null, successful);
	}

	public SpellResult(String message, String sound) {
		this(message, null, sound, true);
	}

	public SpellResult(String message, String sound, boolean successful) {
		this(message, null, sound, successful);
	}

	public SpellResult(String message, Character color) {
		this(message, color, null, true);
	}

	public SpellResult(String message, Character color, boolean successful) {
		this(message, color, null, successful);
	}

	public SpellResult(String message, Character color, String sound) {
		this(message, color, sound, true);
	}

	public SpellResult(boolean successful) {
		this(null, null, null, successful);
	}

	public SpellResult(String message, Character color, String sound, boolean successful) {
		setMessage(message);
		setColor(color);
		setSound(sound);
		setSuccessful(successful);
	}

	public String getMessage() {
		return message;
	}

	public String getColor() {
		return color;
	}

	public String getSound() {
		return sound;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public void setMessage(String message) {
		this.message = message == null ? "" : message;
		if (this == FALLBACK_RESULT) {
			try {
				throw new IllegalAccessException("setMessage(...) applied to FALLBACK_RESULT");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void setColor(Character color) {
		this.color = "§" + (color == null ? '7' : color);
	}

	public void setSound(String sound) {
		this.sound = sound;
	}

	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}
}
