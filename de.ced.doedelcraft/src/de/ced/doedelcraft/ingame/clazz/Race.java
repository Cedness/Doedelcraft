package de.ced.doedelcraft.ingame.clazz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.ced.doedelcraft.account.Language;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.util.SadItem;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * Represents the race of a {@link Char}. Each race has its own clazzes
 * ({@link Clazz}).
 * 
 * @author Ced
 */
public class Race extends ConfigObject {

	private static final int DESCRIPTION_WIDTH = 40;

	private final String color;
	private final Material item;
	private List<String> rawClazzes;
	private final List<Clazz> clazzes;
	private final Map<Language, List<String>> description;

	public Race(ClazzManager clazzManager, YamlConfiguration config) {
		super(clazzManager, config);
		color = "§" + config.get("color", "f");
		item = Material.valueOf(config.getString("item", "BARRIER"));
		rawClazzes = config.getStringList("clazzes");
		clazzes = new ArrayList<>();
		description = new HashMap<>();
		for (Language language : clazzManager.getDoedelcraft().getAccountManager().getLanguageManager().getLanguages()) {
			description.put(language, SadItem.parseText(language.getD("race").getD(name).get("description"), DESCRIPTION_WIDTH));
		}
	}

	public String getColor() {
		return color;
	}

	public Material getItem() {
		return item;
	}

	List<String> getRawClazzes() {
		return rawClazzes;
	}

	void addClazz(Clazz clazz) {
		clazzes.add(clazz);
	}

	public List<Clazz> getClazzes() {
		return clazzes;
	}

	public List<String> getFormattedDescription(Language language) {
		return description.get(language);
	}
}
