package de.ced.doedelcraft.ingame.clazz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.ced.doedelcraft.account.Language;
import de.ced.doedelcraft.ingame.chaa.clazzactions.AbstractClazzActions;
import de.ced.doedelcraft.ingame.chaa.clazzactions.ClazzActions;
import de.ced.doedelcraft.util.Logger;
import de.ced.doedelcraft.util.SadItem;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import de.ced.doedelcraft.ingame.chaa.Char;

/**
 * Represents the clazz a {@link Char} can have. Each clazz has its own
 * {@link Spell}s and belongs to a {@link Race}.
 * Note that clazz designates the ingame-class and class the java-class.
 * 
 * @author Ced
 */
public class Clazz extends ConfigObject {

	protected static final int DESCRIPTION_WIDTH = 40;

	protected final Class<? extends ClazzActions> actionsClass;
	protected final Race race;
	protected final String color;
	protected final Material item;
	protected final Map<Language, List<String>> description;
	protected final int maxHealth;
	protected final double probability;
	protected final String loginSound;
	protected final String manaName;
	protected final SpellResult enableResult;
	protected final SpellResult disableResult;
	protected final SpellResult needsCombatResult;
	protected List<List<String>> rawSpells;
	protected final List<List<Spell>> spells;
	protected final List<Spell> allSpells;

	@SuppressWarnings("unchecked")
	public Clazz(ClazzManager clazzManager, YamlConfiguration config, Race race) {
		super(clazzManager, config);
		StringBuilder builder = new StringBuilder();
		for (String part : name.split("_")) {
			builder.append(part.substring(0, 1).toUpperCase()).append(part.substring(1).toLowerCase());
		}
		Class<? extends ClazzActions> actionsClass = AbstractClazzActions.getClassActions(name);
		if (actionsClass == null) {
			Logger.severe("Could not find ActionsClass for clazz " + name);
		}
		this.actionsClass = actionsClass;
		this.race = race;
		race.addClazz(this);
		color = "§" + config.get("color", "f");
		item = Material.valueOf(config.getString("item", "BARRIER"));
		description = new HashMap<>();
		for (Language language : clazzManager.getDoedelcraft().getAccountManager().getLanguageManager().getLanguages()) {
			description.put(language, SadItem.parseText(language.getD("clazz").getD(name).get("description"), DESCRIPTION_WIDTH));
		}
		maxHealth = config.getInt("max_health", 20);
		probability = config.getDouble("probability", 0.0);
		loginSound = config.getString("login_sound", "login.generic");
		manaName = config.getString("mana_name", "mana");
		enableResult = loadSpellResult(config, "results.enable", true);
		disableResult = loadSpellResult(config, "results.disable", true);
		needsCombatResult = loadSpellResult(config, "results.needs_combat", false);
		rawSpells = new ArrayList<>();
		for (Object obj : config.getList("spells", new ArrayList<>())) {
			rawSpells.add((List<String>) obj);
		}
		spells = new ArrayList<>();
		allSpells = new ArrayList<>();
	}

	private SpellResult loadSpellResult(ConfigurationSection config, String path, boolean successful) {
		if (!config.isConfigurationSection(path)) {
			return SpellResult.getFallbackResult();
		}
		config = config.getConfigurationSection(path);
		String message = config.getString("message", "");
		String colorString = String.valueOf(config.get("color", ""));
		Character color = colorString.isEmpty() ? null : colorString.charAt(0);
		String sound = config.getString("sound", null);
		if ("".equals(message) && color == null && sound == null) {
			return SpellResult.getFallbackResult();
		}
		return new SpellResult(message, color, sound, successful);
	}

	public Class<? extends ClazzActions> getActionsClass() {
		return actionsClass;
	}

	public Race getRace() {
		return race;
	}

	public String getColor() {
		return color;
	}

	public Material getItem() {
		return item;
	}

	public List<String> getFormattedDescription(Language language) {
		return description.get(language);
	}

	public boolean usesInput() {
		return true;
	}

	public boolean feelsDamage() {
		return !name.equals("smoke");
	}

	public int getMaxHealth() {
		return maxHealth;
	}

	public double getProbability() {
		return probability;
	}

	public String getLoginSound() {
		return loginSound;
	}

	public String getManaName(Language language) { // TODO language, is this good?
		return language.getD("generic").get(manaName);
	}

	public SpellResult getEnableResult() {
		return enableResult;
	}

	public SpellResult getDisableResult() {
		return disableResult;
	}

	public SpellResult getNeedsCombatResult() {
		return needsCombatResult;
	}

	List<List<String>> getRawSpells() {
		return rawSpells;
	}

	void addSpells(List<Spell> spells) {
		this.spells.add(spells);
		allSpells.addAll(spells);
	}

	public List<List<Spell>> getSpells() {
		return spells;
	}

	public List<Spell> getAllSpells() {
		return allSpells;
	}

	public List<Spell> getSpells(int spellId) {
		return spells.get(spellId);
	}

	public Spell getSpell(int spellId, int spellLevel) {
		if (spellId < 0 || spellLevel < 0) {
			return null;
		}
		return spells.get(spellId).get(spellLevel);
	}

	public Spell getSpellByName(String spellName) {
		for (List<Spell> spells : spells) {
			for (Spell spell : spells) {
				if (spell.getName().equals(spellName)) {
					return spell;
				}
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return name + " (" + race.getName() + ")";
	}
}
