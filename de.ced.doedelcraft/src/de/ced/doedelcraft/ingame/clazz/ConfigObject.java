package de.ced.doedelcraft.ingame.clazz;

import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigObject {

	protected final ClazzManager clazzManager;
	protected final String name;

	public ConfigObject(ClazzManager clazzManager, YamlConfiguration config) {
		this.clazzManager = clazzManager;
		name = config.getString("name");
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}
}
