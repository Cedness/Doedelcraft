package de.ced.doedelcraft.ingame.aoe;

import de.ced.logic.math.Meth;
import de.ced.logic.vector.Vector3;

public abstract class Cylindric extends Round {

	@Override
	public boolean intersects(Vector3 location, boolean human) {
		lastSquaredDistance = location.distanceSquared(this.location);
		return lastSquaredDistance <= Meth.pow2(radius);
	}
}
