package de.ced.doedelcraft.ingame.aoe;

import de.ced.logic.vector.Vector3;

public class Circle extends Cylindric implements Heightless {

	@Override
	public Vector3 far() {
		return location.addX(Vector3.empty(), radius).addZ(radius);
	}

	@Override
	public Vector3 near() {
		return location.addX(Vector3.empty(), -radius).addZ(-radius);
	}

	@Override
	public boolean intersects(Cuboid cuboid) {
		return cuboid.intersects(this);
	}

	@Override
	public boolean intersects(Plane plane) {
		return plane.intersects(this);
	}

	@Override
	public boolean intersects(Ray ray) {
		return ray.intersects(this);
	}

	@Override
	public boolean intersects(Rectangle rectangle) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Circle circle) {
		return consider2(circle);
	}

	@Override
	public boolean intersects(Cylinder cylinder) {
		return cylinder.intersects(this);
	}

	@Override
	public boolean intersects(Sphere sphere) {
		return sphere.intersects(this);
	}
}
