package de.ced.doedelcraft.ingame.aoe;

import java.util.Collection;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.ingame.world.WorldManager;

public class RealmFactory {

	private final Doedelcraft doedelcraft;

	public RealmFactory(WorldManager worldManager) {
		doedelcraft = worldManager.getDoedelcraft();
	}

	public void tick(int tick) {
		if (tick % 20 == 12) {
			int realmChecks = AbstractRealm.realmChecks;
			int areaChecks = AbstractRealm.areaChecks;
			AbstractRealm.realmChecks = 0;
			AbstractRealm.areaChecks = 0;
			if (realmChecks > 0) {
				// Logger.info(realmChecks + " Realm checks per second");
				// Logger.info(areaChecks + " Area checks per second");
			}
		}
	}

	public Realm createImmutableRealm(String name, RealmType realmType, Collection<Area> areas) {
		int areaCount = areas == null ? 0 : areas.size();
		if (areaCount <= 1) {
			return new ImmutableSimpleRealm(doedelcraft, name, realmType, areaCount < 1 ? null : areas.iterator().next());
		} else {
			return new ImmutableComplexRealm(doedelcraft, name, realmType, areas);
		}
	}
}
