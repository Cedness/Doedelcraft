package de.ced.doedelcraft.ingame.aoe;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.util.Logger;

public class ImmutableSimpleRealm extends AbstractSimpleRealm {

	private final AbstractArea area;

	public ImmutableSimpleRealm(Doedelcraft doedelcraft, String name, RealmType realmType, Area area) {
		super(doedelcraft, name, realmType);
		AbstractArea abstractArea = null;
		if (area != null) {
			if (!(area instanceof AbstractArea)) {
				throw new IllegalArgumentException();
			}
			abstractArea = (AbstractArea) area;
			Realm realm = abstractArea.getRealm();
			if (realm != null) {
				realm.remove(abstractArea);
			}
			abstractArea.setRealm(this);
			abstractArea.changedByRealm();
			changed();
		}
		this.area = abstractArea;
	}

	@Override
	public Area getArea() {
		return area;
	}

	@Override
	public void setArea(Area area) {
		Logger.severe(getClass().getSimpleName() + " " + name + ": Illegal modification prevented");
	}
}
