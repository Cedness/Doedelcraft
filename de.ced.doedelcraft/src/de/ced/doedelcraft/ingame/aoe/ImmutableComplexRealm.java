package de.ced.doedelcraft.ingame.aoe;

import java.util.Collection;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.util.Logger;

public class ImmutableComplexRealm extends AbstractComplexRealm {

	public ImmutableComplexRealm(Doedelcraft doedelcraft, String name, RealmType realmType, int capacity) {
		super(doedelcraft, name, realmType, capacity);
	}

	public ImmutableComplexRealm(Doedelcraft doedelcraft, String name, RealmType realmType, Area[] areas) {
		super(doedelcraft, name, realmType, areas);
	}

	public ImmutableComplexRealm(Doedelcraft doedelcraft, String name, RealmType realmType, Collection<Area> areas) {
		super(doedelcraft, name, realmType, areas);
	}

	@Override
	public Area set(int index, Area area) {
		return illegalModification();
	}

	@Override
	public void add(int index, Area area) {
		illegalModification();
	}

	@Override
	public Area remove(int index) {
		return illegalModification();
	}

	private Area illegalModification() {
		Logger.severe(getClass().getSimpleName() + " " + name + ": Illegal modification prevented");
		return null;
	}
}
