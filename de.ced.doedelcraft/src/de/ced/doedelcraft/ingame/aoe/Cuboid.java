package de.ced.doedelcraft.ingame.aoe;

import de.ced.logic.vector.Vector3;

public class Cuboid extends AbstractArea implements Angular {

	protected Vector3 lwh = Vector3.from(1);

	public Vector3 getLwh() {
		return lwh;
	}

	public void setLwh(Vector3 lwh) {
		this.lwh = lwh;
	}

	@Override
	public Vector3 top() {
		return location.add(Vector3.empty(), lwh);
	}

	@Override
	public boolean intersects(Vector3 location, boolean human) {
		return this.location.compare(human ? location.addY(Vector3.empty(), 1.8) : location, (a, b) -> a < b)
				&& location.compare(top(), (a, b) -> a < b);
	}

	@Override
	public void resized() {
		radius = lwh.length();
	}

	@Override
	public String toString() {
		return super.toString() + " lwh" + lwh;
	}

	@Override
	public boolean intersects(Cuboid cuboid) {
		return location.compare(cuboid.top(), (a, b) -> a < b)
				&& cuboid.location.compare(top(), (a, b) -> a < b);
	}

	@Override
	public boolean intersects(Plane plane) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Ray ray) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Rectangle rectangle) {
		return location.compareXZ(rectangle.top(), (a, b) -> a < b)
				&& rectangle.location.compareXZ(top(), (a, b) -> a < b);
	}

	@Override
	public boolean intersects(Circle circle) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Cylinder cylinder) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Sphere sphere) {
		return sphere.intersects(this);
	}
}
