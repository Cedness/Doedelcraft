package de.ced.doedelcraft.ingame.aoe;

import de.ced.logic.vector.Vector2;
import de.ced.logic.vector.Vector3;

public class Cylinder extends Cylindric implements Angular {

	protected double height;

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public Vector3 far() {
		return location.add(Vector3.empty(), radius, height, radius);
	}

	@Override
	public Vector3 near() {
		return location.addX(Vector3.empty(), -radius).addZ(-radius);
	}

	@Override
	public Vector3 top() {
		return location.addY(Vector3.empty(), height);
	}

	@Override
	public boolean intersects(Vector3 location, boolean human) {
		if (location.y() + (human ? 1.8 : 0) < this.location.y() || location.y() > this.location.y() + height) {
			lastSquaredDistance = Double.MAX_VALUE;
			return false;
		}
		return super.intersects(location, human);
	}

	@Override
	public void resized() {
		radius = Vector2.from(radius, height).length();
	}

	@Override
	public boolean intersects(Cuboid cuboid) {
		return cuboid.intersects(this);
	}

	@Override
	public boolean intersects(Plane plane) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Ray ray) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Rectangle rectangle) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Circle circle) {
		return consider2(circle);
	}

	@Override
	public boolean intersects(Cylinder cylinder) {
		return consider(cylinder) &&
				location.y() < cylinder.location.y() + cylinder.height &&
				cylinder.location.y() < location.y() + height;
	}

	@Override
	public boolean intersects(Sphere sphere) {
		return sphere.intersects(this);
	}
}
