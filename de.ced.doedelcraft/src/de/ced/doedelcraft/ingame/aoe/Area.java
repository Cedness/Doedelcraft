package de.ced.doedelcraft.ingame.aoe;

import de.ced.logic.vector.Vector3;

public interface Area extends Intersecting {

	Realm getRealm();

	Vector3 getLocation();

	void setLocation(Vector3 location);

	boolean hasChanged();

	boolean intersects(Area area);

	/**
	 * Call this method if the SIZE OF THE SPHERE AROUND THE ORIGIN, which contains
	 * this area, has changed. If it grew, you have to call it anyway. If it shrunk,
	 * you may consider only calling it in case the difference is mandatory, since
	 * it forces a call of {@code intersects()} in the future.
	 */
	void resized();
}
