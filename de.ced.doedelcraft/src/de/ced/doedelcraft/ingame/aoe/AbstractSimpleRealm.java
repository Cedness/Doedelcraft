package de.ced.doedelcraft.ingame.aoe;

import java.util.Iterator;

import de.ced.doedelcraft.Doedelcraft;

public abstract class AbstractSimpleRealm extends AbstractRealm {

	public AbstractSimpleRealm(Doedelcraft doedelcraft, String name, RealmType realmType) {
		super(doedelcraft, name, realmType);
	}

	public AbstractSimpleRealm(Doedelcraft doedelcraft, String name, RealmType realmType, Area area) {
		super(doedelcraft, name, realmType);
		setArea(area);
	}

	public abstract Area getArea();

	public abstract void setArea(Area area);

	private Area setArea0(Area area) {
		Area formerArea = getArea();
		setArea(area);
		return formerArea;
	}

	@Override
	public Iterator<Area> iterator() {
		return new Iterator<Area>() {
			boolean acquired = getArea() == null;

			@Override
			public boolean hasNext() {
				return !acquired;
			}

			@Override
			public Area next() {
				acquired = true;
				return getArea();
			}
		};
	}

	@Override
	public Area get(int index) {
		return getArea();
	}

	@Override
	public Area set(int index, Area area) {
		return setArea0(area);
	}

	@Override
	public void add(int index, Area area) {
		setArea(area);
	}

	@Override
	public Area remove(int index) {
		return setArea0(null);
	}

	@Override
	public int size() {
		return getArea() == null ? 0 : 1;
	}

	@Override
	protected boolean calcIntersects1(Realm otherRealm) {
		return otherRealm instanceof AbstractSimpleRealm
				? getArea().intersects(((AbstractSimpleRealm) otherRealm).getArea())
				: super.calcIntersects1(otherRealm);
	}
}
