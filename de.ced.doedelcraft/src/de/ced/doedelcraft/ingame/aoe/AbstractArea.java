package de.ced.doedelcraft.ingame.aoe;

import de.ced.logic.math.Meth;
import de.ced.logic.vector.Vector3;

public abstract class AbstractArea implements Area {

	/*
	 * Where-are-the-methods-guide
	 * Sphere
	 * Cuboid
	 * Cylinder
	 * Plane
	 * Ray
	 * Circle
	 * Rectangle
	 */

	protected Realm realm;
	protected Vector3 location = Vector3.empty();
	protected double radius = 0;
	protected boolean changed = true;

	protected AbstractArea() {
	}

	@Override
	public Realm getRealm() {
		return realm;
	}

	/**
	 * This method should only be called by realms!!
	 */
	void setRealm(Realm realm) {
		this.realm = realm;
	}

	@Override
	public Vector3 getLocation() {
		return location;
	}

	@Override
	public void setLocation(Vector3 location) {
		this.location = location;
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	public void changed() {
		changed = true;
		if (realm != null) {
			realm.changed();
		}
	}

	void changedByRealm() {
		changed = true;
	}

	@Override
	public final boolean intersects(Area area) {
		if (area instanceof Cuboid) {
			return intersects((Cuboid) area);
		} else if (area instanceof Plane) {
			return intersects((Plane) area);
		} else if (area instanceof Ray) {
			return intersects((Ray) area);
		} else if (area instanceof Rectangle) {
			return intersects((Rectangle) area);
		} else if (area instanceof Circle) {
			return intersects((Circle) area);
		} else if (area instanceof Cylinder) {
			return intersects((Cylinder) area);
		} else if (area instanceof Sphere) {
			return intersects((Sphere) area);
		} else {
			return false;
		}
	}

	public abstract boolean intersects(Cuboid cuboid);

	public abstract boolean intersects(Plane plane);

	public abstract boolean intersects(Ray ray);

	public abstract boolean intersects(Rectangle rectangle);

	public abstract boolean intersects(Circle circle);

	public abstract boolean intersects(Cylinder cylinder);

	public abstract boolean intersects(Sphere sphere);

	@Override
	public abstract void resized();

	@Override
	public String toString() {
		return "loc" + location;
	}

	/*
	 * public static boolean intersects(AreaOfEffect area1, AreaOfEffect area2) {
	 * boolean a1, a2;
	 * a1 = area1 instanceof Heightless;
	 * a2 = area2 instanceof Heightless;
	 * boolean oneIsHeightless;
	 * if (oneIsHeightless = a1 || a2) {
	 * if (a1 && a2) {
	 * // Only Rect and Circle left, easy distance test possible
	 * double distance;
	 * if ((distance = LocationTools3.getSquaredDistance2(area1.location,
	 * area2.location)) > area1.squaredRadius + area2.squaredRadius) { // FIXME?
	 * return false;
	 * }
	 * a1 = area1 instanceof Circle;
	 * a2 = area2 instanceof Circle;
	 * if (a1 && a2) {
	 * 
	 * }
	 * }
	 * if (a2) {
	 * AreaOfEffect heightless = area2;
	 * area2 = area1;
	 * area1 = heightless;
	 * }
	 * area1.getLocation()[1] = area2.getLocation()[1];
	 * }
	 * a1 = area1 instanceof Round;
	 * a2 = area2 instanceof Round;
	 * }
	 */

	public boolean consider(AbstractArea area) {
		return location.distanceSquared(area.location) < Meth.pow2(radius + area.radius);
	}

	public boolean consider2(AbstractArea area) {
		return location.distanceSquared(area.location) < Meth.pow2(radius + area.radius);
	}
}
