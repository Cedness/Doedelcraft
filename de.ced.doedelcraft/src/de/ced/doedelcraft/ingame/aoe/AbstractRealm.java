package de.ced.doedelcraft.ingame.aoe;

import java.util.AbstractList;
import java.util.HashMap;
import java.util.Map;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.logic.vector.Vector3;

public abstract class AbstractRealm extends AbstractList<Area> implements Realm {

	protected static int realmChecks;
	protected static int areaChecks;

	protected final Doedelcraft doedelcraft;
	protected final String name;
	protected final RealmType realmType; // Used to speed up the lookup if already calced

	protected final Map<Realm, RealmIntersectingResult> checkedSimilarRealms = new HashMap<>();
	protected final Map<RealmType, Map<Realm, RealmIntersectingResult>> checkedRealms = new HashMap<>();

	protected int changed;

	public AbstractRealm(Doedelcraft doedelcraft, String name, RealmType realmType) {
		this.doedelcraft = doedelcraft;
		this.name = name;
		this.realmType = realmType;
		for (RealmType otherRealmType : RealmType.values()) {
			checkedRealms.put(otherRealmType, realmType.equals(otherRealmType) ? checkedSimilarRealms : new HashMap<>());
		}
		changed = doedelcraft.getTick();
	}

	@Override
	public final String getName() {
		return name;
	}

	@Override
	public final RealmType getRealmType() {
		return realmType;
	}

	@Override
	public boolean intersects(Vector3 location, boolean human) {
		if (isEmpty()) {
			return false;
		}
		realmChecks++;
		for (Area area : this) {
			areaChecks++;
			if (area.intersects(location, human)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Boolean wasIntersecting(Realm realm) {
		RealmType otherRealmType = realm.getRealmType();
		RealmIntersectingResult result = realmType.equals(otherRealmType) ? checkedSimilarRealms.get(realm) : checkedRealms.get(otherRealmType).get(realm);
		if (result != null && realm.getChanged() < result.tick) { // TODO check < vs <=
			return result.intersects;
		} else {
			return null;
		}
	}

	@Override
	public boolean intersects(Realm realm) {
		Boolean wasIntersecting = wasIntersecting(realm);
		return wasIntersecting != null ? wasIntersecting : calcIntersects0(realm, 0);
	}

	@Override
	public boolean calcIntersects(Realm realm) {
		return calcIntersects0(realm, -1);
	}

	protected boolean calcIntersects0(Realm otherRealm, int tickOffset) {
		if (otherRealm == null || equals(otherRealm) || isEmpty() || otherRealm.isEmpty()) {
			return false;
		}
		boolean intersects = calcIntersects1(otherRealm);
		RealmIntersectingResult result = new RealmIntersectingResult(intersects, doedelcraft.getTick() + tickOffset);
		if (!(otherRealm instanceof AbstractRealm)) {
			throw new IllegalArgumentException();
		}
		RealmType otherRealmType = otherRealm.getRealmType();
		if (realmType.equals(otherRealmType)) {
			checkedSimilarRealms.put(otherRealm, result);
			((AbstractRealm) otherRealm).checkedSimilarRealms.put(this, result);
		} else {
			checkedRealms.get(otherRealmType).put(otherRealm, result);
			((AbstractRealm) otherRealm).checkedRealms.get(realmType).put(this, result);
		}
		return intersects;
	}

	protected boolean calcIntersects1(Realm otherRealm) {
		// TODO shift often intersecting areas to the beginning with learning
		for (Area area : this) {
			for (Area otherArea : otherRealm) {
				if (area.intersects(otherArea)) {
					return true;
				}
			}
		}
		return false;
	}

	protected class RealmIntersectingResult {

		protected final boolean intersects;
		protected final int tick;

		protected RealmIntersectingResult(boolean intersects, int tick) {
			this.intersects = intersects;
			this.tick = tick;
		}
	}

	@Override
	public int getChanged() {
		return changed;
	}

	@Override
	public void changed() {
		changed = doedelcraft.getTick();
	}
}
