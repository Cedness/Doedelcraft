package de.ced.doedelcraft.ingame.aoe;

import de.ced.logic.vector.Vector3;

public abstract class Round extends AbstractArea {

	protected double lastSquaredDistance = Double.MAX_VALUE;

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public abstract Vector3 far();

	public abstract Vector3 near();

	public double getLastSquaredDistance() {
		return lastSquaredDistance;
	}

	@Override
	public void resized() {
	}

	@Override
	public String toString() {
		return super.toString() + " r[" + radius + "]";
	}
}
