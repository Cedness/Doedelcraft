package de.ced.doedelcraft.ingame.aoe;

import java.util.Collection;

import de.ced.doedelcraft.Doedelcraft;

public abstract class AbstractComplexRealm extends AbstractRealm {

	protected Area[] areas;
	protected int size;

	public AbstractComplexRealm(Doedelcraft doedelcraft, String name, RealmType realmType, int capacity) {
		super(doedelcraft, name, realmType);
		areas = new Area[capacity];
	}

	public AbstractComplexRealm(Doedelcraft doedelcraft, String name, RealmType realmType, Area... areas) {
		super(doedelcraft, name, realmType);
		this.areas = areas.clone();
		setInitialSize();
	}

	public AbstractComplexRealm(Doedelcraft doedelcraft, String name, RealmType realmType, Collection<Area> areas) {
		super(doedelcraft, name, realmType);
		this.areas = areas.toArray(new Area[areas.size()]);
		setInitialSize();
	}

	private void setInitialSize() {
		int i;
		for (i = areas.length; i >= 0; i--) {
			if (areas[i - 1] != null) {
				break;
			}
		}
		size = i;
		for (i--; i >= 0; i--) {
			if (areas[i] != null) {
				prepare(areas[i]);
			}
		}
	}

	protected Area prepare(Area area) {
		if (area == null) {
			return area;
		}
		if (!(area instanceof AbstractArea)) {
			throw new IllegalArgumentException();
		}
		((AbstractArea) area).changedByRealm();
		return area;
	}

	@Override
	public Area get(int index) {
		return areas[index];
	}

	@Override
	public abstract Area set(int index, Area area);

	@Override
	public abstract void add(int index, Area area);

	@Override
	public abstract Area remove(int index);

	@Override
	public int size() {
		return size;
	}

	public int capacity() {
		return areas.length;
	}
}
