package de.ced.doedelcraft.ingame.aoe;

import de.ced.doedelcraft.Doedelcraft;

public class SimpleRealm extends AbstractSimpleRealm {

	private AbstractArea area;

	public SimpleRealm(Doedelcraft doedelcraft, String name, RealmType realmType) {
		super(doedelcraft, name, realmType);
	}

	public SimpleRealm(Doedelcraft doedelcraft, String name, RealmType realmType, Area area) {
		super(doedelcraft, name, realmType, area);
		setArea(area);
	}

	@Override
	public Area getArea() {
		return area;
	}

	@Override
	public void setArea(Area area) {
		if (this.area == area) {
			return;
		}
		AbstractArea abstractArea = null;
		if (area != null) {
			if (!(area instanceof AbstractArea)) {
				throw new IllegalArgumentException();
			}
			abstractArea = (AbstractArea) area;
			Realm realm = abstractArea.getRealm();
			if (realm != null) {
				realm.remove(abstractArea);
			}
			abstractArea.setRealm(this);
		}
		if (this.area != null) {
			this.area.setRealm(null);
		}
		this.area = abstractArea;
		abstractArea.changedByRealm();
		changed();
	}
}
