package de.ced.doedelcraft.ingame.aoe;

public enum RealmType {

	SPELL,
	LOCATION,
	REGION
}
