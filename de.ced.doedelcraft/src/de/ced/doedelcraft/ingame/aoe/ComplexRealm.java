package de.ced.doedelcraft.ingame.aoe;

import java.util.Collection;

import de.ced.doedelcraft.Doedelcraft;

public class ComplexRealm extends AbstractComplexRealm {

	public ComplexRealm(Doedelcraft doedelcraft, String name, RealmType realmType, int capacity) {
		super(doedelcraft, name, realmType, capacity);
	}

	public ComplexRealm(Doedelcraft doedelcraft, String name, RealmType realmType, Area[] areas) {
		super(doedelcraft, name, realmType, areas);
	}

	public ComplexRealm(Doedelcraft doedelcraft, String name, RealmType realmType, Collection<Area> areas) {
		super(doedelcraft, name, realmType, areas);
	}

	@Override
	public Area set(int index, Area area) {
		if (size <= index) {
			size = index + 1;
		}
		Area formerArea = areas[index];
		areas[index] = prepare(area);
		changed();
		return formerArea;
	}

	@Override
	public void add(int index, Area area) {
		if (size >= areas.length) {
			return;
		}
		for (int i = size; i > index; i--) {
			areas[i] = areas[i - 1];
		}
		size++;
		areas[index] = prepare(area);
		changed();
	}

	@Override
	public Area remove(int index) {
		Area formerArea = areas[index];
		areas[index] = null;
		for (int i = index + 1; i < size; i++) {
			areas[i - 1] = areas[i];
		}
		size--;
		changed();
		return formerArea;
	}
}
