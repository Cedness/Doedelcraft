package de.ced.doedelcraft.ingame.aoe;

import de.ced.logic.vector.Vector3;

public interface Angular {
	Vector3 top();
}
