package de.ced.doedelcraft.ingame.aoe;

import de.ced.logic.vector.Vector3;

public class Ray extends AbstractArea {

	protected Vector3 direction = Vector3.from(1, 0, 0);

	public Vector3 getDirection() {
		return direction;
	}

	public void setDirection(Vector3 direction) {
		this.direction = direction;
	}

	@Override
	public void resized() {
		radius = direction.length();
	}

	@Override
	public boolean intersects(Vector3 location, boolean human) {
		return false;
	}

	@Override
	public boolean intersects(Cuboid cuboid) {
		return cuboid.intersects(this);
	}

	@Override
	public boolean intersects(Plane plane) {
		return plane.intersects(this);
	}

	@Override
	public boolean intersects(Ray ray) {
		return ray.intersects(this);
	}

	@Override
	public boolean intersects(Rectangle rectangle) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Circle circle) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Cylinder cylinder) {
		return cylinder.intersects(this);
	}

	@Override
	public boolean intersects(Sphere sphere) {
		return sphere.intersects(this);
	}
}
