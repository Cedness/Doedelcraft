package de.ced.doedelcraft.ingame.aoe;

import de.ced.logic.math.Meth;
import de.ced.logic.vector.Vector3;

public class Sphere extends Round {

	@Override
	public Vector3 far() {
		return location.add(Vector3.empty(), radius);
	}

	@Override
	public Vector3 near() {
		return location.add(Vector3.empty(), -radius);
	}

	@Override
	public boolean intersects(Vector3 location, boolean human) {
		return (lastSquaredDistance = location.distanceSquared(this.location)) <= Meth.pow2(radius);
	}

	@Override
	public boolean intersects(Cuboid cuboid) {
		boolean[] low = new boolean[3];
		boolean[] inside = new boolean[3];
		int insides = 0;
		boolean result = true;
		for (int i = 0; i < 3; i++) {
			if (low[i] = cuboid.location.get(i) > location.get(i) || location.get(i) > cuboid.location.get(i) + cuboid.lwh.get(i)) { // Center not inside
				result = false;
			} else { // This axis-coordinate of the center is inside
				inside[i] = true;
				insides++;
			}
		}
		if (result) { // Clearly inside
			return true;
		}
		for (int i = 0; i < 3; i++) {
			if (cuboid.location.get(i) > location.get(i) + radius || location.get(i) - radius > cuboid.location.get(i) + cuboid.lwh.get(i)) { // A side is touched
				return false;
			} else if (insides >= 2 && !inside[i]) { // Last missing side is cut
				return true;
			}
		}
		Vector3 moddedLocation = location.clone();
		Vector3 reference = cuboid.location.clone();
		for (int i = 0; i < 3; i++) {
			if (!low[i]) { // Change corner
				reference.add(i, cuboid.lwh.get(i));
			}
			if (inside[i]) { // Axis is inside, so it is adapted to the corner
				moddedLocation.set(i, reference.get(i));
			}
		}
		return location.distanceSquared(reference) < Meth.pow2(radius); // Edging, but cutting the edge (or corner)

		/*
		 * //Old version
		 * boolean xHigh = cuboid.location[0] < location[0];
		 * boolean yHigh = cuboid.location[1] < location[1];
		 * boolean zHigh = cuboid.location[2] < location[2];
		 * if (xHigh && yHigh && zHigh &&
		 * location[0] < cuboid.location[0] + cuboid.lwh[0] &&
		 * location[1] < cuboid.location[1] + cuboid.lwh[1] &&
		 * location[2] < cuboid.location[2] + cuboid.lwh[2]) { // Clearly inside
		 * return true;
		 * }
		 * if (cuboid.location[0] > location[0] + radius ||
		 * cuboid.location[1] > location[1] + radius ||
		 * cuboid.location[2] > location[2] + radius ||
		 * location[0] - radius > cuboid.location[0] + cuboid.lwh[0] ||
		 * location[1] - radius > cuboid.location[1] + cuboid.lwh[1] ||
		 * location[2] - radius > cuboid.location[2] + cuboid.lwh[2]) { // Spheres
		 * center not inside
		 * return false;
		 * }
		 * reference = cuboid.location.clone();
		 * if (xHigh) {
		 * reference[0] += cuboid.lwh[0];
		 * }
		 * if (yHigh) {
		 * reference[1] += cuboid.lwh[1];
		 * }
		 * if (zHigh) {
		 * reference[2] += cuboid.lwh[1];
		 * }
		 * return LocationTools3.getSquaredDistance(location, reference) <
		 * PartyMath.square(radius); // Edging, but cutting the edge
		 * // FIXED in loop version: the edges themselves are not regarded
		 */
	}

	@Override
	public boolean intersects(Plane plane) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Ray ray) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Rectangle rectangle) {
		boolean xHigh = rectangle.location.x() < location.x();
		boolean zHigh = rectangle.location.z() < location.z();
		Vector3 rectangleTop = rectangle.top();
		if (xHigh && zHigh && location.compareXZ(rectangleTop, (a, b) -> a < b)) { // Clearly inside
			return true;
		}
		if (rectangle.location.x() > location.x() + radius ||
				rectangle.location.z() > location.z() + radius ||
				location.x() - radius > rectangle.location.x() + rectangle.lw.x() ||
				location.z() - radius > rectangle.location.z() + rectangle.lw.y()) { // Spheres center not inside
			return false;
		}
		Vector3 reference = rectangle.location.clone();
		if (xHigh) {
			reference.addX(rectangle.lw.x());
		}
		if (zHigh) {
			reference.addZ(rectangle.lw.y());
		}
		return location.distanceSquared(reference) < Meth.pow2(radius); // Edging, but cutting the edge
	}

	@Override
	public boolean intersects(Circle circle) {
		return consider2(circle);
	}

	@Override
	public boolean intersects(Cylinder cylinder) {
		if (!consider2(cylinder)) { // Too far horizontally
			return false;
		}
		boolean isBottom;
		if (isBottom = location.y() < cylinder.location.y() + cylinder.height && cylinder.location.y() < location.y()) { // Clearly inside
			return true;
		}
		if (location.y() - radius > cylinder.location.y() + cylinder.height || cylinder.location.y() > location.y() + radius) { // Spheres center above/below
			return false;
		}
		double squaredRadius = Meth.pow2(radius);
		if (cylinder.location.distanceSquared(location) < squaredRadius) { // Not edging
			return true;
		}
		Vector3 reference = cylinder.location.addX(Vector3.empty(), location.x()).addZ(location.z()).applyLength(cylinder.radius);
		// LocationTools3.mul2(reference, 1 / LocationTools3.getLength2(reference) *
		// cylinder.radius);
		if (!isBottom) {
			reference.addY(cylinder.height);
		}
		return location.distanceSquared(reference) < squaredRadius; // Edging, but cutting the edge
	}

	@Override
	public boolean intersects(Sphere sphere) {
		return consider(sphere);
	}
}
