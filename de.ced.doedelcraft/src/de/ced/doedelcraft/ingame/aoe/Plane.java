package de.ced.doedelcraft.ingame.aoe;

import de.ced.logic.math.Meth;
import de.ced.logic.vector.Vector2;
import de.ced.logic.vector.Vector3;

public class Plane extends AbstractArea {

	private double rotation = 0;
	private Vector2 wh = Vector2.empty();

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	public Vector2 getWh() {
		return wh;
	}

	public void setWh(Vector2 wh) {
		this.wh = wh;
	}

	@Override
	public boolean intersects(Vector3 location, boolean human) {
		return this.location.y() <= location.y() + (human ? 1.8 : 0) &&
				location.y() <= this.location.y() + wh.y() &&
				location.distanceSquared(this.location) <= Meth.pow2(wh.y()); // TODO
	}

	@Override
	public void resized() {
		radius = wh.length();
	}

	@Override
	public boolean intersects(Cuboid cuboid) {
		return cuboid.intersects(this);
	}

	@Override
	public boolean intersects(Plane plane) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Ray ray) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Rectangle rectangle) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Circle circle) {
		return true; // TODO
	}

	@Override
	public boolean intersects(Cylinder cylinder) {
		return cylinder.intersects(this);
	}

	@Override
	public boolean intersects(Sphere sphere) {
		return sphere.intersects(this);
	}
}
