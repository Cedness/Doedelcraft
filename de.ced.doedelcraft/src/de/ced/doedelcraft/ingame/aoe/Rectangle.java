package de.ced.doedelcraft.ingame.aoe;

import de.ced.logic.vector.Vector2;
import de.ced.logic.vector.Vector3;

public class Rectangle extends AbstractArea implements Angular, Heightless {

	protected Vector2 lw = Vector2.from(1);

	public Vector2 getLw() {
		return lw;
	}

	public void setLw(Vector2 lw) {
		this.lw = lw;
	}

	@Override
	public Vector3 top() {
		return location.addX(Vector3.empty(), lw.x()).addZ(lw.y());
	}

	@Override
	public boolean intersects(Vector3 location, boolean human) {
		return this.location.compareXZ(location, (a, b) -> a <= b)
				&& location.compareXZ(top(), (a, b) -> a <= b);
	}

	@Override
	public void resized() {
		radius = lw.length();
	}

	@Override
	public String toString() {
		return super.toString() + " lw" + lw;
	}

	@Override
	public boolean intersects(Cuboid cuboid) {
		return cuboid.intersects(this);
	}

	@Override
	public boolean intersects(Plane plane) {
		return plane.intersects(this);
	}

	@Override
	public boolean intersects(Ray ray) {
		return ray.intersects(this);
	}

	@Override
	public boolean intersects(Rectangle rectangle) {
		return location.compareXZ(rectangle.top(), (a, b) -> a < b)
				&& rectangle.location.compareXZ(top(), (a, b) -> a < b);
	}

	@Override
	public boolean intersects(Circle circle) {
		return circle.intersects(this);
	}

	@Override
	public boolean intersects(Cylinder cylinder) {
		return cylinder.intersects(this);
	}

	@Override
	public boolean intersects(Sphere sphere) {
		return sphere.intersects(this);
	}
}
