package de.ced.doedelcraft.ingame.aoe;

import java.util.List;

public interface Realm extends Intersecting, List<Area> {

	String getName();

	RealmType getRealmType();

	Boolean wasIntersecting(Realm realm);

	boolean intersects(Realm realm);

	boolean calcIntersects(Realm realm);

	int getChanged();

	void changed();
}
