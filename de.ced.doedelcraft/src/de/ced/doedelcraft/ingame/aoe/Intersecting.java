package de.ced.doedelcraft.ingame.aoe;

import de.ced.logic.vector.Vector3;

public interface Intersecting {

	/**
	 * Calculates if a given point intersects with this object.
	 * 
	 * @param location The point which may intersect
	 * @return {@code true} if it intersects
	 */
	default boolean intersects(Vector3 location) {
		return intersects(location, false);
	}

	boolean intersects(Vector3 location, boolean human);
}
