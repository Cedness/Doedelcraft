package de.ced.doedelcraft.ingame.chaa.clazzactions;

import de.ced.doedelcraft.ingame.chaa.Char;

public class AirClazzActions extends AbstractClazzActions {

	public AirClazzActions(Char chaa) {
		super(chaa);
	}

	@Override
	public void add() {
		chaa.getMinecraft().getEntity().setSilent(true);
	}

	@Override
	public void remove() {
		chaa.getMinecraft().getEntity().setSilent(false);
	}
}
