package de.ced.doedelcraft.ingame.chaa.clazzactions;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import de.ced.doedelcraft.ingame.chaa.Char;

public class EarthClazzActions extends AbstractClazzActions {

	public EarthClazzActions(Char chaa) {
		super(chaa);
	}

	@Override
	public void tick(int tick, int cycle) {
		if (chaa.getCombat().isCombatMode()) {
			if (tick % 4 == 0) {
				Entity entity = chaa.getMinecraft().getEntity();
				Location location = entity.getLocation();
				for (Player viewer : Bukkit.getOnlinePlayers()) {
					if (entity.equals(viewer)) {
						continue;
					}
					viewer.spawnParticle(Particle.EXPLOSION_NORMAL, location, 1); // TODO chunk me
				}
			}
		}
	}
}
