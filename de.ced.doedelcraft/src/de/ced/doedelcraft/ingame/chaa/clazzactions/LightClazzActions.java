package de.ced.doedelcraft.ingame.chaa.clazzactions;

import de.ced.doedelcraft.ingame.chaa.stuff.UserStuff;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.logic.vector.Vector3;

public class LightClazzActions extends AbstractClazzActions {

	public LightClazzActions(Char chaa) {
		super(chaa);
	}

	@Override
	public void add() {
		Entity entity = chaa.getMinecraft().getEntity();
		if (!(entity instanceof LivingEntity)) {
			return;
		}
		((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 1000000, 0, false, false, false));
	}

	@Override
	public void remove() {
		Entity entity = chaa.getMinecraft().getEntity();
		if (!(entity instanceof LivingEntity)) {
			return;
		}
		((LivingEntity) entity).removePotionEffect(PotionEffectType.NIGHT_VISION);
	}

	@Override
	public void setCombatMode(boolean enabled) {
		if (!(chaa instanceof User)) {
			return;
		}
		((UserStuff) chaa.getStuff()).setMaterial(enabled ? Material.NETHER_STAR : null);
	}

	@Override
	public void tick(int tick, int cycle) {
		spawnArmParticles(tick, 10);
	}

	@Override
	public void spawnArmParticles(User viewer, Vector3 location) {
		// viewer.spawnParticle(Particle.END_ROD, location, 0);
		// FIXME make particle system
	}
}
