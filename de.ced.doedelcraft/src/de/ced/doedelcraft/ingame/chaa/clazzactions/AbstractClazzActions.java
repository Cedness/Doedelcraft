package de.ced.doedelcraft.ingame.chaa.clazzactions;

import de.ced.doedelcraft.util.PartyMath;
import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.Removable;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.logic.vector.Vector3;

public class AbstractClazzActions extends AbstractUnit implements ClazzActions, Tickable, Removable {

	private static final Class<?>[] CLAZZ_HELPER = new Class<?>[] {
			HumanClazzActions.class,
			SpartanClazzActions.class,
			EarthClazzActions.class,
			AirClazzActions.class,
			FireClazzActions.class,
			WaterClazzActions.class,
			LightningClazzActions.class,
			RestrictionClazzActions.class,
			SmokeClazzActions.class,
			LightClazzActions.class
	};

	public static Class<? extends ClazzActions> getClassActions(String clazzName) {
		for (Object clazzActionsClassObject : CLAZZ_HELPER) {
			@SuppressWarnings("unchecked")
			Class<? extends ClazzActions> clazzActionsClass = (Class<? extends ClazzActions>) clazzActionsClassObject;
			if (clazzActionsClass.getSimpleName().toLowerCase().contains(clazzName.toLowerCase())) {
				return clazzActionsClass;
			}
		}
		return null;
	}

	protected final Char chaa;

	public AbstractClazzActions(Char chaa) {
		this.chaa = chaa;
	}

	@Override
	public void tick(int tick, int cycle) {
	}

	protected void spawnArmParticles(int tick, int refreshRate) {
		if (chaa.getCombat().isCombatMode()) {
			if (tick % refreshRate == 0) {
				Vector3[] armPositions = PartyMath.getArmPositions(chaa);
				for (User viewer : chaa.getCharManager().getUserManager().getUsers()) {
					if (chaa.equals(viewer)) {
						continue;
					}
					for (Vector3 location : armPositions) {
						spawnArmParticles(viewer, location);
					}
				}
			}
		}
	}

	protected void spawnArmParticles(User viewer, Vector3 location) {
		// viewer.spawnParticle(Particle.BARRIER,
		// LocationTools3.pack(viewer.getLocation(), location), 0); //
		// FIXME make particle system
	}

	@Override
	public void add() {
	}

	@Override
	public void remove() {
	}

	@Override
	public void setCombatMode(boolean enabled) {
		if (enabled) {
			enable();
		} else {
			disable();
		}
	}

	@Override
	public void enable() {
		// user.getPlayer().addPotionEffect(new
		// PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000000, 0, true, true,
		// true));
	}

	@Override
	public void disable() {
		// user.getPlayer().removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
	}

	@Override
	public void attack() {
		if (chaa instanceof User) {
			((User) chaa).getMinecraft().title("", "§4You will going to be have dealt DAMAGE!", 5, 25, 5);
		}
	}
}
