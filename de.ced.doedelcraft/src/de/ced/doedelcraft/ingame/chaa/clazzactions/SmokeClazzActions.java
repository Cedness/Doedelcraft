package de.ced.doedelcraft.ingame.chaa.clazzactions;

import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.logic.vector.Vector3;

public class SmokeClazzActions extends AbstractClazzActions {

	public SmokeClazzActions(Char chaa) {
		super(chaa);
	}

	@Override
	public void tick(int tick, int cycle) {
		spawnArmParticles(tick, 10);
	}

	@Override
	public void spawnArmParticles(User viewer, Vector3 location) {
		// viewer.spawnParticle(Particle.SMOKE_NORMAL, location, 0);
		// FIXME make particle system
	}
}
