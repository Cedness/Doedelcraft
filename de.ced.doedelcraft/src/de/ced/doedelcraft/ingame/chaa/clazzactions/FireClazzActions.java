package de.ced.doedelcraft.ingame.chaa.clazzactions;

import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.logic.vector.Vector3;

public class FireClazzActions extends AbstractClazzActions {

	public FireClazzActions(Char chaa) {
		super(chaa);
	}

	@Override
	public void tick(int tick, int cycle) {
		spawnArmParticles(tick, 10);
	}

	@Override
	protected void spawnArmParticles(User viewer, Vector3 location) {
		// ((Player) viewer).spawnParticle(Particle.FLAME, location, 0);
		// FIXME make particle system
	}
}
