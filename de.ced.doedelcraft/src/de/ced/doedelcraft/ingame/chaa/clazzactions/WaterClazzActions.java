package de.ced.doedelcraft.ingame.chaa.clazzactions;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.logic.vector.Vector3;

public class WaterClazzActions extends AbstractClazzActions {

	public WaterClazzActions(Char chaa) {
		super(chaa);
	}

	@Override
	public void add() {
		Entity entity = chaa.getMinecraft().getEntity();
		if (!(entity instanceof LivingEntity)) {
			return;
		}
		((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, 1000000, 0, false, false, false));
	}

	@Override
	public void remove() {
		Entity entity = chaa.getMinecraft().getEntity();
		if (!(entity instanceof LivingEntity)) {
			return;
		}
		((LivingEntity) entity).removePotionEffect(PotionEffectType.WATER_BREATHING);
	}

	@Override
	public void enable() {
		Entity entity = chaa.getMinecraft().getEntity();
		if (!(entity instanceof LivingEntity)) {
			return;
		}
		((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.DOLPHINS_GRACE, 1000000, 0, false, false, false));
	}

	@Override
	public void disable() {
		Entity entity = chaa.getMinecraft().getEntity();
		if (!(entity instanceof LivingEntity)) {
			return;
		}
		((LivingEntity) entity).removePotionEffect(PotionEffectType.DOLPHINS_GRACE);
	}

	@Override
	public void tick(int tick, int cycle) {
		spawnArmParticles(tick, 10);
	}

	@Override
	public void spawnArmParticles(User viewer, Vector3 location) {
		// viewer.getMinecraft().spawnParticle(Particle.DRIP_WATER, location, 0);
		// FIXME make particle system
	}
}
