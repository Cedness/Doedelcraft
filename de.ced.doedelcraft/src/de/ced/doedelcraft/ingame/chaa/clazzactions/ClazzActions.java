package de.ced.doedelcraft.ingame.chaa.clazzactions;

import de.ced.doedelcraft.ingame.chaa.Unit;

public interface ClazzActions extends Unit {

	void add();

	void remove();

	void tick(int tick, int cycle);

	void setCombatMode(boolean enabled);

	void enable();

	void disable();

	void attack();
}
