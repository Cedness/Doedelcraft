package de.ced.doedelcraft.ingame.chaa;

import java.util.List;

import de.ced.doedelcraft.ingame.clazz.Clazz;

public interface Data extends Unit {

	String getName();

	Clazz getClazz();

	public int getLevel();

	public void setLevel(int level);

	public double getProgress();

	public void setProgress(double progress);

	public double getExperience();

	public void setExperience(double experience);

	public double getX();

	public void setX(double x);

	public double getY();

	public void setY(double y);

	public double getZ();

	public void setZ(double z);

	double getYaw();

	void setYaw(double yaw);

	double getPitch();

	void setPitch(double pitch);

	boolean isCombatMode();

	void setCombatMode(boolean combatMode);

	List<Integer> getSpellLevel();

	void setSpellLevel(List<Integer> spellLevel);

	List<Integer> getSelectedSpellLevel();

	void setSelectedSpellLevel(List<Integer> spellLevel);

	public List<List<Integer>> getSpellStrength();

	public void setSpellStrength(List<List<Integer>> spellStrength);

	public List<List<Integer>> getSelectedSpellStrength();

	public void setSelectedSpellStrength(List<List<Integer>> selectedSpellStrength);

	public List<List<Integer>> getSpellCooldown();

	public void setSpellCooldown(List<List<Integer>> spellCooldown);
}
