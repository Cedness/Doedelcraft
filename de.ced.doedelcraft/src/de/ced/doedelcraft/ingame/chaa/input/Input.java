package de.ced.doedelcraft.ingame.chaa.input;

import de.ced.doedelcraft.ingame.chaa.Unit;

public interface Input extends Unit {

	void onLeftClick();

	void onRightClick(boolean[] useItemInHand);

	void onFKey();

	void onQKey(boolean ctrl);

	void onNumKey(int key);

	void onInvNumClick(int slot, int key);

	void onScroll(boolean direction);

	void onShift(boolean shifting);

	boolean isShifting();
}
