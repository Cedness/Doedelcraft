package de.ced.doedelcraft.ingame.chaa.input;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Char;

public abstract class AbstractInput extends AbstractUnit implements Input {

	protected final Char chaa;

	public AbstractInput(Char chaa) {
		this.chaa = chaa;
	}
}
