package de.ced.doedelcraft.ingame.chaa.input;

import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.combat.Combat;
import de.ced.doedelcraft.ingame.chaa.spellinfo.SpellInfo;

public class GenuineUserInput extends AbstractInput implements UserInput, Tickable {

	private final User user;
	private boolean qKey = false;
	private boolean qKeyCtrl = false;
	private boolean shifting = false;

	public GenuineUserInput(User user) {
		super(user);
		this.user = user;
	}

	@Override
	public void tick(int tick, int cycle) {
		if (qKey) {
			qKey = false;
			boolean ctrl = qKeyCtrl;
			qKeyCtrl = false;
			SpellInfo spellInfo = user.getSpellInfo();
			if (!spellInfo.hasSelectedSpell()) {
				return;
			}
			Combat combat = user.getCombat();
			if (shifting) {
				combat.unselectSpell();
			} else {
				combat.nextSpellLevel(spellInfo.getSelectedSpellId(), ctrl);
			}
		}
	}

	@Override
	public void onLeftClick() {
		if (!user.getSpellInfo().hasSpells()) {
			user.getClazzActions().attack();
		}
	}

	@Override
	public void onRightClick(boolean[] useItemInHand) {
		useItemInHand[0] = !user.getSpellInfo().hasSpells();
	}

	@Override
	public void onFKey() {
		SpellInfo spellInfo = user.getSpellInfo();
		if (!spellInfo.hasSpells()) {
			return;
		}
		Combat combat = user.getCombat();
		if (shifting) {
			combat.toggleCombatMode(!combat.isCombatMode());
		} else {
			if (!spellInfo.hasSelectedSpell()) {
				return;
			}
			/*
			 * if (!spellInfo.isSpellUnlocked(spellId)) {
			 * combat.notUnlocked();
			 * return;
			 * }
			 */ // Should not be necessary since locked spells cant be selected
			combat.castSpell(spellInfo.getSpell(spellInfo.getSelectedSpellId()));
		}
	}

	@Override
	public void onQKey(boolean ctrl) {
		qKey = true;
		qKeyCtrl = ctrl;
	}

	@Override
	public void onNumKey(int key) {
		SpellInfo spellInfo = user.getSpellInfo();
		Combat combat = user.getCombat();
		if (shifting) {
			if (!user.getSpellInfo().hasSelectedSpell()) {
				return;
			}
			combat.adjustSpellStrength(key);
		} else {
			if (key >= spellInfo.getSpellCount()) {
				return;
			}
			int spellId = spellInfo.getSpellId(key);
			if (!spellInfo.isSpellUnlocked(spellId)) {
				combat.notUnlocked();
				return;
			}
			combat.selectSpell(spellInfo.getSpell(spellId));
		}
	}

	@Override
	public void onInvNumClick(int slot, int key) {
		SpellInfo spellInfo = user.getSpellInfo();
		int spellCount = spellInfo.getSpellCount();
		if (slot == key || slot >= spellCount || key >= spellCount) {
			return;
		}
		user.getSpellInfo().swapSlots(slot, key);
	}

	@Override
	public void onScroll(boolean direction) {
	}

	@Override
	public void onShift(boolean shifting) {
		this.shifting = shifting;
	}

	@Override
	public boolean isShifting() {
		return shifting;
	}
}
