package de.ced.doedelcraft.ingame.chaa;

import java.util.ArrayList;
import java.util.List;

import de.ced.doedelcraft.util.Logger;

public class AbstractUnitManager extends AbstractUnit implements UnitManager {

	@SuppressWarnings("unchecked")
	private final List<Tickable>[] units = new List[Tickable.getHighestCycle() + 1];
	{
		for (int cycle : supportedCycles()) {
			units[cycle] = new ArrayList<>();
		}
	}
	private final List<Addable> addUnits = new ArrayList<>();
	private final List<Removable> removeUnits = new ArrayList<>();

	@Override
	public Unit addUnit(Unit unit) {
		if (unit instanceof Tickable) {
			Tickable tickable = (Tickable) unit;
			int[] supportedCycles = tickable.supportedCycles();
			if (supportedCycles.length > 0) {
				for (int cycle : supportedCycles) {
					List<Tickable> cycleList = units[cycle];
					if (cycleList == null) {
						cycleList = units[supportedCycles()[0]];
					}
					cycleList.add(tickable);
				}
			}
		}
		if (unit instanceof Addable) {
			addUnits.add((Addable) unit);
		}
		if (unit instanceof Removable) {
			removeUnits.add((Removable) unit);
		}
		return unit;
	}

	@Override
	public void finishUnitAdding() {
		for (int cycle : supportedCycles()) {
			int cycleCopy = cycle;
			sort(units[cycle], (Unit unit) -> ((Tickable) unit).dependencies(cycleCopy));
		}
		sort(addUnits, (Unit unit) -> ((Addable) unit).addableDependencies());
		sort(removeUnits, (Unit unit) -> ((Removable) unit).removableDependencies());
	}

	private interface Dependencies {
		Class<?>[] get(Unit unit);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void sort(List list, Dependencies dependencies) {
		boolean goOn = true;
		int id = 0;
		int dependencyCountForSlot = 0;
		while (true) {
			if (dependencyCountForSlot > list.size()) {
				// This unit depends on a unit which depends directly or via another unit on
				// this unit.
				Logger.fatal("Cannot calc dependency order, ring-dependency detected");
				throw new RuntimeException("Cannot calc dependency order, ring-dependency detected");
			}
			if (goOn) {
				id++;
				dependencyCountForSlot = 0;
			}
			goOn = true;
			if (id >= list.size()) {
				break;
			}
			Unit unit = (Unit) list.get(id);
			dependencyCheck: for (Class<?> dependencyClass : dependencies.get(unit)) {
				int dependencyId = -1;
				for (int tempDependencyId = 0; tempDependencyId < list.size(); tempDependencyId++) {
					if (dependencyClass.isInstance(list.get(tempDependencyId))) {
						// Logger.info(list.get(tempDependencyId).getClass().getSimpleName() + "
						// assignable to " + dependencyClass.getSimpleName());
						if (dependencyId < 0) {
							dependencyId = tempDependencyId;
						} else {
							Logger.severe("Dependency " + dependencyClass.getSimpleName() + " applies to multiple units and is therefore ignored");
							continue dependencyCheck;
						}
					} else {
						// Logger.info(list.get(tempDependencyId).getClass().getSimpleName() + "
						// unassignable to " + dependencyClass.getSimpleName());
					}
				}
				if (dependencyId < 0) {
					Logger.severe("Dependency missing for " + unit.getClass().getSimpleName() + ": " + dependencyClass.getSimpleName());
				} else if (dependencyId > id) { // dependency behind this unit
					list.add(id, list.remove(dependencyId));
					goOn = false;
					dependencyCountForSlot++;
				} else if (dependencyId == id) { // dependency is this unit
					Logger.warning("Dependency " + unit.getClass().getSimpleName() + " applies to itself and is therefore ignored");
				}
			}
		}
	}

	@Override
	public void tick(int tick, int cycle) {
		for (Tickable unit : units[cycle]) {
			invoke("Tick", unit, () -> {
				unit.tick(tick, cycle);
			});
		}
	}

	@Override
	public int[] supportedCycles() {
		return new int[] { EARLY, DEFAULT, LATE, LATEST };
	}

	@Override
	public void add() {
		for (Addable unit : addUnits) {
			invoke("Add", unit, () -> {
				unit.add();
			});
		}
	}

	@Override
	public void remove() {
		for (int i = removeUnits.size() - 1; i >= 0; i--) {
			Removable unit = removeUnits.get(i);
			invoke("Remove", unit, () -> {
				unit.remove();
			});
		}
	}

	private void invoke(String actionName, Unit unit, Runnable runnable) {
		try {
			runnable.run();
		} catch (Exception ex) {
			try {
				Logger.severe(actionName + "-Exception in unit " + unit.getClass().getSimpleName() + " of unit-manager " + toString());
			} catch (Exception ex2) {
				Logger.severe("Exception while printing " + actionName + "-Exception");
			}
			ex.printStackTrace();
		}
	}
}
