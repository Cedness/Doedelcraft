package de.ced.doedelcraft.ingame.chaa.exhaustion;

import de.ced.doedelcraft.ingame.chaa.health.Health;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.stat.AbstractStat;

public abstract class AbstractExhaustion extends AbstractStat implements Exhaustion {

	protected int minecraft = 0;
	private int locked = 0;
	protected boolean jump;

	public AbstractExhaustion(Char chaa) {
		super(chaa, "exhaustion");
		max = 20;
		regen = 0.01;
		prepare();
	}

	@Override
	public Class<?>[] dependencies(int cycle) {
		return new Class<?>[] { Health.class };
	}

	@Override
	public void set(int amount) {
		current = Math.max(0, Math.min(amount, max - locked));
	}

	@Override
	public void setMax(int max) {
		super.setMax(max);
		locked = Math.min(locked, this.max);
	}

	@Override
	public int getLocked() {
		return locked;
	}

	@Override
	public void setLocked(int locked) {
		this.locked = Math.max(0, Math.min(locked, max));
		current = Math.min(current, max - this.locked);
	}

	@Override
	public boolean mayJump() {
		return jump;
	}

	@Override
	protected void apply() {
		int newFoodLevel = (int) (20 * (double) current / (double) max);
		minecraft = newFoodLevel;
		jump = newFoodLevel >= 3;
		chaa.getWalkspeed().setExhaustion(Math.min((double) newFoodLevel / (double) 5, 1));
	}

	public void onFoodLevelChange(int[] foodLevel) {
		if (minecraft != foodLevel[0]) {
			foodLevel[0] = (int) minecraft;
		}
	}
}
