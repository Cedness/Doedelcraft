package de.ced.doedelcraft.ingame.chaa.exhaustion;

import de.ced.doedelcraft.ingame.chaa.effects.UserEffects;
import de.ced.doedelcraft.ingame.chaa.User;

public class GenuineUserExhaustion extends AbstractExhaustion implements UserExhaustion {

	private final User user;

	public GenuineUserExhaustion(User user) {
		super(user);
		this.user = user;
	}

	@Override
	protected void apply() {
		super.apply();
		user.getUI().getExhaustionBar().setCurrent((double) current / max);
		// FIXME create potion effect api
		/*
		 * if (player.hasPotionEffect(PotionEffectType.JUMP)) {
		 * player.removePotionEffect(PotionEffectType.JUMP);
		 * player.removePotionEffect(PotionEffectType.JUMP);
		 * }
		 * if (!jump) {
		 * player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 1000000, 128,
		 * false, false, false));
		 * }
		 */
		UserEffects effects = user.getEffects();
		effects.setDarkSky(minecraft < 7);
		effects.setBlind(!jump);
	}
}
