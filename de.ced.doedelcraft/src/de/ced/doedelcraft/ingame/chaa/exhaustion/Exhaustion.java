package de.ced.doedelcraft.ingame.chaa.exhaustion;

import de.ced.doedelcraft.ingame.chaa.stat.Stat;

public interface Exhaustion extends Stat {

	int getLocked();

	void setLocked(int locked);

	boolean mayJump();
}
