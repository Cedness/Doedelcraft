package de.ced.doedelcraft.ingame.chaa;

import java.lang.reflect.InvocationTargetException;

import de.ced.doedelcraft.ingame.aoe.AbstractArea;
import de.ced.doedelcraft.ingame.aoe.Cylinder;
import de.ced.doedelcraft.ingame.aoe.Round;
import de.ced.doedelcraft.ingame.aoe.Sphere;
import de.ced.doedelcraft.ingame.chaa.damage.CombatType;
import de.ced.doedelcraft.ingame.chaa.damage.Defence;
import de.ced.doedelcraft.ingame.chaa.glow.UserGlow;
import de.ced.doedelcraft.ingame.chaa.minecraft.Minecraft;
import de.ced.doedelcraft.ingame.clazz.Spell;
import de.ced.doedelcraft.ingame.sound.SuperSound;
import de.ced.doedelcraft.ingame.sound.SuperSoundCategory;
import de.ced.doedelcraft.util.PartyMath;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import de.ced.logic.vector.Vector2;
import de.ced.logic.vector.Vector3;

public class CastedSpell {

	private final Char chaa;
	private final Spell spell;
	private final int startTick;
	private boolean afterlife = false;
	private boolean disable = false;
	private boolean remove = false;
	private int cooldown;
	private int strength;
	private int launchMana;
	private double constantMana;
	private int constantManaInterval;
	private int constantManaStartTick;
	private int disableMana;
	private int exhaustion;
	private AbstractArea areaOfEffect;
	private final SpellActions actions;

	public CastedSpell(Char chaa, Spell spell) {
		this.chaa = chaa;
		this.spell = spell;
		startTick = chaa.getCharManager().getTick();
		setCooldown(spell.getCooldown());
		setStrength(chaa.getSpellInfo().getSelectedSpellStrength(spell.getSpellId(), spell.getSpellLevel()));
		SpellActions spellActions = null;
		try {
			spellActions = (SpellActions) spell.getActionsClass().getConstructor(CastedSpell.class).newInstance(this);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		actions = spellActions;
	}

	public void add() {
		if (spell.isSwitchSpell() || spell.isRegistered()) {
			chaa.getCombat().getCastedSpells().add(this);
			if (spell.isInteractive()) {
				chaa.getCharManager().getCastedSpells().add(this);
			}
		}
	}

	public boolean shouldRemove() {
		return remove;
	}

	public void remove() {
		remove = true;
	}

	public Char getChar() {
		return chaa;
	}

	public Spell getSpell() {
		return spell;
	}

	public int getStartTick() {
		return startTick;
	}

	public boolean isAfterlife() {
		return afterlife;
	}

	public void afterlife() {
		afterlife = true;
	}

	public boolean shouldDisable() {
		return disable;
	}

	public void setDisable() {
		disable = true;
	}

	public int getCooldownTicks() {
		return cooldown;
	}

	public void setCooldownTicks(int cooldownTicks) {
		cooldown = cooldownTicks;
	}

	public int getCooldown() {
		return cooldown / 20;
	}

	public void setCooldown(int cooldown) {
		setCooldownTicks(cooldown * 20);
	}

	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
		exhaustion = spell.getExhaustion().getValue(strength);
		launchMana = spell.getLaunchMana().getValue(strength);
		setConstantMana(spell.getConstantMana().getValue(strength));
		disableMana = spell.getDisableMana().getValue(strength);
	}

	public int getLaunchMana() {
		return launchMana;
	}

	public void setLaunchMana(int launchMana) {
		this.launchMana = launchMana;
	}

	public double getConstantManaTicks() {
		return constantMana;
	}

	public void setConstantManaTicks(double constantManaTicks) {
		constantMana = constantManaTicks;
		constantManaInterval = (int) (1 / constantMana);
		constantManaStartTick = chaa.getCharManager().getTick() + 1;
	}

	public double getConstantMana() {
		return constantMana * 20;
	}

	public void setConstantMana(double constantMana) {
		setConstantManaTicks(constantMana / 20);
	}

	public int getConstantManaInterval() {
		return constantManaInterval;
	}

	public int getConstantManaStartTick() {
		return constantManaStartTick;
	}

	public int getDisableMana() {
		return disableMana;
	}

	public void setDisableMana(int disableMana) {
		this.disableMana = disableMana;
	}

	public int getExhaustion() {
		return exhaustion;
	}

	/**
	 * WARNING! To use this method, the spell HAS to be registered to remove the
	 * exhaustion correctly
	 */
	public void setExhaustion(int exhaustion) {
		this.exhaustion = exhaustion;
	}

	public boolean hasAreaOfEffect() {
		return areaOfEffect != null;
	}

	public AbstractArea getAreaOfEffect() {
		return areaOfEffect;
	}

	public void setAreaOfEffect(AbstractArea areaOfEffect) {
		this.areaOfEffect = areaOfEffect;
	}

	public SpellActions getActions() {
		return actions;
	}

	public abstract class SpellActions {

		/**
		 * Adjust mana and exhaustion requirements here
		 */
		public int prepare() {
			return 0;
		}

		/**
		 * Adjust mana and exhaustion requirements here
		 */
		public int prepareStrengthChange(int currentStrength, int newStrength) {
			return 0;
		}

		/**
		 * DON'T adjust mana and exhaustion requirements here
		 */
		public int launch() {
			return 0;
		}

		public int disable() {
			return 1;
		}

		public void tick(int tick) {
		}

		/**
		 * DON'T adjust mana and exhaustion requirements here
		 */
		public int changeStrength(int currentStrength, int newStrength) {
			return 0;
		}

		public Boolean adaptFrom(CastedSpell lastCastedSpell, boolean ctrl) {
			return null;
		}

		public void updateItem(ItemStack item) {
		}

		public void userTick(Char chaa) {
		}

		protected SuperSound getSpellSound(SuperSoundCategory soundCategory, String name) {
			return chaa.getCharManager().getSoundManager().getSound(soundCategory, "spell." + spell.getClazz().getName() + "." + spell.getName() + "." + name);
		}
	}

	// EARTH

	public class MiningSpellActions extends SpellActions {

	}

	public class FuneralSpellActions extends SpellActions {

	}

	public class SlowdownSpellActions extends SpellActions {

	}

	public class ClutchSpellActions extends SpellActions {

	}

	public class CaterpillarSpellActions extends SpellActions {

	}

	public class EarthSwirlSpellActions extends SpellActions {

	}

	public class EarthWallSpellActions extends SpellActions {

	}

	public class BunkerSpellActions extends SpellActions {

	}

	public class EarthquakeSpellActions extends SpellActions {

	}

	// AIR

	public class KnockBackSpellActions extends SpellActions {

	}

	public class PullSpellActions extends SpellActions {

	}

	public class MoveSpellActions extends SpellActions {

	}

	public class BoostMovementSpellActions extends SpellActions {

		private static final int MAX_SPEED = 30;
		private static final float SPEED_STEP = 0.01f;

		private int speed = 0;

		@Override
		public int launch() {
			applySpeed();
			return -1;
		}

		@Override
		public int disable() {
			applySpeed();
			return -1;
		}

		@Override
		public void tick(int tick) {
			if (tick % 2 != 0) {
				return;
			}

			int maxSpeed = MAX_SPEED + MAX_SPEED / 5 * getStrength() * 3;
			if (speed > maxSpeed) {
				speed = maxSpeed;
			}
			if (chaa.getMinecraft().getLocation().equals(chaa.getLastTick().getLastLocation().location())) {
				speed -= 2;
				if (speed < 0) {
					speed = 0;
				}
			} else {
				if (speed < maxSpeed) {
					speed++;
				}
			}
			applySpeed();
		}

		private void applySpeed() {
			chaa.getWalkspeed().setBoostMovement(1 + speed * SPEED_STEP);
		}

		@Override
		public void updateItem(ItemStack item) {
			super.updateItem(item);
			ItemMeta meta = item.getItemMeta();
			if (!(meta instanceof LeatherArmorMeta)) {
				return;
			}
			LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) meta;
			leatherArmorMeta.setColor(Color.fromRGB(128, 255, 255));
			item.setItemMeta(leatherArmorMeta);
		}
	}

	public class BreakFallSpellActions extends SpellActions {

		@Override
		public int prepare() {
			Minecraft minecraft = chaa.getMinecraft();
			if (minecraft.isOnGround() || minecraft.getFallDistance() < 3) {
				setCooldown(getCooldown() / 2);
				setLaunchMana(getLaunchMana() / 2);
				return 1;
			}
			return 0;
		}

		@Override
		public int launch() {
			Minecraft minecraft = chaa.getMinecraft();
			minecraft.setFallDistance(0);
			minecraft.getVelocity().mul(-0.1);
			minecraft.applyVelocity();
			return 0;
		}
	}

	public class LevitateSpellActions extends SpellActions {

		@Override
		public int launch() {
			setLevitation();
			return 0;
		}

		@Override
		public int disable() {
			removeLevitation();
			return 0;
		}

		@Override
		public void tick(int tick) {
			if (chaa.getMinecraft().isOnGround() && chaa.getLastTick().hasOnGroundChanged()) {
				setDisable();
			}
		}

		@Override
		public int changeStrength(int currentStrength, int newStrength) {
			removeLevitation();
			setLevitation();
			return 0;
		}

		/*
		 * @Override
		 * public double getConstantMana(int multiplier) { // TODO
		 * return super.getConstantMana(multiplier > 1 ? multiplier - 1 : 0);
		 * }
		 */

		private void setLevitation() {
			int strength = getStrength();
			int amplifier = strength > 2 ? 2 * (strength - 3) + 1 : 255 + 2 * (strength - 2);
			if (chaa.getMinecraft().isLiving()) {
				((LivingEntity) chaa.getMinecraft().getEntity()).addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, 1000000, amplifier, false, false, false));
			}
		}

		private void removeLevitation() {
			if (chaa.getMinecraft().isLiving()) {
				((LivingEntity) chaa.getMinecraft().getEntity()).removePotionEffect(PotionEffectType.LEVITATION);
			}
		}
	}

	public class HardAirSpellActions extends SpellActions {

		@Override
		public int launch() {
			if (chaa instanceof User) {
				User user = (User) chaa;
				user.getEffects().setBlood(50);
				Sphere sphere = new Sphere();
				sphere.setLocation(user.getMinecraft().getLocation());
				sphere.setRadius(3);
				sphere.resized();
				setAreaOfEffect(sphere);
			}
			return 0;
		}

		@Override
		public int disable() {
			if (chaa instanceof User) {
				((User) chaa).getEffects().setBlood(0);
			}
			return 0;
		}
	}

	public class StormSpellActions extends SpellActions {

		@Override
		public int launch() {
			Cylinder cylinder = new Cylinder();
			cylinder.getLocation().set(chaa.getMinecraft().getLocation().location()).addY(-5);
			cylinder.setHeight(70);
			cylinder.setRadius(15);
			setAreaOfEffect(cylinder);
			return 0;
		}

		@Override
		public void tick(int tick) { // TODO beauty me up (easy)
			Vector3 location = getAreaOfEffect().getLocation().clone();
			location.addY(5);
			Vector3 distance = chaa.getMinecraft().getLocation().add(Vector3.empty(), location.invert());
			double length = distance.length();
			if (length > 0.001) {
				if (length > 0.1) {
					distance.mul(0.1 / length);
				}
				location.add(distance);
				location.addY(-5);
				getAreaOfEffect().setLocation(location);
			}
			// Logger.info(Arrays.toString(location));

			World world = chaa.getCharManager().getDoedelcraft().getWorldManager().getWorld();
			Round round = (Cylinder) getAreaOfEffect();
			double radius = round.getRadius();
			double intensity = 0.1;
			for (int i = 0; i < (int) (intensity * radius * radius) + 1; i++) {
				Vector3 xyz = Vector3.from(location);
				int attempt = 0;
				do {
					xyz.setX(location.x() + 2 * radius * (Math.random() - 0.5));
					xyz.setZ(location.z() + 2 * radius * (Math.random() - 0.5));
					attempt++;
				} while (!round.intersects(xyz, true) && attempt < 10);
				if (attempt >= 10) {
					return;
				}
				xyz.setY(world.getHighestBlockYAt((int) xyz.x(), (int) xyz.z())
						+ 30 * Math.abs(Math.random() + Math.random() - 1));
				Particle particle;
				double particleRandom = Math.random();
				if (particleRandom < 0.1) {
					particle = Particle.SWEEP_ATTACK;
				} else if (particleRandom < 0.2) {
					particle = Particle.CRIT;
				} else if (particleRandom < 0.5) {
					particle = Particle.CLOUD;
				} else if (particleRandom < 0.7) {
					particle = Particle.CAMPFIRE_COSY_SMOKE;
				} else if (particleRandom < 0.99) {
					particle = Particle.CAMPFIRE_SIGNAL_SMOKE;
				} else {
					particle = Particle.EXPLOSION_NORMAL;
				}
				world.spawnParticle(particle, xyz.x(), xyz.y(), xyz.z(), 1, 0, 0, 0, Math.random());
			}
		}

		@Override
		public void userTick(Char chaa) {
			Round areaOfEffect = (Round) getAreaOfEffect();
			Minecraft minecraft = chaa.getMinecraft();
			if (CastedSpell.this.chaa == chaa || !areaOfEffect.intersects(minecraft.getLocation(), true)) {
				return;
			}
			double radius = areaOfEffect.getRadius();
			double distance = 1 - areaOfEffect.getLastSquaredDistance() / (radius * radius);
			double random = Math.random();
			if (random < 0.1 * distance) {
				random = Math.random();
				Defence defence = chaa.getDefence();
				defence.damage(random < 0.15 * distance ? 2 : 1, CombatType.KINETIC);
				defence.setLastDamager(CastedSpell.this);
			}
			random = Math.random();
			if (random < 0.15 * distance) {
				minecraft.addVelocity(Vector3.from(Math.random() - 0.5, (Math.random() - 0.5) * 0.5, Math.random() - 0.5).mul(0.5));
			}
		}
	}

	public class TornadoSpellActions extends SpellActions {

	}

	public class LightningSpellActions extends SpellActions {

		@Override
		public int prepare() {
			boolean stormActive = chaa.getSpellInfo().getSpellState(4);
			if (!stormActive) {
				setLaunchMana(0);
				setExhaustion(0);
				setCooldown(10);
				return 0;
			}
			return 1;
		}
	}

	// FIRE

	public class FireballSpellActions extends SpellActions {

	}

	public class FireBombSpellActions extends SpellActions {

	}

	public class FireBlastSpellActions extends SpellActions {

	}

	public class FireShieldSpellActions extends SpellActions {

		private static final double RADIUS = 1;
		private static final double HEIGHT = 0.1;
		private static final double HEIGHT_STEP = 0.5;
		private static final double RANDOM_OFFSET = 0.3;

		private int progress = 0;

		@Override
		public void tick(int tick) {
			if (tick % 4 != 0) {
				return;
			}
			progress = (progress + 1) % 12;

			World world = chaa.getCharManager().getDoedelcraft().getWorldManager().getWorld();
			Vector3 location = chaa.getMinecraft().getLocation();

			for (int i = 0; i < 12; i++) {
				for (int k = 0; k < 2; k++) {
					double percent = i / 12.0 + k * (1 / 24.0);
					Vector2 offset = PartyMath.getCircleXY(percent, RADIUS);
					double height = HEIGHT + HEIGHT_STEP * (progress / 24.0) + k * (HEIGHT_STEP / 2.0);

					for (int j = 0; j < 3; j++) {
						Vector3 randoms = PartyMath.getRandoms(RANDOM_OFFSET);
						randoms.add(location).add(offset.x(), height, offset.y());
						world.spawnParticle(
								Particle.FLAME,
								randoms.x(),
								randoms.y(),
								randoms.z(),
								0
						);

						height += HEIGHT_STEP;
					}
				}
			}
		}
	}

	public class FireSwirlSpellActions extends SpellActions {

	}

	public class LavaDitchSpellActions extends SpellActions {

	}

	public class LavaCircleSpellActions extends SpellActions {

	}

	public class FireWhipSpellActions extends SpellActions {

	}

	public class FirePillarSpellActions extends SpellActions {

	}

	public class InnerFireSpellActions extends SpellActions {

	}

	// WATER

	public class WaterBeamSpellActions extends SpellActions {

	}

	public class WaterWhipSpellActions extends SpellActions {

	}

	public class IceBeamSpellActions extends SpellActions {

	}

	public class WaterShieldSpellActions extends SpellActions {

	}

	public class IcyShieldSpellActions extends SpellActions {

	}

	public class IceBlockSpellActions extends SpellActions {
		// 0.025

		private final String[] messages = {
				"Freeze yourself",
				"in the ice",
				"block the mo-",
				"ment you own",
				"it you better",
				"never let it",
				"melt.",
				"You only got",
				"one block,",
				"do not miss",
				"your chance to",
				"freeze, this oppor-",
				"tunity comes",
				"once until the",
				"cooldown elapses.",
				" "
		};

		@Override
		public void tick(int tick) {
			if (!(chaa instanceof User)) {
				return;
			}
			User user = (User) chaa;
			int i = tick - getStartTick();
			if (i % 15 != 0) {
				return;
			}
			i /= 15;
			if (i <= 0 || i > messages.length) {
				return;
			}
			String message = "§e" + messages[i - 1];
			user.getUI().getBottomLabel().setItemText(0.75, message);
		}
	}

	public class WaveSpellActions extends SpellActions {

		@Override
		public void updateItem(ItemStack item) {
			super.updateItem(item);
			ItemMeta meta = item.getItemMeta();
			if (!(meta instanceof PotionMeta)) {
				return;
			}
			PotionMeta potionMeta = (PotionMeta) meta;
			potionMeta.setColor(Color.BLUE);
			item.setItemMeta(potionMeta);
		}
	}

	public class IceWaveSpellActions extends SpellActions {

		@Override
		public void updateItem(ItemStack item) {
			super.updateItem(item);
			ItemMeta meta = item.getItemMeta();
			if (!(meta instanceof PotionMeta)) {
				return;
			}
			PotionMeta potionMeta = (PotionMeta) meta;
			potionMeta.setColor(Color.AQUA);
			item.setItemMeta(potionMeta);
		}
	}

	public class FloodSpellActions extends SpellActions {

	}

	public class WaterSphereSpellActions extends SpellActions {

	}

	public class IcySphereSpellActions extends SpellActions {

	}

	public class BloodstopSpellActions extends SpellActions {

		@Override
		public void updateItem(ItemStack item) {
			super.updateItem(item);
			ItemMeta meta = item.getItemMeta();
			if (!(meta instanceof PotionMeta)) {
				return;
			}
			PotionMeta potionMeta = (PotionMeta) meta;
			potionMeta.setColor(Color.RED);
			item.setItemMeta(potionMeta);
		}
	}

	// LIGHTNING

	public class SparkSpellActions extends SpellActions {

	}

	public class PulseLightningSpellActions extends SpellActions {

	}

	public class SparkArmorSpellActions extends SpellActions {

	}

	public class WebLightningSpellActions extends SpellActions {

	}

	public class ThunderboltSpellActions extends SpellActions {

	}

	public class BrainSparkSpellActions extends SpellActions {

	}

	// RESTRICTION

	public class ParrySpellActions extends SpellActions {

	}

	public class TargeSpellActions extends SpellActions {

	}

	public class BodyShieldSpellActions extends SpellActions {

	}

	public class SteadyShieldSpellActions extends SpellActions {

	}

	public class PrisonSpellActions extends SpellActions {

	}

	public class RunwaySpellActions extends SpellActions {

	}

	public class StairsSpellActions extends SpellActions {

	}

	// SMOKE

	public class SmokeRaySpellActions extends SpellActions {

	}

	public class SmokeShieldSpellActions extends SpellActions {

	}

	public class SmokeNetSpellActions extends SpellActions {

	}

	public class DodgeSpellActions extends SpellActions {

	}

	public class DeflectionSpellActions extends SpellActions {

	}

	public class SmokeBlowSpellActions extends SpellActions {

	}

	public class CursedFogSpellActions extends SpellActions {

	}

	public class NoSpellSpellActions extends SpellActions {

	}

	// LIGHT

	public class LightBeamSpellActions extends SpellActions {

	}

	public class PlasmaBeamSpellActions extends SpellActions {

	}

	public class LightBallSpellActions extends SpellActions {

	}

	public class PlasmaBallSpellActions extends SpellActions {

	}

	public class PlasmaBombSpellActions extends SpellActions {

	}

	public class PlasmaArmorSpellActions extends SpellActions {

	}

	public class PlasmaSwirlSpellActions extends SpellActions {

	}

	public class PlasmaShieldSpellActions extends SpellActions {

	}

	public class SpylightSpellActions extends SpellActions {

		private final int[] DISTANCES = { 5, 11, 18, 35, 72 };

		@Override
		public int disable() {
			if (chaa instanceof User) {
				((User) chaa).getGlow().clear();
			}
			return 0;
		}

		@Override
		public void tick(int tick) {
			if ((tick - getStartTick()) % 7 != 1) {
				return;
			}
			if (!(chaa instanceof User)) {
				return;
			}
			double distance = DISTANCES[getStrength()];
			distance *= distance;
			Vector3 location = chaa.getMinecraft().getLocation();
			UserGlow glow = ((User) chaa).getGlow();
			for (Char anotherChar : chaa.getCharManager().getChars()) { // TODO make use of chunks
				if (chaa.equals(anotherChar)) {
					continue;
				}
				boolean inRange = location.distanceSquared(anotherChar.getMinecraft().getLocation()) < distance;
				if (glow.setGlowing(anotherChar, inRange)) {
					chaa.getEffects().play(getSpellSound(SuperSoundCategory.SPELLINFO, inRange
							? "intrude"
							: "leave"));
				}
			}
		}
	}

	public class TwilightSpellActions extends SpellActions {

	}

	public class GloomSpellActions extends SpellActions {

	}

	public class SilenceSpellActions extends SpellActions {

	}

	public class PlasmaNooseSpellActions extends SpellActions {

	}
}
