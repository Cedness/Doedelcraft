package de.ced.doedelcraft.ingame.chaa.ui.bar;

import de.ced.doedelcraft.ingame.ease.OutSinEaseFunction;
import de.ced.doedelcraft.ingame.chaa.ui.UserUI;

public class ManaBar extends AbstractFloatingBar {

	public ManaBar(UserUI ui) {
		super(ui, 0, 100, false, 10, new OutSinEaseFunction());
	}

	@Override
	protected void change() {
		ui.getUser().getMinecraft().setExperienceProgress((float) getCurrent() / max);
	}
}
