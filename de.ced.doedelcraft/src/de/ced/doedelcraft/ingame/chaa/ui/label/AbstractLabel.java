package de.ced.doedelcraft.ingame.chaa.ui.label;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.ui.UserUI;

public abstract class AbstractLabel extends AbstractUnit implements Label {

	protected final UserUI ui;
	protected final User user;

	public AbstractLabel(UserUI ui) {
		this.ui = ui;
		user = ui.getUser();
	}
}
