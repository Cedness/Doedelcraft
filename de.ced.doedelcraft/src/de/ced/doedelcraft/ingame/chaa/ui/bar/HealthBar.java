package de.ced.doedelcraft.ingame.chaa.ui.bar;

import de.ced.doedelcraft.ingame.ease.LinearEaseFunction;
import de.ced.doedelcraft.util.Logger;
import de.ced.doedelcraft.ingame.chaa.minecraft.UserMinecraft;
import de.ced.doedelcraft.ingame.chaa.ui.UserUI;

public class HealthBar extends AbstractFloatingBar {

	public HealthBar(UserUI ui) {
		super(ui, 1, 20, true, 5, new LinearEaseFunction());
	}

	@Override
	protected void initialChange() {
		UserMinecraft minecraft = user.getMinecraft();
		if (user.getClazz().feelsDamage()) {
			if (!getDirection()) {
				minecraft.damageAnimation();
			}
		} else {
			minecraft.setHealth(max);
		}
	}

	@Override
	protected void change() {
		if (!user.getClazz().feelsDamage()) {
			return;
		}
		user.getMinecraft().setHealth(getCurrent());
		Logger.info("health: " + getCurrent());
		// int oldHealth = minecraft.getHealth();
		// if (oldHealth != getLast()) {
		// Logger.warning("Health out of sync");
		// }
		// int healthDifference = newHealth - oldHealth;
		// Logger.info("healthChange " + healthDifference);
		// if (healthDifference >= 0) {
		// minecraft.setHealth(newHealth, true);
		// }
		// if (healthDifference < 0) {
		// minecraft.damage(-healthDifference);
		// minecraft.damage(0);
		// }
	}
}
