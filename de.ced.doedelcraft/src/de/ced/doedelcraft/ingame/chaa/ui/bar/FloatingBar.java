package de.ced.doedelcraft.ingame.chaa.ui.bar;

public interface FloatingBar extends Bar {

    boolean isFloating();

    void setFloating(boolean floating);
}
