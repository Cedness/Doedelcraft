package de.ced.doedelcraft.ingame.chaa.ui.label;

import de.ced.doedelcraft.account.Dictionary;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.chaa.UserData;
import de.ced.doedelcraft.ingame.chaa.ui.UserUI;
import de.ced.doedelcraft.ingame.clazz.Clazz;

public class TopLabel extends AbstractLabel implements Tickable {

	private final String COMBATMODE;
	private final String CONNECTOR = "§7          ";

	private final String prefix, midfix, suffix;
	private String combatmode1 = "", combatmode2 = "", agecolor = "", age = "";

	public TopLabel(UserUI ui) {
		super(ui);
		Dictionary dictionary = user.getLanguage().getD("generic");
		COMBATMODE = "§7|§n§o " + dictionary.get("combatmode") + " §7|";
		prefix = "§7§l" + user.getIdentity().getName() + " §8- " + dictionary.get("age") + ": ";
		midfix = "§l";
		Clazz clazz = user.getClazz();
		suffix = "§8 - " + dictionary.get("ability") + ": " + clazz.getColor() + "§l" + dictionary.get(clazz.getName());

		setCombatMode(user.getCombat().isCombatMode());
	}

	@Override
	public void tick(int tick, int cycle) {
		displayText();
	}

	public void setAge(int age) {
		agecolor = UserData.getLevelColor(age);
		this.age = String.valueOf(age);
	}

	public void setCombatMode(boolean combatMode) {
		if (combatMode) {
			combatmode1 = COMBATMODE + CONNECTOR;
			combatmode2 = CONNECTOR + COMBATMODE;
		} else {
			combatmode1 = "";
			combatmode2 = "";
		}
	}

	private void displayText() {
		ui.getUser().getMinecraft().setBossbarText(combatmode1 + prefix + agecolor + midfix + age + suffix + combatmode2);
	}
}
