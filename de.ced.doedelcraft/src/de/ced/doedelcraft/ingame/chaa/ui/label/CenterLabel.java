package de.ced.doedelcraft.ingame.chaa.ui.label;

import de.ced.doedelcraft.ingame.chaa.ui.UserUI;

public class CenterLabel extends AbstractLabel {

	/**
	 * Dependencies: Minecraft, Identity
	 */
	public CenterLabel(UserUI ui) {
		super(ui);
		user.getMinecraft().title("", "§a" + user.getLanguage().getD("mainmenu").getD("selection").get("login", "'§e" + user.getIdentity().getName() + "§a'"), 10, 30, 10);
	}

	public void manaUsed(int mana) {
		user.getMinecraft().title("", "§a-" + mana + " " + user.getClazz().getManaName(user.getLanguage()), 2, (int) (mana * 0.05), 10);
	}
}
