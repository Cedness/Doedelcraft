package de.ced.doedelcraft.ingame.chaa.ui.label;

import java.util.ArrayList;
import java.util.List;

import de.ced.doedelcraft.ingame.clazz.Spell;
import de.ced.doedelcraft.util.Logger;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import de.ced.doedelcraft.account.Dictionary;
import de.ced.doedelcraft.ingame.chaa.Removable;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.chaa.spellinfo.SpellInfo;
import de.ced.doedelcraft.ingame.chaa.ui.UserUI;

public class SideLabel extends AbstractLabel implements Tickable, Removable {

	private final static char ARROW_UP = '\u2191';

	private final Dictionary dictionary;
	private boolean sidebarState;
	private final Scoreboard scoreboard;
	@SuppressWarnings("unused")
	private final Team team;
	private final Objective controlObjective;
	private boolean showControlObjective;
	private Objective spellObjective;
	private Objective currentObjective;
	private Spell displayedSpell;
	private int setTick = Integer.MIN_VALUE;

	public SideLabel(UserUI ui) {
		super(ui);
		sidebarState = user.getData().getSidebarState();

		scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		user.getMinecraft().setScoreboard(scoreboard);
		team = scoreboard.registerNewTeam("team");

		dictionary = user.getLanguage().getD("ui").getD("sidelabel");
		Dictionary d = dictionary.getD("controls");
		List<String> text = List.of(
				"§8§l" + d.get("title"),
				"§0----------------",
				"§8[§6§l1§8-§6§l6§8]   §7" + d.get("select_spell"),
				"§8[§6§lF§8]      §7" + d.get("launch"),
				"§8[§6§lQ§8]      §7" + d.get("next_form"),
				"§6ctrl§8[§6§lQ§8] §7" + d.get("last_form"),
				"§6" + ARROW_UP + "§8[§6§l1§8-§6§l6§8]  §7" + d.get("select_strength"),
				"§6" + ARROW_UP + "§8[§6§lQ§8]     §7" + d.get("unselect_spell"),
				"§6" + ARROW_UP + "§8[§6§lF§8]     §7" + d.get("toggle_combat_mode"),
				"§f§0----------------");
		controlObjective = buildObjective("control", text);

		showControlObjective = user.getData().getTutorialMode();
	}

	@Override
	public void remove() {
		user.getMinecraft().setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
	}

	@Override
	public void tick(int tick, int cycle) {
		if (tick == setTick) {
			displaySpell(null);
		}
	}

	public boolean getSidebarState() {
		return sidebarState;
	}

	public void setSidebarState(boolean sidebarState) {
		if (this.sidebarState == sidebarState) {
			return;
		}
		this.sidebarState = sidebarState;
		ui.getUser().getData().setSidebarState(sidebarState);
		updateSidebarState();
	}

	public boolean getShowControlObjective() {
		return showControlObjective;
	}

	public void setShowControlObjective(boolean showControlObjective) {
		this.showControlObjective = showControlObjective;
		updateSidebarState();
	}

	private void updateSidebarState() {
		currentObjective = null;
		if (sidebarState) {
			if (spellObjective != null) {
				currentObjective = spellObjective;
			} else if (showControlObjective) {
				currentObjective = controlObjective;
			}
		}
		display(currentObjective);
	}

	private void display(Objective objective) {
		if (objective != null) {
			objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		} else {
			scoreboard.clearSlot(DisplaySlot.SIDEBAR);
		}
	}

	public Spell getDisplayedSpell() {
		return displayedSpell;
	}

	public void displaySpell(Spell spell) {
		displaySpell(spell, true);
	}

	public void displaySpell(Spell spell, boolean delay) {
		scoreboard.clearSlot(DisplaySlot.SIDEBAR);
		if (spellObjective != null) {
			spellObjective.unregister();
		}
		if (spell == null) {
			currentObjective = controlObjective;
			spellObjective = null;
		} else {
			displayedSpell = spell;
			SpellInfo spellInfo = user.getSpellInfo();
			int spellId = spell.getSpellId();
			int spellLevel = spell.getSpellLevel();

			boolean hasState = spell.isSwitchSpell();
			int selectedLevel = spellInfo.getSelectedSpellLevel(spellId) + 1;
			int unlockedLevel = spellInfo.getSpellLevel(spellId) + 1;
			boolean hasUnlockedLevel = unlockedLevel > 1;
			int maxLevel = spell.getClazz().getSpells(spellId).size();
			boolean hasMaxLevel = maxLevel > 1;
			int selectedStrength = spellInfo.getSelectedSpellStrength(spellId, spellLevel) + 1;
			int unlockedStrength = spellInfo.getSpellStrength(spellId, spellLevel) + 1;
			boolean hasUnlockedStrength = unlockedStrength > 1;
			int maxStrength = spell.getMaxStrength();
			boolean hasMaxStrength = maxStrength > 1;

			Dictionary d = dictionary.getD("spell");
			List<String> spellEntries = new ArrayList<>();
			spellEntries.add(spell.getColor() + "§l" + user.getLanguage().getD("spell").getD(spell.getClazz().getName()).getD(spell.getName()).get("name"));
			if (hasState) {
				spellEntries.add("");
				spellEntries.add(spellInfo.getSpellState(spell) ? "§7§l---§a§l" + d.get("state_active") + "§7§l---" : "§7§l--§c§l" + d.get("state_inactive") + "§7§l--");
			}
			String form = d.get("form");
			String strength = d.get("strength");
			if (hasUnlockedLevel || hasUnlockedStrength) {
				spellEntries.add(" ");
				spellEntries.add("§7§l--" + d.get("selected") + "--");
				if (hasUnlockedLevel) {
					spellEntries.add("§7" + form + ": §o" + selectedLevel + "§7/§o" + unlockedLevel);
				}
				if (hasUnlockedStrength) {
					spellEntries.add("§7" + strength + ": §o" + selectedStrength + "§7/§o" + unlockedStrength);
				}
			}
			if (hasMaxLevel || hasMaxStrength) {
				spellEntries.add("  ");
				spellEntries.add("§8§l--" + d.get("unlocked") + "--");
				if (hasMaxLevel) {
					spellEntries.add("§8" + form + ": §o" + unlockedLevel + "§8/§o" + maxLevel);
				}
				if (hasMaxStrength) {
					spellEntries.add("§8" + strength + ": §o" + unlockedStrength + "§8/§o" + maxStrength);
				}
			}
			spellEntries.add("   ");

			spellObjective = buildObjective("spell", spellEntries);

			setTick = getTick() + (delay ? 150 : 0);
			currentObjective = spellObjective;
		}
		updateSidebarState();
	}

	private Objective buildObjective(String name, List<String> text) {
		Objective objcetive = scoreboard.registerNewObjective(name, "dummy", ".");
		int length = text.size();
		if (length < 1) {
			return objcetive;
		}
		objcetive.setDisplayName(text.get(0));
		for (int i = 1; i < length; i++) {
			String line = text.get(i);
			if (line.length() > 40) {
				Logger.warning("Sidelabel-line \"" + line + "\" is " + (line.length() - 40) + " letters too long, shortening...");
				line = line.substring(0, 40);
			}
			objcetive.getScore(line).setScore(length - 1 - i);
		}
		return objcetive;
	}
}
