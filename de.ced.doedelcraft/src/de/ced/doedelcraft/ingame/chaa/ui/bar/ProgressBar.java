package de.ced.doedelcraft.ingame.chaa.ui.bar;

import de.ced.doedelcraft.ingame.ease.OutSinEaseFunction;
import de.ced.doedelcraft.ingame.chaa.minecraft.UserMinecraft;
import de.ced.doedelcraft.ingame.chaa.ui.UserUI;

public class ProgressBar extends AbstractFloatingBar {

	public ProgressBar(UserUI ui) {
		super(ui, 0, 100, false, 30, new OutSinEaseFunction());
	}

	@Override
	protected void change() {
		ui.getUser().getMinecraft().setBossHealth((double) getCurrent() / max);
	}

	@Override
	protected void applyDirection() {
		ui.getUser().getMinecraft().setBossColor(isRising() ? UserMinecraft.BOSS_COLOR_YELLOW : UserMinecraft.BOSS_COLOR_RED);
	}
}
