package de.ced.doedelcraft.ingame.chaa.ui;

import de.ced.doedelcraft.ingame.chaa.UnitManager;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.ui.bar.ExhaustionBar;
import de.ced.doedelcraft.ingame.chaa.ui.bar.HealthBar;
import de.ced.doedelcraft.ingame.chaa.ui.bar.ManaBar;
import de.ced.doedelcraft.ingame.chaa.ui.bar.ProgressBar;
import de.ced.doedelcraft.ingame.chaa.ui.bar.ShieldBar1;
import de.ced.doedelcraft.ingame.chaa.ui.bar.ShieldBar2;
import de.ced.doedelcraft.ingame.chaa.ui.label.BottomLabel;
import de.ced.doedelcraft.ingame.chaa.ui.label.CenterLabel;
import de.ced.doedelcraft.ingame.chaa.ui.label.SideLabel;
import de.ced.doedelcraft.ingame.chaa.ui.label.TabLabel;
import de.ced.doedelcraft.ingame.chaa.ui.label.TopLabel;

public interface UserUI extends UnitManager {

	User getUser();

	boolean isActive();

	void setActive(boolean active);

	boolean isAnimatedbars();

	void setAnimatedbars(boolean animatedbars);

	HealthBar getHealthBar();

	ExhaustionBar getExhaustionBar();

	ManaBar getManaBar();

	ProgressBar getProgressBar();

	ShieldBar1 getShieldBar1();

	ShieldBar2 getShieldBar2();

	TabLabel getTabLabel();

	TopLabel getTopLabel();

	SideLabel getSideLabel();

	BottomLabel getBottomLabel();

	CenterLabel getCenterLabel();
}
