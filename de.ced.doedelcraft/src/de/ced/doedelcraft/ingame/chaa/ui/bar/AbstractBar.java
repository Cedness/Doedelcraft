package de.ced.doedelcraft.ingame.chaa.ui.bar;

import de.ced.doedelcraft.ingame.chaa.ui.UserUI;
import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.chaa.User;

public abstract class AbstractBar extends AbstractUnit implements Bar, Tickable {

	protected final UserUI ui;
	protected final User user;
	protected final int min, max;
	private int current, last;
	private boolean direction;

	public AbstractBar(UserUI ui, int min, int max, boolean initMax) {
		this.ui = ui;
		user = ui.getUser();
		this.min = min;
		this.max = max;
		current = initMax ? max : min;
		last = initMax ? max + 1 : min - 1;
	}

	@Override
	public void tick(int tick, int cycle) {
		if (last == current) {
			return;
		}
		if (direction != current > last) {
			direction = !direction;
			applyDirection();
		}
		apply();
		last = current;
	}

	protected int getCurrent() {
		return current;
	}

	protected int getLast() {
		return last;
	}

	public boolean isRising() {
		return direction;
	}

	@Override
	public void setCurrent(double percent) {
		setCurrent((int) (percent * max));
	}

	@Override
	public void setCurrent(int current) {
		current = Math.max(min, Math.min(current, max));
		if (this.current == current) {
			return;
		}
		this.current = current;
	}

	protected abstract void apply();

	protected void applyDirection() {
	}
}
