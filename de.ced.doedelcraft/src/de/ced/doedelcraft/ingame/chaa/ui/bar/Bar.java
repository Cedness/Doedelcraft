package de.ced.doedelcraft.ingame.chaa.ui.bar;

import de.ced.doedelcraft.ingame.chaa.Unit;

public interface Bar extends Unit {

	/**
	 * Sets the value hold by this bar. Can be updated as often as needed per tick
	 * since the display is only changed once at the end of a tick.
	 */
	void setCurrent(double percent);

	void setCurrent(int current);
}
