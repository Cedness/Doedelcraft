package de.ced.doedelcraft.ingame.chaa.ui.bar;

import de.ced.doedelcraft.ingame.chaa.ui.UserUI;
import de.ced.doedelcraft.ingame.ease.LinearEaseFunction;

public class ExhaustionBar extends AbstractFloatingBar {

	public ExhaustionBar(UserUI ui) {
		super(ui, 0, 20, true, 5, new LinearEaseFunction());
	}

	@Override
	protected void change() {
		ui.getUser().getMinecraft().setHunger(getCurrent());
	}
}
