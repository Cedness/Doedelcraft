package de.ced.doedelcraft.ingame.chaa.ui.bar;

import static org.bukkit.Material.*;

import de.ced.doedelcraft.ingame.ease.LinearEaseFunction;
import de.ced.doedelcraft.util.SadItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import de.ced.doedelcraft.ingame.chaa.ui.UserUI;

public class ShieldBar2 extends AbstractFloatingBar {

	private static final Material[][] ARMOR = {
			{ null, null, null, null }, // 0
			{ GOLDEN_BOOTS, null, null, null }, // 1
			{ null, null, null, GOLDEN_HELMET }, // 2
			{ null, GOLDEN_LEGGINGS, null, null }, // 3
			{ GOLDEN_BOOTS, GOLDEN_LEGGINGS, null, null }, // 4
			{ null, null, GOLDEN_CHESTPLATE, null }, // 5
			{ GOLDEN_BOOTS, GOLDEN_LEGGINGS, null, GOLDEN_HELMET }, // 6
			{ null, null, GOLDEN_CHESTPLATE, GOLDEN_HELMET }, // 7
			{ null, GOLDEN_LEGGINGS, GOLDEN_CHESTPLATE, null }, // 8
			{ GOLDEN_BOOTS, GOLDEN_LEGGINGS, GOLDEN_CHESTPLATE, null }, // 9
			{ null, GOLDEN_LEGGINGS, GOLDEN_CHESTPLATE, GOLDEN_HELMET }, // 10
			{ GOLDEN_BOOTS, GOLDEN_LEGGINGS, GOLDEN_CHESTPLATE, GOLDEN_HELMET }, // 11
			{ GOLDEN_BOOTS, GOLDEN_LEGGINGS, GOLDEN_CHESTPLATE, DIAMOND_HELMET }, // 12
			{ DIAMOND_BOOTS, GOLDEN_LEGGINGS, GOLDEN_CHESTPLATE, GOLDEN_HELMET }, // 13
			{ DIAMOND_BOOTS, GOLDEN_LEGGINGS, GOLDEN_CHESTPLATE, DIAMOND_HELMET }, // 14
			{ GOLDEN_BOOTS, DIAMOND_LEGGINGS, GOLDEN_CHESTPLATE, DIAMOND_HELMET }, // 15
			{ DIAMOND_BOOTS, DIAMOND_LEGGINGS, GOLDEN_CHESTPLATE, GOLDEN_HELMET }, // 16
			{ GOLDEN_BOOTS, DIAMOND_LEGGINGS, DIAMOND_CHESTPLATE, GOLDEN_HELMET }, // 17
			{ GOLDEN_BOOTS, DIAMOND_LEGGINGS, DIAMOND_CHESTPLATE, DIAMOND_HELMET }, // 18
			{ DIAMOND_BOOTS, DIAMOND_LEGGINGS, DIAMOND_CHESTPLATE, GOLDEN_HELMET }, // 19
			{ DIAMOND_BOOTS, DIAMOND_LEGGINGS, DIAMOND_CHESTPLATE, DIAMOND_HELMET } // 20
	};

	public ShieldBar2(UserUI ui) {
		super(ui, 0, 20, true, 20, new LinearEaseFunction());
	}

	@Override
	protected void change() {
		Material[] materials = ARMOR[getCurrent()];
		SadItem[] armor = new SadItem[4];
		for (int i = 0; i < materials.length; i++) {
			Material material = materials[i];
			armor[i] = material != null ? new SadItem(material, " ") : null;
		}
		((Player) ui.getUser().getMinecraft().getEntity()).getInventory().setArmorContents(armor);
	}
}
