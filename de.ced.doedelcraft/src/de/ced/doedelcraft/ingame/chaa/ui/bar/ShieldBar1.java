package de.ced.doedelcraft.ingame.chaa.ui.bar;

import de.ced.doedelcraft.ingame.ease.LinearEaseFunction;
import de.ced.doedelcraft.ingame.chaa.ui.UserUI;

public class ShieldBar1 extends AbstractFloatingBar {

	public ShieldBar1(UserUI ui) {
		super(ui, 0, 20, true, 20, new LinearEaseFunction());
	}

	@Override
	protected void change() {
		ui.getUser().getMinecraft().setAbsorption(getCurrent());
	}
}
