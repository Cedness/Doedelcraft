package de.ced.doedelcraft.ingame.chaa.ui.label;

import de.ced.doedelcraft.ingame.chaa.exhaustion.UserExhaustion;
import de.ced.doedelcraft.ingame.chaa.health.UserHealth;
import de.ced.doedelcraft.ingame.chaa.mana.UserMana;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.chaa.combat.UserCombat;
import de.ced.doedelcraft.ingame.chaa.ui.UserUI;

public class BottomLabel extends AbstractLabel implements Tickable {

	private static final int DEFAULT_TEXT_TICKS = 30;
	private static final String[] SYMBOLS = { "-", "\\", " |", "/", "-", "\\", "| ", "/" };
	private static final char[][] COLORS = {
			{ '7', 'c', 'a', 'b' },
			{ '8', '4', '2', '3' }
	};

	private int startTick = 0;
	private String actionbarText = "";
	private int actionbarCooldown = 0;

	private String persistentItemText = " ";
	private String itemText = " ";
	private int itemCooldown = 0;

	public BottomLabel(UserUI ui) {
		super(ui);
	}

	@Override
	public void tick(int tick, int cycle) {

		// Actionbar message

		String text;
		if (actionbarCooldown > 0) {
			text = actionbarText;
			actionbarCooldown--;
		} else if (actionbarCooldown < 0) {
			actionbarCooldown++;
			text = null;
		} else {
			UserCombat combat = user.getCombat();
			boolean combatMode = combat.isCombatMode();
			char[] c = combatMode ? COLORS[0] : COLORS[1];

			String symbol = combatMode ? SYMBOLS[(tick - startTick) % 32 / 4] : "/";

			StringBuilder builder = new StringBuilder();

			UserHealth health = user.getHealth();
			builder.append('§').append(c[1]).append(user.getClazz().getName().equals("smoke") ? "§" + c[0] + "?" : health.get()).append("§8")
					.append(symbol).append('§').append(c[0]).append(health.getMax());

			UserMana mana = user.getMana();
			builder.append("      ").append("§").append(c[2]).append(mana.get()).append("§8").append(symbol).append("§").append(c[0]).append(mana.getMax());

			UserExhaustion exhaustion = user.getExhaustion();
			builder.append("      ").append("§").append(c[3]).append(exhaustion.get()).append("§8").append(symbol).append("§").append(c[0]).append(exhaustion.getMax());

			text = builder.toString();
		}
		if (!user.getUI().isActive()) {
			text = "";
		}
		if (text == null) {
			return;
		}
		user.getMinecraft().actionbar(text);

		// Item message

		if (itemCooldown > 0) {
			text = itemText;
			itemCooldown--;
		} else {
			text = persistentItemText;
		}
		if (tick % 60 < 30) {
			text += "§f";
		}
		user.getStuff().setItemMessage(text);
	}

	public void resetActionbarStartTick() {
		startTick = getTick();
	}

	public void setActionbarText(double timing, String actionbarText) {
		setActionbarText(actionbarText, (int) (timing * DEFAULT_TEXT_TICKS));
	}

	public void setActionbarText(String actionbarText, int ticks) {
		this.actionbarText = prepareText(actionbarText);
		actionbarCooldown = ticks;
	}

	public void setActionbarText(String actionbarText) {
		setActionbarText(actionbarText, DEFAULT_TEXT_TICKS);
	}

	private String prepareText(String text) {
		return itemText == null ? "" : text;
	}

	public void setItemText(double timing, String itemText) {
		setItemText(itemText, (int) (timing * DEFAULT_TEXT_TICKS));
	}

	public void setItemText(String itemText, int ticks) {
		this.itemText = prepareItemText(itemText);
		itemCooldown = ticks;
	}

	public void setItemText(String itemText) {
		setItemText(itemText, DEFAULT_TEXT_TICKS);
	}

	public void setPersistentItemText(String itemText) {
		persistentItemText = prepareItemText(itemText);
	}

	private String prepareItemText(String itemText) {
		return itemText == null || itemText.isEmpty() ? " " : itemText;
	}
}
