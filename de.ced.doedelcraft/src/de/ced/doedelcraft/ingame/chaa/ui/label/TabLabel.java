package de.ced.doedelcraft.ingame.chaa.ui.label;

import org.bukkit.entity.Player;

import de.ced.doedelcraft.ingame.chaa.ui.UserUI;

public class TabLabel extends AbstractLabel {

	public TabLabel(UserUI ui) {
		super(ui);
		Player player = (Player) user.getMinecraft().getEntity();
		player.setPlayerListName(user.getIdentity().getName());
	}
}
