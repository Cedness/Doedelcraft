package de.ced.doedelcraft.ingame.chaa.ui;

import de.ced.doedelcraft.ingame.chaa.exhaustion.Exhaustion;
import de.ced.doedelcraft.ingame.chaa.health.Health;
import de.ced.doedelcraft.ingame.chaa.input.Input;
import de.ced.doedelcraft.ingame.chaa.mana.Mana;
import de.ced.doedelcraft.ingame.chaa.progress.Progress;
import de.ced.doedelcraft.ingame.chaa.AbstractUnitManager;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.UserData;
import de.ced.doedelcraft.ingame.chaa.combat.Combat;
import de.ced.doedelcraft.ingame.chaa.minecraft.Minecraft;
import de.ced.doedelcraft.ingame.chaa.ui.bar.ExhaustionBar;
import de.ced.doedelcraft.ingame.chaa.ui.bar.HealthBar;
import de.ced.doedelcraft.ingame.chaa.ui.bar.ManaBar;
import de.ced.doedelcraft.ingame.chaa.ui.bar.ProgressBar;
import de.ced.doedelcraft.ingame.chaa.ui.bar.ShieldBar1;
import de.ced.doedelcraft.ingame.chaa.ui.bar.ShieldBar2;
import de.ced.doedelcraft.ingame.chaa.ui.label.BottomLabel;
import de.ced.doedelcraft.ingame.chaa.ui.label.CenterLabel;
import de.ced.doedelcraft.ingame.chaa.ui.label.SideLabel;
import de.ced.doedelcraft.ingame.chaa.ui.label.TabLabel;
import de.ced.doedelcraft.ingame.chaa.ui.label.TopLabel;

public class GenuineUserUI extends AbstractUnitManager implements UserUI {

	private final User user;
	private final HealthBar healthBar;
	private final ExhaustionBar exhaustionBar;
	private final ManaBar manaBar;
	private final ProgressBar progressBar;
	private final ShieldBar1 shieldBar1;
	private final ShieldBar2 shieldBar2;
	private final TabLabel tabLabel;
	private final TopLabel topLabel;
	private final SideLabel sideLabel;
	private final BottomLabel bottomLabel;
	private final CenterLabel centerLabel;

	private boolean active = false;

	/**
	 * Dependencies: Minecraft, Identity, Combat
	 */
	public GenuineUserUI(User user) {
		this.user = user;
		healthBar = (HealthBar) addUnit(new HealthBar(this));
		exhaustionBar = (ExhaustionBar) addUnit(new ExhaustionBar(this));
		manaBar = (ManaBar) addUnit(new ManaBar(this));
		progressBar = (ProgressBar) addUnit(new ProgressBar(this));
		shieldBar1 = (ShieldBar1) addUnit(new ShieldBar1(this));
		shieldBar2 = (ShieldBar2) addUnit(new ShieldBar2(this));
		tabLabel = (TabLabel) addUnit(new TabLabel(this));
		topLabel = (TopLabel) addUnit(new TopLabel(this));
		sideLabel = (SideLabel) addUnit(new SideLabel(this));
		bottomLabel = (BottomLabel) addUnit(new BottomLabel(this));
		centerLabel = (CenterLabel) addUnit(new CenterLabel(this));
		setActive(true);
		setAnimatedbars(user.getData().getAnimatedbarsState());
	}

	@Override
	public int[] supportedCycles() {
		return new int[] { LATE };
	}

	@Override
	public Class<?>[] removableDependencies() {
		return new Class<?>[] { Minecraft.class };
	}

	@Override
	public Class<?>[] dependencies(int cycle) {
		return new Class<?>[] { Health.class, Exhaustion.class, Mana.class, Progress.class, Combat.class, Input.class };
	}

	@Override
	public User getUser() {
		return user;
	}

	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public void setActive(boolean active) {
		if (this.active == active) {
			return;
		}
		this.active = active;
		user.getMinecraft().setGamemode(active ? 2 : 3);
	}

	@Override
	public boolean isAnimatedbars() {
		return healthBar.isFloating();
	}

	@Override
	public void setAnimatedbars(boolean animatedbars) {
		if (isAnimatedbars() == animatedbars) {
			return;
		}
		healthBar.setFloating(animatedbars);
		exhaustionBar.setFloating(animatedbars);
		manaBar.setFloating(animatedbars);
		progressBar.setFloating(animatedbars);
		shieldBar1.setFloating(animatedbars);
		shieldBar2.setFloating(animatedbars);
		UserData data = user.getData();
		if (animatedbars != data.getAnimatedbarsState()) {
			data.setAnimatedbarsState(animatedbars);
		}
	}

	@Override
	public HealthBar getHealthBar() {
		return healthBar;
	}

	@Override
	public ExhaustionBar getExhaustionBar() {
		return exhaustionBar;
	}

	@Override
	public ManaBar getManaBar() {
		return manaBar;
	}

	@Override
	public ProgressBar getProgressBar() {
		return progressBar;
	}

	@Override
	public ShieldBar1 getShieldBar1() {
		return shieldBar1;
	}

	@Override
	public ShieldBar2 getShieldBar2() {
		return shieldBar2;
	}

	@Override
	public TabLabel getTabLabel() {
		return tabLabel;
	}

	@Override
	public TopLabel getTopLabel() {
		return topLabel;
	}

	@Override
	public SideLabel getSideLabel() {
		return sideLabel;
	}

	@Override
	public BottomLabel getBottomLabel() {
		return bottomLabel;
	}

	@Override
	public CenterLabel getCenterLabel() {
		return centerLabel;
	}
}
