package de.ced.doedelcraft.ingame.chaa.ui.bar;

import de.ced.doedelcraft.ingame.chaa.ui.UserUI;
import de.ced.doedelcraft.ingame.ease.EaseFunction;
import de.ced.doedelcraft.ingame.chaa.Tickable;

public abstract class AbstractFloatingBar extends AbstractBar implements FloatingBar, Tickable {

	private double current, last;
	private double from, to;
	private int startTick;
	private final int duration;
	private final EaseFunction easeFunction;
	private boolean floating = true;

	public AbstractFloatingBar(UserUI ui, int min, int max, boolean initMax, int duration, EaseFunction easeFunction) {
		super(ui, min, max, initMax);
		this.duration = duration;
		this.easeFunction = easeFunction;
		current = super.getCurrent();
		last = super.getLast();
		change();
	}

	@Override
	public void tick(int tick, int cycle) {
		super.tick(tick, cycle);

		tick -= startTick;
		double timeProgress = floating ? (double) tick / duration : 1;
		double fromToProgress = timeProgress < 1 ? timeProgress <= 0 ? 0 : easeFunction.function(timeProgress) : 1;
		double absoluteProgress = from + (to - from) * fromToProgress;

		current = (int) Math.round(absoluteProgress);
		if (last != current && last == from) {
			initialChange();
		}
		if ((int) last != (int) current) {
			change();
		}
		last = current;
	}

	@Override
	protected int getCurrent() {
		return (int) current;
	}

	@Override
	protected int getLast() {
		return (int) last;
	}

	protected boolean getDirection() {
		return current >= last;
	}

	@Override
	protected final void apply() {
		from = current;
		to = super.getCurrent();
		startTick = getTick();
	}

	protected void initialChange() {
	}

	protected abstract void change();

	@Override
	public boolean isFloating() {
		return floating;
	}

	@Override
	public void setFloating(boolean floating) {
		this.floating = floating;
	}
}
