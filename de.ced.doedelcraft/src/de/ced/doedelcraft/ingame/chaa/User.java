package de.ced.doedelcraft.ingame.chaa;

import de.ced.doedelcraft.account.Account;
import de.ced.doedelcraft.account.Language;
import de.ced.doedelcraft.ingame.chaa.chat.UserChat;
import de.ced.doedelcraft.ingame.chaa.chunkloader.UserChunkLoader;
import de.ced.doedelcraft.ingame.chaa.combat.UserCombat;
import de.ced.doedelcraft.ingame.chaa.damage.UserDefence;
import de.ced.doedelcraft.ingame.chaa.effects.UserEffects;
import de.ced.doedelcraft.ingame.chaa.exhaustion.UserExhaustion;
import de.ced.doedelcraft.ingame.chaa.glow.UserGlow;
import de.ced.doedelcraft.ingame.chaa.handler.UserHandler;
import de.ced.doedelcraft.ingame.chaa.health.UserHealth;
import de.ced.doedelcraft.ingame.chaa.identity.UserIdentity;
import de.ced.doedelcraft.ingame.chaa.input.UserInput;
import de.ced.doedelcraft.ingame.chaa.inputpreprocess.UserInputPreprocess;
import de.ced.doedelcraft.ingame.chaa.lasttick.UserLastTick;
import de.ced.doedelcraft.ingame.chaa.mana.UserMana;
import de.ced.doedelcraft.ingame.chaa.minecraft.UserMinecraft;
import de.ced.doedelcraft.ingame.chaa.positioner.UserPositioner;
import de.ced.doedelcraft.ingame.chaa.progress.UserProgress;
import de.ced.doedelcraft.ingame.chaa.spellinfo.UserSpellInfo;
import de.ced.doedelcraft.ingame.chaa.stuff.UserStuff;
import de.ced.doedelcraft.ingame.chaa.ui.UserUI;
import de.ced.doedelcraft.ingame.chaa.walkspeed.UserWalkspeed;

/**
 * Represents a {@link Char} who is a player.
 * 
 * @author Ced
 */
public interface User extends Char {

	UserManager getUserManager();

	Account getAccount();

	Language getLanguage();

	UserInputPreprocess getInputPreprocess();

	UserUI getUI();

	UserGlow getGlow();

	UserPositioner getPositioner();

	UserChunkLoader getChunkLoader();

	@Override
	UserData getData();

	@Override
	UserMinecraft getMinecraft();

	@Override
	UserHandler getHandler();

	@Override
	UserInput getInput();

	@Override
	UserSpellInfo getSpellInfo();

	@Override
	UserIdentity getIdentity();

	@Override
	UserEffects getEffects();

	@Override
	UserProgress getProgress();

	@Override
	UserHealth getHealth();

	@Override
	UserExhaustion getExhaustion();

	@Override
	UserMana getMana();

	@Override
	UserWalkspeed getWalkspeed();

	@Override
	UserCombat getCombat();

	@Override
	UserStuff getStuff();

	@Override
	UserDefence getDefence();

	@Override
	UserChat getChat();

	@Override
	UserLastTick getLastTick();
}
