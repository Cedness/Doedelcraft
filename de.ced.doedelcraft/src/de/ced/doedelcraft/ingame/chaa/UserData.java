package de.ced.doedelcraft.ingame.chaa;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.account.Account;
import de.ced.doedelcraft.util.Logger;
import org.bukkit.configuration.file.YamlConfiguration;

import de.ced.doedelcraft.ingame.clazz.Clazz;
import de.ced.doedelcraft.ingame.clazz.ClazzManager;

public class UserData implements Data, Tickable, Addable, Removable {

	private final static int[] levels = { 0, 20, 35, 60, 120, 200, 300, 500, 1000 };
	private final static char[] colors = { '2', 'a', 'e', 'b', '3', '5', '6', '0', '0' };
	private final static Map<Integer, Character> levelColors = new TreeMap<>();

	static {
		for (int i = 0; i < levels.length && i < colors.length; i++) {
			levelColors.put(levels[i], colors[i]);
		}
	}

	private final ClazzManager clazzManager;

	private final Account account;
	private final int userDataId;
	private int charId;
	private final File configFile;
	private final YamlConfiguration config;
	private final long creation;
	private long login;
	private long logout;
	private long online;
	private long playtime;
	private String name;
	private Clazz clazz;
	private int level;
	private double progress;
	private double experience;
	private double x;
	private double y;
	private double z;
	private double yaw;
	private double pitch;
	private Boolean sidebarState;
	private Boolean animatedbarsState;
	private Boolean tutorialMode;

	public UserData(Account account, Doedelcraft doedelcraft, int userDataId, int charId) {
		this.clazzManager = doedelcraft.getCharManager().getClazzManager();
		this.account = account;
		this.userDataId = userDataId;
		this.charId = charId;
		configFile = new File(doedelcraft.getDataFolder(), "players/" + account.getPlayer().getUniqueId() + "/char" + userDataId + ".yml");
		boolean newChar = !configFile.exists();
		config = YamlConfiguration.loadConfiguration(configFile);
		try {
			config.setDefaults(doedelcraft.getDefaultCharConfig());
			config.options().copyDefaults(true);
		} catch (Exception ex) {
			Logger.severe("Error while applying default Char file to char file " + userDataId + " of player " + account.getName());
			ex.printStackTrace();
		}

		if (newChar) {
			config.set("creation", System.currentTimeMillis());
			Logger.info(account.getName() + " created a char");
			// Logger.info("Char file with id " + id + " for player " + account.getName() +
			// " created");
		}

		creation = config.getLong("creation", -1);
		login = config.getLong("login", -1);
		logout = config.getLong("logout", -1);
		online = config.getLong("online", -1);
		playtime = config.getLong("playtime", 0);
		if (online < login) {
			setOnline(login);
		}
		if (logout < login) {
			setLogout(online);
			addPlaytime();
		}

		name = config.getString("name");
		// Logger.info("Loading savefile for class " + config.getString("class") + " " +
		// charId);
		setClazz(config.getString("class").toLowerCase());
		level = config.getInt("experience.level");
		progress = config.getDouble("experience.progress");
		experience = config.getDouble("experience.experience");

		x = config.getDouble("location.x");
		y = config.getDouble("location.y");
		z = config.getDouble("location.z");
		yaw = config.getDouble("location.yaw");
		pitch = config.getDouble("location.pitch");

		saveConfig();
		// Logger.info("Char file with id " + id + " for player " + account.getName() +
		// " loaded");
	}

	@Override
	public void tick(int tick, int cycle) {
		if (tick % 201 != 3) {
			return;
		}
		setOnline();
		saveConfig();
	}

	@Override
	public void add() {
		setLogin();
	}

	@Override
	public void remove() {
		setLogout();
		saveConfig();
	}

	public Account getAccount() {
		return account;
	}

	public int getSaveFileId() {
		return userDataId;
	}

	public int getCharId() {
		return charId;
	}

	public void setCharId(int charId) {
		this.charId = charId;
	}

	@Deprecated
	public YamlConfiguration getConfig() {
		return config;
	}

	public void saveConfig() {
		try {
			config.save(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Logger.info("Char file for char " + name + " of player " + account.getName()
		// + " updated");
	}

	public long getCreation() {
		return creation;
	}

	public long getLogin() {
		return login;
	}

	public void setLogin() {
		login = System.currentTimeMillis();
		config.set("login", login);
		setOnline(login);
	}

	public long getLogout() {
		return logout;
	}

	public void setLogout() {
		setLogout(System.currentTimeMillis());
	}

	private void setLogout(long logout) {
		this.logout = logout;
		config.set("logout", logout);
		addPlaytime();
	}

	public long getOnline() {
		return online;
	}

	public void setOnline() {
		setOnline(System.currentTimeMillis());
	}

	private void setOnline(long online) {
		this.online = online;
		config.set("online", online);
	}

	public long getPlaytime() {
		return playtime;
	}

	public void addPlaytime() {
		playtime += logout - login;
		config.set("playtime", playtime);
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		config.set("name", name);
	}

	@Override
	public Clazz getClazz() {
		return clazz;
	}

	public void setClazz(Clazz clazz) {
		if (clazz == null) {
			clazz = clazzManager.getClazzByName("human");
		}
		this.clazz = clazz;
		setClazz0(clazz.getName());
	}

	private void setClazz(String name) {
		clazz = clazzManager.getClazzByName(name);
		if (clazz == null) {
			name = "human";
			clazz = clazzManager.getClazzByName(name);
		}
		setClazz0(name);
	}

	private void setClazz0(String name) {
		config.set("class", clazz.getName());
	}

	@Override
	public int getLevel() {
		return level;
	}

	@Override
	public void setLevel(int level) {
		this.level = level;
		config.set("experience.level", level);
	}

	public String getLevelColor() {
		return getLevelColor(level);
	}

	public static String getLevelColor(int level) {
		Character last = null;
		for (int requiredLevel : levelColors.keySet()) {
			if (level < requiredLevel) {
				break;
			}
			last = levelColors.get(requiredLevel);
		}
		return "§" + last;
	}

	@Override
	public double getProgress() {
		return progress;
	}

	@Override
	public void setProgress(double progress) {
		this.progress = progress;
		config.set("experience.progress", progress);
	}

	@Override
	public double getExperience() {
		return experience;
	}

	@Override
	public void setExperience(double experience) {
		this.experience = experience;
		config.set("experience.experience", experience);
	}

	@Override
	public double getX() {
		return x;
	}

	@Override
	public void setX(double x) {
		this.x = x;
		config.set("location.x", x);
	}

	@Override
	public double getY() {
		return y;
	}

	@Override
	public void setY(double y) {
		this.y = y;
		config.set("location.y", y);
	}

	@Override
	public double getZ() {
		return z;
	}

	@Override
	public void setZ(double z) {
		this.z = z;
		config.set("location.z", z);
	}

	@Override
	public double getYaw() {
		return yaw;
	}

	@Override
	public void setYaw(double yaw) {
		this.yaw = yaw;
		config.set("location.yaw", yaw);
	}

	@Override
	public double getPitch() {
		return pitch;
	}

	@Override
	public void setPitch(double pitch) {
		this.pitch = pitch;
		config.set("location.pitch", pitch);
	}

	@Override
	public boolean isCombatMode() {
		return config.getBoolean("combatmode");
	}

	@Override
	public void setCombatMode(boolean combatMode) {
		config.set("combatmode", combatMode);
	}

	public boolean isInstantMode() {
		return config.getBoolean("instantmode");
	}

	public void setInstantMode(boolean instantMode) {
		config.set("instantmode", instantMode);
	}

	public List<Integer> getSpellOrder() {
		return config.getIntegerList("spells.order");
	}

	public void setSpellOrder(List<Integer> spellOrder) {
		config.set("spells.order", spellOrder);
	}

	@Override
	public List<Integer> getSpellLevel() {
		return config.getIntegerList("spells.level");
	}

	@Override
	public void setSpellLevel(List<Integer> spellLevel) {
		config.set("spells.level", spellLevel);
	}

	@Override
	public List<Integer> getSelectedSpellLevel() {
		return config.getIntegerList("spells.selectedLevel");
	}

	@Override
	public void setSelectedSpellLevel(List<Integer> selectedSpellLevel) {
		config.set("spells.selectedLevel", selectedSpellLevel);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<List<Integer>> getSpellStrength() {
		return (List<List<Integer>>) config.getList("spells.strength");
	}

	@Override
	public void setSpellStrength(List<List<Integer>> spellStrength) {
		config.set("spells.strength", spellStrength);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<List<Integer>> getSelectedSpellStrength() {
		return (List<List<Integer>>) config.getList("spells.selectedStrength");
	}

	@Override
	public void setSelectedSpellStrength(List<List<Integer>> selectedSpellStrength) {
		config.set("spells.selectedStrength", selectedSpellStrength);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<List<Integer>> getSpellCooldown() {
		return (List<List<Integer>>) config.getList("spells.cooldown");
	}

	@Override
	public void setSpellCooldown(List<List<Integer>> spellCooldown) {
		config.set("spells.cooldown", spellCooldown);
	}

	public boolean getSidebarState() {
		return sidebarState != null ? sidebarState : (sidebarState = config.getBoolean("sidebar", true));
	}

	public void setSidebarState(boolean sidebarState) {
		config.set("sidebar", this.sidebarState = sidebarState);
	}

	public boolean getAnimatedbarsState() {
		return animatedbarsState != null ? animatedbarsState : (animatedbarsState = config.getBoolean("animatedbars", true));
	}

	public void setAnimatedbarsState(boolean animatedbarsState) {
		config.set("animatedbars", this.animatedbarsState = animatedbarsState);
	}

	public boolean getTutorialMode() {
		return tutorialMode != null ? tutorialMode : (tutorialMode = config.getBoolean("tutorial", true));
	}

	public void setTutorialMode(boolean tutorialMode) {
		config.set("tutorial", this.tutorialMode = tutorialMode);
	}

	public List<Map<?, ?>> getStuff() {
		return config.getMapList("stuff");
	}

	public void setStuff(List<Map<String, Object>> stuff) {
		config.set("stuff", stuff);
	}
}
