package de.ced.doedelcraft.ingame.chaa.combat;

import java.util.Collection;

import de.ced.doedelcraft.ingame.clazz.Spell;
import de.ced.doedelcraft.ingame.chaa.CastedSpell;
import de.ced.doedelcraft.ingame.chaa.Unit;

public interface Combat extends Unit {

	void toggleCombatMode(boolean combatMode);

	void toggleCombatMode(boolean combatMode, boolean disableAll);

	boolean isCombatMode();

	/**
	 * Unselect a spell.
	 */
	void unselectSpell();

	/**
	 * Call it when a not unlocked spell was launched by mistake.
	 */
	void notUnlocked();

	/**
	 * Casts the spell or disables it if already enabled. Does not check if the
	 * spell is unlocked!
	 */
	void castSpell(Spell spell);

	void nextSpellLevel(int spellId, boolean ctrl);

	void selectSpell(Spell spell);

	void adjustSpellStrength(int strength);

	Collection<CastedSpell> getCastedSpells();
}
