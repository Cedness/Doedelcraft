package de.ced.doedelcraft.ingame.chaa.combat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.ced.doedelcraft.ingame.chaa.exhaustion.Exhaustion;
import de.ced.doedelcraft.ingame.chaa.mana.Mana;
import de.ced.doedelcraft.ingame.chaa.spellinfo.SpellInfo;
import de.ced.doedelcraft.ingame.clazz.Spell;
import de.ced.doedelcraft.util.Logger;
import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.CastedSpell;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.Data;
import de.ced.doedelcraft.ingame.chaa.Removable;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.clazz.Clazz;
import de.ced.doedelcraft.ingame.clazz.SpellResult;

public abstract class AbstractCombat extends AbstractUnit implements Combat, Tickable, Removable {

	protected final Char chaa;
	protected boolean combatMode;

	protected List<CastedSpell> castedSpells = new ArrayList<>();

	protected boolean setup = false;

	public AbstractCombat(Char chaa) {
		this.chaa = chaa;
		Data data = chaa.getData();
		combatMode = data.isCombatMode();

		chaa.getClazzActions().add(); // TODO clazzactions add, why here?

		toggleCombatMode(combatMode);
	}

	private SpellInfo spellInfo() {
		return chaa.getSpellInfo();
	}

	protected void finishSetup() {
	}

	@Override
	public void remove() {
		for (Spell spell : spellInfo().getCooldowningSpells()) {
			outputSpellCooldownChange(spell, 0);
		}
	}

	@Override
	public int[] supportedCycles() {
		return new int[] { EARLY, DEFAULT };
	}

	@Override
	public void tick(int tick, int cycle) {
		switch (cycle) {
		case EARLY: {
			if (!setup) {
				setup = true;
				finishSetup();
			}
			for (CastedSpell castedSpell : chaa.getCharManager().getCastedSpells()) {
				try {
					castedSpell.getActions().userTick(chaa);
				} catch (Exception ex) {
					Logger.severe("Exception while executing user-tick of spell " + castedSpell.getSpell() + " for " + chaa);
					ex.printStackTrace();
				}
			}
			break;
		}
		case DEFAULT: {
			chaa.getClazzActions().tick(tick, cycle);

			for (CastedSpell castedSpell : castedSpells) {
				if (castedSpell.shouldRemove()) {
					continue;
				}
				try {
					castedSpell.getActions().tick(tick);
				} catch (Exception ex) {
					Logger.severe("Exception while ticking spell " + castedSpell.getSpell() + " for " + chaa);
					ex.printStackTrace();
				}
				if (castedSpell.shouldDisable()) {
					if (!castedSpell.isAfterlife()) {
						disable(castedSpell);
					}
				}
			}
			boolean changed = false;
			for (Iterator<CastedSpell> iterator = castedSpells.iterator(); iterator.hasNext();) {
				CastedSpell castedSpell = iterator.next();
				if (!castedSpell.shouldRemove()) {
					continue;
				}
				iterator.remove();
				chaa.getCharManager().getCastedSpells().remove(castedSpell);
				changed = true;
			}
			if (changed) {
				updateLockedExhaustion();
				updateManaRegen();
			}
			break;
		}
		}
	}

	// Combat mode

	@Override
	public void toggleCombatMode(boolean combatMode) {
		toggleCombatMode(combatMode, false);
	}

	@Override
	public void toggleCombatMode(boolean combatMode, boolean disableAll) {
		this.combatMode = combatMode;
		chaa.getData().setCombatMode(combatMode);

		if (!combatMode) {
			List<Spell> toDisable = new ArrayList<>();
			for (Spell spell : spellInfo().getActiveSpells()) {
				if (spell.isSwitchSpell() && (disableAll || spell.needsCombatMode())) {
					toDisable.add(spell);
				}
			}
			for (Spell spell : toDisable) {
				disable(getCastedSpell(spell));
			}
		}

		try {
			chaa.getClazzActions().setCombatMode(combatMode);
		} catch (Exception e) {
			Logger.severe("Exception while " + (combatMode ? "enabling" : "disabling") + " combatmode for char " + chaa);
			e.printStackTrace();
		}

		Clazz clazz = chaa.getClazz();
		SpellResult result = combatMode ? clazz.getEnableResult() : clazz.getDisableResult();

		outputCombatModeChange(combatMode, result);
		playResultSound(result);
	}

	protected abstract void outputCombatModeChange(boolean combatMode, SpellResult result);

	// Spell casting

	@Override
	public void notUnlocked() {
		unselectSpell();
		outputNotUnlocked();
	}

	protected abstract void outputNotUnlocked();

	protected void resetSpellCooldown(CastedSpell castedSpell) {
		spellInfo().setSpellCooldown(castedSpell.getSpell(), castedSpell.getCooldownTicks());
	}

	protected void preOutputSpellCooldownChange(Spell spell) {
		SpellInfo spellInfo = spellInfo();
		if (!spellInfo.isCooldowningSpell(spell)) {
			return;
		}
		outputSpellCooldownChange(spell, spellInfo.getSpellCooldown(spell));
	}

	protected abstract void outputSpellCooldownChange(Spell spell, int cooldown);

	public SpellResult launch(CastedSpell castedSpell) {
		SpellResult result = getSpellResult(castedSpell.getSpell(), launchCastedSpell(castedSpell), Integer.MIN_VALUE, Integer.MIN_VALUE);
		playResultSound(result);
		return result;
	}

	protected int launchCastedSpell(CastedSpell castedSpell) {
		Spell spell = castedSpell.getSpell();
		if (!combatMode && spell.needsCombatMode()) {
			return Integer.MIN_VALUE;
		}
		SpellInfo spellInfo = spellInfo();
		if (spellInfo.isCooldowningSpell(spell)) {
			return Integer.MIN_VALUE + 1;
		}

		int prepareId;
		try {
			prepareId = castedSpell.getActions().prepare();
		} catch (Exception ex) {
			Logger.severe("Exception while preparing spell " + spell + " for " + chaa);
			ex.printStackTrace();
			prepareId = 0;
		}
		boolean launch = prepareId == 0;

		int spellExhaustion = castedSpell.getExhaustion();
		if (!checkExhasution(spellExhaustion)) {
			return Integer.MIN_VALUE + 2;
		}

		int spellMana = castedSpell.getLaunchMana();
		if (!checkMana(spellMana)) {
			return Integer.MIN_VALUE + 3;
		}

		boolean isSwitchSpell = spell.isSwitchSpell();
		if (!launch || !isSwitchSpell) {
			useExhaustion(spellExhaustion);
		}

		useMana(spellMana);

		if (!launch || !isSwitchSpell) {
			resetSpellCooldown(castedSpell);
		}

		if (!launch) {
			return prepareId;
		}

		castedSpell.add();
		updateManaRegen();

		if (isSwitchSpell) {

			updateLockedExhaustion();

			spellInfo.setSpellState(spell.getSpellId(), spell.getSpellLevel(), true);
		}

		int resultId;
		try {
			resultId = castedSpell.getActions().launch();
		} catch (Exception ex) {
			Logger.severe("Exception while launching spell " + spell + " for " + chaa);
			ex.printStackTrace();
			resultId = 0;
		}
		outputSpellStateUpdate(spell);
		return resultId;
	}

	protected CastedSpell getCastedSpell(Spell spell) {
		for (CastedSpell someCastedSpell : castedSpells) {
			if (someCastedSpell.getSpell().equals(spell)) {
				return someCastedSpell;
			}
		}
		return new CastedSpell(chaa, spell);
	}

	public SpellResult disable(CastedSpell castedSpell) {
		SpellResult result = getSpellResult(castedSpell.getSpell(), disableCastedSpell(castedSpell), Integer.MIN_VALUE, Integer.MIN_VALUE);
		if (result.isSuccessful()) {
			castedSpell.remove();
		} else {
			castedSpell.afterlife(); // TODO afterlife
		}
		playResultSound(result);
		return result;
	}

	protected int disableCastedSpell(CastedSpell castedSpell) {
		Spell spell = castedSpell.getSpell();
		spellInfo().setSpellState(spell.getSpellId(), spell.getSpellLevel(), false);

		useMana(castedSpell.getDisableMana());

		int resultId;
		try {
			resultId = castedSpell.getActions().disable();
		} catch (Exception ex) {
			Logger.severe("Exception while disabling spell " + spell + " for " + chaa);
			ex.printStackTrace();
			resultId = 0;
		}

		outputSpellStateUpdate(spell);
		resetSpellCooldown(castedSpell);
		preOutputSpellCooldownChange(spell);
		return resultId;
	}

	protected abstract void outputSpellStateUpdate(Spell spell);

	@Override
	public void castSpell(Spell spell) {
		// unselectSpell();

		SpellResult result = spell.isSwitchSpell() && spellInfo().getSpellState(spell) ? disable(getCastedSpell(spell)) : launch(new CastedSpell(chaa, spell));

		preOutputSpellCooldownChange(spell);
		outputSpellCast(spell, result);
	}

	protected abstract void outputSpellCast(Spell spell, SpellResult result);

	@Override
	public void selectSpell(Spell spell) {
		spellInfo().setSelectedSpellId(spell.getSpellId());
		outputSpellSelect(spell);
	}

	protected abstract void outputSpellSelect(Spell spell);

	@Override
	public void unselectSpell() {
		spellInfo().setSelectedSpellId(-1);
		outputSpellUnselect();
	}

	protected abstract void outputSpellUnselect();

	// Spell level

	@Override
	public void nextSpellLevel(int spellId, boolean ctrl) {
		SpellInfo spellInfo = chaa.getSpellInfo();
		int currentSpellLevel = spellInfo.getSelectedSpellLevel(spellId);
		Spell currentSpell = spellInfo.getSpell(spellId, currentSpellLevel);
		if (spellInfo.getSpellLevelCount(spellId) <= 1) {
			outputSpellLevelError1(currentSpell);
			return;
		}
		int spellLevel = spellInfo.getSpellLevel(spellId);
		if (spellInfo.getSpellLevelCount(spellId) <= 1 || spellLevel <= 0) {
			outputSpellLevelError2(currentSpell);
			return;
		}
		int increment = ctrl ? spellLevel : 1;
		int nextSpellLevel = (currentSpellLevel + increment) % (spellLevel + 1);
		Spell nextSpell = spellInfo.getSpell(spellId, nextSpellLevel);

		SpellResult enableResult = null;
		if (currentSpell.isSwitchSpell() && spellInfo.getSpellState(currentSpell)) {
			CastedSpell castedCurrentSpell = getCastedSpell(currentSpell);
			CastedSpell castedNextSpell = new CastedSpell(chaa, nextSpell);
			boolean enableNext = nextSpell.adapts();
			if ((combatMode || !nextSpell.needsCombatMode()) && enableNext) {
				Boolean adaptResult = castedNextSpell.getActions().adaptFrom(castedCurrentSpell, ctrl);
				if (adaptResult != null) {
					enableNext = adaptResult;
				}
			}
			disable(castedCurrentSpell);
			if (enableNext) {
				updateLockedExhaustion();
				enableResult = launch(castedNextSpell);
			}
		}

		spellInfo.setSelectedSpellLevel(spellId, nextSpellLevel);

		outputSpellLevelChange(nextSpell, enableResult);
		preOutputSpellCooldownChange(nextSpell);
	}

	protected abstract void outputSpellLevelChange(Spell nextSpell, SpellResult result);

	protected abstract void outputSpellLevelError1(Spell currentSpell);

	protected abstract void outputSpellLevelError2(Spell currentSpell);

	// Spell strength

	protected int adjustSpellStrength(CastedSpell castedSpell, int currentStrength, int newStrength, int requestedStrength) {
		Spell spell = castedSpell.getSpell();
		int spellId = spell.getSpellId();
		if (requestedStrength == currentStrength) {
			return Integer.MIN_VALUE + 0;
		}
		int maxStrength = spell.getMaxStrength() - 1;
		if (maxStrength <= 0) {
			return Integer.MIN_VALUE + 1;
		}
		if (currentStrength == newStrength) {
			return Integer.MIN_VALUE + (requestedStrength > currentStrength ? newStrength == maxStrength ? 4 : 3 : 2);
		}
		SpellInfo spellInfo = chaa.getSpellInfo();
		if (!spell.isSwitchSpell() || !spellInfo.getSpellState(spellId)) {
			castedSpell.setStrength(newStrength);
			spellInfo.setSelectedSpellStrength(spellId, spell.getSpellLevel(), newStrength);
			return 0;
		}

		if (newStrength > currentStrength) {
			int prepareId;
			try {
				prepareId = castedSpell.getActions().prepareStrengthChange(currentStrength, newStrength);
			} catch (Exception ex) {
				Logger.severe("Exception while preparing strength change of spell " + spell + " for " + chaa);
				ex.printStackTrace();
				prepareId = 0;
			}
			boolean launch = prepareId == 0;

			int oldLaunchMana = castedSpell.getLaunchMana();
			int oldExhaustion = castedSpell.getExhaustion();
			castedSpell.setStrength(newStrength);

			int spellExhaustion = castedSpell.getExhaustion() - oldExhaustion;
			if (!checkExhasution(spellExhaustion)) {
				castedSpell.setStrength(currentStrength);
				return Integer.MIN_VALUE + 5;
			}

			int spellMana = castedSpell.getLaunchMana() - oldLaunchMana;
			if (!checkMana(spellMana)) {
				castedSpell.setStrength(currentStrength);
				return Integer.MIN_VALUE + 6;
			}

			if (!launch) {
				useExhaustion(spellExhaustion);
			}

			useMana(spellMana);

			if (!launch) {
				castedSpell.setStrength(currentStrength);
				return prepareId;
			}
		} else {
			castedSpell.setStrength(newStrength);
		}

		updateManaRegen();

		updateLockedExhaustion();

		int resultId;
		try {
			resultId = castedSpell.getActions().changeStrength(currentStrength, newStrength);
		} catch (Exception ex) {
			Logger.severe("Exception while changing strength of spell " + spell + " for " + chaa);
			ex.printStackTrace();
			resultId = 0;
		}

		spellInfo.setSelectedSpellStrength(spellId, spell.getSpellLevel(), newStrength);

		return resultId;
	}

	@Override
	public void adjustSpellStrength(int requestedStrength) {
		SpellInfo spellInfo = chaa.getSpellInfo();
		int spellId = spellInfo.getSelectedSpellId();
		int spellLevel = spellInfo.getSelectedSpellLevel(spellId);
		int currentStrength = spellInfo.getSelectedSpellStrength(spellId, spellLevel);
		int unlockedStrength = spellInfo.getSpellStrength(spellId, spellLevel);

		int newStrength = Math.min(Math.max(0, requestedStrength), unlockedStrength);

		Spell spell = spellInfo.getSpell(spellId, spellLevel);
		CastedSpell castedSpell = getCastedSpell(spell);
		int resultId = adjustSpellStrength(castedSpell, currentStrength, newStrength, requestedStrength);
		SpellResult result = getSpellResult(spell, resultId, newStrength, unlockedStrength);
		outputSpellStrengthAdjustment(spell, result);
	}

	protected abstract void outputSpellStrengthAdjustment(Spell spell, SpellResult result);

	//

	protected boolean checkMana(int spellMana) {
		return spellMana <= chaa.getMana().get();
	}

	protected void useMana(int spellMana) {
		if (spellMana < 1) {
			return;
		}
		Mana mana = chaa.getMana();
		mana.set(mana.get() - spellMana);
		outputManaUse(spellMana);
	}

	protected abstract void outputManaUse(int spellMana);

	protected void updateManaRegen() {
		chaa.getMana().calcRegen();
	}

	protected boolean checkExhasution(int spellExhaustion) {
		return spellExhaustion <= chaa.getExhaustion().get();
	}

	protected void useExhaustion(int spellExhaustion) {
		if (spellExhaustion < 1) {
			return;
		}
		Exhaustion exhaustion = chaa.getExhaustion();
		int newCurrentExhaustion = exhaustion.getMax() - (exhaustion.getLocked() + spellExhaustion);
		if (exhaustion.get() > newCurrentExhaustion) {
			exhaustion.set(newCurrentExhaustion, false);
		}
	}

	protected void updateLockedExhaustion() {
		int locked = 0;
		for (CastedSpell castedSpell : castedSpells) {
			locked += castedSpell.getExhaustion();
		}
		Exhaustion exhaustion = chaa.getExhaustion();
		exhaustion.setLocked(locked);
		exhaustion.setStartTick(false);
	}

	// Utility methods

	protected SpellResult getSpellResult(Spell spell, int resultId, int newStrength, int unlockedStrength) {
		SpellResult result = null;
		newStrength++;
		if (Integer.MIN_VALUE + 1000 < resultId && resultId != 0) {
			result = spell.getResult(Math.abs(resultId));
		}
		if (result == null) {
			result = SpellResult.getFallbackResult();
		}
		return result;
	}

	protected void playResultSound(SpellResult result) {
		String sound = result.getSound();
		if (sound == null) {
			return;
		}
		chaa.getEffects().play("spell." + chaa.getClazz().getName() + "." + sound);
	}

	// Getter / Setter

	@Override
	public boolean isCombatMode() {
		return combatMode;
	}

	@Override
	public List<CastedSpell> getCastedSpells() {
		return castedSpells;
	}
}
