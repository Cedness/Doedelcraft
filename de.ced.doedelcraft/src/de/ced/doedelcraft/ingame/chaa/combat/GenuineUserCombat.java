package de.ced.doedelcraft.ingame.chaa.combat;

import java.util.ArrayList;
import java.util.List;

import de.ced.doedelcraft.account.Language;
import de.ced.doedelcraft.ingame.chaa.spellinfo.AbstractSpellInfo;
import de.ced.doedelcraft.ingame.chaa.spellinfo.SpellInfo;
import de.ced.doedelcraft.ingame.chaa.ui.UserUI;
import de.ced.doedelcraft.ingame.clazz.Spell;
import de.ced.doedelcraft.util.Logger;
import de.ced.doedelcraft.util.SadItem;
import org.bukkit.Material;

import de.ced.doedelcraft.account.Dictionary;
import de.ced.doedelcraft.ingame.chaa.CastedSpell;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.minecraft.Minecraft;
import de.ced.doedelcraft.ingame.chaa.minecraft.UserMinecraft;
import de.ced.doedelcraft.ingame.clazz.SpellResult;

public class GenuineUserCombat extends AbstractCombat implements UserCombat {

	private final User user;

	private final List<SadItem> spellItems = new ArrayList<>();
	private final List<String> spellItemMessages = new ArrayList<>();

	private Dictionary clazzMessages;
	private Dictionary spellMessages;
	private Dictionary genericSpellMessages;

	/**
	 * Dependencies: SpellInfo
	 */
	public GenuineUserCombat(User user) {
		super(user);
		this.user = user;

		SpellInfo spellInfo = user.getSpellInfo();
		for (int i = 0; i < spellInfo.getSpellCount(); i++) {
			spellItems.add(new SadItem(Material.SNOW, "§8§oLoading..."));
			spellItemMessages.add("");
		}

		// for (int i = 0; i < spellCount; i++) { applySpellItem(i); }

		for (AbstractSpellInfo.CooldownIterator iterator = (AbstractSpellInfo.CooldownIterator) spellInfo.getCooldowningSpells().iterator(); iterator.hasNext();) {
			outputSpellCooldownChange(iterator.next(), iterator.getCooldown());
		}
	}

	@Override
	protected void finishSetup() {
		Language language = user.getLanguage();
		String clazzName = user.getClazz().getName();
		clazzMessages = language.getD("clazz").getD(clazzName);
		spellMessages = language.getD("spell").getD(clazzName);
		genericSpellMessages = language.getD("spell").getD("generic");

		SpellInfo spellInfo = user.getSpellInfo();
		for (int i = 0; i < spellInfo.getSpellCount(); i++) {
			int spellId = spellInfo.getSpellId(i);
			updateSpellIcon(spellId, spellInfo.getSpell(spellId));
		}
	}

	// Combat mode

	@Override
	protected void outputCombatModeChange(boolean combatMode, SpellResult result) {
		if (!setup) {
			return;
		}
		UserUI ui = user.getUI();
		ui.getTopLabel().setCombatMode(combatMode);

		String message = result.getMessage();
		if (message.isEmpty()) {
			message = "combat_" + (combatMode ? "on" : "off");
		}
		message = result.getColor() + "§l" + clazzMessages.get(message);

		ui.getBottomLabel().resetActionbarStartTick();
		displayItemMessage(message);
	}

	// Spell casting

	@Override
	protected void outputNotUnlocked() {
		user.getEffects().play("spell.notunlocked");
		displayItemMessage("§4Not unlocked");
	}

	@Override
	public Class<?>[] removableDependencies() {
		return new Class<?>[] { Minecraft.class };
	}

	@Override
	protected void outputSpellCooldownChange(Spell spell, int cooldown) {
		Material spellMaterial = getSpellItem(spell.getSpellId()).getType();
		UserMinecraft minecraft = user.getMinecraft();
		if (cooldown > 0 && minecraft.hasCooldown(spellMaterial)) {
			return;
		}
		Logger.info("Set cooldown " + cooldown + " for spell " + spell);
		user.getMinecraft().setCooldown(spellMaterial, cooldown);
	}

	@Override
	protected void outputSpellStateUpdate(Spell spell) {
		updateSpellIcon(spell);
	}

	@Override
	protected void outputSpellCast(Spell spell, SpellResult result) {
		displaySpellResult(spell, result);

		user.getUI().getSideLabel().displaySpell(spell, true);
	}

	public void outputCurrentSpellSelect() {
		SpellInfo spellInfo = user.getSpellInfo();
		if (!spellInfo.hasSelectedSpell()) {
			outputSpellUnselect();
			return;
		}
		Spell spell = spellInfo.getSpell(spellInfo.getSelectedSpellId());
		outputCurrentSpellSelect(spell);
		if (spell.equals(user.getUI().getSideLabel().getDisplayedSpell())) {
			return;
		}
		user.getUI().getSideLabel().displaySpell(spell);
	}

	@Override
	protected void outputSpellSelect(Spell spell) {
		outputCurrentSpellSelect(spell);
		user.getUI().getSideLabel().displaySpell(spell);
	}

	private void outputCurrentSpellSelect(Spell spell) {
		int spellId = spell.getSpellId();
		user.getStuff().setAmount(user.getSpellInfo().getSpellLevel(spellId) + 1);
		displayPersistentItemMessage(spell);
		updateSpellIcon(spell);
		displaySpellStrength(spell);
	}

	@Override
	protected void outputSpellUnselect() {
		user.getStuff().setAmount(1);
		displayPersistentItemMessage(null);
		user.getUI().getSideLabel().displaySpell(null);
		displaySpellStrength(null);
	}

	// Spell level

	@Override
	protected void outputSpellLevelChange(Spell nextSpell, SpellResult result) {
		outputSpellSelect(nextSpell);
		if (result != null) {
			displaySpellResult(nextSpell, result);
		}
	}

	@Override
	protected void outputSpellLevelError1(Spell currentSpell) {
		outputSpellSelect(currentSpell);
		String message = "§c" + currentSpell.getName() + " §chas only one level"; // TODO translate?
		displayItemMessage(message);
	}

	@Override
	protected void outputSpellLevelError2(Spell currentSpell) {
		outputSpellSelect(currentSpell);
		String message = "§cYou have only unlocked the first level"; // TODO translate?
		displayItemMessage(message);
	}

	// Spell strength

	@Override
	protected void outputSpellStrengthAdjustment(Spell spell, SpellResult result) {
		updateSpellIcon(spell);
		displaySpellResult(spell, result);
		displaySpellStrength(spell);
		user.getUI().getSideLabel().displaySpell(spell, true);
	}

	// Spell utility methods

	@Override
	protected void outputManaUse(int spellMana) {
		user.getUI().getCenterLabel().manaUsed(spellMana);
	}

	// Utility methods

	@Override
	protected SpellResult getSpellResult(Spell spell, int resultId, int newStrength, int unlockedStrength) {
		String message = null;
		boolean translated = false;
		int failId = resultId - Integer.MIN_VALUE;
		if (newStrength == Integer.MIN_VALUE && unlockedStrength == Integer.MIN_VALUE) {
			switch (failId) {
			case 0:
				message = user.getClazz().getNeedsCombatResult().getColor() + "§l" + clazzMessages.get("needs_combat");
				translated = true;
				break;
			case 1:
				// int remainingSeconds = (userManager.getCooldowningSpells(user).get(spell) -
				// doedelcraft.getTick()) / 20;
				message = "launch_fail_cooldown";
				break;
			case 2:
				message = "launch_fail_exhausted";
				break;
			case 3:
				message = "launch_fail_mana";
				break;
			}
		} else {
			newStrength++;
			if (resultId == 0) {
				message = "§7§l" + genericSpellMessages.get("strength") + ": §6§l" + newStrength + "§7§l/" + (unlockedStrength + 1);
				translated = true;
			} else {
				switch (failId) {
				case 0:
					message = "strength_fail_equal";
					break;
				case 1:
					message = "strength_fail_amount";
					break;
				case 2:
					message = "strength_fail_low";
					break;
				case 3:
					message = "strength_fail_locked";
					break;
				case 4:
					message = "strength_fail_high";
					break;
				case 5:
					message = "strength_fail_exhausted";
					break;
				case 6:
					message = "strength_fail_mana";
					break;
				}
			}
		}
		SpellResult result = null;
		if (message == null) {
			result = spell.getResult(Math.abs(resultId));
			if (result != null) {
				result = new SpellResult(result);
			} else { // only to avoid crashs. if resultId is invalid use default output
				Logger.severe("ResultId " + resultId + " is invalid for spell " + spell);
				result = SpellResult.getFallbackResult();
			}
			message = result.getMessage();
			if (message.isEmpty()) { // Get generic success message
				message = getSpellNameString(spell);
				if (!spell.isSwitchSpell()) {
					message += " §7" + genericSpellMessages.get("launch");
				}
			} else if (!message.startsWith("§")) { // translate specialized success message
				message = result.getColor() + "§l" + spellMessages.getD(spell.getName()).getD("results").get(message);
			}
			result.setMessage(message);
		} else if (translated) {
			result = new SpellResult(message);
		} else { // Translate rejection message
			result = new SpellResult("§c§l" + genericSpellMessages.get(message, spellMessages.getD(spell.getName()).get("name"),
					user.getClazz().getManaName(user.getLanguage()).toLowerCase(), "§e§l" + newStrength + "§c§l"));
		}
		return result;
	}

	private void displaySpellResult(Spell spell, SpellResult result) {
		displayItemMessage(addSpellInfo(spell, result.getMessage()));
	}

	private void displayItemMessage(String message) {
		user.getUI().getBottomLabel().setItemText(message);
	}

	private String addSpellInfo(Spell spell, String message) {
		if (spell.isSwitchSpell()) {
			char symbol = '\u2588';
			String state = (user.getSpellInfo().getSpellState(spell) ? "§a" : "§7") + symbol;
			message = state + "§f " + message + "§f " + state;
		}
		return message;
	}

	private String getSpellNameString(Spell spell) {
		return spell.getColor() + "§l" + spellMessages.getD(spell.getName()).get("name");
	}

	private void displayPersistentItemMessage(Spell spell) {
		user.getUI().getBottomLabel().setPersistentItemText(spell == null ? " " : addSpellInfo(spell, getSpellNameString(spell)));
	}

	public void applySpellItem(int spellId) {
		SpellInfo spellInfo = user.getSpellInfo();
		if (!spellInfo.hasSpells()) {
			return;
		}
		user.getMinecraft().getInventory().setItem(spellInfo.getSlotId(spellId), spellItems.get(spellId));
	}

	/**
	 * @param spell must not be null
	 */
	private void updateSpellIcon(Spell spell) {
		updateSpellIcon(spell.getSpellId(), spell);
	}

	/**
	 * Expects both spell and id because spell could be null ifit's not unlocked,
	 * then id is used to determine the slot.
	 */
	private void updateSpellIcon(int spellId, Spell spell) {
		SadItem item = getSpellItem(spellId);
		SpellInfo spellInfo = user.getSpellInfo();
		if (spell != null || spellInfo.isSpellUnlocked(spellId)) {
			Material material = spell.getItem();
			item.setItem(material).setText(addSpellInfo(spell, getSpellNameString(spell)), "", "§8Press the key of another spell to swap slots");
			item.setAmount(spellInfo.getSpellLevel(spellId) + 1);
			if (spell.isSwitchSpell()) {
				item.enchant(spellInfo.getSpellState(spell));
			}
			try {
				new CastedSpell(user, spell).getActions().updateItem(item); // TODO this is beautiless
			} catch (Exception ex) {
				Logger.severe("Exception while updating item for spell " + spell.getName() + " for "
						+ user.getClazz().getName() + " user " + user.getIdentity().getName() + " of player "
						+ user.getAccount().getName());
				ex.printStackTrace();
			}
		} else {
			item.setItem(Material.SNOW).setText("§4Not unlocked", "", "§8Press the key of another spell to swap slots");
		}
		applySpellItem(spellId);
	}

	private void displaySpellStrength(Spell spell) {
		int strength = 0;
		if (spell != null) {
			int spellId = spell.getSpellId();
			int spellLevel = spell.getSpellLevel();
			SpellInfo spellInfo = user.getSpellInfo();
			if (spellInfo.getSpellStrength(spellId, spellLevel) > 0) {
				strength = spellInfo.getSelectedSpellStrength(spellId, spellLevel) + 1;
			}
		}
		user.getMinecraft().setLevel(strength);
	}

	// Getter / Setter

	public SadItem getSpellItem(int spellId) {
		return spellItems.get(spellId);
	}

	public void onLevelChange() {
		int strength = 0;
		SpellInfo spellInfo = user.getSpellInfo();
		int spellId = spellInfo.getSelectedSpellId();
		int spellLevel = spellInfo.getSelectedSpellLevel(spellId);
		if (spellInfo.hasSelectedSpell() && spellInfo.getSpellStrength(spellId, spellLevel) > 0) {
			strength = spellInfo.getSelectedSpellStrength(spellId, spellLevel) + 1;
		}
		user.getMinecraft().setLevel(strength);
	}
}
