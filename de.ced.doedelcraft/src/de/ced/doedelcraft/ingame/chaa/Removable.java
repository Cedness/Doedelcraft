package de.ced.doedelcraft.ingame.chaa;

public interface Removable extends Unit {

	void remove();

	default Class<?>[] removableDependencies() {
		return new Class<?>[] {};
	}
}
