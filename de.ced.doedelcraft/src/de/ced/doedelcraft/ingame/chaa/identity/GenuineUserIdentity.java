package de.ced.doedelcraft.ingame.chaa.identity;

import org.bukkit.entity.Player;

import de.ced.doedelcraft.ingame.chaa.User;

public class GenuineUserIdentity extends AbstractIdentity implements UserIdentity {

	private final User user;

	/**
	 * Dependencies: Minecraft
	 */
	public GenuineUserIdentity(User user) {
		super(user);
		this.user = user;
		Player player = (Player) user.getMinecraft().getEntity();
		player.setDisplayName(name);
		player.setCustomName(name);
		player.setCustomNameVisible(true);
	}

	@Override
	public String getFormattedName() {
		return user.getAccount().getRank().getColor() + name;
	}
}
