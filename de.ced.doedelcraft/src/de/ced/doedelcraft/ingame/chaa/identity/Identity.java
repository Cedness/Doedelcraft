package de.ced.doedelcraft.ingame.chaa.identity;

import java.util.UUID;

import de.ced.doedelcraft.ingame.chaa.Unit;

public interface Identity extends Unit {

	String getName();

	String getFormattedName();

	void setName(String name);

	UUID getUniqueId();
}
