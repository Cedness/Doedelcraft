package de.ced.doedelcraft.ingame.chaa.identity;

import java.util.UUID;

import de.ced.doedelcraft.util.Logger;
import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Char;

public abstract class AbstractIdentity extends AbstractUnit implements Identity {

	protected final Char chaa;
	protected String name;

	public AbstractIdentity(Char chaa) {
		this.chaa = chaa;
		setName(null);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getFormattedName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name == null ? chaa.getData().getName() : name;
	}

	@Override
	public UUID getUniqueId() {
		return chaa.getMinecraft().getUniqueId();
	}

	@Override
	public String toString() {
		Logger.warning("Identity used as String. Use getName() instead.");
		return name;
	}
}
