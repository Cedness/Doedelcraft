package de.ced.doedelcraft.ingame.chaa;

import java.lang.reflect.InvocationTargetException;

import de.ced.doedelcraft.ingame.chaa.chat.Chat;
import de.ced.doedelcraft.ingame.chaa.chat.GenuineUserChat;
import de.ced.doedelcraft.ingame.chaa.clazzactions.ClazzActions;
import de.ced.doedelcraft.ingame.chaa.combat.Combat;
import de.ced.doedelcraft.ingame.chaa.combat.GenuineUserCombat;
import de.ced.doedelcraft.ingame.chaa.damage.Defence;
import de.ced.doedelcraft.ingame.chaa.damage.GenuineUserDefence;
import de.ced.doedelcraft.ingame.chaa.effects.Effects;
import de.ced.doedelcraft.ingame.chaa.effects.GenuineUserEffects;
import de.ced.doedelcraft.ingame.chaa.exhaustion.Exhaustion;
import de.ced.doedelcraft.ingame.chaa.exhaustion.GenuineUserExhaustion;
import de.ced.doedelcraft.ingame.chaa.handler.GenuineUserHandler;
import de.ced.doedelcraft.ingame.chaa.handler.Handler;
import de.ced.doedelcraft.ingame.chaa.health.GenuineUserHealth;
import de.ced.doedelcraft.ingame.chaa.health.Health;
import de.ced.doedelcraft.ingame.chaa.identity.GenuineUserIdentity;
import de.ced.doedelcraft.ingame.chaa.identity.Identity;
import de.ced.doedelcraft.ingame.chaa.input.GenuineUserInput;
import de.ced.doedelcraft.ingame.chaa.input.Input;
import de.ced.doedelcraft.ingame.chaa.lasttick.GenuineUserLastTick;
import de.ced.doedelcraft.ingame.chaa.lasttick.LastTick;
import de.ced.doedelcraft.ingame.chaa.mana.GenuineUserMana;
import de.ced.doedelcraft.ingame.chaa.mana.Mana;
import de.ced.doedelcraft.ingame.chaa.minecraft.GenuineUserMinecraft;
import de.ced.doedelcraft.ingame.chaa.minecraft.Minecraft;
import de.ced.doedelcraft.ingame.chaa.progress.GenuineUserProgress;
import de.ced.doedelcraft.ingame.chaa.progress.Progress;
import de.ced.doedelcraft.ingame.chaa.spellinfo.GenuineUserSpellInfo;
import de.ced.doedelcraft.ingame.chaa.spellinfo.SpellInfo;
import de.ced.doedelcraft.ingame.chaa.stuff.GenuineUserStuff;
import de.ced.doedelcraft.ingame.chaa.stuff.Stuff;
import de.ced.doedelcraft.ingame.chaa.ui.UserUI;
import de.ced.doedelcraft.ingame.chaa.walkspeed.GenuineUserWalkspeed;
import de.ced.doedelcraft.ingame.chaa.walkspeed.Walkspeed;
import de.ced.doedelcraft.util.Logger;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class GenuineUserConstructor implements CharConstructor {

	private User user;
	private Player player;

	private UserUI tempUI;

	GenuineUserConstructor() {
	}

	@Override
	public void setConstructionData(Char chaa, Entity entity) {
		user = (User) chaa;
		player = (Player) entity;
	}

	@Override
	public void construct() {
	}

	@Override
	public Minecraft getMinecraft() {
		return new GenuineUserMinecraft(user.getCharManager().getDoedelcraft(), player);
	}

	@Override
	public Handler getHandler() {
		return new GenuineUserHandler(user);
	}

	@Override
	public Input getInput() {
		return new GenuineUserInput(user);
	}

	@Override
	public Identity getIdentity() {
		return new GenuineUserIdentity(user);
	}

	@Override
	public ClazzActions getClazzActions() {
		ClazzActions clazzActions = null;
		try {
			clazzActions = (ClazzActions) user.getClazz().getActionsClass().getConstructor(Char.class).newInstance(user);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			Logger.severe("ClazzActions instantiation error");
			e.printStackTrace();
		}
		return clazzActions;
	}

	@Override
	public SpellInfo getSpellInfo() {
		return new GenuineUserSpellInfo(user);
	}

	@Override
	public Effects getEffects() {
		return new GenuineUserEffects(user);
	}

	@Override
	public Progress getProgress() {
		return new GenuineUserProgress(user);
	}

	@Override
	public Health getHealth() {
		return new GenuineUserHealth(user);
	}

	@Override
	public Exhaustion getExhaustion() {
		return new GenuineUserExhaustion(user);
	}

	@Override
	public Mana getMana() {
		return new GenuineUserMana(user);
	}

	@Override
	public Walkspeed getWalkspeed() {
		return new GenuineUserWalkspeed(user);
	}

	@Override
	public Combat getCombat() {
		return new GenuineUserCombat(user);
	}

	@Override
	public Stuff getStuff() {
		return new GenuineUserStuff(user);
	}

	@Override
	public Defence getDefence() {
		return new GenuineUserDefence(user);
	}

	@Override
	public Chat getChat() {
		return new GenuineUserChat(user);
	}

	@Override
	public LastTick getLastTick() {
		return new GenuineUserLastTick(user);
	}
}
