package de.ced.doedelcraft.ingame.chaa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.ingame.clazz.Clazz;

@Deprecated
public class NPCFile implements Data {

	private final Doedelcraft doedelcraft;

	public NPCFile(Doedelcraft doedelcraft) {
		this.doedelcraft = doedelcraft;
	}

	@Override
	public String getName() {
		return "NPC";
	}

	@Override
	public Clazz getClazz() {
		return doedelcraft.getCharManager().getClazzManager().getClazzByName("human");
	}

	@Override
	public boolean isCombatMode() {
		return false;
	}

	@Override
	public void setCombatMode(boolean combatMode) {
	}

	@Override
	public List<Integer> getSpellLevel() {
		return Arrays.asList(new Integer[6]);
	}

	@Override
	public void setSpellLevel(List<Integer> spellLevel) {
	}

	@Override
	public List<Integer> getSelectedSpellLevel() {
		return Arrays.asList(new Integer[6]);
	}

	@Override
	public void setSelectedSpellLevel(List<Integer> spellLevel) {
	}

	@Override
	public List<List<Integer>> getSpellStrength() {
		List<List<Integer>> list = new ArrayList<>();
		for (int i = 0; i < 6; i++) {
			list.add(new ArrayList<Integer>());
		}
		return list;
	}

	@Override
	public void setSpellStrength(List<List<Integer>> spellStrength) {
	}

	@Override
	public List<List<Integer>> getSelectedSpellStrength() {
		List<List<Integer>> list = new ArrayList<>();
		for (int i = 0; i < 6; i++) {
			list.add(new ArrayList<Integer>());
		}
		return list;
	}

	@Override
	public void setSelectedSpellStrength(List<List<Integer>> selectedSpellStrength) {
	}

	@Override
	public int getLevel() {
		return 0;
	}

	@Override
	public void setLevel(int level) {
	}

	@Override
	public double getProgress() {
		return 0;
	}

	@Override
	public void setProgress(double progress) {
	}

	@Override
	public double getExperience() {
		return 0;
	}

	@Override
	public void setExperience(double experience) {
	}

	@Override
	public double getX() {
		return 0;
	}

	@Override
	public void setX(double x) {
	}

	@Override
	public double getY() {
		return 0;
	}

	@Override
	public void setY(double y) {
	}

	@Override
	public double getZ() {
		return 0;
	}

	@Override
	public void setZ(double z) {
	}

	@Override
	public double getYaw() {
		return 0;
	}

	@Override
	public void setYaw(double yaw) {
	}

	@Override
	public double getPitch() {
		return 0;
	}

	@Override
	public void setPitch(double pitch) {
	}

	@Override
	public List<List<Integer>> getSpellCooldown() {
		return null;
	}

	@Override
	public void setSpellCooldown(List<List<Integer>> spellCooldown) {
	}
}
