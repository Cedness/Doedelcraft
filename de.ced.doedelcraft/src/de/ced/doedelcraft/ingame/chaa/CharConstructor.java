package de.ced.doedelcraft.ingame.chaa;

import org.bukkit.entity.Entity;

import de.ced.doedelcraft.ingame.clazz.Clazz;

public interface CharConstructor extends Char {

	void setConstructionData(Char chaa, Entity entity);

	void construct();

	@Override
	default CharManager getCharManager() {
		return null;
	}

	@Override
	default Data getData() {
		return null;
	}

	@Override
	default Clazz getClazz() {
		return null;
	}
}
