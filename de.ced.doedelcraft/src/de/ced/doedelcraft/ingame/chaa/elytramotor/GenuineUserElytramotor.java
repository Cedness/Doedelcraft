package de.ced.doedelcraft.ingame.chaa.elytramotor;

import org.bukkit.Material;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.chaa.User;

public class GenuineUserElytramotor extends AbstractUnit implements UserElytramotor, Tickable {

	private final ItemStack[] resetQueue = new ItemStack[2];
	private int run = 0;

	public GenuineUserElytramotor(User user) {
	}

	@Override
	public void onInteract(PlayerInteractEvent e) {
		ItemStack item = e.getItem();
		if (e.getAction() != Action.RIGHT_CLICK_AIR && e.getAction() != Action.RIGHT_CLICK_BLOCK || !e.hasItem() || item.getType() != Material.FIREWORK_ROCKET) {
			return;
		}
		item.setAmount(2);
		resetQueue[resetQueue.length - 1] = item;
		run = resetQueue.length;
	}

	@Override
	public void tick(int tick, int cycle) {
		if (run <= 0) {
			return;
		}
		if (resetQueue[0] != null) {
			resetQueue[0].setAmount(2);
			run = resetQueue.length;
		}
		for (int i = 1; i < resetQueue.length; i++) {
			resetQueue[i - 1] = resetQueue[i];
		}
		resetQueue[resetQueue.length - 1] = null;
		run--;
	}
}
