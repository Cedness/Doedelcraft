package de.ced.doedelcraft.ingame.chaa.elytramotor;

import org.bukkit.event.player.PlayerInteractEvent;

import de.ced.doedelcraft.ingame.chaa.Unit;

public interface UserElytramotor extends Unit {

	void onInteract(PlayerInteractEvent e);
}
