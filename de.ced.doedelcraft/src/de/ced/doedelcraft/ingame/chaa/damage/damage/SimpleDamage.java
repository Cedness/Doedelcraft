package de.ced.doedelcraft.ingame.chaa.damage.damage;

public class SimpleDamage extends AbstractDamage {

    static SimpleDamage createIdentity() {
        return new SimpleDamage(1);
    }

    public SimpleDamage(long damage) {
        super(damage);
    }
}
