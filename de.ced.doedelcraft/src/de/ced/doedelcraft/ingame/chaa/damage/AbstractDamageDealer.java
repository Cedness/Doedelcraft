package de.ced.doedelcraft.ingame.chaa.damage;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.damage.damage.DamageModifier;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.damage.damage.Damage;

public abstract class AbstractDamageDealer extends AbstractUnit implements DamageDealer {

    private final Char chaa;
    private Damage baseDamage = Damage.identity();

    public AbstractDamageDealer(Char chaa) {
        this.chaa = chaa;
    }

    @Override
    public void dealDamageTo(DamageReceiver damageReceiver, DamageModifier damageModifier) {
        Damage damage = baseDamage.clone();
        damageModifier.applyTo(damage);
        damageReceiver.receiveDamage(damage);
    }
}
