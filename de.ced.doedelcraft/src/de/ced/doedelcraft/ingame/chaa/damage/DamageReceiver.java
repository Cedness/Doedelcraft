package de.ced.doedelcraft.ingame.chaa.damage;

import de.ced.doedelcraft.ingame.chaa.damage.damage.Damage;

public interface DamageReceiver {

    void receiveDamage(Damage damage);
}
