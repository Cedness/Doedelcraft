package de.ced.doedelcraft.ingame.chaa.damage;

import de.ced.doedelcraft.ingame.chaa.Unit;
import de.ced.doedelcraft.ingame.chaa.damage.damage.DamageModifier;

public interface DamageDealer extends Unit {

    void dealDamageTo(DamageReceiver damageReceiver, DamageModifier damageModifier);
}
