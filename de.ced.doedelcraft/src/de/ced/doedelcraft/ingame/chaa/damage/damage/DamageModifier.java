package de.ced.doedelcraft.ingame.chaa.damage.damage;

public interface DamageModifier {

    static DamageModifier identity() {
        return SimpleDamageModifier.createIdentity();
    }

    void applyTo(Damage damage);
}
