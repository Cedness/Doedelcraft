package de.ced.doedelcraft.ingame.chaa.damage.damage;

public abstract class AbstractDamageModifier implements DamageModifier {

    protected final double factor;

    public AbstractDamageModifier(double factor) {
        this.factor = factor;
    }

    @Override
    public void applyTo(Damage damage) {
        damage.setDamage(Math.round(damage.getDamage() * factor));
    }
}
