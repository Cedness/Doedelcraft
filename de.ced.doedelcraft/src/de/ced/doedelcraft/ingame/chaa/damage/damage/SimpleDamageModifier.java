package de.ced.doedelcraft.ingame.chaa.damage.damage;

public class SimpleDamageModifier extends AbstractDamageModifier {

    public static DamageModifier createIdentity() {
        return new SimpleDamageModifier(1);
    }

    public SimpleDamageModifier(double factor) {
        super(factor);
    }
}
