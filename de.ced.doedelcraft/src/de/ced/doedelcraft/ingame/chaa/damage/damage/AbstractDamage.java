package de.ced.doedelcraft.ingame.chaa.damage.damage;

public class AbstractDamage implements Damage, Cloneable {

    protected long damage;

    public AbstractDamage(long damage) {
        this.damage = damage;
    }

    @Override
    public long getDamage() {
        return damage;
    }

    @Override
    public void setDamage(long damage) {
        this.damage = damage;
    }

    @Override
    public Damage clone() {
        try {
            return (Damage) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
