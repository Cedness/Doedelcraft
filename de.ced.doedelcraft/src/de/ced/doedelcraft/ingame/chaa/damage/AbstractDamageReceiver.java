package de.ced.doedelcraft.ingame.chaa.damage;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.health.Health;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.damage.damage.Damage;
import de.ced.doedelcraft.ingame.chaa.damage.damage.DamageModifier;

public abstract class AbstractDamageReceiver extends AbstractUnit implements DamageReceiver {

    private final Char chaa;
    private DamageModifier defenceMultiplier = DamageModifier.identity();

    public AbstractDamageReceiver(Char chaa) {
        this.chaa = chaa;
    }

    @Override
    public void receiveDamage(Damage damage) {
        defenceMultiplier.applyTo(damage);
        Health health = chaa.getHealth();
        health.set((int) (health.get() - damage.getDamage()));
    }
}
