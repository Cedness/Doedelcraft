package de.ced.doedelcraft.ingame.chaa.damage.damage;

public interface Damage {

    static Damage identity() {
        return SimpleDamage.createIdentity();
    }

    long getDamage();

    void setDamage(long damage);

    Damage clone();
}
