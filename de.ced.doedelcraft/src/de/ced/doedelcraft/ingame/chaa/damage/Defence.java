package de.ced.doedelcraft.ingame.chaa.damage;

import de.ced.doedelcraft.ingame.chaa.CastedSpell;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.Unit;
import de.ced.logic.vector.Vector3;

public interface Defence extends Unit {

	void addModifier(CombatType combatType, Object key, double value);

	void removeModifier(CombatType combatType, Object key);

	void damage(double damage, CombatType combatType, Vector3 velocity);

	void damage(double damage, CombatType combatType);

	void punch(Vector3 velocity);

	Char getLastDamager();

	CastedSpell getLastDamagingSpell();

	int getLastDamageTick();

	void setLastDamager(Char chaa);

	void setLastDamager(CastedSpell castedSpell);
}
