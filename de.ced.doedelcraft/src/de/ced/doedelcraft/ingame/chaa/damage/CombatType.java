package de.ced.doedelcraft.ingame.chaa.damage;

import org.bukkit.Material;

public enum CombatType {

	GENERIC("Generic", "f", null,
			"Affects all types of combat."),
	PHYSICAL("Physical", "c", null,
			"Physical combat which uses",
			"weapons or just hands."),
	KINETIC("Kinetic", "e", null,
			"This is the knockback dealt by a hit,",
			"but also every other type of",
			"combat that moves enemies around."),
	ENERGY("Energy", "b", null,
			"Every type of combat which",
			"uses a media to transport",
			"energy that does the dirty job,",
			"like fire or light do."),
	MENTAL("Mental", "d", null,
			"A rarely seen form of combat",
			"which attacks the thoughts of",
			"the opponent.");

	private final String name;
	private final String color;
	private final Material material;
	private final String[] description;

	CombatType(String name, String color, Material material, String... description) {
		this.name = name;
		this.color = color;
		this.material = material;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public String getColor() {
		return color;
	}

	public Material getMaterial() {
		return material;
	}

	public String[] getDescription() {
		return description;
	}
}
