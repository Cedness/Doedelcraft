package de.ced.doedelcraft.ingame.chaa.damage;

import java.util.HashMap;
import java.util.Map;

import de.ced.doedelcraft.ingame.chaa.health.Health;
import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.CastedSpell;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.logic.vector.Vector3;

public abstract class AbstractDefence extends AbstractUnit implements Defence {

	private final Char chaa;
	private final Map<CombatType, Map<Object, Double>> modifiers = new HashMap<>();
	private final Map<CombatType, Double> modifierValues = new HashMap<>();
	private Char lastDamager;
	private CastedSpell lastDamagingSpell;
	private int lastDamageTick = Integer.MIN_VALUE;

	public AbstractDefence(Char chaa) {
		this.chaa = chaa;
		for (CombatType combatType : CombatType.values()) {
			modifiers.put(combatType, new HashMap<>());
			modifierValues.put(combatType, 1.0);
		}
	}

	@Override
	public void addModifier(CombatType combatType, Object key, double value) {
		removeModifier(combatType, key);
		if (value < 0) {
			value = 0;
		}
		modifiers.get(combatType).put(key, value);
		setValue(combatType, modifierValues.get(combatType) * value);
	}

	@Override
	public void removeModifier(CombatType combatType, Object key) {
		Map<Object, Double> modifiers = this.modifiers.get(combatType);
		if (!modifiers.containsKey(key)) {
			return;
		}
		modifiers.remove(key);

		double result = 1;
		for (double value : modifiers.values()) {
			result *= value;
		}
		setValue(combatType, result);
	}

	private void setValue(CombatType combatType, double value) {
		modifierValues.put(combatType, Math.max(0.5, value));
	}

	@Override
	public void damage(double damage, CombatType combatType, Vector3 direction) {
		damage(damage, combatType);
		punch(direction);
	}

	@Override
	public void damage(double damage, CombatType combatType) {
		double defence = modifierValues.get(CombatType.GENERIC);
		// Logger.info("" + defence);
		if (combatType != null && combatType != CombatType.GENERIC) {
			// Logger.info("" + modifierValues.get(combatType));
			defence *= modifierValues.get(combatType);
		}
		// Logger.info("" + defence);
		damage /= Math.max(0.5, defence);
		int finalDamage = (int) damage;
		if (finalDamage != damage) {
			finalDamage++;
		}

		// Logger.info(name + " " + finalDamage);
		Health health = chaa.getHealth();
		health.set(health.get() - finalDamage);

		setLastDamager((Char) null);
		lastDamageTick = getTick();
	}

	@Override
	public void punch(Vector3 direction) {
		direction.mul(1 / modifierValues.get(CombatType.KINETIC));
		chaa.getMinecraft().addVelocity(direction);
	}

	@Override
	public Char getLastDamager() {
		return lastDamager;
	}

	@Override
	public CastedSpell getLastDamagingSpell() {
		return lastDamagingSpell;
	}

	@Override
	public int getLastDamageTick() {
		return lastDamageTick;
	}

	@Override
	public void setLastDamager(Char chaa) {
		setLastDamager(chaa, null);
	}

	@Override
	public void setLastDamager(CastedSpell castedSpell) {
		setLastDamager(castedSpell.getChar(), castedSpell);
	}

	private void setLastDamager(Char chaa, CastedSpell castedSpell) {
		lastDamagingSpell = castedSpell;
		lastDamager = chaa;
	}
}
