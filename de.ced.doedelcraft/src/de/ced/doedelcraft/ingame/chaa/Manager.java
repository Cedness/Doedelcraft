package de.ced.doedelcraft.ingame.chaa;

import de.ced.doedelcraft.Doedelcraft;

public abstract class Manager {

	protected final Doedelcraft doedelcraft;
	protected final CharManager charManager;

	public Manager(CharManager charManager) {
		this.charManager = charManager;
		this.doedelcraft = charManager.getDoedelcraft();
	}

	public int getTick() {
		return doedelcraft.getTick();
	}

	public Doedelcraft getDoedelcraft() {
		return doedelcraft;
	}

	public CharManager getCharManager() {
		return charManager;
	}
}
