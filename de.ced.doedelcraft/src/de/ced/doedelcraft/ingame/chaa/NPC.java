package de.ced.doedelcraft.ingame.chaa;

import org.bukkit.entity.Entity;

/**
 * Represents a {@link Char} who is no player.
 * 
 * @author Ced
 */
@Deprecated
public class NPC implements Removable {

	private final Entity entity;
	private final NPManager npManager;

	public NPC(Entity entity, NPManager npManager) {
		this.entity = entity;
		this.npManager = npManager;
	}

	public NPManager getNpManager() {
		return npManager;
	}

	@Override
	public void remove() {
		entity.remove();
	}
}
