package de.ced.doedelcraft.ingame.chaa;

import de.ced.doedelcraft.account.Account;
import de.ced.doedelcraft.ingame.chaa.chat.Chat;
import de.ced.doedelcraft.ingame.chaa.clazzactions.ClazzActions;
import de.ced.doedelcraft.ingame.chaa.combat.Combat;
import de.ced.doedelcraft.ingame.chaa.damage.Defence;
import de.ced.doedelcraft.ingame.chaa.effects.Effects;
import de.ced.doedelcraft.ingame.chaa.exhaustion.Exhaustion;
import de.ced.doedelcraft.ingame.chaa.handler.Handler;
import de.ced.doedelcraft.ingame.chaa.health.Health;
import de.ced.doedelcraft.ingame.chaa.identity.Identity;
import de.ced.doedelcraft.ingame.chaa.input.Input;
import de.ced.doedelcraft.ingame.chaa.lasttick.LastTick;
import de.ced.doedelcraft.ingame.chaa.mana.Mana;
import de.ced.doedelcraft.ingame.chaa.minecraft.Minecraft;
import de.ced.doedelcraft.ingame.chaa.progress.Progress;
import de.ced.doedelcraft.ingame.chaa.spellinfo.SpellInfo;
import de.ced.doedelcraft.ingame.chaa.stuff.Stuff;
import de.ced.doedelcraft.ingame.chaa.walkspeed.Walkspeed;
import de.ced.doedelcraft.ingame.clazz.Clazz;
import de.ced.doedelcraft.menu.MenuPlayer;

/**
 * Represents any player- or non-player-ingame-character. So it has a race and a
 * clazz and all laws of physics apply to it. A char which is a player, is a
 * {@link User}, all other chars are {@link NPC}s. {@link MenuPlayer}s are of
 * course no chars or users. Any {@link Account}/player can have multiple chars,
 * but only one can be loaded at a time. For offline/saved chars, see
 * {@link Data}.
 * 
 * @author Ced
 */
public interface Char extends Unit {

	CharManager getCharManager();

	Data getData();

	Minecraft getMinecraft();

	Handler getHandler();

	Input getInput();

	Identity getIdentity();

	Clazz getClazz();

	ClazzActions getClazzActions();

	SpellInfo getSpellInfo();

	Effects getEffects();

	Progress getProgress();

	Health getHealth();

	Exhaustion getExhaustion();

	Mana getMana();

	Walkspeed getWalkspeed();

	Combat getCombat();

	Stuff getStuff();

	Defence getDefence();

	Chat getChat();

	LastTick getLastTick();
}
