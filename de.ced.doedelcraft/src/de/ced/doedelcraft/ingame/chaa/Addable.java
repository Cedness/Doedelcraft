package de.ced.doedelcraft.ingame.chaa;

public interface Addable extends Unit {

	void add();

	default Class<?>[] addableDependencies() {
		return new Class<?>[] {};
	}
}
