package de.ced.doedelcraft.ingame.chaa.progress;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.Data;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.chaa.stat.StatLike;

public abstract class AbstractProgress extends AbstractUnit implements Progress, Tickable {

	protected static final int MAX_LEVEL = 2000;

	private final Char chaa;
	protected double progress;
	protected double lastExperience;
	protected int level;
	protected int lastLevel;
	protected double experience;

	protected final Progress progressBar;
	protected final Level levelBar;

	public AbstractProgress(Char chaa) {
		this.chaa = chaa;
		Data data = chaa.getData();
		progress = data.getProgress();
		level = data.getLevel();
		lastLevel = -1;
		experience = data.getProgress();
		lastExperience = -1;

		progressBar = new Progress();
		levelBar = new Level();
	}

	@Override
	public void tick(int tick, int cycle) {
		if (experience != lastExperience) {
			displayProgress();
			lastExperience = experience;
			if (level != lastLevel) {
				displayLevel();
				lastLevel = level;

				chaa.getMana().calcMax();
			}
		}
	}

	protected abstract void displayProgress();

	protected abstract void displayLevel();

	/**
	 * The usual way to give experience to a char
	 * 
	 * @param exp the amount of experience
	 */
	@Override
	public void giveExp(double exp) {
		if (exp <= 0) {
			return;
		}
		exp /= 365.0; // TODO
		experience += exp;
		progress += exp;
		while (progress >= 1) {
			progress--;
			if (level < MAX_LEVEL) {
				level++;
				outputLevelup();
			}
		}
		Data data = chaa.getData();
		data.setExperience(experience);
		data.setProgress(progress);
		data.setLevel(level);
	}

	protected abstract void outputLevelup();

	@Override
	public double getProgress() {
		return progress;
	}

	private void setProgress(double progress) {
		this.progress = progress;
		chaa.getData().setProgress(progress);
		lastExperience = -1;
	}

	@Override
	public int getLevel() {
		return level;
	}

	private void setLevel(int level) {
		this.level = level;
		chaa.getData().setLevel(level);
		lastExperience = -1;
	}

	@Override
	public double getExperience() {
		return experience;
	}

	@SuppressWarnings("unused")
	private void setExperience(double experience) {
		this.experience = experience;
		chaa.getData().setExperience(experience);
	}

	@Override
	public Progress getProgressBar() {
		return progressBar;
	}

	@Override
	public Level getLevelBar() {
		return levelBar;
	}

	public class Progress implements StatLike {
		private Progress() {
		}

		@Override
		public int get() {
			return Math.min((int) (getProgress() * (getMax() + 1)), getMax());
		}

		@Override
		public void set(int amount) {
			setProgress(amount / (double) (getMax() + 1));
		}

		@Override
		public int getMin() {
			return 0;
		}

		@Override
		public int getMax() {
			return 364;
		}
	}

	public class Level implements StatLike {
		private Level() {
		}

		@Override
		public int get() {
			return getLevel();
		}

		@Override
		public void set(int amount) {
			setLevel(amount);
		}

		@Override
		public int getMin() {
			return 1;
		}

		@Override
		public int getMax() {
			return MAX_LEVEL;
		}
	}
}
