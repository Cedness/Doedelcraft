package de.ced.doedelcraft.ingame.chaa.progress;

import java.util.ArrayList;
import java.util.List;

import de.ced.doedelcraft.ingame.chaa.User;

public class GenuineUserProgress extends AbstractProgress implements UserProgress {

	private static final int[] LEVELS = { 1, 10, 50, 100, 1000 };
	private static final int[] DURATIONS = { 2, 6, 7, 15, 60 * 10 };
	private static final String[] SOUNDS = {
			"levelup",
			"levelup.10",
			"levelup.50",
			"levelup.100",
			"levelup.1000"
	};

	private final User user;
	private final List<Integer> levelups = new ArrayList<>();
	private int levelupTick = 0;

	public GenuineUserProgress(User user) {
		super(user);
		this.user = user;
	}

	@Override
	protected void displayProgress() {
		user.getUI().getProgressBar().setCurrent(progress);
	}

	@Override
	protected void displayLevel() {
		user.getUI().getTopLabel().setAge(level);
	}

	@Override
	public void tick(int tick, int cycle) {
		super.tick(tick, cycle);
		if (levelups.isEmpty() || tick < levelupTick) {
			return;
		}
		int level = levelups.remove(0); // TODO translate
		String text = "§e§l§m==========================§f\n\n\n\n           §6Congratulations!\n    §6You are now §e§l" + level
				+ "§6 years old!§f\n\n\n\n§e§l§m==========================";
		user.getMinecraft().message(text);
		for (int i = LEVELS.length - 1; i >= 0; i--) {
			if (level % LEVELS[i] != 0) {
				continue;
			}
			user.getEffects().play(SOUNDS[i]);
			levelupTick = tick + 20 * DURATIONS[i];
			break;
		}
	}

	@Override
	protected void outputLevelup() {
		if (levelups.isEmpty() && levelupTick < getTick()) {
			levelupTick = getTick() + 1;
		}
		levelups.add(level);
	}
}
