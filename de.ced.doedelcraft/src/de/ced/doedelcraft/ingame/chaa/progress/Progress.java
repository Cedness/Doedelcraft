package de.ced.doedelcraft.ingame.chaa.progress;

import de.ced.doedelcraft.ingame.chaa.Unit;
import de.ced.doedelcraft.ingame.chaa.progress.AbstractProgress.Level;

public interface Progress extends Unit {

	public void giveExp(double exp);

	public double getProgress();

	public int getLevel();

	public double getExperience();

	AbstractProgress.Progress getProgressBar();

	Level getLevelBar();
}
