package de.ced.doedelcraft.ingame.chaa.effects;

public interface UserEffects extends Effects {

	public boolean isBlind();

	public void setBlind(boolean blind);

	public boolean getDarkSky();

	public void setDarkSky(boolean darkSky);

	public double getBlood();

	public void setBlood(double value);
}
