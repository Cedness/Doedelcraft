package de.ced.doedelcraft.ingame.chaa.effects;

import de.ced.doedelcraft.ingame.sound.SuperSound;
import de.ced.doedelcraft.ingame.chaa.Unit;
import de.ced.doedelcraft.ingame.sound.SuperSoundCategory;
import de.ced.logic.vector.Vector3;

public interface Effects extends Unit {

	public void play(String sound);

	public void play(SuperSound superSound);

	public void play(SuperSoundCategory category, String sound);

	public void play(String sound, Vector3 location);

	public void play(SuperSound superSound, Vector3 location);

	public void play(SuperSoundCategory category, String sound, Vector3 location);
}
