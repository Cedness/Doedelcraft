package de.ced.doedelcraft.ingame.chaa.effects;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_16_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;

import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.health.GenuineUserHealth;
import de.ced.doedelcraft.ingame.sound.SuperSoundCategory;
import de.ced.logic.vector.Vector5;
import net.minecraft.server.v1_16_R2.PacketPlayOutWorldBorder;
import net.minecraft.server.v1_16_R2.PacketPlayOutWorldBorder.EnumWorldBorderAction;
import net.minecraft.server.v1_16_R2.PlayerConnection;
import net.minecraft.server.v1_16_R2.WorldBorder;

public class GenuineUserEffects extends AbstractEffects implements UserEffects {

	private static final WorldBorder border;
	static {
		border = new WorldBorder();
		border.setSize(50000);
		border.setCenter(0, 0);
		border.setDamageAmount(0);
		border.setDamageBuffer(1);
		border.setWarningDistance(0);
		border.setWarningTime(1);
		border.world = ((CraftWorld) Bukkit.getWorlds().get(0)).getHandle(); // TODO remove static bukkit access
	}

	private final User user;
	private boolean blind = false;
	private boolean darkSky = false;
	private double blood = -1;

	/**
	 * No dependencies
	 */
	public GenuineUserEffects(User user) {
		super(user);
		this.user = user;
		play(SuperSoundCategory.UI, user.getClazz().getLoginSound());
	}

	@Override
	public void tick(int tick, int cycle) {
		super.tick(tick, cycle);
		if (blood > 0 && ((GenuineUserHealth) user.getHealth()).getMinecraft() > 4) {
			setBlood(blood - 1);
		}
	}

	@Override
	public boolean isBlind() {
		return blind;
	}

	@Override
	public void setBlind(boolean blind) {
		if (this.blind == blind) {
			return;
		}
		this.blind = blind;
		if (blind) {
			// FIXME create PotionEffects functionality in Minecraft
			// player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 1000000,
			// 0, false, false, false));
		} else {
			// player.removePotionEffect(PotionEffectType.BLINDNESS);
		}
	}

	@Override
	public boolean getDarkSky() {
		return darkSky;
	}

	@Override
	public void setDarkSky(boolean darkSky) {
		if (this.darkSky == darkSky) {
			return;
		}
		this.darkSky = darkSky;
		user.getMinecraft().setBossbarDarkSky(darkSky);
	}

	@Override
	public double getBlood() {
		return blood;
	}

	@Override
	public void setBlood(double value) {
		blood = value;
		Vector5 location = user.getMinecraft().getLocation();
		border.setCenter(location.x(), location.z());
		border.setWarningDistance((int) (value * 2000));
		sendBorder(EnumWorldBorderAction.INITIALIZE);
		sendBorder(EnumWorldBorderAction.SET_SIZE);
		sendBorder(EnumWorldBorderAction.SET_CENTER);
		sendBorder(EnumWorldBorderAction.SET_WARNING_BLOCKS);
	}

	@SuppressWarnings("deprecation")
	private void sendBorder(EnumWorldBorderAction action) {
		PacketPlayOutWorldBorder packet = new PacketPlayOutWorldBorder(border, action);
		PlayerConnection connection = ((CraftPlayer) user.getMinecraft().getEntity()).getHandle().playerConnection;
		connection.sendPacket(packet);
	}
}
