package de.ced.doedelcraft.ingame.chaa.effects;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.ced.doedelcraft.ingame.chaa.health.Health;
import de.ced.doedelcraft.ingame.sound.PlayingSound;
import de.ced.doedelcraft.ingame.sound.SuperSound;
import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.minecraft.Minecraft;
import de.ced.doedelcraft.ingame.sound.SoundManager;
import de.ced.doedelcraft.ingame.sound.SuperSoundCategory;
import de.ced.logic.vector.Vector3;

public abstract class AbstractEffects extends AbstractUnit implements Effects, Tickable {

	protected final Char chaa;
	protected final User user;
	protected final SoundManager soundManager;
	protected final List<PlayingSound> playingSounds = new ArrayList<>();

	protected AbstractEffects(Char chaa) {
		this.chaa = chaa;
		user = chaa instanceof User ? (User) chaa : null;
		soundManager = chaa.getCharManager().getSoundManager();
	}

	@Override
	public int[] supportedCycles() {
		return new int[] { LATE };
	}

	@Override
	public Class<?>[] dependencies(int cycle) {
		return new Class<?>[] { Minecraft.class, Health.class };
	}

	@Override
	public void tick(int tick, int cycle) {
		for (Iterator<PlayingSound> iterator = playingSounds.iterator(); iterator.hasNext();) {
			PlayingSound playingSound = iterator.next();
			int time = tick - playingSound.getStartTick();
			if (time < 0) {
				continue;
			}
			SuperSound superSound = playingSound.getSuperSound();
			if (superSound.getDuration() <= time) {
				iterator.remove();
				continue;
			}
			superSound.apply(time, playingSound.hasLocation() ? playingSound.getLocation() : chaa.getMinecraft().getLocation(), user);
		}
	}

	@Override
	public void play(SuperSoundCategory category, String sound) {
		SuperSound superSound = soundManager.getSound(category, sound);
		if (superSound == null) {
			return;
		}
		play(superSound);
	}

	@Override
	public void play(String sound) {
		SuperSound superSound = soundManager.getSound(sound);
		if (superSound == null) {
			return;
		}
		play(superSound);
	}

	@Override
	public void play(SuperSound superSound) {
		if (user == null && !superSound.getCategory().isAmbient()) {
			return;
		}
		add(superSound, null);
	}

	@Override
	public void play(SuperSoundCategory category, String sound, Vector3 location) {
		SuperSound superSound = soundManager.getSound(category, sound);
		if (superSound == null) {
			return;
		}
		play(superSound, location);
	}

	@Override
	public void play(String sound, Vector3 location) {
		SuperSound superSound = soundManager.getSound(sound);
		if (superSound == null) {
			return;
		}
		play(superSound, location);
	}

	@Override
	public void play(SuperSound superSound, Vector3 location) {
		if (user == null && !superSound.getCategory().isAmbient()) {
			return;
		}
		add(superSound, location);
	}

	protected void add(SuperSound superSound, Vector3 location) {
		playingSounds.add(new PlayingSound(superSound, location, getTick() + 1));
	}
}
