package de.ced.doedelcraft.ingame.chaa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.ingame.chaa.handler.Handler;
import de.ced.doedelcraft.ingame.clazz.ClazzManager;
import de.ced.doedelcraft.ingame.piece.PieceManager;
import de.ced.doedelcraft.ingame.sound.SoundManager;
import de.ced.doedelcraft.util.Logger;
import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;

import com.comphenix.protocol.events.PacketEvent;

import de.ced.doedelcraft.ingame.place.PlaceManager;

/**
 * Manages all {@link Char}s
 * 
 * @author Ced
 */
public class CharManager {

	private final Doedelcraft doedelcraft;

	private final UserManager userManager;
	private final NPManager npManager;
	private final PlaceManager placeManager;
	private final PieceManager pieceManager;
	private final SoundManager soundManager;
	private final ClazzManager clazzManager;

	private final Map<Entity, Char> chars = new HashMap<>();

	private final List<CastedSpell> castedSpells = new ArrayList<>();

	public CharManager(Doedelcraft doedelcraft) {
		this.doedelcraft = doedelcraft;
		userManager = new UserManager(this);
		npManager = new NPManager(this);
		placeManager = new PlaceManager(this);
		pieceManager = new PieceManager(this);
		soundManager = new SoundManager(this);
		clazzManager = new ClazzManager(this);
	}

	public void tick(int tick) {
		for (int cycle = 0; cycle < Tickable.getHighestCycle(); cycle++) {
			for (Char chaa : chars.values()) {
				try {
					((Tickable) chaa).tick(tick, cycle);
				} catch (Exception e) {
					String name;
					try {
						name = chaa.toString();
					} catch (Exception e2) {
						name = "<unknown>";
						e2.printStackTrace();
					}
					Logger.severe("Tick " + tick + " , cycle " + cycle + ": Exception in CharManager for Char " + name);
					e.printStackTrace();
				}
			}
		}
		soundManager.tick(tick);
	}

	public void add(Char chaa) {
		chars.put(chaa.getMinecraft().getEntity(), chaa);
	}

	public void remove(Char chaa) {
		chars.remove(chaa.getMinecraft().getEntity());
	}

	public int getTick() {
		return doedelcraft.getTick();
	}

	public Doedelcraft getDoedelcraft() {
		return doedelcraft;
	}

	public UserManager getUserManager() {
		return userManager;
	}

	public NPManager getNPManager() {
		return npManager;
	}

	public PlaceManager getPlaceManager() {
		return placeManager;
	}

	public PieceManager getPieceManager() {
		return pieceManager;
	}

	public SoundManager getSoundManager() {
		return soundManager;
	}

	public ClazzManager getClazzManager() {
		return clazzManager;
	}

	public List<CastedSpell> getCastedSpells() {
		return castedSpells;
	}

	public Char getChar(Entity entity) {
		return chars.get(entity);
	}

	public boolean hasChar(Entity entity) {
		return chars.containsKey(entity);
	}

	public Collection<Char> getChars() {
		return chars.values();
	}

	// EVENTS

	private Handler getHandler(Entity entity) {
		return getChar(entity).getHandler();
	}

	@SuppressWarnings("unused")
	private void launchEvent(Entity entity, PacketEvent e, Runnable launchableEvent) {
		launchEvent(entity, "ProtocolLib/PacketEvent", launchableEvent);
	}

	private void launchEvent(Entity entity, Event e, Runnable launchableEvent) {
		launchEvent(entity, e.getEventName(), launchableEvent);
	}

	private void launchEvent(Entity entity, String eventName, Runnable launchableEvent) {
		try {
			launchableEvent.run();
		} catch (Exception ex) {
			try {
				Char chaa = getChar(entity);
				Logger.severe("Exception while executing event " + eventName + " for chaa " + chaa.getIdentity().getName());
			} catch (Exception ex2) {
				Logger.severe("Exception while printing exception");
				ex2.printStackTrace();
			}
			ex.printStackTrace();
		}
	}

	public void onDamage(EntityDamageEvent e) {
		Entity entity = e.getEntity();
		if (!hasChar(entity)) {
			return;
		}
		launchEvent(entity, e, () -> {
			getHandler(entity).onDamage(e);
		});
	}

	public void onRegainHealth(EntityRegainHealthEvent e) {
		Entity entity = e.getEntity();
		if (!hasChar(entity)) {
			return;
		}
		launchEvent(entity, e, () -> {
			getHandler(entity).onRegainHealth(e);
		});
	}
}
