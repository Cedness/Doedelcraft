package de.ced.doedelcraft.ingame.chaa.spellinfo;

import java.util.ArrayList;
import java.util.List;

import de.ced.doedelcraft.ingame.chaa.User;

public class GenuineUserSpellInfo extends AbstractSpellInfo implements UserSpellInfo {

	private final User user;
	private List<Integer> spellOrder;

	public GenuineUserSpellInfo(User user) {
		super(user);
		this.user = user;
	}

	@Override
	protected void load() {
		super.load();
		spellOrder = growSpellInfoList(((User) chaa).getData().getSpellOrder());
		List<Integer> copy = new ArrayList<>(spellOrder);
		boolean valid = true;
		for (Integer /* IMPORTANT that it is no int */ mapping : spellOrder.subList(0, getSpellCount())) {
			if (!copy.remove(mapping)) {
				valid = false;
				break;
			}
		}
		if (!valid || !copy.isEmpty()) {
			// RESET
			for (int spellId = 0; spellId < spellOrder.size(); spellId++) {
				spellOrder.set(spellId, spellId);
			}
		}
	}

	private List<Integer> growSpellInfoList(List<Integer> list) {
		if (list == null) {
			list = new ArrayList<>();
		}
		return growList(list, getSpellCount(), Integer.MIN_VALUE);
	}

	@Override
	protected void save() {
		super.save();
		user.getData().setSpellOrder(spellOrder);
	}

	@Override
	public int getSpellId(int slotId) {
		return spellOrder.size() > slotId ? spellOrder.get(slotId) : -1;
	}

	@Override
	public int getSlotId(int spellId) {
		return spellOrder.indexOf(spellId);
	}

	@Override
	public void swapSlots(int slotId1, int slotId2) {
		int temp = spellOrder.get(slotId1);
		spellOrder.set(slotId1, spellOrder.set(slotId2, temp));
	}
}
