package de.ced.doedelcraft.ingame.chaa.spellinfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import de.ced.doedelcraft.ingame.clazz.Spell;
import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.Data;
import de.ced.doedelcraft.ingame.chaa.Removable;

public abstract class AbstractSpellInfo extends AbstractUnit implements SpellInfo, Removable {

	protected final Char chaa;
	private int spellId;
	private int spellCount;
	private List<Integer> spellLevel;
	private List<Integer> selectedSpellLevel;
	private List<List<Integer>> spellStrength = new ArrayList<>();
	private List<List<Integer>> selectedSpellStrength = new ArrayList<>();
	private List<List<Integer>> spellCooldown = new ArrayList<>();
	private final List<Boolean> spellState = new ArrayList<>();
	private final Set<Spell> activeSwitchSpells = new HashSet<>();
	protected Map<Spell, Integer> cooldowningSpells = new HashMap<>();

	public AbstractSpellInfo(Char chaa) {
		this.chaa = chaa;
		load();
	}

	protected void load() {
		Data data = chaa.getData();

		spellCount = getSpellCount();

		spellLevel = growLevelList(data.getSpellLevel());
		selectedSpellLevel = growLevelList(data.getSelectedSpellLevel());
		spellStrength = grow2dList(data.getSpellStrength());
		selectedSpellStrength = grow2dList(data.getSelectedSpellStrength());
		spellCooldown = grow2dList(data.getSpellCooldown());

		data.setSpellCooldown(spellCooldown); // Can later only be saved after converting back

		validate();

		data.setSpellLevel(spellLevel);
		data.setSelectedSpellLevel(selectedSpellLevel);
		data.setSpellStrength(spellStrength);
		data.setSelectedSpellStrength(selectedSpellStrength);
	}

	protected final List<Integer> growList(List<Integer> list, int size, int defaultValue) {
		for (int spellId = list.size(); spellId < size; spellId++) {
			list.add(defaultValue == Integer.MIN_VALUE ? spellId : defaultValue);
		}
		return list;
	}

	private List<Integer> growLevelList(List<Integer> list) {
		if (list == null) {
			list = new ArrayList<>();
		}
		return growList(list, spellCount, -1);
	}

	private List<List<Integer>> grow2dList(List<List<Integer>> list) {
		if (list == null) {
			list = new ArrayList<>();
		}
		for (int spellId = list.size(); spellId < spellCount; spellId++) {
			list.add(new ArrayList<>());
		}
		for (int spellId = 0; spellId < spellCount; spellId++) {
			growList(list.get(spellId), getSpellLevelCount(spellId), 0);
		}
		return list;
	}

	private void validate() {
		validateState();
		validateLevel();
		validateStrength();
		validateCooldown();
	}

	private void validateState() {
		for (int spellId = 0; spellId < spellCount; spellId++) {
			spellState.add(false);
		}
	}

	private void validateLevel() {
		for (int spellId = 0; spellId < spellCount; spellId++) {
			int maxLevel = getSpellLevelCount(spellId) - 1;
			setSpellLevel0(spellId, Math.min(getSpellLevel(spellId), maxLevel));
		}
	}

	private void validateStrength() {
		for (int spellId = 0; spellId < spellCount; spellId++) {
			for (int spellLevel = 0; spellLevel < getSpellLevelCount(spellId); spellLevel++) {
				int strength = Math.min(getSpellStrength(spellId, spellLevel), getSpell(spellId, spellLevel).getMaxStrength() - 1);
				if (!setSpellStrength0(spellId, spellLevel, strength)) {
					setSelectedSpellStrength(spellId, spellLevel, Math.min(getSelectedSpellStrength(spellId, spellLevel), strength));
				}
			}
		}
	}

	private void validateCooldown() {
		for (int spellId = 0; spellId < spellCount; spellId++) {
			List<Integer> slotCooldown = spellCooldown.get(spellId);
			for (int spellLevel = 0; spellLevel < getSpellLevelCount(spellId); spellLevel++) {
				Integer cooldown = slotCooldown.get(spellLevel);
				if (cooldown == null) {
					slotCooldown.set(spellLevel, cooldown = 0);
				}
				if (cooldown > 0) {
					setSpellCooldown(getSpell(spellId, spellLevel), cooldown);
				}
			}
		}
	}

	@Override
	public Class<?>[] removableDependencies() {
		return new Class<?>[] { Data.class };
	}

	@Override
	public void remove() {
		save();
	}

	protected void save() {
		Data data = chaa.getData();
		List<List<Integer>> saveSpellCooldown = new ArrayList<>();
		for (int spellId = 0; spellId < spellCount; spellId++) {
			List<Integer> saveSlotCooldown = new ArrayList<>();
			saveSpellCooldown.add(saveSlotCooldown);
			for (int spellLevel = 0; spellLevel < getSpellLevelCount(spellId); spellLevel++) {
				saveSlotCooldown.add(getSpellCooldown(getSpell(spellId, spellLevel)));
			}
		}
		data.setSpellCooldown(saveSpellCooldown);

		data.setSelectedSpellLevel(selectedSpellLevel);
		data.setSelectedSpellStrength(selectedSpellStrength);
	}

	@Override
	public void reload() {
		save();
		load();
	}

	@Override
	public boolean hasSpells() {
		return getSpellCount() > 0;
	}

	@Override
	public int getSpellCount() {
		return chaa.getClazz().getSpells().size();
	}

	@Override
	public boolean hasSelectedSpell() {
		return spellId >= 0;
	}

	@Override
	public int getSelectedSpellId() {
		return spellId;
	}

	@Override
	public void setSelectedSpellId(int spellId) {
		// this.spellId = Math.min(Math.max(-1, spellId), spellCount - 1);
		this.spellId = spellId;
	}

	@Override
	public int getSpellId(int slotId) {
		return slotId;
	}

	@Override
	public int getSlotId(int spellId) {
		return spellId;
	}

	@Override
	public void swapSlots(int slotId1, int slotId2) {
	}

	@Override
	public boolean isSpellUnlocked(int spellId) {
		return getSpellLevel(spellId) >= 0;
	}

	@Override
	public int getSpellLevelCount(int spellId) {
		return chaa.getClazz().getSpells(spellId).size();
	}

	@Override
	public int getSpellLevel(int spellId) {
		return spellLevel.get(spellId);
	}

	private boolean setSpellLevel0(int spellId, int level) {
		int oldLevel = spellLevel.set(spellId, level);
		if (oldLevel != level) {
			if (level < getSelectedSpellLevel(spellId)) {
				setSelectedSpellLevel(spellId, level);
			} else if (oldLevel == -1) {
				setSelectedSpellLevel(spellId, 0);
			}
			// FIXME display changes
			return true;
		}
		return false;
	}

	@Override
	public void setSpellLevel(int spellId, int level) {
		if (setSpellLevel0(spellId, level)) {
			chaa.getData().setSpellLevel(spellLevel);
		}
	}

	@Override
	public int getSelectedSpellLevel(int spellId) {
		if (spellId < 0) {
			return -1; // TODO makes sense?
		}
		return selectedSpellLevel.get(spellId);
	}

	@Override
	public void setSelectedSpellLevel(int spellId, int level) {
		selectedSpellLevel.set(spellId, level);
	}

	/**
	 * IMPORTANT!!! Uses the spell-level which is currently selected by the user!
	 */
	@Override
	public Spell getSpell(int spellId) {
		return getSpell(spellId, getSelectedSpellLevel(spellId));
	}

	@Override
	public Spell getSpell(int spellId, int level) {
		return chaa.getClazz().getSpell(spellId, level);
	}

	@Override
	public int getSpellStrengthCount(int spellId, int level) {
		return getSpell(spellId, level).getMaxStrength();
	}

	@Override
	public int getSpellStrength(int spellId, int level) {
		return spellStrength.get(spellId).get(level);
	}

	private boolean setSpellStrength0(int spellId, int level, int strength) {
		int oldStrength = spellStrength.get(spellId).set(level, strength);
		if (oldStrength != strength) {
			if (strength < getSelectedSpellStrength(spellId, level)) {
				setSelectedSpellStrength(spellId, level, strength);
			}
			// FIXME display changes
			return true;
		}
		return false;
	}

	@Override
	public void setSpellStrength(int spellId, int level, int strength) {
		if (setSpellStrength0(spellId, level, strength)) {
			chaa.getData().setSpellStrength(spellStrength);
		}
	}

	@Override
	public int getSelectedSpellStrength(int spellId, int level) {
		return selectedSpellStrength.get(spellId).get(level);
	}

	@Override
	public void setSelectedSpellStrength(int spellId, int level, int strength) {
		selectedSpellStrength.get(spellId).set(level, strength);
	}

	private int checkCooldown(Spell spell, int cooldown) {
		if (cooldown < 0) {
			cooldowningSpells.remove(spell);
			spellCooldown.get(spell.getSpellId()).set(spell.getSpellLevel(), 0);
			return 0;
		}
		return cooldown;
	}

	@Override
	public Iterable<Spell> getCooldowningSpells() {
		return CooldownIterator::new;
	}

	public class CooldownIterator implements Iterator<Spell> {
		Iterator<Entry<Spell, Integer>> iterator = cooldowningSpells.entrySet().iterator();
		Spell spell = null;
		int cooldown = 0;

		@Override
		public boolean hasNext() {
			if (spell != null) {
				return true;
			}
			while (iterator.hasNext()) {
				Entry<Spell, Integer> entry = iterator.next();
				spell = entry.getKey();
				cooldown = entry.getValue();
				if (cooldown - getTick() > 0) {
					return true;
				} else {
					iterator.remove();
					spellCooldown.get(spell.getSpellId()).set(spell.getSpellLevel(), 0);
				}
			}
			spell = null;
			cooldown = 0;
			return false;
		}

		@Override
		public Spell next() {
			if (spell == null && !hasNext()) {
				return null;
			}
			Spell spell = this.spell;
			this.spell = null;
			return spell;
		}

		public int getCooldown() {
			return cooldown;
		}

		@Override
		public void remove() {
			iterator.remove();
			spell = null;
		}
	}

	@Override
	public boolean hasCooldowningSpells() {
		return getCooldowningSpells().iterator().hasNext();
	}

	@Override
	public boolean isCooldowningSpell(Spell spell) {
		return getSpellCooldown(spell) > 0;
	}

	@Override
	public int getSpellCooldown(Spell spell) {
		Integer over = cooldowningSpells.get(spell);
		return over != null ? checkCooldown(spell, over - getTick()) : 0;
	}

	@Override
	public void setSpellCooldown(Spell spell, int cooldown) {
		if ((cooldown = checkCooldown(spell, cooldown)) < 0) {
			return;
		}
		cooldown += getTick();
		cooldowningSpells.put(spell, cooldown);
		spellCooldown.get(spell.getSpellId()).set(spell.getSpellLevel(), cooldown);
	}

	@Override
	public void removeSpellCooldown(Spell spell) {
		cooldowningSpells.remove(spell);
		spellCooldown.get(spell.getSpellId()).set(spell.getSpellLevel(), 0);
	}

	@Override
	public Iterable<Spell> getActiveSpells() {
		return () -> new Iterator<Spell>() {
			Iterator<Spell> iterator = activeSwitchSpells.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public Spell next() {
				return iterator.next();
			}

			@Override
			public void remove() {
				Iterator.super.remove();
			}
		};
	}

	@Override
	public boolean getSpellState(Spell spell) {
		return activeSwitchSpells.contains(spell);
	}

	@Override
	public boolean getSpellState(int spellId) {
		return spellState.get(spellId);
	}

	@Override
	public void setSpellState(int spellId, int level, boolean state) {
		spellState.set(spellId, state);
		if (state) {
			activeSwitchSpells.add(getSpell(spellId, level));
		} else {
			activeSwitchSpells.remove(getSpell(spellId, level));
		}
	}
}
