package de.ced.doedelcraft.ingame.chaa.spellinfo;

public class Cooldown {

	private final int start;
	private final int duration;

	Cooldown(int start, int duration) {
		this.start = start;
		this.duration = duration;
	}

	public int getStart() {
		return start;
	}

	public int getDuration() {
		return duration;
	}

	public boolean isValid(int tick) {
		return start + duration < tick;
	}

	public int getLeft(int tick) {
		return start + duration - tick;
	}
}
