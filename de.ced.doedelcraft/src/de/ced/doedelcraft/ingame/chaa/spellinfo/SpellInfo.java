package de.ced.doedelcraft.ingame.chaa.spellinfo;

import de.ced.doedelcraft.ingame.clazz.Spell;
import de.ced.doedelcraft.ingame.chaa.Unit;

public interface SpellInfo extends Unit {

	/**
	 * This method is called on construction and on clazz change
	 */
	void reload();

	boolean hasSpells();

	int getSpellCount();

	/**
	 * getSelectedSpellId() > 0
	 */
	boolean hasSelectedSpell();

	int getSelectedSpellId();

	void setSelectedSpellId(int spellId);

	int getSpellId(int slotId);

	int getSlotId(int spellId);

	void swapSlots(int slotId1, int slotId2);

	boolean isSpellUnlocked(int spellId);

	int getSpellLevelCount(int spellId);

	int getSpellLevel(int spellId);

	void setSpellLevel(int spellId, int level);

	int getSelectedSpellLevel(int spellId);

	void setSelectedSpellLevel(int spellId, int level);

	Spell getSpell(int spellId);

	Spell getSpell(int spellId, int level);

	int getSpellStrengthCount(int spellId, int level);

	int getSpellStrength(int spellId, int level);

	void setSpellStrength(int spellId, int level, int strength);

	int getSelectedSpellStrength(int spellId, int level);

	void setSelectedSpellStrength(int spellId, int level, int strength);

	/**
	 * This call is more expensive than you think.
	 */
	boolean hasCooldowningSpells();

	Iterable<Spell> getCooldowningSpells();

	boolean isCooldowningSpell(Spell spell);

	int getSpellCooldown(Spell spell);

	void setSpellCooldown(Spell spell, int cooldown);

	void removeSpellCooldown(Spell spell);

	Iterable<Spell> getActiveSpells();

	boolean getSpellState(Spell spell);

	boolean getSpellState(int spellId);

	void setSpellState(int spellId, int level, boolean state);
}
