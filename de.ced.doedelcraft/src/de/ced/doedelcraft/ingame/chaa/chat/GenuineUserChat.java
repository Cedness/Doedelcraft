package de.ced.doedelcraft.ingame.chaa.chat;

import de.ced.doedelcraft.util.Logger;
import org.bukkit.ChatColor;

import de.ced.doedelcraft.ingame.chaa.User;

public class GenuineUserChat extends AbstractChat implements UserChat {

	private static final String SYMBOLS = ".,-:;_#+'*~<>|@€µ”“„„¢^!\"§$%&/()=?´°¹²³{[]}\\…·–";

	public GenuineUserChat(User user) {
		super(user);
	}

	public void onChat(String message) {
		char[] letters = ChatColor.stripColor(message).toCharArray();
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < letters.length; i++) {
			char letter = letters[i];
			if (Character.isLetterOrDigit(letter) || Character.isSpaceChar(letter) || SYMBOLS.indexOf(letter) >= 0) {
				builder.append(letter);
			}
		}
		speak(builder.toString());
	}

	@Override
	protected void outputSpeak(String message) {
		Logger.info(chaa + ": " + message);
	}

	@Override
	protected void outputHear(String message) {
		chaa.getMinecraft().message(message);
	}
}
