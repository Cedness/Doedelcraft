package de.ced.doedelcraft.ingame.chaa.chat;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.logic.math.Meth;
import de.ced.logic.vector.Vector3;

public abstract class AbstractChat extends AbstractUnit implements Chat {

	protected final Char chaa;
	private static final int VOICE_RANGE = Meth.pow2(30);
	private static final int UNDERSTANDABLE_RANGE = Meth.pow2(20);

	public AbstractChat(Char chaa) {
		this.chaa = chaa;
	}

	@Override
	public void speak(String message) {
		outputSpeak(message);
		String format = (chaa.getCombat().isCombatMode() ? chaa.getClazz().getColor() : "§7") + chaa.getIdentity().getName() + "§7: §f§o";
		Vector3 location = chaa.getMinecraft().getLocation().location();

		for (User anotherUser : chaa.getCharManager().getUserManager().getUsers()) {
			anotherUser.getChat().hearRanged(format, message, location);
		}
	}

	protected void outputSpeak(String message) {
	}

	@Override
	public void hearRanged(String format, String message, Vector3 source) {
		double distance = source.distanceSquared(chaa.getMinecraft().getLocation());

		if (distance > VOICE_RANGE) {
			return;
		}

		StringBuilder builder = new StringBuilder().append(format);
		if (distance > UNDERSTANDABLE_RANGE) {
			distance = (distance - UNDERSTANDABLE_RANGE) / (VOICE_RANGE - UNDERSTANDABLE_RANGE);

			for (char letter : message.toCharArray()) {
				builder.append(Math.random() > distance ? letter : '*');
			}
		} else {
			builder.append(message);
		}
		hear(builder.toString());
	}

	@Override
	public void hear(String message) {
		outputHear(message);
	}

	protected void outputHear(String message) {
	}
}
