package de.ced.doedelcraft.ingame.chaa.chat;

import de.ced.doedelcraft.ingame.chaa.Unit;
import de.ced.logic.vector.Vector3;

public interface Chat extends Unit {

	public void speak(String message);

	public void hearRanged(String format, String message, Vector3 source);

	public void hear(String message);
}
