package de.ced.doedelcraft.ingame.chaa.inputpreprocess;

import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryType.SlotType;

import de.ced.doedelcraft.ingame.chaa.Unit;

public interface UserInputPreprocess extends Unit {

	void onHeldItemChange(boolean[] cancelled, int[] newSlot, int previousSlot);

	void onSwapHandItems(boolean[] cancelled);

	void onAnimation();

	void onInteract(boolean[] useItemInHand, boolean[] useInteractedBlock, boolean right);

	void onDrop(boolean[] cancelled, boolean[] removeItem, int slotId, boolean droppedAll, int maxStackSize);

	void onInventoryClick(boolean[] cancelled, int slotId, int hotbarButtonId, SlotType slotType, ClickType clickType, InventoryAction inventoryAction);

	void onSneak(boolean[] cancelled, boolean sneaking);
}
