package de.ced.doedelcraft.ingame.chaa.inputpreprocess;

import de.ced.doedelcraft.ingame.chaa.input.UserInput;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.combat.GenuineUserCombat;
import de.ced.doedelcraft.ingame.chaa.spellinfo.SpellInfo;

public class GenuineUserInputPreprocess extends AbstractUnit implements UserInputPreprocess {

	private final User user;

	/**
	 * Dependencies: Minecraft, Handler
	 */
	public GenuineUserInputPreprocess(User user) {
		this.user = user;
		user.getMinecraft().setHeldItemSlot(8);
	}

	private boolean usesInput() {
		return user.getClazz().usesInput();
	}

	private UserInput getInput() {
		return user.getInput();
	}

	// Event methods

	@Override
	public void onHeldItemChange(boolean[] cancelled, int[] newSlot, int previousSlot) {
		if (!usesInput()) {
			return;
		}
		if (newSlot[0] == 8) {
			return;
		}
		getInput().onNumKey(newSlot[0]);
		if (previousSlot != 8) {
			newSlot[0] = 8;
		} else {
			cancelled[0] = true;
		}
	}

	@Override
	public void onSwapHandItems(boolean[] cancelled) {
		cancelled[0] = true;
		getInput().onFKey();
	}

	@Override
	public void onAnimation() {
		getInput().onLeftClick();
	}

	@Override
	public void onInteract(boolean[] useItemInHand, boolean[] useInteractedBlock, boolean right) {
		if (!right) {
			return;
		}
		getInput().onRightClick(useItemInHand);
	}

	@Override
	public void onInventoryClick(boolean[] cancelled, int slotId, int hotbarButtonId, SlotType slotType, ClickType clickType, InventoryAction inventoryAction) {
		if (!usesInput()) {
			return;
		}
		// Logger.info(e.getAction().name() + " " + e.isShiftClick() + " " +
		// e.getClick().name() + " " + e.getSlotType().name());
		if (slotType != InventoryType.SlotType.QUICKBAR) {
			if (clickType == ClickType.NUMBER_KEY) {
				cancelled[0] = true;
			}
			return;
		}
		if (slotId >= 8) {
			cancelled[0] = true;
			return;
		}
		if (inventoryAction != InventoryAction.HOTBAR_SWAP) {
			cancelled[0] = true;
			return;
		}
		// Logger.info(e.getHotbarButton() + " " + e.getSlot() + " " + e.getRawSlot());
		getInput().onInvNumClick(slotId, hotbarButtonId);
	}

	private void fixDropBug(boolean[] cancelled, boolean[] removeItem, int slotId, int maxStackSize) {
		if (slotId >= 8) {
			cancelled[0] = true;
			return;
		}
		SpellInfo spellInfo = user.getSpellInfo();
		int spellId = spellInfo.getSpellId(slotId);
		int level = spellInfo.getSpellLevel(spellId) + 1;
		if (maxStackSize < level) {
			removeItem[0] = true;
			GenuineUserCombat combat = (GenuineUserCombat) user.getCombat();
			combat.getSpellItem(spellId).setAmount(level);
			combat.applySpellItem(spellId);
			cancelled[0] = false;
		} else {
			cancelled[0] = true;
		}
	}

	@Override
	public void onDrop(boolean[] cancelled, boolean[] removeItem, int slotId, boolean ctrl, int maxStackSize) {
		if (!usesInput() || user.getStuff().isInventoryOpen()) {
			Bukkit.broadcastMessage("cycling cancelled");
			return;
		}
		fixDropBug(cancelled, removeItem, slotId, maxStackSize);
		getInput().onQKey(ctrl);
	}

	@Override
	public void onSneak(boolean[] cancelled, boolean sneaking) {
		if (!usesInput()) {
			return;
		}
		cancelled[0] = true;
		getInput().onShift(sneaking);
		user.getWalkspeed().setShifting(sneaking);
	}
}
