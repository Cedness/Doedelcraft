package de.ced.doedelcraft.ingame.chaa.stat;

public interface StatLike {

	int get();

	void set(int amount);

	default void set(int amount, boolean setReset) {
		set(amount);
	}

	int getMin();

	int getMax();
}
