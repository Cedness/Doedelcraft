package de.ced.doedelcraft.ingame.chaa.stat;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.GenuineUser;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.chaa.User;

public abstract class AbstractStat extends AbstractUnit implements Stat, StatLike, Tickable {

	protected final Char chaa;
	protected final String path;
	protected ConfigurationSection config;
	protected int startTick;
	protected int current = 20;
	protected int last;
	protected int min = 0;
	protected int max = 20;
	protected double regen = 0.01;
	protected int interval = 1;

	protected AbstractStat(Char chaa, String path) {
		this.chaa = chaa;
		this.path = path;
		if (path != null && chaa instanceof User) {
			YamlConfiguration parent = ((GenuineUser) chaa).getData().getConfig(); // FIXME make value access for bars abstract
			config = parent.getConfigurationSection(path);
		}
		setStartTick(false);
	}

	protected void prepare() {
		setMax(max);
		set(config == null ? max : config.getInt("current"));
		last = -1;
		setRegen(regen);
	}

	protected boolean regen(int tick) {
		if ((tick - startTick) % interval == 0) {
			int regen = (int) this.regen;
			set(current + (regen == 0 ? this.regen < 0 ? -1 : 1 : regen));
			return true;
		}
		return false;
	}

	@Override
	public void tick(int tick, int cycle) {
		if (current == (regen > 0 ? max : min)) {
			setStartTick(false);
		}
		regen(tick);

		if (hasChanged()) {
			if (config != null) {
				config.set("current", current);
			}
			int current = this.current; // may be modified by apply()
			apply();
			last = current;
		}
	}

	@Override
	public int get() {
		return current;
	}

	@Override
	public void set(int amount) {
		current = Math.max(0, Math.min(amount, max));
	}

	@Override
	public void set(int amount, boolean setReset) {
		set(amount);
		setStartTick(setReset);
	}

	@Override
	public int getMin() {
		return min;
	}

	@Override
	public int getMax() {
		return max;
	}

	@Override
	public void setMax(int max) {
		this.max = Math.max(0, max);
		current = Math.min(current, this.max);
	}

	@Override
	public double getRegen() {
		return regen;
	}

	@Override
	public void setRegen(double regen) {
		this.regen = regen;
		interval = regen == 0 ? Integer.MAX_VALUE : Math.max(1, (int) (1 / Math.abs(regen)));
	}

	@Override
	public int getInterval() {
		return interval;
	}

	@Override
	public void setStartTick(boolean setReset) {
		startTick = getTick() + (setReset ? 1 : -1);
	}

	@Override
	public boolean hasChanged() {
		return last != current;
	}

	protected abstract void apply();
}
