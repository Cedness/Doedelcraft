package de.ced.doedelcraft.ingame.chaa.stat;

import de.ced.doedelcraft.ingame.chaa.Unit;

public interface Stat extends StatLike, Unit {

	/**
	 * @return The current value hold by this bar
	 */
	@Override
	int get();

	/**
	 * WARNING! This changes the value of the Bar directly. Usually this is not the
	 * common way to change its value, preprocessing may be bypassed.
	 */
	@Override
	void set(int amount);

	/**
	 * WARNING! This changes the value of the Bar directly. Usually this is not the
	 * common way to change its value, preprocessing may be bypassed.
	 * 
	 * Like {@link set(int amount)}, but sets or resets the start tick for regen.
	 * 
	 * @param setReset true for set, false for reset
	 */
	void set(int amount, boolean setReset);

	/**
	 * @return the minimum value which can be hold by this bar
	 */
	@Override
	int getMin();

	/**
	 * @return the current maximum value which can be hold by this bar
	 */
	@Override
	int getMax();

	/**
	 * @param max the current maximum value which can be hold by this bar
	 */
	void setMax(int max);

	/**
	 * @return the value regenerated per tick
	 */
	double getRegen();

	/**
	 * If this value is beyond 1, a larger interval is set to accommodate this.
	 * 
	 * @param regen the value regenerated per tick
	 */
	void setRegen(double regen);

	/**
	 * The interval is used internal to translate the regen to ticks, since no
	 * floating point numbers are used for values.
	 * 
	 * @return the interval in ticks
	 */
	int getInterval();

	/**
	 * Sets the start tick so that the next regen a) happens immediately or b) has
	 * just happened, so that the next regen happens not until {@link getInterval}
	 * ticks are over.
	 * 
	 * @param setReset true for set, false for reset
	 */
	void setStartTick(boolean setReset);

	/**
	 * @return if the value just changed and so will be displayed with the next tick
	 */
	boolean hasChanged();
}
