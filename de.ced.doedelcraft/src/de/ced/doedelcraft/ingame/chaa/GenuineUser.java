package de.ced.doedelcraft.ingame.chaa;

import de.ced.doedelcraft.account.Account;
import de.ced.doedelcraft.account.Language;
import de.ced.doedelcraft.ingame.chaa.chat.GenuineUserChat;
import de.ced.doedelcraft.ingame.chaa.chunkloader.GenuineUserChunkLoader;
import de.ced.doedelcraft.ingame.chaa.combat.GenuineUserCombat;
import de.ced.doedelcraft.ingame.chaa.damage.GenuineUserDefence;
import de.ced.doedelcraft.ingame.chaa.effects.GenuineUserEffects;
import de.ced.doedelcraft.ingame.chaa.exhaustion.GenuineUserExhaustion;
import de.ced.doedelcraft.ingame.chaa.glow.GenuineUserGlow;
import de.ced.doedelcraft.ingame.chaa.handler.GenuineUserHandler;
import de.ced.doedelcraft.ingame.chaa.health.GenuineUserHealth;
import de.ced.doedelcraft.ingame.chaa.identity.GenuineUserIdentity;
import de.ced.doedelcraft.ingame.chaa.input.GenuineUserInput;
import de.ced.doedelcraft.ingame.chaa.inputpreprocess.GenuineUserInputPreprocess;
import de.ced.doedelcraft.ingame.chaa.lasttick.GenuineUserLastTick;
import de.ced.doedelcraft.ingame.chaa.mana.GenuineUserMana;
import de.ced.doedelcraft.ingame.chaa.minecraft.GenuineUserMinecraft;
import de.ced.doedelcraft.ingame.chaa.positioner.GenuineUserPositioner;
import de.ced.doedelcraft.ingame.chaa.progress.GenuineUserProgress;
import de.ced.doedelcraft.ingame.chaa.spellinfo.GenuineUserSpellInfo;
import de.ced.doedelcraft.ingame.chaa.stuff.GenuineUserStuff;
import de.ced.doedelcraft.ingame.chaa.ui.GenuineUserUI;
import de.ced.doedelcraft.ingame.chaa.walkspeed.GenuineUserWalkspeed;
import de.ced.doedelcraft.util.Logger;

public class GenuineUser extends AbstractChar implements User {

	private final UserManager userManager;

	// TOPIC ordering here

	private final Account account;
	private final Language language;

	private final GenuineUserInputPreprocess inputPreprocess;
	private final GenuineUserUI ui;
	private final GenuineUserGlow glow;
	private final GenuineUserChunkLoader chunkLoader;
	private final GenuineUserPositioner positioner;

	public GenuineUser(UserManager userManager, UserData userData) {
		super(userManager.getCharManager(), userData, new GenuineUserConstructor(), userData.getAccount().getPlayer());

		this.userManager = userManager;
		account = (Account) addUnit(userData.getAccount());
		language = account.getLanguage();
		account.setUser(this);
		userData.setLogin();

		// INSTANTIATION order here

		inputPreprocess = (GenuineUserInputPreprocess) addUnit(new GenuineUserInputPreprocess(this));
		ui = (GenuineUserUI) addUnit(new GenuineUserUI(this));
		glow = (GenuineUserGlow) addUnit(new GenuineUserGlow(this));
		chunkLoader = (GenuineUserChunkLoader) addUnit(new GenuineUserChunkLoader(this));
		positioner = (GenuineUserPositioner) addUnit(new GenuineUserPositioner(this));

		// PacketTools.setName(player, name, userManager.getDoedelcraft());
		Logger.info(userData.getAccount().getName() + " logged in as '" + getIdentity().getName() + "'");

		finishUnitAdding();
	}

	@Override
	public void remove() {
		super.remove();
		Logger.info(account.getName() + " logged out, data has been saved");
	}

	// INFO

	@Override
	public String toString() {
		return getClazz().getName() + " user " + getIdentity().getName() + " of player " + account.getName();
	}

	// Getter

	@Override
	public UserManager getUserManager() {
		return userManager;
	}

	@Override
	public Account getAccount() {
		return account;
	}

	@Override
	public Language getLanguage() {
		return language;
	}

	@Override
	public GenuineUserInputPreprocess getInputPreprocess() {
		return inputPreprocess;
	}

	@Override
	public GenuineUserUI getUI() {
		return ui;
	}

	@Override
	public GenuineUserGlow getGlow() {
		return glow;
	}

	@Override
	public GenuineUserPositioner getPositioner() {
		return positioner;
	}

	@Override
	public GenuineUserChunkLoader getChunkLoader() {
		return chunkLoader;
	}

	@Override
	public UserData getData() {
		return (UserData) super.getData();
	}

	@Override
	public GenuineUserMinecraft getMinecraft() {
		return (GenuineUserMinecraft) super.getMinecraft();
	}

	@Override
	public GenuineUserHandler getHandler() {
		return (GenuineUserHandler) super.getHandler();
	}

	@Override
	public GenuineUserInput getInput() {
		return (GenuineUserInput) super.getInput();
	}

	@Override
	public GenuineUserSpellInfo getSpellInfo() {
		return (GenuineUserSpellInfo) super.getSpellInfo();
	}

	@Override
	public GenuineUserIdentity getIdentity() {
		return (GenuineUserIdentity) super.getIdentity();
	}

	@Override
	public GenuineUserEffects getEffects() {
		return (GenuineUserEffects) super.getEffects();
	}

	@Override
	public GenuineUserProgress getProgress() {
		return (GenuineUserProgress) super.getProgress();
	}

	@Override
	public GenuineUserHealth getHealth() {
		return (GenuineUserHealth) super.getHealth();
	}

	@Override
	public GenuineUserExhaustion getExhaustion() {
		return (GenuineUserExhaustion) super.getExhaustion();
	}

	@Override
	public GenuineUserMana getMana() {
		return (GenuineUserMana) super.getMana();
	}

	@Override
	public GenuineUserWalkspeed getWalkspeed() {
		return (GenuineUserWalkspeed) super.getWalkspeed();
	}

	@Override
	public GenuineUserCombat getCombat() {
		return (GenuineUserCombat) super.getCombat();
	}

	@Override
	public GenuineUserStuff getStuff() {
		return (GenuineUserStuff) super.getStuff();
	}

	@Override
	public GenuineUserDefence getDefence() {
		return (GenuineUserDefence) super.getDefence();
	}

	@Override
	public GenuineUserChat getChat() {
		return (GenuineUserChat) super.getChat();
	}

	@Override
	public GenuineUserLastTick getLastTick() {
		return (GenuineUserLastTick) super.getLastTick();
	}
}
