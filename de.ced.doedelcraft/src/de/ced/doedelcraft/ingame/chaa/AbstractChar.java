package de.ced.doedelcraft.ingame.chaa;

import de.ced.doedelcraft.ingame.chaa.chat.AbstractChat;
import de.ced.doedelcraft.ingame.chaa.clazzactions.AbstractClazzActions;
import de.ced.doedelcraft.ingame.chaa.combat.AbstractCombat;
import de.ced.doedelcraft.ingame.chaa.damage.AbstractDefence;
import de.ced.doedelcraft.ingame.chaa.effects.AbstractEffects;
import de.ced.doedelcraft.ingame.chaa.exhaustion.AbstractExhaustion;
import de.ced.doedelcraft.ingame.chaa.handler.AbstractHandler;
import de.ced.doedelcraft.ingame.chaa.health.AbstractHealth;
import de.ced.doedelcraft.ingame.chaa.identity.AbstractIdentity;
import de.ced.doedelcraft.ingame.chaa.input.AbstractInput;
import de.ced.doedelcraft.ingame.chaa.lasttick.AbstractLastTick;
import de.ced.doedelcraft.ingame.chaa.mana.AbstractMana;
import de.ced.doedelcraft.ingame.chaa.minecraft.AbstractMinecraft;
import de.ced.doedelcraft.ingame.chaa.progress.AbstractProgress;
import de.ced.doedelcraft.ingame.chaa.spellinfo.AbstractSpellInfo;
import de.ced.doedelcraft.ingame.chaa.stuff.AbstractStuff;
import de.ced.doedelcraft.ingame.chaa.walkspeed.AbstractWalkspeed;
import de.ced.doedelcraft.util.Logger;
import org.bukkit.entity.Entity;

import de.ced.doedelcraft.ingame.clazz.Clazz;

public abstract class AbstractChar extends AbstractUnitManager implements Char {

	private final CharManager charManager;
	private final Data data;
	private final AbstractMinecraft minecraft;
	private final AbstractHandler handler;
	private final AbstractInput input;
	private final AbstractIdentity identity;
	private final Clazz clazz;
	private final AbstractClazzActions clazzActions;
	private final AbstractSpellInfo spellInfo;

	private final AbstractEffects effects;
	private final AbstractProgress experience;
	private final AbstractHealth health;
	private final AbstractExhaustion exhaustion;
	private final AbstractMana mana;
	private final AbstractWalkspeed walkspeed;
	private final AbstractCombat combat;
	private final AbstractStuff stuff;
	private final AbstractDefence defence;
	private final AbstractChat chat;
	private final AbstractLastTick lastTick;

	@SuppressWarnings("unchecked")
	public AbstractChar(CharManager charManager, Data data, CharConstructor charConstructor, Entity entity) {
		this.charManager = charManager;
		this.data = (Data) addUnit(data);
		clazz = data.getClazz();
		if (clazz == null) {
			Logger.severe("clazz is null");
		}

		charConstructor.setConstructionData(this, entity);
		charConstructor.construct();
		minecraft = (AbstractMinecraft) addUnit(charConstructor.getMinecraft());
		handler = (AbstractHandler) addUnit(charConstructor.getHandler());
		input = (AbstractInput) addUnit(charConstructor.getInput());
		identity = (AbstractIdentity) addUnit(charConstructor.getIdentity());
		clazzActions = (AbstractClazzActions) addUnit(charConstructor.getClazzActions());
		spellInfo = (AbstractSpellInfo) addUnit(charConstructor.getSpellInfo());
		effects = (AbstractEffects) addUnit(charConstructor.getEffects());
		experience = (AbstractProgress) addUnit(charConstructor.getProgress());
		health = (AbstractHealth) addUnit(charConstructor.getHealth());
		exhaustion = (AbstractExhaustion) addUnit(charConstructor.getExhaustion());
		mana = (AbstractMana) addUnit(charConstructor.getMana());
		walkspeed = (AbstractWalkspeed) addUnit(charConstructor.getWalkspeed());
		combat = (AbstractCombat) addUnit(charConstructor.getCombat());
		stuff = (AbstractStuff) addUnit(charConstructor.getStuff());
		defence = (AbstractDefence) addUnit(charConstructor.getDefence());
		chat = (AbstractChat) addUnit(charConstructor.getChat());
		lastTick = (AbstractLastTick) addUnit(charConstructor.getLastTick());
	}

	@Override
	public String toString() {
		return clazz.getName() + " char " + identity.getName();
	}

	// Getter

	@Override
	public CharManager getCharManager() {
		return charManager;
	}

	@Override
	public Data getData() {
		return data;
	}

	@Override
	public AbstractMinecraft getMinecraft() {
		return minecraft;
	}

	@Override
	public AbstractHandler getHandler() {
		return handler;
	}

	@Override
	public AbstractInput getInput() {
		return input;
	}

	@Override
	public AbstractIdentity getIdentity() {
		return identity;
	}

	@Override
	public Clazz getClazz() {
		return clazz;
	}

	@Override
	public AbstractClazzActions getClazzActions() {
		return clazzActions;
	}

	@Override
	public AbstractSpellInfo getSpellInfo() {
		return spellInfo;
	}

	@Override
	public AbstractEffects getEffects() {
		return effects;
	}

	@Override
	public AbstractProgress getProgress() {
		return experience;
	}

	@Override
	public AbstractHealth getHealth() {
		return health;
	}

	@Override
	public AbstractExhaustion getExhaustion() {
		return exhaustion;
	}

	@Override
	public AbstractMana getMana() {
		return mana;
	}

	@Override
	public AbstractWalkspeed getWalkspeed() {
		return walkspeed;
	}

	@Override
	public AbstractCombat getCombat() {
		return combat;
	}

	@Override
	public AbstractStuff getStuff() {
		return stuff;
	}

	@Override
	public AbstractDefence getDefence() {
		return defence;
	}

	@Override
	public AbstractChat getChat() {
		return chat;
	}

	@Override
	public AbstractLastTick getLastTick() {
		return lastTick;
	}
}
