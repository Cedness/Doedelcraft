package de.ced.doedelcraft.ingame.chaa.chunkloader;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Removable;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.minecraft.Minecraft;
import de.ced.doedelcraft.ingame.world.chunk.ChunkStorage;
import de.ced.logic.vector.Vector5;

public class GenuineUserChunkLoader extends AbstractUnit implements UserChunkLoader, Tickable, Removable {

	private static final int RANGE = 4;

	@SuppressWarnings("unused")
	private final ChunkStorage chunkStorage;
	private final User user;

	/**
	 * Dependencies: Minecraft
	 */
	public GenuineUserChunkLoader(User user) {
		this.user = user;
		chunkStorage = user.getCharManager().getDoedelcraft().getWorldManager().getChunkStorage();
		Vector5 location = user.getMinecraft().getLocation();
		int chunkX = convert(location.x());
		int chunkZ = convert(location.z());
		for (int x = chunkX - RANGE; x < chunkX + RANGE; x++) {
			for (int z = chunkZ - RANGE; z < chunkZ + RANGE; z++) {
				load(x, z, true);
			}
		}
	}

	@Override
	public Class<?>[] removableDependencies() {
		return new Class<?>[] { Minecraft.class };
	}

	@Override
	public void remove() {
		Vector5 location = user.getMinecraft().getLocation();
		int chunkX = convert(location.x());
		int chunkZ = convert(location.z());
		for (int x = chunkX - RANGE; x < chunkX + RANGE; x++) {
			for (int z = chunkZ - RANGE; z < chunkZ + RANGE; z++) {
				load(x, z, false);
			}
		}
	}

	@Override
	public int[] supportedCycles() {
		return new int[] { EARLY };
	}

	@Override
	public Class<?>[] dependencies(int cycle) {
		return new Class<?>[] { Minecraft.class };
	}

	@Override
	public void tick(int tick, int cycle) {
		Vector5 lastLocation = user.getLastTick().getLastLocation();
		Vector5 location = user.getMinecraft().getLocation();
		move(lastLocation.x(), lastLocation.z(), location.x(), location.z());
	}

	private void move(double xFromRaw, double zFromRaw, double xToRaw, double zToRaw) {
		int xFrom = convert(xFromRaw);
		int zFrom = convert(zFromRaw);
		int xTo = convert(xToRaw);
		int zTo = convert(zToRaw);
		int[] from = new int[] { xFrom, zFrom };
		int[] to = new int[] { xTo, zTo };
		for (int axis = 0; axis < 2; axis++) {
			if (from[axis] == to[axis]) {
				continue;
			}
			// Logger.info("Chunk-Move (" + (axis > 0 ? "z" : "x") + ") " + from[axis] +
			// "->" + to[axis]);
			int change = from[axis] < to[axis] ? -1 : 1;
			int[][] data = { from, to };
			int invAxis = (axis + 1) % 2;
			for (int mode = 0; mode < 2; mode++) { // mode 0 = removing, mode 1 = adding
				int m = (mode * 2 - 1) * change; // multiplier; 0 is -1 and 1 is 1
//					int beginValue = data[mode][axis] - RANGE * m;
//					int compareValue = data[(mode + 1) % 2][axis] - RANGE * m;
//					Logger.info("lol" + " " + beginValue + " " + compareValue + " " + m);
				for (int a = data[mode][axis] - RANGE * m; a != data[(mode + 1) % 2][axis] - RANGE * m; a += m) {
					// Logger.info("lul" + a);
					for (int b = data[mode][invAxis] - RANGE; b <= data[mode][invAxis] + RANGE; b++) {
						load(a, b, mode > 0);
					}
					if (a == data[mode][axis] + RANGE * m) {
						break;
					}
				}
			}
		}
	}

	private void load(int x, int z, boolean load) {
		// Logger.info((load ? "Load " : "Unload ") + x + " " + z);
		if (0 > x || x > ChunkStorage.INITIAL_CAPACITY || 0 > z || z > ChunkStorage.INITIAL_CAPACITY) {
			// Logger.warning("Chunk request OUT OF RANGE");
			return;
		}
		if (load) {
			// chunkStorage.getLoadedChunk(a, b).removeUser(User.this);
		} else {
			// chunkStorage.getChunk(a, b).addUser(User.this);
		}
	}

	private int convert(double d) {
		return (int) d >> 4;
	}
}
