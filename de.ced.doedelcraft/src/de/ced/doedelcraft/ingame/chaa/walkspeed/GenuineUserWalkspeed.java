package de.ced.doedelcraft.ingame.chaa.walkspeed;

import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.input.Input;

public class GenuineUserWalkspeed extends AbstractWalkspeed implements UserWalkspeed {

	private final User user;

	public GenuineUserWalkspeed(User user) {
		this.user = user;
	}

	@Override
	public Class<?>[] dependencies(int cycle) {
		return new Class<?>[] { Input.class };
	}

	@Override
	protected void output() {
		user.getMinecraft().setWalkspeed((float) walkspeed);
	}
}
