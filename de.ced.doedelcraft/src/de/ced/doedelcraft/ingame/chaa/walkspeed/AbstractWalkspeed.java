package de.ced.doedelcraft.ingame.chaa.walkspeed;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Tickable;

public abstract class AbstractWalkspeed extends AbstractUnit implements Walkspeed, Tickable {

	private static final double BASE_WALKSPEED = 0.2;
	protected double walkspeed;
	private double exhaustion = 1;
	private double boostMovement = 1;
	private double shifting = 1;

	@Override
	public void tick(int tick, int cycle) {
		if (tick % 63 == 0) {
			output();
		}
	}

	private void apply() {
		walkspeed = BASE_WALKSPEED * exhaustion * boostMovement * shifting;
		output();
	}

	protected abstract void output();

	@Override
	public double getExhaustion() {
		return exhaustion;
	}

	@Override
	public void setExhaustion(double exhaustion) {
		this.exhaustion = exhaustion;
		apply();
	}

	@Override
	public double getBoostMovement() {
		return boostMovement;
	}

	@Override
	public void setBoostMovement(double boostMovement) {
		this.boostMovement = boostMovement;
		apply();
	}

	@Override
	public double getShifting() {
		return shifting;
	}

	@Override
	public void setShifting(boolean shifting) {
		this.shifting = shifting ? 2 : 1;
		apply();
	}

	@Override
	public double getWalkspeed() {
		return walkspeed;
	}
}
