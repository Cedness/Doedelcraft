package de.ced.doedelcraft.ingame.chaa.walkspeed;

import de.ced.doedelcraft.ingame.chaa.Unit;

public interface Walkspeed extends Unit {

	public double getExhaustion();

	public void setExhaustion(double exhaustion);

	public double getBoostMovement();

	public void setBoostMovement(double boostMovement);

	double getShifting();

	void setShifting(boolean shifting);

	public double getWalkspeed();
}
