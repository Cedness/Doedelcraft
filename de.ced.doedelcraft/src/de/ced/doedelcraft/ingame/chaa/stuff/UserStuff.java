package de.ced.doedelcraft.ingame.chaa.stuff;

public interface UserStuff extends Stuff {

	void setAmount(int amount);

	@Deprecated
	void setItemMessage(String message);

	boolean isInventoryOpen();
}
