package de.ced.doedelcraft.ingame.chaa.stuff;

import de.ced.doedelcraft.util.SadItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.DragType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import de.ced.doedelcraft.ingame.chaa.Data;
import de.ced.doedelcraft.ingame.chaa.Removable;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.combat.Combat;
import de.ced.doedelcraft.ingame.chaa.combat.UserCombat;
import de.ced.doedelcraft.ingame.chaa.minecraft.UserMinecraft;
import de.ced.doedelcraft.ingame.chaa.spellinfo.SpellInfo;
import de.ced.doedelcraft.ingame.piece.Bag;
import de.ced.doedelcraft.ingame.piece.Piece;
import de.ced.doedelcraft.ingame.piece.definition.HandPiece;
import de.ced.doedelcraft.ingame.piece.definition.PieceDefinition;

public class GenuineUserStuff extends AbstractStuff implements UserStuff, Removable {

	private final User user;
	private final Inventory settings;
	private boolean inventoryOpen = false;
	private String message;

	private final PlayerInventory playerInventory;
	private Inventory bagInventory;

	private final Bag bag;
	private Bag openBag;

	private Piece flyingPiece;

	public GenuineUserStuff(User user) {
		super(user);
		this.user = user;
		playerInventory = user.getMinecraft().getInventory();

		handItem = new SadItem(Material.SNOW, " ");
		offhandItem = new SadItem(Material.SNOW, "§f§lToggle Combat-Mode");

		settings = Bukkit.createInventory(null, 3 * 9, "§c§lSettings");

		playerInventory.setItem(6, new SadItem(Material.SNOW, " ", "§c Settings ", ""));
		playerInventory.setItem(8, new SadItem(Material.SNOW, " "));

		apply();

		bag = new Bag(3, user.getData().getStuff(), pieceManager);

		bag.applyTo(playerInventory);
	}

	@Override
	public Class<?>[] removableDependencies() {
		return new Class<?>[] { Data.class };
	}

	@Override
	public void remove() {
		user.getData().setStuff(bag.save());
	}

	private void reloadInventory() {
		bag.applyTo(playerInventory);
	}

	@Override
	protected void addDescription(Piece piece, boolean offhand, SadItem item) {
		PieceDefinition definition = piece.getDefinition();
		item.setItem(definition.getMaterial()).setText(offhand ? " " : message, "", "§e§l" + definition.getName());
	}

	@Override
	public void setAmount(int amount) {
		handItem.setAmount(amount);
	}

	@Override
	public void setItemMessage(String message) {
		this.message = message;
		handItem.setName(message);
	}

	@Override
	public void apply() {
		ItemStack actualHandItem = playerInventory.getItem(7);
		if ((actualHandItem != null || handItem != null)
				&& !(actualHandItem != null ? actualHandItem.equals(handItem) : handItem.equals(actualHandItem))) {
			playerInventory.setItem(8, handItem);
		}
		ItemStack actualOffhandItem = playerInventory.getItem(8);
		if ((actualOffhandItem != null || offhandItem != null)
				&& !(actualOffhandItem != null ? actualOffhandItem.equals(offhandItem)
						: offhandItem.equals(actualOffhandItem))) {
			// playerInventory.setItem(8, offhandItem);
		}
	}

	@Override
	public boolean isInventoryOpen() {
		return inventoryOpen;
	}

	private boolean contentClick(Bag bag, int contentId, InventoryClickEvent e) {
		int slot = e.getSlot();
		if (this.bag == bag) {
			slot -= 9;
		}

		ClickType click = e.getClick();
		InventoryAction action = e.getAction();

		int result = Integer.MIN_VALUE;
		Piece cursor = flyingPiece;
		boolean cancel = false;
		switch (action) {
		case PICKUP_ALL:
			cursor = bag.takeFrom(slot);
			break;
		case PICKUP_HALF:
			// Use piece
			Piece piece = bag.get(slot);
			if (piece != null) {
				PieceDefinition definition = piece.getDefinition();
				if (definition.use(user, piece) && definition instanceof HandPiece) {
					equip(piece, handPiece != null && !((HandPiece) definition).isTwoHanded());
				}
			}
			cancel = true;
			break;
		case PLACE_ALL:
			cursor = bag.placeTo(slot, cursor);
			break;
		case PLACE_SOME:
			cursor = bag.placeSomeTo(slot, cursor);
			break;
		case PLACE_ONE:
			cursor = bag.placeOneTo(slot, cursor);
			break;
		case SWAP_WITH_CURSOR:
			cursor = bag.swap(slot, cursor);
			if (bag.getChanged() < 0) {
				if (click == ClickType.LEFT) {
					cursor = bag.placeUnsupportedTo(slot, cursor);
				} else if (click == ClickType.RIGHT) {
					cursor = bag.placeOneUnsupportedTo(slot, cursor);
				}
			}
			break;
		case MOVE_TO_OTHER_INVENTORY:
			if (openBag == null) {
				cancel = true;
				break;
			}
			break;
		case COLLECT_TO_CURSOR:
		case DROP_ALL_CURSOR:
		case DROP_ONE_CURSOR:
		case DROP_ALL_SLOT:
		case DROP_ONE_SLOT:
			cancel = true;
			break;
		case NOTHING:
			if (!bag.has(slot)) {
				break;
			}
			if (click == ClickType.LEFT) {
				cursor = bag.placeUnsupportedTo(slot, cursor);
			} else if (click == ClickType.RIGHT) {
				cursor = bag.placeOneUnsupportedTo(slot, cursor);
			}
			break;
		default:
		}
		if (result == Integer.MIN_VALUE) {
			result = bag.getChanged();
		}
		if (cancel || result <= 0) {
			return true;
		}
		flyingPiece = cursor;
		return false;
	}

	private void spellClick(int slotId, ClickType clickType) {
		boolean ctrl = false;
		UserCombat combat = user.getCombat();
		switch (clickType) {
		case LEFT: {
			int spellId = checkAndPrepare(slotId);
			if (spellId >= 0) {
				user.getCombat().selectSpell(user.getSpellInfo().getSpell(spellId));
			}
		}
			break;
		case RIGHT:
			combat.unselectSpell();
			break;
		case DOUBLE_CLICK:
			user.getInput().onFKey();
			break;
		case CONTROL_DROP:
			ctrl = true;
		case DROP: {
			int spellId = checkAndPrepare(slotId);
			if (spellId >= 0) {
				spellId = select(slotId);
				user.getInput().onQKey(ctrl);
				undoSelect(spellId);
			}
			break;
		}
		case SHIFT_LEFT:
			ctrl = true;
		case SHIFT_RIGHT: {
			int spellId = checkAndPrepare(slotId);
			if (spellId >= 0) {
				spellId = select(slotId);
				user.getInput().onScroll(ctrl);
				undoSelect(spellId);
			}
			break;
		}
		default:
			break;
		}
	}

	private int checkAndPrepare(int slotId) {
		SpellInfo spellInfo = user.getSpellInfo();
		int spellId = spellInfo.getSpellId(slotId);
		return spellInfo.isSpellUnlocked(spellId) ? spellId : -1;
	}

	private int select(int spellId) {
		SpellInfo spellInfo = user.getSpellInfo();
		int formerSpellId = spellInfo.getSelectedSpellId();
		spellInfo.setSelectedSpellId(spellId);
		return formerSpellId;
	}

	private void undoSelect(int spellId) {
		UserCombat combat = user.getCombat();
		if (spellId < 0) {
			combat.unselectSpell();
		} else {
			combat.selectSpell(user.getSpellInfo().getSpell(spellId));
		}
	}

	public void onInventoryClick(InventoryClickEvent e) {
		if (!inventoryOpen) {
			inventoryOpen = true;
		}
		Inventory inventory = e.getClickedInventory();
		int slotId = e.getSlot();
		ClickType clickType = e.getClick();

		Bukkit.broadcastMessage("§e" + e.getSlot() + " §f" + clickType.name().toLowerCase() + " -> "
				+ e.getAction().name().toLowerCase());
		if (inventory == null) {

		} else if (inventory.equals(playerInventory)) {
			UserMinecraft minecraft = user.getMinecraft();
			boolean cancel = true;
			if (slotId < 0) {

			} else if (slotId < 6) {
				spellClick(slotId, clickType);
				cancel = false;
				minecraft.message("§c§oSpell-Display-Slot");
			} else if (slotId < 7) {
				if (clickType == ClickType.LEFT) {
					minecraft.openInventory(settings);
				}
				minecraft.message("§c§oSettings...");
			} else if (slotId < 8) {
				minecraft.message("§c§oHand-Item-Display-Slot");
			} else if (slotId < 9) {
				minecraft.message("§c§oOther-Hand-Item-Display-Slot");
			} else if (slotId < 36) {
				if (clickType != ClickType.NUMBER_KEY) {
					cancel = false;
				}
				cancel = contentClick(bag, slotId - 9, e);
				minecraft.message("§e§oContent-Slot");
			} else if (slotId < 40) {
				minecraft.message("§c§oArmor-Display-Slot");
			} else if (slotId < 41) {
				if (clickType == ClickType.LEFT) {
					Combat combat = user.getCombat();
					combat.toggleCombatMode(!combat.isCombatMode());
				}
				minecraft.message("§c§oOffhand-Display-Slot");
			} else {

			}
			if (cancel) {
				e.setCancelled(true);
			}
		} else if (inventory.equals(bagInventory)) {
			if (contentClick(openBag, slotId, e)) {
				e.setCancelled(true);
			}
		} else {
			e.setCancelled(true);
		}

		// Logger.info("f: " + flyingPiece);
		// bag.print();
	}

	public void onInventoryDrag(InventoryDragEvent e) {
		if (!inventoryOpen) {
			inventoryOpen = true;
		}
		DragType dragType = e.getType();

		Bukkit.broadcastMessage("§e" + e.getRawSlots().size() + " §f" + dragType.name().toLowerCase());

		e.setCancelled(true);
	}

	public void onInventoryOpen(InventoryOpenEvent e) {
		if (!inventoryOpen) {
			inventoryOpen = true;
		}
	}

	public void onInventoryClose(InventoryCloseEvent e) {
		if (inventoryOpen) {
			inventoryOpen = false;
		}
		reloadInventory();
	}

	public void onPickup(EntityPickupItemEvent e) {

	}

	public void onDrop(PlayerDropItemEvent e) {

	}
}
