package de.ced.doedelcraft.ingame.chaa.stuff;

import de.ced.doedelcraft.util.SadItem;
import org.bukkit.Material;
import org.bukkit.inventory.EntityEquipment;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.chaa.ui.UserUI;
import de.ced.doedelcraft.ingame.piece.Piece;
import de.ced.doedelcraft.ingame.piece.PieceManager;
import de.ced.doedelcraft.ingame.piece.definition.HandPiece;
import de.ced.doedelcraft.ingame.piece.definition.PieceDefinition;

public abstract class AbstractStuff extends AbstractUnit implements Stuff, Tickable {

	protected final PieceManager pieceManager;
	protected final Char chaa;

	protected Piece handPiece = null;
	protected Piece offhandPiece = null;

	protected Material beforeMaterial;
	protected SadItem handItem;
	protected SadItem offhandItem;

	public AbstractStuff(Char chaa) {
		this.chaa = chaa;
		pieceManager = chaa.getCharManager().getPieceManager();
	}

	@Override
	public Class<?>[] dependencies(int cycle) {
		return new Class<?>[] { UserUI.class };
	}

	@Override
	public void tick(int tick, int cycle) {
		apply();
	}

	public boolean equip(Piece piece, boolean offhand) {
		PieceDefinition definition = piece.getDefinition();
		if (definition instanceof HandPiece) {
			boolean twoHanded = ((HandPiece) definition).isTwoHanded();
			if (twoHanded) {
				if (offhand) {
					return false;
				}
				offhandPiece = null;
			}

			Material material = definition.getMaterial();
			SadItem item;

			if (offhand) {
				if (offhandItem == null) {
					offhandItem = new SadItem(material, " ");
				} else {
					offhandItem.setType(material);
				}
				item = offhandItem;
				offhandPiece = piece;
			} else {
				if (handItem == null) {
					handItem = new SadItem(material, " ");
				} else {
					handItem.setType(material);
				}
				item = handItem;
				handPiece = piece;
			}

			addDescription(piece, offhand, item);
		}
		return true;
	}

	protected void addDescription(Piece piece, boolean offhand, SadItem item) {
	}

	// returns true when 'apply()' is needed
	@Override
	public boolean setMaterial(Material material) {
		if (material == null) {
			if (beforeMaterial == null) {
				// offhandItem = null; return true;
				material = Material.SNOW;
			} else {
				material = beforeMaterial;
			}
		} else if (beforeMaterial == null) {
			beforeMaterial = offhandItem != null ? offhandItem.getType() : null;
		}

		if (offhandItem == null) {
			offhandItem = new SadItem(material, "§f§lToggle Combat-Mode");
			return true;
		}
		offhandItem.setType(material);
		return false;
	}

	public void apply() {
		EntityEquipment equipment = chaa.getMinecraft().getEquipment();
		if (equipment.getItemInMainHand() != handItem) {
			equipment.setItemInMainHand(handItem);
		}
		if (equipment.getItemInOffHand() != offhandItem) {
			equipment.setItemInOffHand(offhandItem);
		}
	}
}
