package de.ced.doedelcraft.ingame.chaa.health;

import de.ced.doedelcraft.ingame.chaa.stat.AbstractStat;
import org.bukkit.event.entity.EntityDamageEvent.*;

import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.damage.CombatType;
import de.ced.doedelcraft.ingame.chaa.damage.Defence;
import de.ced.doedelcraft.ingame.clazz.Clazz;

public abstract class AbstractHealth extends AbstractStat implements Health {

	protected int minecraft = 1;
	protected int lastDeathTick = -6;
	protected boolean dead = false;

	public AbstractHealth(Char chaa) {
		super(chaa, "health");
		max = chaa.getClazz().getMaxHealth();
		regen = 0.01;
		prepare();
	}

	@Override
	public Class<?>[] dependencies(int cycle) {
		return new Class<?>[] { Defence.class };
	}

	@Override
	protected void apply() {
		double newHealth = 20.0 * (double) current / (double) max;
		minecraft = Math.max(1, (int) newHealth);
	}

	/**
	 * WARNING! This changes the health directly and should only be used to achieve
	 * exactly this. Otherwise use {@code Defence.damage()}.
	 */
	@Override
	public void set(int amount) {
		if (getTick() - lastDeathTick < 10) {
			return;
		}
		if (amount < current && getTick() - 1 != startTick) {
			outputDamaged(current - amount);
		}
		super.set(amount);
		if (current <= 0) {
			dead = true;
		}
	}

	protected abstract void outputDamaged(int damage);

	@Override
	public void setMax(int max) {
		super.setMax(Math.max(1, max));
	}

	@Override
	public void tick(int tick, int cycle) {
		super.tick(tick, cycle);
		int livingTicks = tick - lastDeathTick;
		if (livingTicks == 5) {
			chaa.getMinecraft().setFireTicks(0);
		}
		if (dead) {
			dead = false;
			onDeath();
		}
	}

	/**
	 * @return the current health in Minecraft-heart scale.
	 */
	public int getMinecraft() {
		return minecraft;
	}

	protected abstract void onDeath();

	private void vanillaDamage(double damage, DamageCause cause) {
		Clazz clazz = chaa.getClazz();
		Defence defence = chaa.getDefence();
		switch (cause) {
		case DROWNING:
			damage = clazz.getName().equals("water") ? 0 : damage * 2;
			break;
		case HOT_FLOOR:
			damage *= 1.5;
		case FIRE:
		case FIRE_TICK:
			damage *= 2;
		case LAVA:
			damage = clazz.getName().equals("fire") ? 0 : damage * 1;
			break;
		case FALL:
		case FLY_INTO_WALL:
			damage -= 3;
			break;
		case VOID:
			defence.setLastDamager(chaa);
			set(0);
			break;
		default:
			damage = 0;
		}
		if (damage <= 0) {
			return;
		}
		defence.damage((int) Math.max(0, damage), CombatType.PHYSICAL);
	}

	/**
	 * Handles incoming damage to minecraft. Filters out vanilla damage and
	 * processes it while damage done via the ui is let through.
	 */
	public void onDamage(double[] damage, boolean[] cancelled, double formerHealth, boolean isLiving, DamageCause damageCause) {
		if (damageCause == DamageCause.CUSTOM) {
			// damage[0] = Math.max(0, isLiving ? formerHealth - minecraft : 0);
			if (damage[0] <= 0) {
				// cancelled[0] = true;
			}
		} else {
			vanillaDamage(damage[0], damageCause);
			cancelled[0] = true;
		}
	}

	/**
	 * Prevents unwanted health-regeneration
	 */
	public void onRegainHealth(double[] amount, double formerHealth, boolean isLiving) {
		if (!isLiving) {
			return;
		}
		if (minecraft != formerHealth + amount[0]) {
			amount[0] = minecraft - formerHealth;
		}
	}
}
