package de.ced.doedelcraft.ingame.chaa.health;

import de.ced.doedelcraft.ingame.chaa.mana.Mana;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.combat.UserCombat;
import de.ced.doedelcraft.ingame.chaa.damage.UserDefence;
import de.ced.doedelcraft.ingame.chaa.exhaustion.Exhaustion;
import de.ced.doedelcraft.ingame.chaa.minecraft.UserMinecraft;

public class GenuineUserHealth extends AbstractHealth implements UserHealth {

	private final User user;

	/**
	 * Dependencies: Effects
	 */
	public GenuineUserHealth(User user) {
		super(user);
		this.user = user;
	}

	@Override
	protected void apply() {
		super.apply();
		user.getUI().getHealthBar().setCurrent((double) current / max);
	}

	@Override
	protected void outputDamaged(int damage) {
		if (chaa.getClazz().getName().equals("smoke")) {
			return;
		}
		((User) chaa).getEffects().setBlood(Math.min(damage * 15 + 35, 90));
	}

	@Override
	protected void onDeath() {
		outputPreDeath();

		UserCombat combat = user.getCombat();
		if (combat.isCombatMode()) {
			combat.toggleCombatMode(false, true);
		}
		if (user.getSpellInfo().hasSelectedSpell()) {
			combat.unselectSpell();
		}
		setStartTick(false);
		Exhaustion exhaustion = user.getExhaustion();
		exhaustion.set(exhaustion.getMin(), false);
		Mana mana = user.getMana();
		mana.set(mana.getMin(), false);

		UserMinecraft minecraft = user.getMinecraft();
		minecraft.getVelocity().set(0);
		minecraft.applyVelocity();
		minecraft.getLocation().replace(user.getUserManager().getDoedelcraft().getWorldManager().getWorld().getSpawnLocation()).add(0.5, 1, 0.5);
		minecraft.teleport();

		minecraft.setFireTicks(0);
		lastDeathTick = getTick();

		outputPostDeath();
	}

	protected void outputPreDeath() {
		user.getPositioner().setTeleportReason("§4You died");
		user.getUI().getBottomLabel().setActionbarText("§cBut surprisingly, you survived it", 50);

		String message;
		String broadcast;

		UserDefence defence = user.getDefence();
		String name = user.getIdentity().getName();
		Char killer = defence.getLastDamager();
		if (killer != null && getTick() - defence.getLastDamageTick() < 60) {
			if (killer.equals(user)) {
				message = "You killed yourself";
				broadcast = name + " commited suicide";
			} else {
				String killerName = killer.getIdentity().getName();
				message = "You have been slain by " + killerName;
				broadcast = name + " has been slain by " + killerName;
			}
		} else {
			message = "You died";
			broadcast = name + " died";
		}
		user.getMinecraft().message("§4" + message);
		broadcast = "§c" + broadcast;
		for (User receiver : user.getUserManager().getUsers()) {
			if (!user.equals(receiver)) {
				receiver.getMinecraft().message(broadcast);
			}
		}
	}

	protected void outputPostDeath() {
		user.getEffects().play("user.death");
	}
}
