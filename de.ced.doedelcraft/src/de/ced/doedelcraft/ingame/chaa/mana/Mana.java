package de.ced.doedelcraft.ingame.chaa.mana;

import de.ced.doedelcraft.ingame.chaa.stat.Stat;

public interface Mana extends Stat {

	void calcMax();

	void calcRegen();
}
