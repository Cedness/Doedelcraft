package de.ced.doedelcraft.ingame.chaa.mana;

import de.ced.doedelcraft.ingame.chaa.effects.Effects;
import de.ced.doedelcraft.ingame.chaa.User;

public class GenuineUserMana extends AbstractMana implements UserMana {

	private static final String empty = "mana.empty";
	private static final String full = "mana.full";
	private static final String regenerating = "mana.regenerating";
	private static final String regeneratingInterrupt = "mana.regenerating.interrupt";
	private static final String degenerating = "mana.degenerating";
	private final User user;

	/**
	 * {@inheritDoc}
	 * Dependencies: Progress, Effects
	 */
	public GenuineUserMana(User user) {
		super(user);
		this.user = user;
	}

	@Override
	protected void apply() {
		super.apply();
		user.getUI().getManaBar().setCurrent((double) current / max);
	}

	@Override
	protected void outputEmpty() {
		chaa.getEffects().play(empty);
	}

	@Override
	protected void outputFull() {
		Effects effects = chaa.getEffects();
		effects.play(regeneratingInterrupt);
		effects.play(full);
	}

	@Override
	protected void outputRegenerating(boolean positive) {
		if (!positive) {
			chaa.getEffects().play(degenerating);
		}
	}

	@Override
	public void outputInterruptRegen() {
		chaa.getEffects().play(regeneratingInterrupt);
	}

	@Override
	protected void outputTick(boolean notFull, boolean positive) {
		if (notFull && positive) {
			chaa.getEffects().play(regenerating);
		}
	}

	public void onExpChange() {
		last = -1;
	}
}