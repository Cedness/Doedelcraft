package de.ced.doedelcraft.ingame.chaa.mana;

import de.ced.doedelcraft.ingame.chaa.CastedSpell;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.combat.AbstractCombat;
import de.ced.doedelcraft.ingame.chaa.combat.Combat;
import de.ced.doedelcraft.ingame.chaa.health.Health;
import de.ced.doedelcraft.ingame.chaa.stat.AbstractStat;

public abstract class AbstractMana extends AbstractStat implements Mana {

	protected float minecraft = 0;
	private double baseRegen;
	private boolean calcLocked;

	private boolean reached = true;

	/**
	 * Dependencies: Progress
	 */
	public AbstractMana(Char chaa) {
		super(chaa, "mana");
		calcMax();
		calcLocked = true;
		prepare();
		calcLocked = false;
		setRegen(baseRegen);
	}

	@Override
	public Class<?>[] dependencies(int cycle) {
		return new Class<?>[] { Health.class };
	}

	@Override
	public void set(int amount) {
		int current = this.current;
		super.set(amount);
		if (this.current < current) {
			outputInterruptRegen();
		}
	}

	@Override
	public void setMax(int max) {
		super.setMax(max);
		baseRegen = 0.02 * (max / 100.0 + 1);
		calcRegen();
	}

	@Override
	protected void apply() {
		minecraft = (float) ((double) current / max);
	}

	@Override
	public void calcMax() {
		int max = chaa.getProgress().getLevel();
		switch (chaa.getClazz().getName()) {
		case "spartan":
			max = 50 + max / 2;
			break;
		case "human":
			max = 1;
			break;
		default:
		}
		int toGive = max - this.max;
		this.max = max;
		if (toGive <= 0) {
			return;
		}
		set(current + toGive);
	}

	@Override
	public void calcRegen() {
		if (calcLocked) {
			return;
		}
		double regen = baseRegen;
		for (CastedSpell castedSpell : chaa.getCombat().getCastedSpells()) { // No dependency because calcLocked
			regen -= castedSpell.getConstantManaTicks();
		}
		setRegen(regen);
		setStartTick(regen <= 0);
	}

	@Override
	protected boolean regen(int tick) {
		boolean isRegen = super.regen(tick);
		if (isRegen) {
			if (current <= 0) {
				outputEmpty();
				calcLocked = true;
				Combat combat = chaa.getCombat();
				for (CastedSpell castedSpell : combat.getCastedSpells()) {
					if (castedSpell.getConstantManaTicks() > 0 && castedSpell.getSpell().isSwitchSpell()) {
						((AbstractCombat) combat).disable(castedSpell); // TODO remove direct access from mana to AbstractCombat
					}
				}
				calcLocked = false;
				calcRegen();
			} else if (current >= max) {
				if (!reached) {
					outputFull();
				}
				reached = true;
			} else {
				outputRegenerating(regen > 0);
				reached = false;
			}
		}
		if ((tick - startTick) % 20 == 0) {
			outputTick(current < max, regen > 0);
		}
		return isRegen;
	}

	protected abstract void outputEmpty();

	protected abstract void outputFull();

	protected abstract void outputRegenerating(boolean positive);

	public abstract void outputInterruptRegen();

	protected abstract void outputTick(boolean notFull, boolean positive);

	public double getBaseRegen() {
		return baseRegen;
	}

	public void setBaseRegen(double baseRegen) {
		this.baseRegen = baseRegen;
	}
}
