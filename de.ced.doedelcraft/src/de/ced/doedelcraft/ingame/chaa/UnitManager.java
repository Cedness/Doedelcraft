package de.ced.doedelcraft.ingame.chaa;

public interface UnitManager extends Unit, Tickable, Addable, Removable {

	Unit addUnit(Unit unit);

	void finishUnitAdding();
}
