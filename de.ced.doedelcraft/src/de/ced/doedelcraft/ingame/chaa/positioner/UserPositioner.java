package de.ced.doedelcraft.ingame.chaa.positioner;

import de.ced.doedelcraft.ingame.chaa.Unit;

public interface UserPositioner extends Unit {

	/**
	 * This is a workaround due to minecraft not allowing to set the title and
	 * subtitle individually.
	 */
	void setTeleportReason(String teleportReason);
}
