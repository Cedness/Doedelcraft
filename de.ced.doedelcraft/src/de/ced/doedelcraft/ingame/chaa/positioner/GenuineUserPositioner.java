package de.ced.doedelcraft.ingame.chaa.positioner;

import java.util.List;

import de.ced.doedelcraft.ingame.chaa.chunkloader.UserChunkLoader;
import de.ced.doedelcraft.ingame.chaa.health.Health;
import de.ced.doedelcraft.ingame.aoe.Realm;
import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Data;
import de.ced.doedelcraft.ingame.chaa.Removable;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.UserData;
import de.ced.doedelcraft.ingame.chaa.minecraft.Minecraft;
import de.ced.doedelcraft.ingame.chaa.minecraft.UserMinecraft;
import de.ced.doedelcraft.ingame.place.Place;
import de.ced.doedelcraft.ingame.place.PlaceManager;
import de.ced.logic.vector.Vector3;
import de.ced.logic.vector.Vector5;

public class GenuineUserPositioner extends AbstractUnit implements UserPositioner, Tickable, Removable {

	private static final double LOGIN_HEIGHT_OFFSET = 1;

	private final PlaceManager placeManager;
	private final User user;
	private int startTick;
	private boolean locationChanged = true;
	private Place country = null;
	private Place region = null;
	private Place town = null;
	private Place place = null;
	private Place location = null;

	private String placeInfo = "";
	private String teleportTitle = "";

	/**
	 * Dependencies: Minecraft, ChunkLoader
	 */
	public GenuineUserPositioner(User user) {
		this.user = user;
		placeManager = user.getCharManager().getPlaceManager();

		UserMinecraft minecraft = user.getMinecraft();
		UserData data = user.getData();
		Vector5 location = minecraft.getLocation();
		location.set(data.getX(), data.getY() + LOGIN_HEIGHT_OFFSET, data.getZ(), data.getYaw(), 0);
		minecraft.teleport();

		startTick = getTick();
		country = updatePlace(location, placeManager.getCountries());
		region = updatePlace(location, placeManager.getRegions());
		town = updatePlace(location, placeManager.getTowns());
		place = updatePlace(location, placeManager.getPlaces());
		this.location = updatePlace(location, placeManager.getLocations());
	}

	@Override
	public Class<?>[] removableDependencies() {
		return new Class<?>[] { Minecraft.class, Data.class };
	}

	@Override
	public void remove() {
		UserData userData = user.getData();
		UserMinecraft minecraft = user.getMinecraft();
		Vector5 location = minecraft.getLocation();
		userData.setX(location.x());
		userData.setY(location.y());
		userData.setZ(location.z());
		userData.setYaw(location.a());
		userData.setPitch(location.b());
	}

	@Override
	public int[] supportedCycles() {
		return new int[] { LATE };
	}

	@Override
	public Class<?>[] dependencies(int cycle) {
		return new Class<?>[] { Minecraft.class, UserChunkLoader.class, Health.class };
	}

	@Override
	public void tick(int tick, int cycle) {
		if (user.getLastTick().hasLocationChanged()) {
			locationChanged = true;
		}
		if (!locationChanged || (tick - startTick) % 10 != 0) {
			return;
		}
		locationChanged = false;
		updatePlace(user.getMinecraft().getLocation());
	}

	private void updatePlace(Vector3 location) {
		placeInfo = "";
		country = display(country, updatePlace(location, placeManager.getCountries()));
		region = display(region, updatePlace(location, placeManager.getRegions()));
		town = display(town, updatePlace(location, placeManager.getTowns()));
		place = display(place, updatePlace(location, placeManager.getPlaces()));
		this.location = display(this.location, updatePlace(location, placeManager.getLocations()));
		display();
	}

	private Place display(Place last, Place current) {
		if (last == current) {
			return current;
		}
		/*
		 * if (last != null) { Logger.info(user.getName() + " left " + last.getName());
		 * } if (current != null) { Logger.info(user.getName() + " entered " +
		 * current.getName()); }
		 */

		placeInfo = last == null ? null : "§7§m" + last.getName();
		placeInfo = current == null ? placeInfo : "§7" + current.getName();
		return current;
	}

	private void display() {
		if ("".equals(teleportTitle) && "".equals(placeInfo)) {
			return;
		}
		user.getMinecraft().title(teleportTitle, placeInfo, 5, 40, 5);
	}

	private Place updatePlace(Vector3 location, List<Place> list) {
		for (Place place : list) {
			Realm realm = place.getRealm();
			if (realm != null && realm.intersects(location, true)) {
				return place;
			}
		}
		return null;
	}

	public String getTeleportReason() {
		return teleportTitle;
	}

	@Override
	public void setTeleportReason(String teleportReason) {
		teleportTitle = teleportReason;
	}

	public void onTeleport(Vector3 to) {
		updatePlace(to);
		teleportTitle = "";
		startTick = getTick() - 1;
	}
}
