package de.ced.doedelcraft.ingame.chaa;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

import de.ced.logic.vector.Vector3;

@Deprecated
public class NPManager extends Manager {

	private final Map<Entity, NPC> npcs = new HashMap<>();

	public NPManager(CharManager charManager) {
		super(charManager);
	}

	public void spawnTestNPC(Vector3 location) {
		World world = doedelcraft.getWorldManager().getWorld();
		Entity entity = world.spawnEntity(location.toLocation(world), EntityType.ZOMBIE);
		add(entity);
	}

	public void add(Entity entity) {
		new NPC(entity, this);
	}

	public void remove(Entity entity) {
		NPC npc = npcs.get(entity);
		if (npc == null) {
			return;
		}
		npc.remove();
		npcs.remove(entity);
		// charManager.remove(npc);
	}

	public void cleanup() {
		for (Entity entity : doedelcraft.getWorldManager().getWorld().getEntities()) {
			if (!(entity instanceof LivingEntity) || ((LivingEntity) entity).hasAI()) {
				continue;
			}
			remove(entity);
			if (!entity.isDead()) {
				entity.remove();
			}
		}
	}
}
