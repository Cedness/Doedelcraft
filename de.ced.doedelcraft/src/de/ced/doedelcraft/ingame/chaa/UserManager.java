package de.ced.doedelcraft.ingame.chaa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.ced.doedelcraft.account.Account;
import de.ced.doedelcraft.ingame.chaa.handler.UserHandler;
import de.ced.doedelcraft.util.Logger;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.PlayerInventory;

import com.comphenix.protocol.events.PacketEvent;

/**
 * Creates, stores and destroys all users. They can be acquired here via their
 * {@link Player} handle or their name.
 * Also, all events regarding users are redirected to the correct user by this
 * class.
 * 
 * @author ced
 */
public class UserManager extends Manager {

	private final Map<Player, User> users = new HashMap<>();
	private final Map<String, User> nameUserMap = new HashMap<>();
	private final List<String> userNamesForTabComplete = new ArrayList<>();

	public UserManager(CharManager charManager) {
		super(charManager);
	}

	public void add(UserData saveFile) {
		Account account = saveFile.getAccount();
		User user;
		try {
			user = new GenuineUser(this, saveFile);
		} catch (Exception ex) {
			try {
				Logger.severe("Exception while instantiating user " + saveFile.getName() + " of account " + account.getName());
			} catch (Exception ex2) {
				Logger.severe("Exception while printing exception");
				ex2.printStackTrace();
			}
			ex.printStackTrace();
			account.kick("§cOoops!\nSorry, we encountered a problem.\nAs you can see, this server is still in alpha.");
			return;
		}
		charManager.add(user);
		users.put(account.getPlayer(), user);
		String name = user.getIdentity().getName();
		nameUserMap.put(name, user);
		int nameHash = name.hashCode();
		for (int i = 0; i <= userNamesForTabComplete.size(); i++) {
			if (i == userNamesForTabComplete.size() || nameHash < userNamesForTabComplete.get(i).hashCode()) {
				userNamesForTabComplete.add(i, name);
				break;
			}
		}

		// Logger.info("Adding done");
	}

	public void remove(Account account) {
		Player player = account.getPlayer();
		if (!users.containsKey(player)) {
			return;
		}
		GenuineUser user = (GenuineUser) users.get(player);

		try {
			for (Iterator<CastedSpell> iterator = charManager.getCastedSpells().iterator(); iterator.hasNext();) {
				CastedSpell castedSpell = iterator.next();
				if (user != castedSpell.getChar()) {
					continue;
				}
				if (castedSpell.getSpell().isSwitchSpell()) {
					user.getCombat().disable(castedSpell);
				}
				iterator.remove(); // TODO afterlife still ignored
			}
		} catch (Exception ex) {
			Logger.severe("Exception while disabling spells for " + user);
			ex.printStackTrace();
		}

		try {
			user.remove();
		} catch (Exception ex) {
			Logger.severe("Exception while removing " + user);
			ex.printStackTrace();
		}

		users.remove(player);
		String name = user.getIdentity().getName();
		nameUserMap.remove(name);
		userNamesForTabComplete.remove(name);
		charManager.remove(user);
	}

	public User getUser(Player player) {
		return users.get(player);
	}

	public boolean hasUser(Player player) {
		return users.containsKey(player);
	}

	public User getUser(String name) {
		return nameUserMap.get(name);
	}

	public boolean hasUser(String name) {
		return nameUserMap.containsKey(name);
	}

	public Collection<User> getUsers() {
		return users.values();
	}

	public List<String> getUserNames() {
		return userNamesForTabComplete;
	}

	// EVENTS

	private UserHandler getHandler(Player player) {
		return getUser(player).getHandler();
	}

	private void launchEvent(Player player, PacketEvent e, Runnable launchableEvent) {
		launchEvent(player, "ProtocolLib/PacketEvent", launchableEvent);
	}

	private void launchEvent(Player player, Event e, Runnable launchableEvent) {
		launchEvent(player, e.getEventName(), launchableEvent);
	}

	private void launchEvent(Player player, String eventName, Runnable launchableEvent) {
		try {
			launchableEvent.run();
		} catch (Exception ex) {
			try {
				User user = getUser(player);
				Logger.severe("Exception while executing event " + eventName + " for user " + user.getIdentity().getName() + " of player " + user.getAccount().getName());
			} catch (Exception ex2) {
				Logger.severe("Exception while printing exception");
				ex2.printStackTrace();
			}
			ex.printStackTrace();
		}
	}

	public void onPacket(PacketEvent e) {
		Player player = e.getPlayer();
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onPacket(e);
		});
	}

	public void onMove(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onMove(e);
		});
	}

	public void onTeleport(PlayerTeleportEvent e) {
		Player player = e.getPlayer();
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onTeleport(e);
		});
	}

	public void onFoodLevelChange(FoodLevelChangeEvent e) {
		HumanEntity entity = e.getEntity();
		if (!(entity instanceof Player)) {
			return;
		}
		Player player = (Player) entity;
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onFoodLevelChange(e);
		});
	}

	public void onHeldItemChange(PlayerItemHeldEvent e) {
		Player player = e.getPlayer();
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onItemHeldChange(e);
		});
	}

	public void onSwapHandItems(PlayerSwapHandItemsEvent e) {
		Player player = e.getPlayer();
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onSwapHandItems(e);
		});
	}

	public void onAnimation(PlayerAnimationEvent e) {
		Player player = e.getPlayer();
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onAnimation(e);
		});
	}

	public void onInteract(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onInteract(e);
		});
	}

	public void onInventoryOpen(InventoryOpenEvent e) {
		HumanEntity humanEntity = e.getPlayer();
		if (!(humanEntity instanceof Player)) {
			return;
		}
		Player player = (Player) humanEntity;
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onInventoryOpen(e);
		});
	}

	public void onInventoryClose(InventoryCloseEvent e) {
		HumanEntity humanEntity = e.getPlayer();
		if (!(humanEntity instanceof Player)) {
			return;
		}
		Player player = (Player) humanEntity;
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onInventoryClose(e);
		});
	}

	public void onInventoryClick(InventoryClickEvent e) {
		HumanEntity humanEntity = e.getWhoClicked();
		if (!(humanEntity instanceof Player)) {
			return;
		}
		Player player = (Player) humanEntity;
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onInventoryClick(e);
		});
	}

	public void onInventoryDrag(InventoryDragEvent e) {
		HumanEntity humanEntity = e.getWhoClicked();
		if (!(humanEntity instanceof Player)) {
			return;
		}
		Player player = (Player) humanEntity;
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onInventoryDrag(e);
		});
	}

	public void onInventoryMove(InventoryMoveItemEvent e) {
		Inventory[] inventories = { e.getSource(), e.getDestination() };
		for (boolean isTarget = false; !isTarget; isTarget = true) {
			Inventory inventory = inventories[isTarget ? 1 : 0];
			if (!(inventory instanceof PlayerInventory)) {
				continue;
			}
			HumanEntity humanEntity = ((PlayerInventory) inventory).getHolder();
			if (!(humanEntity instanceof Player)) {
				return;
			}
			Player player = (Player) humanEntity;
			if (!hasUser(player)) {
				return;
			}
			boolean isTargetCopy = isTarget;
			UserHandler handler = getHandler(player);
			launchEvent(player, e, () -> {
				handler.onInventoryMoveItem(e, isTargetCopy);
			});
		}
	}

	public void onPickup(EntityPickupItemEvent e) {
		if (e.getEntityType() != EntityType.PLAYER) {
			e.setCancelled(true);
			return;
		}
		Player player = (Player) e.getEntity();
		if (!hasUser(player)) {
			e.setCancelled(true);
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onPickupItem(e);
		});
	}

	public void onDrop(PlayerDropItemEvent e) {
		Player player = e.getPlayer();
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onDropItem(e);
		});
	}

	public void onSneak(PlayerToggleSneakEvent e) {
		Player player = e.getPlayer();
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onToggleSneak(e);
		});
	}

	public void onChat(AsyncPlayerChatEvent e) {
		if (e.isCancelled()) {
			return;
		}
		Player player = e.getPlayer();
		if (!hasUser(player)) {
			e.setCancelled(true);
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onChat(e);
		});
	}

	public void onExperienceChange(PlayerExpChangeEvent e) {
		Player player = e.getPlayer();
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onExpChange(e);
		});
	}

	public void onLevelChange(PlayerLevelChangeEvent e) {
		Player player = e.getPlayer();
		if (!hasUser(player)) {
			return;
		}
		launchEvent(player, e, () -> {
			getHandler(player).onLevelChange(e);
		});
	}
}
