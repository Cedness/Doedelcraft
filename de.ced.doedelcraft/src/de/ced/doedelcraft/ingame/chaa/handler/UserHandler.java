package de.ced.doedelcraft.ingame.chaa.handler;

import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import com.comphenix.protocol.events.PacketEvent;

public interface UserHandler extends Handler {

	public void onPacket(PacketEvent e);

	public void onMove(PlayerMoveEvent e);

	public void onTeleport(PlayerTeleportEvent e);

	public void onFoodLevelChange(FoodLevelChangeEvent e);

	public void onItemHeldChange(PlayerItemHeldEvent e);

	public void onSwapHandItems(PlayerSwapHandItemsEvent e);

	public void onAnimation(PlayerAnimationEvent e);

	public void onInteract(PlayerInteractEvent e);

	public void onInventoryOpen(InventoryOpenEvent e);

	public void onInventoryClose(InventoryCloseEvent e);

	public void onInventoryClick(InventoryClickEvent e);

	public void onInventoryDrag(InventoryDragEvent e);

	public void onInventoryMoveItem(InventoryMoveItemEvent e, boolean isTarget);

	public void onPickupItem(EntityPickupItemEvent e);

	public void onDropItem(PlayerDropItemEvent e);

	public void onToggleSneak(PlayerToggleSneakEvent e);

	public void onChat(AsyncPlayerChatEvent e);

	public void onExpChange(PlayerExpChangeEvent e);

	public void onLevelChange(PlayerLevelChangeEvent e);
}
