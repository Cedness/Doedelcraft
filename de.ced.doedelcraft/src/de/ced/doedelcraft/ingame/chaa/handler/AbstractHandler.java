package de.ced.doedelcraft.ingame.chaa.handler;

import de.ced.doedelcraft.ingame.chaa.health.AbstractHealth;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;
import org.bukkit.event.entity.EntityRegainHealthEvent;

import de.ced.doedelcraft.ingame.chaa.Char;

@SuppressWarnings("deprecation")
public class AbstractHandler implements Handler {

	private final Char chaa;

	public AbstractHandler(Char chaa) {
		this.chaa = chaa;
	}

	@Override
	public void onDamage(EntityDamageEvent e) {
		double[] damage = { e.getDamage() };
		for (EntityDamageEvent.DamageModifier modifier : EntityDamageEvent.DamageModifier.values()) {
			if (e.isApplicable(modifier)) {
				e.setDamage(modifier, 0);
			}
		}
		boolean[] cancelled = { e.isCancelled() };
		Entity entity = e.getEntity();
		boolean isLiving = entity instanceof LivingEntity;
		((AbstractHealth) chaa.getHealth()).onDamage(damage, cancelled, isLiving ? ((LivingEntity) entity).getHealth() : 0, isLiving, e.getCause());
		e.setDamage(DamageModifier.BASE, damage[0]);
		e.setCancelled(cancelled[0]);
	}

	@Override
	public void onRegainHealth(EntityRegainHealthEvent e) {
		double[] amount = { e.getAmount() };
		Entity entity = e.getEntity();
		boolean isLiving = entity instanceof LivingEntity;
		((AbstractHealth) chaa.getHealth()).onRegainHealth(amount, isLiving ? ((LivingEntity) entity).getHealth() : 0, isLiving);
		// e.setAmount(amount[0]);
		e.setCancelled(true);
	}
}
