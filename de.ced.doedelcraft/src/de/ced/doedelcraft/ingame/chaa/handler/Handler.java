package de.ced.doedelcraft.ingame.chaa.handler;

import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;

import de.ced.doedelcraft.ingame.chaa.Unit;

/**
 * Handles events regarding this Char
 */
public interface Handler extends Unit {

	public void onDamage(EntityDamageEvent e);

	public void onRegainHealth(EntityRegainHealthEvent e);
}
