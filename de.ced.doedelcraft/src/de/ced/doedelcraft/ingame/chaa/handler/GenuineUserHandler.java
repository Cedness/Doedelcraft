package de.ced.doedelcraft.ingame.chaa.handler;

import de.ced.doedelcraft.ingame.chaa.chat.GenuineUserChat;
import de.ced.doedelcraft.ingame.chaa.combat.GenuineUserCombat;
import de.ced.doedelcraft.ingame.chaa.exhaustion.AbstractExhaustion;
import de.ced.doedelcraft.ingame.chaa.glow.GenuineUserGlow;
import de.ced.doedelcraft.ingame.chaa.inputpreprocess.UserInputPreprocess;
import de.ced.doedelcraft.ingame.chaa.mana.GenuineUserMana;
import de.ced.doedelcraft.ingame.chaa.positioner.GenuineUserPositioner;
import de.ced.doedelcraft.ingame.chaa.stuff.GenuineUserStuff;
import de.ced.doedelcraft.util.SadItem;
import org.bukkit.entity.Item;
import org.bukkit.event.Event.Result;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import com.comphenix.protocol.events.PacketEvent;

import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.logic.vector.IndependentLocationVector3;

public class GenuineUserHandler extends AbstractHandler implements UserHandler {

	private final User user;
	private int interactBugTick = 0;

	public GenuineUserHandler(User user) {
		super(user);
		this.user = user;
	}

	private UserInputPreprocess getInputPreprocess() {
		return user.getInputPreprocess();
	}

	private GenuineUserStuff getStuff() {
		return (GenuineUserStuff) user.getStuff();
	}

	@Override
	public void onPacket(PacketEvent e) {
		((GenuineUserGlow) user.getGlow()).onPacketSending(e.getPacket());
	}

	@Override
	public void onMove(PlayerMoveEvent e) {
	}

	@Override
	public void onTeleport(PlayerTeleportEvent e) {
		((GenuineUserPositioner) user.getPositioner()).onTeleport(new IndependentLocationVector3(e.getTo()));
	}

	@Override
	public void onFoodLevelChange(FoodLevelChangeEvent e) {
		int[] foodLevel = { e.getFoodLevel() };

		((AbstractExhaustion) user.getExhaustion()).onFoodLevelChange(foodLevel);

		e.setFoodLevel(foodLevel[0]);
	}

	@Override
	public void onItemHeldChange(PlayerItemHeldEvent e) {
		boolean[] cancelled = { e.isCancelled() };
		int originalNewSlot = e.getNewSlot();
		int[] newSlot = { originalNewSlot };

		getInputPreprocess().onHeldItemChange(cancelled, newSlot, e.getPreviousSlot());

		e.setCancelled(cancelled[0]);
		if (originalNewSlot != newSlot[0]) {
			user.getMinecraft().setHeldItemSlot(newSlot[0]);
		}
	}

	@Override
	public void onSwapHandItems(PlayerSwapHandItemsEvent e) {
		boolean[] cancelled = { e.isCancelled() };

		getInputPreprocess().onSwapHandItems(cancelled);

		e.setCancelled(cancelled[0]);
	}

	@Override
	public void onAnimation(PlayerAnimationEvent e) {
		getInputPreprocess().onAnimation();
	}

	@Override
	public void onInteract(PlayerInteractEvent e) {
		int tick = getTick();
		if (interactBugTick == tick) {
			return;
		}
		interactBugTick = tick;

		boolean[] useItemInHand = { true };
		boolean[] useInteractedBlock = { true };
		Action action = e.getAction();
		boolean right = action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK;

		getInputPreprocess().onInteract(useItemInHand, useInteractedBlock, right);

		if (!useItemInHand[0]) {
			e.setUseItemInHand(Result.DENY);
		}
		if (!useInteractedBlock[0]) {
			e.setUseInteractedBlock(Result.DENY);
		}
	}

	@Override
	public void onInventoryOpen(InventoryOpenEvent e) {
		getStuff().onInventoryOpen(e);
	}

	@Override
	public void onInventoryClose(InventoryCloseEvent e) {
		getStuff().onInventoryClose(e);
	}

	@Override
	public void onInventoryClick(InventoryClickEvent e) {
		boolean[] cancelled = { e.isCancelled() };
		int slotId = e.getSlot();
		int hotbarButtonId = e.getHotbarButton();
		SlotType slotType = e.getSlotType();
		ClickType clickType = e.getClick();
		InventoryAction inventoryAction = e.getAction();

		getInputPreprocess().onInventoryClick(cancelled, slotId, hotbarButtonId, slotType, clickType, inventoryAction);
		e.setCancelled(cancelled[0]);

		getStuff().onInventoryClick(e);
	}

	@Override
	public void onInventoryDrag(InventoryDragEvent e) {
		getStuff().onInventoryDrag(e);
	}

	@Override
	public void onInventoryMoveItem(InventoryMoveItemEvent e, boolean isTarget) {
	}

	@Override
	public void onPickupItem(EntityPickupItemEvent e) {
		getStuff().onPickup(e);
	}

	@Override
	public void onDropItem(PlayerDropItemEvent e) {
		Item itemEntity = e.getItemDrop();
		SadItem item = new SadItem(itemEntity.getItemStack());
		boolean[] removeItem = { false };

		GenuineUserStuff stuff = (GenuineUserStuff) user.getStuff();
		stuff.onDrop(e);

		boolean[] cancelled = { e.isCancelled() };
		if (!stuff.isInventoryOpen()) {
			getInputPreprocess().onDrop(cancelled, removeItem, user.getMinecraft().getHeldItemSlot(), item.getAmount() > 1, item.getMaxStackSize());
		}

		e.setCancelled(cancelled[0]);
		if (removeItem[0]) {
			itemEntity.remove();
		}
	}

	@Override
	public void onToggleSneak(PlayerToggleSneakEvent e) {
		boolean[] cancelled = { e.isCancelled() };
		getInputPreprocess().onSneak(cancelled, e.isSneaking());
		e.setCancelled(cancelled[0]);
	}

	@Override
	public void onChat(AsyncPlayerChatEvent e) {
		e.setCancelled(true);
		((GenuineUserChat) user.getChat()).onChat(e.getMessage());
	}

	@Override
	public void onExpChange(PlayerExpChangeEvent e) {
		((GenuineUserMana) user.getMana()).onExpChange();
	}

	@Override
	public void onLevelChange(PlayerLevelChangeEvent e) {
		((GenuineUserCombat) user.getCombat()).onLevelChange();
	}
}
