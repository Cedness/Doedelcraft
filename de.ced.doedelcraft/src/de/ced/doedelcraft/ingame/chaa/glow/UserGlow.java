package de.ced.doedelcraft.ingame.chaa.glow;

import java.util.Collection;

import org.bukkit.entity.Entity;

import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.Unit;

public interface UserGlow extends Unit {

	boolean isGlowing(Char chaa);

	boolean setGlowing(Char chaa, boolean enable);

	boolean glow(Char chaa);

	boolean unglow(Char chaa);

	Collection<Entity> getGlowing();

	void clear();
}
