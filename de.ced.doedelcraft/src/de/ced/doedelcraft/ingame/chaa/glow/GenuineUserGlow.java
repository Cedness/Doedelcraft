package de.ced.doedelcraft.ingame.chaa.glow;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.craftbukkit.v1_16_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedWatchableObject;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.Removable;
import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.chaa.minecraft.Minecraft;
import net.minecraft.server.v1_16_R2.ChunkProviderServer;
import net.minecraft.server.v1_16_R2.EntityPlayer;
import net.minecraft.server.v1_16_R2.PlayerChunkMap;
import net.minecraft.server.v1_16_R2.WorldServer;

@SuppressWarnings("deprecation")
public class GenuineUserGlow extends AbstractUnit implements UserGlow, Removable {

	private final User user;
	private final List<Char> glowing = new ArrayList<>();
	private List<Entity> glowingEntities = new ArrayList<>();

	/**
	 * Dependencies: -
	 */
	public GenuineUserGlow(User user) {
		this.user = user;
	}

	@Override
	public Class<?>[] removableDependencies() {
		return new Class<?>[] { UserGlow.class, Minecraft.class };
	}

	@Override
	public void remove() {
		for (User otherUser : user.getUserManager().getUsers()) {
			((GenuineUserGlow) otherUser.getGlow()).unglow(user, false);
		}
	}

	public void onPacketSending(PacketContainer packet) {
		int entityId = packet.getIntegers().read(0);
		if (user.getMinecraft().getEntity().getEntityId() == entityId) {
			return;
		}
		boolean contains = false;
		for (Entity entity : glowingEntities) {
			if (entityId == entity.getEntityId()) {
				contains = true;
				break;
			}
		}
		if (!contains) {
			return;
		}
		List<WrappedWatchableObject> watchableObjectList = packet.getWatchableCollectionModifier().read(0);
		for (WrappedWatchableObject metadata : watchableObjectList) {
			if (metadata.getIndex() == 0) {
				byte b = (byte) metadata.getValue();
				b |= 0b01000000;
				metadata.setValue(b);
			}
		}
	}

	@Override
	public boolean isGlowing(Char chaa) {
		return glowing.contains(chaa);
	}

	@Override
	public boolean setGlowing(Char chaa, boolean enable) {
		return enable ? glow(chaa) : unglow(chaa);
	}

	@Override
	public boolean glow(Char chaa) {
		if (glowing.contains(chaa)) {
			return false;
		}
		glowing.add(chaa);
		Entity entity = chaa.getMinecraft().getEntity();
		glowingEntities.add(entity);
		refresh(entity);
		return true;
	}

	@Override
	public boolean unglow(Char chaa) {
		return unglow(chaa, true);
	}

	private boolean unglow(Char chaa, boolean online) {
		if (!glowing.contains(chaa)) {
			return false;
		}
		glowing.remove(chaa);
		Entity entity = chaa.getMinecraft().getEntity();
		glowingEntities.remove(entity);
		if (online) {
			refresh(entity);
		}
		return true;
	}

	@Override
	public List<Entity> getGlowing() {
		return glowingEntities;
	}

	@Override
	public void clear() {
		List<Entity> glowing = glowingEntities;
		glowingEntities = new ArrayList<>();
		for (Entity entity : glowing) {
			refresh(entity);
		}
	}

	private void refresh(Entity entity) {
		if (!(entity instanceof Player)) {
			return;
		}
		Player player = (Player) entity;
		ChunkProviderServer chunkProviderServer = ((WorldServer) ((CraftWorld) player.getWorld()).getHandle()).getChunkProvider();
		PlayerChunkMap tracker = chunkProviderServer.playerChunkMap;
		EntityPlayer entityPlayer = ((CraftPlayer) user.getMinecraft().getEntity()).getHandle();
		EntityPlayer otherEntityPlayer = ((CraftPlayer) player).getHandle();
		PlayerChunkMap.EntityTracker entry = (PlayerChunkMap.EntityTracker) tracker.trackedEntities.get(otherEntityPlayer.getId());

		if (entry != null) {
			entry.clear(entityPlayer);
			entry.updatePlayer(entityPlayer);
		}
//			if (otherEntityPlayer.sentListPacket) {
//				Packet<?> packet1 = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, new EntityPlayer[] { otherEntityPlayer });
//				entityPlayer.playerConnection.sendPacket(packet1);
//			}
//			Packet<?> packet2 = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, new EntityPlayer[] { otherEntityPlayer });
//			entityPlayer.playerConnection.sendPacket(packet2);
		// Logger.info("REFRESH " + entityPlayer.getName() + " " +
		// otherEntityPlayer.getName());
	}
}
