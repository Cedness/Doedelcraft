package de.ced.doedelcraft.ingame.chaa.minecraft;

import java.util.UUID;

import de.ced.doedelcraft.Doedelcraft;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.util.Vector;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Removable;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.logic.vector.BukkitVector3;
import de.ced.logic.vector.LocationVector5;
import de.ced.logic.vector.Vector3;

public abstract class AbstractMinecraft extends AbstractUnit implements Minecraft, Tickable, Removable {

	protected final Doedelcraft doedelcraft;
	protected final Entity entity;
	protected final boolean living;
	protected final LocationVector5 location;
	private boolean onGround;
	protected final BukkitVector3 velocity;

	public AbstractMinecraft(Doedelcraft doedelcraft, Entity entity) {
		this.doedelcraft = doedelcraft;
		this.entity = entity;
		living = entity instanceof LivingEntity;
		location = new LocationVector5(new Location(doedelcraft.getWorldManager().getWorld(), 0, 0, 0));
		onGround = entity.isOnGround();
		velocity = new BukkitVector3(new Vector());
	}

	@Override
	public void remove() {
	}

	@Override
	public int[] supportedCycles() {
		return new int[] { EARLY };
	}

	/**
	 * Needs early tick to update location.
	 */
	@Override
	public void tick(int tick, int cycle) {
		entity.getLocation(location.toLocation());
		onGround = entity.isOnGround();
		velocity.replace(entity.getVelocity());
	}

	private LivingEntity livingEntity() {
		return (LivingEntity) entity;
	}

	@Override
	public boolean isLiving() {
		return living;
	}

	@Deprecated
	@Override
	public Entity getEntity() {
		return entity;
	}

	@Override
	public UUID getUniqueId() {
		return entity.getUniqueId();
	}

	@Override
	public void message(String text) {
		entity.sendMessage(text);
	}

	@Override
	public LocationVector5 getLocation() {
		return location;
	}

	@Override
	public void teleport() {
		entity.teleport(location.toLocation(), TeleportCause.PLUGIN);
		onGround = entity.isOnGround();
	}

	@Override
	public boolean isOnGround() {
		return onGround;
	}

	@Override
	public BukkitVector3 getVelocity() {
		return velocity;
	}

	@Override
	public void addVelocity(Vector3 velocity) {
		this.velocity.add(velocity);
		applyVelocity();
	}

	@Override
	public void applyVelocity() {
		entity.setVelocity(velocity.toBukkitVector());
	}

	@Override
	public float getFallDistance() {
		return entity.getFallDistance();
	}

	@Override
	public void setFallDistance(float distance) {
		entity.setFallDistance(distance);
	}

	@Override
	public void setFireTicks(int ticks) {
		entity.setFireTicks(ticks);
	}

	@Override
	public EntityEquipment getEquipment() {
		return livingEntity().getEquipment();
	}
}
