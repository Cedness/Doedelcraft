package de.ced.doedelcraft.ingame.chaa.minecraft;

import org.bukkit.Material;
import org.bukkit.SoundCategory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scoreboard.Scoreboard;

import de.ced.logic.vector.Vector3;

public interface UserMinecraft extends Minecraft {

	int BOSS_COLOR_PINK = 5,
			BOSS_COLOR_BLUE = 4,
			BOSS_COLOR_RED = 1,
			BOSS_COLOR_GREEN = 2,
			BOSS_COLOR_YELLOW = 3,
			BOSS_COLOR_PURPLE = 6,
			BOSS_COLOR_WHITE = 0;

	boolean isSneaking();

	void setWalkspeed(float walkspeed);

	void actionbar(String text);

	void title(String title, String subtitle);

	void titleTimings(int fadeIn, int stay, int fadeOut);

	void title(String title, String subtitle, int fadeIn, int stay, int fadeOut);

	double getHealth();

	void setHealth(double health);

	void damage(double damage);

	void damageAnimation();

	void setHunger(int hunger);

	void setExperienceProgress(float progress);

	void setLevel(int level);

	void setBossHealth(double bossHealth);

	void setBossColor(int bossColor);

	void setBossbarText(String bossbarText);

	void setBossbarDarkSky(boolean enable);

	@Deprecated
	void setScoreboard(Scoreboard mainScoreboard);

	void setAbsorption(int absorption);

	void setGamemode(int gamemode);

	void playSound(String sound, SoundCategory category, float volume, float pitch);

	void playSound(Vector3 location, String sound, SoundCategory category, float volume, float pitch);

	void stopSound(String sound, SoundCategory category);

	int getHeldItemSlot();

	void setHeldItemSlot(int slotId);

	@Deprecated
	PlayerInventory getInventory();

	@Deprecated
	void openInventory(Inventory inventory);

	boolean hasCooldown(Material material);

	void setCooldown(Material material, int cooldown);
}
