package de.ced.doedelcraft.ingame.chaa.minecraft;

import de.ced.doedelcraft.Doedelcraft;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.SoundCategory;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.KeyedBossBar;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftLivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scoreboard.Scoreboard;

import de.ced.logic.vector.Vector3;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class GenuineUserMinecraft extends AbstractMinecraft implements UserMinecraft {

	private static final String DEFAULT_TITLE = "§7§oLoading...";
	private static final BarColor DEFAULT_BAR_COLOR = BarColor.YELLOW;
	private static final BarStyle DEFAULT_BAR_STYLE = BarStyle.SEGMENTED_12;

	private final Player player;
	private boolean sneaking;
	private int fadeIn = 5;
	private int stay = 20;
	private int fadeOut = 5;
	private final NamespacedKey key;
	private KeyedBossBar bar;
	private final PlayerInventory inventory;

	public GenuineUserMinecraft(Doedelcraft doedelcraft, Player player) {
		super(doedelcraft, player);
		this.player = player;
		key = new NamespacedKey(doedelcraft, player.getUniqueId().toString());
		bar = Bukkit.getBossBar(key);
		if (bar == null) {
			bar = Bukkit.createBossBar(key, DEFAULT_TITLE, DEFAULT_BAR_COLOR, DEFAULT_BAR_STYLE);
		}
		bar.setTitle(DEFAULT_TITLE);
		bar.setColor(DEFAULT_BAR_COLOR);
		bar.setStyle(DEFAULT_BAR_STYLE);
		bar.addPlayer(player);
		inventory = player.getInventory();
	}

	@Override
	public void remove() {
		super.remove();
		bar.removePlayer(player);
		Bukkit.removeBossBar(key);
	}

	@Override
	public void tick(int tick, int cycle) {
		super.tick(tick, cycle);
		sneaking = player.isSneaking();
	}

	@Override
	public boolean isSneaking() {
		return sneaking;
	}

	@Override
	public void setWalkspeed(float walkspeed) {
		player.setWalkSpeed(walkspeed);
	}

	@Override
	public void actionbar(String text) {
		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(text));
	}

	@Override
	public void title(String title, String subtitle) {
		title(title, subtitle, -1, -1, -1);
	}

	@Override
	public void titleTimings(int fadeIn, int stay, int fadeOut) {
		this.fadeIn = fadeIn;
		this.stay = stay;
		this.fadeOut = fadeOut;
	}

	@Override
	public void title(String title, String subtitle, int fadeIn, int stay, int fadeOut) {
		player.sendTitle(title, subtitle,
				fadeIn >= 0 ? fadeIn : this.fadeIn,
				stay >= 0 ? stay : this.stay,
				fadeOut >= 0 ? fadeOut : this.fadeOut);
	}

	@Override
	public double getHealth() {
		return player.getHealth();
	}

	@Override
	public void setHealth(double health) {
		player.setHealth(health);
//		double healthDifference = health - getHealth();
//		if (healthDifference >= 0 || !damageAnimation) {
//		} else {
//			damage(-healthDifference);
//		}
	}

	@Override
	public void damage(double damage) {
		player.damage(damage);
	}

	@Override
	public void damageAnimation() {
		double health = player.getHealth();
		player.damage(0.3);
		player.setHealth(health);
	}

	@Override
	public void setHunger(int hunger) {
		player.setFoodLevel(hunger);
	}

	@Override
	public void setExperienceProgress(float progress) {
		player.setExp(progress);
	}

	@Override
	public void setLevel(int level) {
		player.setLevel(level);
	}

	@Override
	public void setBossHealth(double bossHealth) {
		bar.setProgress(bossHealth);
	}

	@Override
	public void setBossColor(int bossColor) {
		bar.setColor(switch (bossColor) {
		case UserMinecraft.BOSS_COLOR_WHITE:
			yield BarColor.WHITE;
		case UserMinecraft.BOSS_COLOR_RED:
			yield BarColor.RED;
		case UserMinecraft.BOSS_COLOR_GREEN:
			yield BarColor.GREEN;
		case UserMinecraft.BOSS_COLOR_BLUE:
			yield BarColor.BLUE;
		case UserMinecraft.BOSS_COLOR_YELLOW:
			yield BarColor.YELLOW;
		case UserMinecraft.BOSS_COLOR_PINK:
			yield BarColor.PINK;
		case UserMinecraft.BOSS_COLOR_PURPLE:
			yield BarColor.PURPLE;
		default:
			yield BarColor.YELLOW;
		});
	}

	@Override
	public void setBossbarText(String bossbarText) {
		bar.setTitle(bossbarText);
	}

	@Override
	public void setBossbarDarkSky(boolean darkSky) {
		if (darkSky) {
			bar.addFlag(BarFlag.DARKEN_SKY);
		} else {
			bar.removeFlag(BarFlag.DARKEN_SKY);
		}
	}

	@Override
	public void setScoreboard(Scoreboard scoreboard) {
		player.setScoreboard(scoreboard);
	}

	@Override
	public void setAbsorption(int absorption) {
		((CraftLivingEntity) player).setAbsorptionAmount(absorption);
	}

	@Override
	public void setGamemode(int gamemode) {
		player.setGameMode(switch (gamemode) {
		case 0:
			yield GameMode.SURVIVAL;
		case 1:
			yield GameMode.CREATIVE;
		case 3:
			yield GameMode.SPECTATOR;
		default:
			yield GameMode.ADVENTURE;
		});
	}

	@Override
	public void playSound(String sound, SoundCategory category, float volume, float pitch) {
		playSound(location, sound, category, volume, pitch);
	}

	@Override
	public void playSound(Vector3 location, String sound, SoundCategory category, float volume, float pitch) {
		player.playSound(location.toLocation(), sound, category, volume, pitch);
	}

	@Override
	public void stopSound(String sound, SoundCategory category) {
		player.stopSound(sound, category);
	}

	@Override
	public int getHeldItemSlot() {
		return inventory.getHeldItemSlot();
	}

	@Override
	public void setHeldItemSlot(int slotId) {
		inventory.setHeldItemSlot(slotId);
	}

	@Override
	public PlayerInventory getInventory() {
		return inventory;
	}

	@Override
	public void openInventory(Inventory inventory) {
		player.openInventory(inventory);
	}

	@Override
	public boolean hasCooldown(Material material) {
		return player.hasCooldown(material);
	}

	@Override
	public void setCooldown(Material material, int cooldown) {
		player.setCooldown(material, cooldown);
	}
}
