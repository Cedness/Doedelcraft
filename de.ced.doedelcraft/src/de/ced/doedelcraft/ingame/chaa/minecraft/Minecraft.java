package de.ced.doedelcraft.ingame.chaa.minecraft;

import java.util.UUID;

import de.ced.logic.vector.BukkitVector3;
import de.ced.logic.vector.LocationVector5;
import de.ced.logic.vector.Vector3;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.EntityEquipment;

import de.ced.doedelcraft.ingame.chaa.Unit;

public interface Minecraft extends Unit {

	boolean isLiving();

	@Deprecated
	Entity getEntity();

	UUID getUniqueId();

	void message(String text);

	LocationVector5 getLocation();

	void teleport();

	boolean isOnGround();

	BukkitVector3 getVelocity();

	void addVelocity(Vector3 velocity);

	void applyVelocity();

	float getFallDistance();

	void setFallDistance(float distance);

	void setFireTicks(int ticks);

	EntityEquipment getEquipment();
}
