package de.ced.doedelcraft.ingame.chaa.lasttick;

import de.ced.doedelcraft.ingame.chaa.Unit;
import de.ced.logic.vector.LocationVector5;

public interface LastTick extends Unit {

	public LocationVector5 getLastLocation();

	public boolean hasLocationChanged();

	public int getLastLocationChangeTick();

	public boolean getLastOnGround();

	public boolean hasOnGroundChanged();

	public int getLastOnGroundChangeTick();
}
