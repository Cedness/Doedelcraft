package de.ced.doedelcraft.ingame.chaa.lasttick;

import de.ced.doedelcraft.ingame.chaa.chunkloader.UserChunkLoader;
import org.bukkit.Location;

import de.ced.doedelcraft.ingame.chaa.AbstractUnit;
import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.doedelcraft.ingame.chaa.Tickable;
import de.ced.doedelcraft.ingame.chaa.combat.Combat;
import de.ced.doedelcraft.ingame.chaa.minecraft.Minecraft;
import de.ced.doedelcraft.ingame.chaa.positioner.UserPositioner;
import de.ced.logic.vector.LocationVector5;
import de.ced.logic.vector.Vector5;

public abstract class AbstractLastTick extends AbstractUnit implements LastTick, Tickable {

	private final Char chaa;
	private final LocationVector5 lastLocation;
	private boolean locationChanged = false;
	private int lastLocationChangeTick = Integer.MIN_VALUE;
	private boolean lastOnGround;
	private boolean onGroundChanged = false;
	private int lastOnGroundChangeTick = Integer.MIN_VALUE;

	/**
	 * Dependencies: Minecraft
	 */
	public AbstractLastTick(Char chaa) {
		this.chaa = chaa;
		Minecraft minecraft = chaa.getMinecraft();
		lastLocation = new LocationVector5(new Location(chaa.getCharManager().getDoedelcraft().getWorldManager().getWorld(), 0, 0, 0));
		lastLocation.set(minecraft.getLocation());
		lastOnGround = minecraft.isOnGround();
	}

	@Override
	public Class<?>[] dependencies(int cycle) {
		return new Class<?>[] { UserChunkLoader.class, UserPositioner.class, Combat.class };
		// TODO check if combat is neccessary
	}

	@Override
	public void tick(int tick, int cycle) {
		Minecraft minecraft = chaa.getMinecraft();
		Vector5 currentLocation = minecraft.getLocation();
		locationChanged = !lastLocation.equals(currentLocation);
		if (locationChanged) {
			lastLocationChangeTick = tick;
			lastLocation.set(currentLocation);
		}
		boolean currentOnGround = minecraft.isOnGround();
		onGroundChanged = lastOnGround != currentOnGround;
		if (onGroundChanged) {
			lastOnGroundChangeTick = tick;
		}
		lastOnGround = currentOnGround;
	}

	@Override
	public LocationVector5 getLastLocation() {
		return lastLocation;
	}

	@Override
	public boolean hasLocationChanged() {
		return locationChanged;
	}

	@Override
	public int getLastLocationChangeTick() {
		return lastLocationChangeTick;
	}

	@Override
	public boolean getLastOnGround() {
		return lastOnGround;
	}

	@Override
	public boolean hasOnGroundChanged() {
		return onGroundChanged;
	}

	@Override
	public int getLastOnGroundChangeTick() {
		return lastOnGroundChangeTick;
	}
}
