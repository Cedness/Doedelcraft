package de.ced.doedelcraft.ingame.chaa.lasttick;

import de.ced.doedelcraft.ingame.chaa.User;

public class GenuineUserLastTick extends AbstractLastTick implements UserLastTick {

	/**
	 * {@inheritDoc}
	 * Dependencies: Minecraft
	 */
	public GenuineUserLastTick(User user) {
		super(user);
	}
}
