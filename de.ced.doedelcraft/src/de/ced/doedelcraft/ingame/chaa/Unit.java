package de.ced.doedelcraft.ingame.chaa;

import de.ced.doedelcraft.Doedelcraft;

public interface Unit {

	default int getTick() {
		return Doedelcraft.currentTick();
	}

	static Class<?>[] concat(Class<?>[] dep1, Class<?>[] dep2) {
		int length = dep1.length + dep2.length;
		for (Class<?> aDep : dep1) {
			for (int i = 0; i < dep2.length; i++) {
				if (aDep.equals(dep2[i])) {
					length--;
					dep2[i] = null;
				}
			}
		}
		Class<?>[] dep = new Class<?>[length];
		int i = 0;
		for (Class<?> aDep : dep1) {
			dep[i++] = aDep;
		}
		for (Class<?> aDep : dep2) {
			if (aDep != null) {
				dep[i++] = aDep;
			}
		}
		return dep;
	}
}
