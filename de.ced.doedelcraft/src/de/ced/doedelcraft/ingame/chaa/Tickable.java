package de.ced.doedelcraft.ingame.chaa;

public interface Tickable extends Unit {

	public static final int EARLY = 0, DEFAULT = 1, LATE = 2, LATEST = 3;

	static int getHighestCycle() {
		return LATEST;
	}

	void tick(int tick, int cycle);

	default int[] supportedCycles() {
		return new int[] { DEFAULT };
	}

	default Class<?>[] dependencies(int cycle) {
		return new Class<?>[] {};
	}
}
