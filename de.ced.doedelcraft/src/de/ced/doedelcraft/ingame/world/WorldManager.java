package de.ced.doedelcraft.ingame.world;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.ingame.aoe.RealmFactory;
import de.ced.doedelcraft.ingame.world.time.TimeManager;
import org.bukkit.Bukkit;
import org.bukkit.World;

import de.ced.doedelcraft.ingame.world.chunk.ChunkStorage;

public class WorldManager {

	private final Doedelcraft doedelcraft;
	private final RealmFactory realmFactory;
	private final TimeManager timeManager;
	private final ChunkStorage chunkStorage;

	private final World world;

	public WorldManager(Doedelcraft doedelcraft) {
		this.doedelcraft = doedelcraft;
		world = Bukkit.getWorlds().get(0);
		realmFactory = new RealmFactory(this);
		timeManager = new TimeManager(this);
		new VanillaPreparator(this);
		chunkStorage = new ChunkStorage(this);
	}

	public Doedelcraft getDoedelcraft() {
		return doedelcraft;
	}

	public RealmFactory getRealmFactory() {
		return realmFactory;
	}

	public TimeManager getTimeManager() {
		return timeManager;
	}

	public ChunkStorage getChunkStorage() {
		return chunkStorage;
	}

	public World getWorld() {
		return world;
	}

	public void tick(int tick) {
		realmFactory.tick(tick);
		timeManager.tick(tick);
		chunkStorage.tick(tick);
	}
}
