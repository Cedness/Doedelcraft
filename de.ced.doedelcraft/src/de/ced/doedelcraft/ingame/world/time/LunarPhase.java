package de.ced.doedelcraft.ingame.world.time;

import java.util.List;
import java.util.stream.IntStream;

/**
 * Represents the lunar phases which can be displayed by minecraft
 */
public enum LunarPhase {
	FULL_MOON(1),
	WANING_GIBBOUS(3),
	THIRD_QUARTER(3),
	WANING_CRESCENT(3),
	NEW_MOON(1),
	WAXING_CRESCENT(3),
	FIRST_QUARTER(2),
	WAXING_GIBBOUS(3);

	private static final List<LunarPhase> LUNAR_PHASES = List.of(values());
	static {
		IntStream.range(0, LUNAR_PHASES.size()).forEach(id -> valueOf(id).id = id);
	}
	private static final int LUNAR_CYCLE_DAYS = LUNAR_PHASES.stream().mapToInt(LunarPhase::getDuration).sum();

	public static List<LunarPhase> lunarPhases() {
		return LUNAR_PHASES;
	}

	public static int size() {
		return LUNAR_PHASES.size();
	}

	public static LunarPhase valueOf(int id) {
		return LUNAR_PHASES.get(id);
	}

	public static int lunarCycleDays() {
		return LUNAR_CYCLE_DAYS;
	}

	private int id;
	/**
	 * How many days this phase should last
	 */
	private final int duration;

	LunarPhase(int duration) {
		assert duration > 0;
		this.duration = duration;
	}

	public int getId() {
		return id;
	}

	public int getDuration() {
		return duration;
	}
}