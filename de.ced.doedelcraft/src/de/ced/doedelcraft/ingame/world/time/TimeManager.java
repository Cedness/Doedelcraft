package de.ced.doedelcraft.ingame.world.time;

import org.bukkit.World;

import de.ced.doedelcraft.ingame.world.WorldManager;
import de.ced.doedelcraft.util.RealTimeManager;

/*
 * Agenda
 * 
 * Zeitverschiebung in MC  DONE
 * Zeiteinstellung beim Starten DONE
 * Mondphase bleibt gleich DONE
 * Unterschiedliche Mondphase je nach Belegung
 */

public class TimeManager {
	private static final long TICKS_PER_DAY = 20 * 60 * 60 * 24;
	private static final long MINECRAFT_TICKS_PER_DAY = 1000 * 24;
	private static final long MINECRAFT_TIME_OFFSET = 6000;
	private static final long LUNAR_PHASE_CHANGE_TICK = 6000;
	private static final long SPEED = 1;

	private final WorldManager worldManager;

	/**
	 * Time of the day in 20th of seconds
	 */
	private long time;
	/**
	 * Ingame time of the day in ticks
	 */
	private long minecraftTime;
	private LunarPhase lunarPhase;
	private int progressOfLunarPhase = 0;

	public TimeManager(WorldManager worldManager) {
		this.worldManager = worldManager;

		// Load real time
		long timeSinceEpoch = RealTimeManager.currentTimeTick() * SPEED;
		time = timeSinceEpoch % TICKS_PER_DAY;

		int lunarPhaseId = (int) (timeSinceEpoch / TICKS_PER_DAY % LunarPhase.lunarCycleDays());

		int count = 0;
		for (LunarPhase lunarPhase : LunarPhase.lunarPhases()) {
			progressOfLunarPhase = lunarPhaseId - count;
			count += lunarPhase.getDuration();

			if (count > lunarPhaseId) {
				this.lunarPhase = lunarPhase;
				break;
			}
		}


		// Sync real time to minecraft
		minecraftTime = convertToMinecraft(time);

		final long minecraftTimeSinceEpoch = worldManager.getWorld().getFullTime();

		long timeToAdd = minecraftTime - minecraftTimeSinceEpoch % MINECRAFT_TICKS_PER_DAY;
		timeToAdd += calculateTicksToSkip((int) (minecraftTimeSinceEpoch / MINECRAFT_TICKS_PER_DAY % LunarPhase.size()), lunarPhase.getId());
		updateTime(timeToAdd);
	}

	public void tick(int tick) {
		time = (time + SPEED) % TICKS_PER_DAY;

		long wantedMinecraftTime = convertToMinecraft(time);

		if (minecraftTime != wantedMinecraftTime) { // Minecraft time needs to advance
			long timeToAdd = wantedMinecraftTime - minecraftTime;

			if (checkForLunarPhaseChangeTick(minecraftTime, wantedMinecraftTime)) {
				if (progressOfLunarPhase + 1 >= lunarPhase.getDuration()) {
					lunarPhase = LunarPhase.valueOf((lunarPhase.getId() + 1) % LunarPhase.size());
					progressOfLunarPhase = 0;
					timeToAdd += MINECRAFT_TICKS_PER_DAY; // next lunar phase
				} else {
					progressOfLunarPhase++;
				}
			}

			updateTime(timeToAdd);

			minecraftTime = wantedMinecraftTime;
		}
	}

	public String getFormattedTime() {
		long hour, minute, second;
		long dayTick = this.time;

		dayTick /= 20; // sec

		hour = dayTick / 3600;
		dayTick -= hour * 3600;
		minute = dayTick / 60;
		dayTick -= minute * 60;
		second = dayTick;

		return (hour < 10 ? "0" : "") + hour + ":" + (minute < 10 ? "0" : "") + minute + ":" + (second < 10 ? "0" : "") + second;
	}

	public boolean setDayTime(int hour, int minute, int second, int tick) {
		long dayTick = hour * 60L;
		dayTick = (dayTick + minute) * 60;
		dayTick = (dayTick + second) * 20;
		dayTick += tick;

		if (dayTick < 0 || dayTick >= TICKS_PER_DAY) {
			return false;
		}

		this.time = dayTick;
		return true;
	}

	public int getHour() {
		return (int) (time * 24 / TICKS_PER_DAY);
	}

	public LunarPhase getLunarPhase() {
		return lunarPhase;
	}

	public void setLunarPhase(LunarPhase lunarPhase) {
		long timeToAdd = calculateTicksToSkip(this.lunarPhase.getId(), lunarPhase.getId());
		this.lunarPhase = lunarPhase;
		progressOfLunarPhase = 0;
		updateTimeUnchecked(timeToAdd);
	}

	public int getProgressOfLunarPhase() {
		return progressOfLunarPhase;
	}

	private long convertToMinecraft(long ticks) {
		long minecraftTicks = ticks * MINECRAFT_TICKS_PER_DAY / TICKS_PER_DAY;
		minecraftTicks = (minecraftTicks + MINECRAFT_TICKS_PER_DAY - MINECRAFT_TIME_OFFSET) % MINECRAFT_TICKS_PER_DAY;
		return minecraftTicks;
	}

	private boolean checkForLunarPhaseChangeTick(long fromTick, long toTick) {
		return fromTick < toTick
				? fromTick < LUNAR_PHASE_CHANGE_TICK && LUNAR_PHASE_CHANGE_TICK <= toTick
				: LUNAR_PHASE_CHANGE_TICK <= fromTick == LUNAR_PHASE_CHANGE_TICK <= toTick;
	}

	private long calculateTicksToSkip(int oldLunarPhase, int newLunarPhase) {
		int lunarPhasesCount = LunarPhase.size();
		return (newLunarPhase + lunarPhasesCount - oldLunarPhase) % lunarPhasesCount * MINECRAFT_TICKS_PER_DAY;
	}

	private void updateTime(long timeToAdd) {
		if (timeToAdd < 0) {
			timeToAdd += LunarPhase.size() * MINECRAFT_TICKS_PER_DAY;
		}
		updateTimeUnchecked(timeToAdd);
	}

	private void updateTimeUnchecked(long timeToAdd) {
		World world = worldManager.getWorld();
		world.setFullTime(world.getFullTime() + timeToAdd);
	}
}