package de.ced.doedelcraft.ingame.world.chunk;

import java.util.HashSet;
import java.util.Set;

import de.ced.doedelcraft.ingame.chaa.User;

public class AbstractChunk implements SadChunk {

	private final int chunkX;
	private final int chunkZ;
	private int loadedChunkId;

	private final Set<User> users = new HashSet<>();

	public AbstractChunk(int chunkX, int chunkZ) {
		this.chunkX = chunkX;
		this.chunkZ = chunkZ;
	}

	@Override
	public int getChunkX() {
		return chunkX;
	}

	@Override
	public int getChunkZ() {
		return chunkZ;
	}

	int getLoadedChunkId() {
		return loadedChunkId;
	}

	void setLoadedChunkId(int loadedChunkId) {
		this.loadedChunkId = loadedChunkId;
	}

	@Override
	public void addUser(User user) {
		users.add(user);
	}

	@Override
	public void removeUser(User user) {
		users.remove(user);
	}

	@Override
	public Set<User> getUsers() {
		return users;
	}

	@Override
	public int userCount() {
		return users.size();
	}
}
