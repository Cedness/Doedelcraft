package de.ced.doedelcraft.ingame.world.chunk;

public class EmptyChunk extends AbstractChunk {

	public EmptyChunk(int chunkX, int chunkZ) {
		super(chunkX, chunkZ);
	}
}
