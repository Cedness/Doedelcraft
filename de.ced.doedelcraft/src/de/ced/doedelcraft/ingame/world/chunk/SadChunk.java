package de.ced.doedelcraft.ingame.world.chunk;

import java.util.Collection;

import de.ced.doedelcraft.ingame.chaa.User;

public interface SadChunk {

	int getChunkX();

	int getChunkZ();

	void addUser(User user);

	void removeUser(User user);

	Collection<User> getUsers();

	int userCount();
}
