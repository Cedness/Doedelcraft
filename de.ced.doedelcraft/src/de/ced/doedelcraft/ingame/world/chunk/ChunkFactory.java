package de.ced.doedelcraft.ingame.world.chunk;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.util.Logger;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

public class ChunkFactory {

	private final Doedelcraft doedelcraft;
	private final ChunkStorage chunkStorage;
	private final List<List<ChunkSection>> chunkSections;
	private final List<ChunkSection> loadedChunkSections;
	private final File directory;

	public ChunkFactory(ChunkStorage chunkStorage) {
		doedelcraft = chunkStorage.getWorldManager().getDoedelcraft();
		this.chunkStorage = chunkStorage;
		chunkSections = new ArrayList<>(chunkStorage.getCapacity() >>> 4);
		for (int i = 0; i < chunkSections.size(); i++) {
			chunkSections.add(new ArrayList<>(Arrays.asList(new ChunkSection[chunkSections.size()])));
		}
		loadedChunkSections = new ArrayList<>();
		directory = new File(doedelcraft.getDataFolder().getPath() + "/chunks");
		directory.mkdir();
	}

	public void tick(int tick) {
		for (ChunkSection chunkSection : loadedChunkSections) {
			if (tick - chunkSection.lastAccess > 47) {
				if (chunkSection.loadedChunks <= 0) {
					unloadChunkSection(chunkSection);
				} else {
					chunkSection.config = null;
				}
			}
		}
	}

	public SadChunk loadChunk(int chunkX, int chunkZ) { // TODO loading multiple chunks with only one ChunkSection load
		int fileX = chunkX >>> 4;
		int fileZ = chunkZ >>> 4;
		ChunkSection chunkSection = loadChunkSection(fileX, fileZ);
		int fileChunkX = chunkX - (fileX << 4);
		int fileChunkZ = chunkZ - (fileZ << 4);
		return loadChunk(chunkSection, loadChunkData(chunkSection, fileChunkX, fileChunkZ), chunkX, chunkZ, fileChunkX, fileChunkZ);
	}

	public SadChunk loadOrCreateChunk(int chunkX, int chunkZ) { // TODO loading multiple chunks with only one ChunkSection load
		int fileX = chunkX >>> 4;
		int fileZ = chunkZ >>> 4;
		ChunkSection chunkSection = loadChunkSection(fileX, fileZ);
		int fileChunkX = chunkX - (fileX << 4);
		int fileChunkZ = chunkZ - (fileZ << 4);
		return loadChunk(chunkSection, loadOrCreateChunkData(chunkSection, fileChunkX, fileChunkZ), chunkX, chunkZ, fileChunkX, fileChunkZ);
	}

	public void unloadChunk(int chunkX, int chunkZ) {
		unloadChunk(chunkStorage.getChunk(chunkX, chunkZ));
	}

	public void unloadChunk(SadChunk chunk) {
		if (!(chunk instanceof AbstractChunk)) {
			return;
		}
		if (chunk instanceof EmptyChunk) {
			removeChunk((AbstractChunk) chunk);
			return;
		}
		if (!(chunk instanceof ContentChunk)) {
			return;
		}
		int chunkX = chunk.getChunkX();
		int chunkZ = chunk.getChunkZ();
		int fileX = chunkX >>> 4;
		int fileZ = chunkZ >>> 4;
		ChunkSection chunkSection = loadChunkSection(fileX, fileZ);
		int fileChunkX = chunkX - (fileX << 4);
		int fileChunkZ = chunkZ - (fileZ << 4);
		unloadChunk(chunkSection, (ContentChunk) chunk, fileChunkX, fileChunkZ);
	}

	private ChunkSection loadChunkSection(int fileX, int fileZ) {
		ChunkSection chunkSection = chunkSections.get(fileZ).get(fileX);
		boolean hasToCreate;
		if (hasToCreate = chunkSection == null) {
			chunkSection = new ChunkSection(fileX, fileZ, loadedChunkSections.size());
			chunkSections.get(fileZ).set(fileX, chunkSection);
			loadedChunkSections.add(chunkSection);
		}
		if (hasToCreate || chunkSection.config == null) {
			String name = fileX + "." + fileZ + ".chunk";
			File file = new File(directory, name);
			YamlConfiguration config;
			if (!file.exists()) {
				config = new YamlConfiguration();
				config.createSection("chunks");
				try {
					file.createNewFile();
					config.save(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				config = YamlConfiguration.loadConfiguration(file);
			}
			chunkSection.config = config;
		}
		chunkSection.lastAccess = doedelcraft.getTick();
		return chunkSection;
	}

	private void unloadChunkSection(ChunkSection chunkSection) {
		String name = chunkSection.chunkSectionX + "." + chunkSection.chunkSectionZ + ".chunk";
		File file = new File(directory, name);
		try {
			chunkSection.config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		chunkSections.get(chunkSection.chunkSectionX).set(chunkSection.chunkSectionZ, null);
		loadedChunkSections.set(chunkSection.chunkSectionId, null);
	}

	private class ChunkSection {
		private final int chunkSectionX;
		private final int chunkSectionZ;
		private final int chunkSectionId;
		private YamlConfiguration config;
		private int loadedChunks;
		private int lastAccess;

		ChunkSection(int chunkSectionX, int chunkSectionZ, int chunkSectionId) {
			this.chunkSectionX = chunkSectionX;
			this.chunkSectionZ = chunkSectionZ;
			this.chunkSectionId = chunkSectionId;
		}
	}

	private AbstractChunk loadChunk(ChunkSection chunkSection, ConfigurationSection chunkConfig, int chunkX, int chunkZ, int fileChunkX, int fileChunkZ) {
		AbstractChunk chunk;
		if (chunkConfig == null) {
			chunk = createEmptyChunk(chunkX, chunkZ);
			Logger.info("Empty Chunk " + chunkX + " " + chunkZ + " created");
		} else {
			chunk = loadChunk(chunkConfig, chunkX, chunkZ);
			chunkSection.loadedChunks++;
			Logger.info("Chunk " + chunkX + " " + chunkZ + " loaded from config");
		}
		chunkStorage.addChunk(chunk);
		return chunk;
	}

	private void unloadChunk(ChunkSection chunkSection, ContentChunk chunk, int fileChunkX, int fileChunkZ) {
		ConfigurationSection chunkConfig = loadExistingChunkData(chunkSection, fileChunkX, fileChunkZ);
		saveChunk(chunkConfig, chunk);
		chunkSection.loadedChunks--;
		removeChunk(chunk);
	}

	private void removeChunk(AbstractChunk chunk) {
		chunkStorage.removeChunk(chunk);
	}

	private ConfigurationSection loadExistingChunkData(ChunkSection chunkSection, int fileChunkX, int fileChunkZ) {
		return chunkSection.config
				.getConfigurationSection("chunks")
				.getConfigurationSection(String.valueOf(fileChunkZ))
				.getConfigurationSection(String.valueOf(fileChunkX));
	}

	private ConfigurationSection loadChunkData(ChunkSection chunkSection, int fileChunkX, int fileChunkZ) {
		ConfigurationSection rows = chunkSection.config.getConfigurationSection("chunks").getConfigurationSection(String.valueOf(fileChunkZ));
		return rows == null ? null : rows.getConfigurationSection(String.valueOf(fileChunkX));
	}

	private ConfigurationSection loadOrCreateChunkData(ChunkSection chunkSection, int fileChunkX, int fileChunkZ) {
		YamlConfiguration config = chunkSection.config;
		boolean notCreated = false;
		String pathZ = String.valueOf(fileChunkZ);
		String pathX = String.valueOf(fileChunkX);
		ConfigurationSection chunks = config.getConfigurationSection("chunks");
		ConfigurationSection rows = chunks.getConfigurationSection(pathZ);
		if (notCreated = rows == null) {
			rows = config.createSection(pathZ);
		}
		ConfigurationSection chunkConfig;
		if (notCreated || (chunkConfig = rows.getConfigurationSection(pathX)) == null) {
			chunkConfig = rows.createSection(pathX);
		}
		return chunkConfig;
	}

	private EmptyChunk createEmptyChunk(int chunkX, int chunkZ) {
		return new EmptyChunk(chunkX, chunkZ);
	}

	private ContentChunk loadChunk(ConfigurationSection chunkConfig, int chunkX, int chunkZ) {
		return new ContentChunk(chunkX, chunkZ);
	}

	private void saveChunk(ConfigurationSection chunkConfig, ContentChunk chunk) {
	}
}
