package de.ced.doedelcraft.ingame.world.chunk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.ced.doedelcraft.ingame.world.WorldManager;

public class ChunkStorage {

	public static final int INITIAL_CAPACITY = 1024;

	private final WorldManager worldManager;
	private final List<List<SadChunk>> chunks = new ArrayList<>(INITIAL_CAPACITY);
	private final List<SadChunk> loadedChunks = new ArrayList<>();
	private final ChunkFactory chunkFactory;
	private final ChunkAccess[] chunkAccesses = { new RegularChunkAccess(), new LoadChunkAccess() };
	private ChunkAccess chunkAccess;

	public ChunkStorage(WorldManager worldManager) {
		this.worldManager = worldManager;
		for (int i = 0; i < chunks.size(); i++) {
			chunks.add(new ArrayList<>(Arrays.asList(new SadChunk[chunks.size()])));
		}
		chunkFactory = new ChunkFactory(this);
	}

	public WorldManager getWorldManager() {
		return worldManager;
	}

	public ChunkFactory getChunkFactory() {
		return chunkFactory;
	}

	public void tick(int tick) {
		chunkFactory.tick(tick);
	}

	public void setChunkAccess(int chunkAccess) {
		this.chunkAccess = chunkAccesses[chunkAccess];
	}

	private abstract class ChunkAccess {
		SadChunk getLoadedChunk(int x, int z) {
			return chunks.get(z).get(x);
		}

		SadChunk getChunk(int x, int z) {
			SadChunk chunk = getLoadedChunk(x, z);
			if (chunk == null) {
				chunk = loadChunk(x, z);
				chunks.get(z).set(x, chunk);
			}
			return chunk;
		}

		abstract SadChunk loadChunk(int x, int z);
	}

	private class RegularChunkAccess extends ChunkAccess {
		@Override
		SadChunk loadChunk(int x, int z) {
			return chunkFactory.loadChunk(x, z);
		}
	}

	private class LoadChunkAccess extends ChunkAccess {
		@Override
		SadChunk loadChunk(int x, int z) {
			return chunkFactory.loadOrCreateChunk(x, z);
		}
	}

	public boolean translateIsLoaded(int x, int z) {
		x >>>= 4;
		z >>>= 4;
		return isLoaded(x, z);
	}

	public boolean isLoaded(int x, int z) {
		return chunkAccess.getLoadedChunk(x, z) != null;
	}

	public SadChunk translateGetLoadedChunk(int x, int z) {
		x >>>= 4;
		z >>>= 4;
		return getLoadedChunk(x, z);
	}

	public SadChunk getLoadedChunk(int x, int z) {
		return chunkAccess.getLoadedChunk(x, z);
	}

	public SadChunk translateGetChunk(int x, int z) {
		x >>>= 4;
		z >>>= 4;
		return getChunk(x, z);
	}

	public SadChunk getChunk(int x, int z) {
		return chunkAccess.getChunk(x, z);
	}

	void addChunk(AbstractChunk chunk) {
		chunks.get(chunk.getChunkZ()).set(chunk.getChunkX(), chunk);
		chunk.setLoadedChunkId(loadedChunks.size());
		loadedChunks.add(chunk);
	}

	void removeChunk(AbstractChunk chunk) {
		chunks.get(chunk.getChunkZ()).set(chunk.getChunkX(), null);
		loadedChunks.set(chunk.getLoadedChunkId(), null);
		chunk.setLoadedChunkId(0);
	}

	int getCapacity() {
		return chunks.size();
	}
}
