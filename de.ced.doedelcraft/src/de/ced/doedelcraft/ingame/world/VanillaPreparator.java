package de.ced.doedelcraft.ingame.world;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import de.ced.doedelcraft.util.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.GameRule;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

@SuppressWarnings("rawtypes")
public class VanillaPreparator {

	@SuppressWarnings("unchecked")
	public VanillaPreparator(WorldManager worldManager) {
		World world = worldManager.getWorld();
		YamlConfiguration config = worldManager.getDoedelcraft().loadConfig("vanilla");
		Bukkit.setIdleTimeout(0);
		Bukkit.setWhitelist(false);
		Bukkit.setDefaultGameMode(GameMode.valueOf(config.getString("gamemode", GameMode.ADVENTURE.name())));
		world.setDifficulty(Difficulty.valueOf(config.getString("difficulty", Difficulty.NORMAL.name())));

		List<GameRule> unconfiguredGamerules = new ArrayList<>();
		for (GameRule gamerule : GameRule.values()) {
			unconfiguredGamerules.add(gamerule);
		}

		for (Entry<String, Object> entry : config.getConfigurationSection("gamerules").getValues(false).entrySet()) {
			String name = entry.getKey();
			GameRule gamerule = GameRule.getByName(name);
			if (gamerule == null) {
				Logger.warning("Gamerule " + name + " seems to be no longer supported");
				continue;
			}
			unconfiguredGamerules.remove(gamerule);
			Object value = entry.getValue();
			Class<?> excpectedType = gamerule.getType();
			Class<?> givenType = value == null ? null : value.getClass();
			if (!excpectedType.equals(givenType)) {
				Logger.warning("Wrong value type for gamerule " + gamerule.getName() + ": " +
						(givenType == null ? "null" : givenType.getSimpleName()) + " given, " +
						excpectedType.getSimpleName() + " excptected");
				continue;
			}
			world.setGameRule(gamerule, value);
		}
		if (!unconfiguredGamerules.isEmpty()) {
			Logger.warning(unconfiguredGamerules.size() + " unconfigured gamerule(s):");
			for (GameRule<?> gamerule : unconfiguredGamerules) {
				Logger.warning(gamerule.getName());
			}
		}
	}
}
