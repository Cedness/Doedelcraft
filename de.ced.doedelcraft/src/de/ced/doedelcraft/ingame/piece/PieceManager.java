package de.ced.doedelcraft.ingame.piece;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.util.Logger;
import org.bukkit.configuration.file.YamlConfiguration;

import de.ced.doedelcraft.ingame.chaa.CharManager;
import de.ced.doedelcraft.ingame.piece.definition.PieceDefinition;

/*
 * Axt mehr Crit-Chance
 * Schwert schneller
 * Dolche Crit von hinten
 */
public class PieceManager {

	private String classPrefix;

	private final Doedelcraft doedelcraft;
	private YamlConfiguration pieceConfig;

	private List<PieceDefinition> definitions = new ArrayList<>();

	@SuppressWarnings("unchecked")
	public PieceManager(CharManager charManager) {
		classPrefix = getClass().getPackageName() + ".definition.";
		doedelcraft = charManager.getDoedelcraft();
		try {
			pieceConfig = doedelcraft.loadConfig("pieces");
			for (Map<?, ?> map : pieceConfig.getMapList("pieces")) {
				try {
					loadDefintion((Map<String, Object>) map);
				} catch (Exception ex) {
					Logger.warning("Exception while loading a PieceDefintion");
					ex.printStackTrace();
				}
			}
		} catch (Exception ex) {
			Logger.severe("Exception while loading PieceDefintions");
			ex.printStackTrace();
		}
	}

	private void loadDefintion(Map<String, Object> map) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException {
		int id = (int) map.get("id");
		String type = (String) map.getOrDefault("type", "");

		String className = classPrefix + type + "Piece";
		// Logger.info(className);
		@SuppressWarnings("unchecked")
		Class<? extends PieceDefinition> definitionClass = (Class<? extends PieceDefinition>) Class.forName(className);

		PieceDefinition definition = definitionClass.getConstructor(getClass(), Map.class).newInstance(this, map);
		// Logger.info("Loaded " + definition);

		while (definitions.size() < id + 1) {
			definitions.add(null);
		}
		definitions.set(id, definition);
	}

	public PieceDefinition getDefinition(int id) {
		return id < definitions.size() ? definitions.get(id) : null;
	}
}
