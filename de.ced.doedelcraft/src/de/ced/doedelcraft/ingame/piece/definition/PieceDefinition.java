package de.ced.doedelcraft.ingame.piece.definition;

import java.util.HashMap;
import java.util.Map;

import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.piece.Piece;
import de.ced.doedelcraft.ingame.piece.PieceManager;
import de.ced.doedelcraft.util.SadItem;
import org.bukkit.Material;

public abstract class PieceDefinition {

	protected final PieceManager pieceManager;
	protected final int id;
	protected final String name;
	protected final Material material;

	public PieceDefinition(PieceManager pieceManager, Map<String, Object> map) {
		this.pieceManager = pieceManager;
		id = (int) map.get("id");
		name = (String) map.get("name");
		material = Material.valueOf(((String) map.getOrDefault("material", "BARRIER")).toUpperCase());
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Material getMaterial() {
		return material;
	}

	public boolean areStackable(Piece piece, Piece anotherPiece) {
		return this == piece.getDefinition() && this == anotherPiece.getDefinition();
	}

	public Object getDataCopy(Piece piece) {
		return null;
	}

	public boolean use(User user, Piece piece) {
		return false;
	}

	public SadItem toItem(Piece piece) {
		return toItem(piece, new SadItem(material));
	}

	public SadItem toItem(Piece piece, SadItem item) {
		item.setItem(material).setText("§e" + name, "", String.valueOf(id));
		if (this instanceof Stackable) {
			item.setCount(piece.getAmount());
		}
		return item;
	}

	public Piece create() {
		return new Piece(this);
	}

	public Piece load(Map<String, Object> pieceMap) {
		Piece piece = create();
		piece.setAmount(this instanceof Stackable && pieceMap.containsKey("amount") ? (int) pieceMap.get("amount") : 1);
		return piece;
	}

	public Map<String, Object> save(Piece piece) {
		Map<String, Object> pieceMap = new HashMap<String, Object>();
		pieceMap.put("id", id);
		int amount = piece.getAmount();
		if (amount != 1) {
			pieceMap.put("amount", amount);
		}
		return pieceMap;
	}

	@Override
	public String toString() {
		return id + "/" + name + "[" + material.name().toLowerCase() + (this instanceof Stackable ? ",stackable" : "")
				+ "]";
	}
}
