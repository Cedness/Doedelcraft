package de.ced.doedelcraft.ingame.piece.definition;

import java.util.Map;

import de.ced.doedelcraft.ingame.piece.PieceManager;

public class UselessPiece extends PieceDefinition implements Stackable {

	public UselessPiece(PieceManager pieceManager, Map<String, Object> map) {
		super(pieceManager, map);
	}
}
