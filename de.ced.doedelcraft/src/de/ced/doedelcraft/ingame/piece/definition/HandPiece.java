package de.ced.doedelcraft.ingame.piece.definition;

import java.util.Map;

import de.ced.doedelcraft.ingame.chaa.User;
import de.ced.doedelcraft.ingame.piece.Piece;
import de.ced.doedelcraft.ingame.piece.PieceManager;

public class HandPiece extends PieceDefinition {

	private final boolean twoHanded;

	public HandPiece(PieceManager pieceManager, Map<String, Object> map) {
		super(pieceManager, map);
		twoHanded = (boolean) map.getOrDefault("twoHanded", false);
	}

	public boolean isTwoHanded() {
		return twoHanded;
	}

	@Override
	public boolean use(User user, Piece piece) {
		return true;
	}
}
