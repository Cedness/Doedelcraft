package de.ced.doedelcraft.ingame.piece.definition;

import java.util.List;
import java.util.Map;

import de.ced.doedelcraft.ingame.piece.Bag;
import de.ced.doedelcraft.ingame.piece.Piece;
import de.ced.doedelcraft.ingame.piece.PieceManager;

public class BagPiece extends PieceDefinition {

	protected final int rows;

	public BagPiece(PieceManager pieceManager, Map<String, Object> map) {
		super(pieceManager, map);
		this.rows = (int) map.getOrDefault("rows", 3);
	}

	@Override
	public Piece create() {
		Piece piece = new Piece(this);
		piece.setData(new Bag(rows));
		return piece;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Piece load(Map<String, Object> pieceMap) {
		Piece piece = new Piece(this);
		piece.setData(pieceMap.containsKey("bagContent") ? new Bag(rows, (List<Map<?, ?>>) pieceMap.get("bagContent"), pieceManager) : new Bag(rows));
		return piece;
	}

	@Override
	public Map<String, Object> save(Piece piece) {
		Map<String, Object> pieceMap = super.save(piece);
		pieceMap.put("bagContent", ((Bag) piece.getData()).save());
		return pieceMap;
	}
}
