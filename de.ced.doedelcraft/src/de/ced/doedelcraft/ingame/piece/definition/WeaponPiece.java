package de.ced.doedelcraft.ingame.piece.definition;

import java.util.Map;

import de.ced.doedelcraft.ingame.piece.PieceManager;

public class WeaponPiece extends HandPiece {

	protected final int damage;

	public WeaponPiece(PieceManager pieceManager, Map<String, Object> map) {
		super(pieceManager, map);
		damage = (int) map.getOrDefault("damage", 1);
	}

	public int getDamage() {
		return damage;
	}
}
