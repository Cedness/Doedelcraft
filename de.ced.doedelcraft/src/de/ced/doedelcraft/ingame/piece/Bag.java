package de.ced.doedelcraft.ingame.piece;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.ced.doedelcraft.ingame.piece.definition.Stackable;
import de.ced.doedelcraft.util.Logger;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.PlayerInventory;

import de.ced.doedelcraft.ingame.piece.definition.PieceDefinition;

public class Bag {

	private List<Piece> content = new ArrayList<>();
	private int changed = 1;

	public Bag(int rows) {
		for (int i = 0; i < 9 * rows; i++) {
			content.add(null);
		}
	}

	public Bag(int rows, List<Map<?, ?>> content, PieceManager pieceManager) {
		this(rows);
		for (Map<?, ?> pieceMapRaw : content) {
			try {
				@SuppressWarnings("unchecked")
				Map<String, Object> pieceMap = (Map<String, Object>) pieceMapRaw;
				int slot = (int) pieceMap.get("slot");
				int id = (int) pieceMap.get("id");
				PieceDefinition definition = pieceManager.getDefinition(id);
				Piece piece = definition.load(pieceMap);
				this.content.set(slot, piece);
			} catch (Exception ex) {
				Logger.warning("Exception while loading a piece");
				ex.printStackTrace();
			}
		}
	}

	public List<Map<String, Object>> save() {
		List<Map<String, Object>> content = new ArrayList<>();
		for (int i = 0; i < this.content.size(); i++) {
			Piece piece = this.content.get(i);
			// Logger.info(i + "");
			if (piece == null)
				continue;
			Map<String, Object> pieceMap = piece.getDefinition().save(piece);
			pieceMap.put("slot", i);
			content.add(pieceMap);
			Logger.info("Saved " + piece.getDefinition().getName());
		}
		return content;
	}

	public void applyTo(Inventory inventory) {
		int start = inventory instanceof PlayerInventory ? 9 : 0;
		for (int i = 0; i < content.size(); i++) {
			Piece piece = content.get(i);
			inventory.setItem(i + start, piece != null ? piece.getDefinition().toItem(piece) : null);
		}
	}

	public void applyTo(int slot, Inventory inventory) {
		Piece piece = content.get(slot);
		inventory.setItem(slot + (inventory instanceof PlayerInventory ? 9 : 0),
				piece != null ? piece.getDefinition().toItem(piece) : null);
	}

	public int getChanged() {
		return changed;
	}

	public boolean has(int slot) {
		return content.get(slot) != null;
	}

	public Piece get(int slot) {
		return content.get(slot);
	}

	// Click actions

	public Piece takeFrom(int slot) {
		changed = has(slot) ? 1 : 0;
		return content.set(slot, null);
	}

	public Piece takeHalfFrom(int slot) {
		if (!has(slot)) {
			changed = 0;
			return null;
		}
		Piece piece = content.get(slot);
		int amount = piece.getAmount();
		int remaining = amount / 2;
		if (remaining < 1) {
			content.set(slot, null);
			changed = 1;
			return piece;
		} else if (!(piece.getDefinition() instanceof Stackable)) {
			content.set(slot, null);
			changed = -1;
			return piece;
		}
		Piece result = piece.clone();
		piece.setAmount(remaining);
		result.setAmount(amount - remaining);
		changed = 1;
		return result;
	}

	public Piece swap(int slot, Piece piece) { // TODO Check for equality
		if (!has(slot)) {
			Piece there = content.get(slot);
			if (there.getDefinition().areStackable(there, piece)) {
				changed = -1;
				return piece;
			}
		}

		changed = 1;
		return content.set(slot, piece);
	}

	public Piece placeUnsupportedTo(int slot, Piece piece) {
		if (!has(slot)) {
			content.set(slot, piece);
			changed = 1;
			return null;
		} else {
			Piece there = content.get(slot);
			int thereAmount = there.getAmount();
			int availableAmount = 64 - thereAmount;
			if (!there.getDefinition().areStackable(there, piece) || availableAmount <= 0) {
				changed = -1;
				return piece;
			}
			int amountToAdd = piece.getAmount();
			if (amountToAdd > availableAmount) {
				there.setAmount(64);
				piece.setAmount(amountToAdd - availableAmount);
			} else {
				there.setAmount(thereAmount + amountToAdd);
				piece = null;
			}
			changed = 1;
			return piece;
		}
	}

	public Piece placeOneUnsupportedTo(int slot, Piece piece) {
		if (!has(slot)) {
			Piece copy = piece.clone();
			copy.setAmount(1);
			content.set(slot, copy);
		} else {
			Piece there = content.get(slot);
			int thereAmount = there.getAmount();
			int availableAmount = 64 - thereAmount;
			if (!there.getDefinition().areStackable(there, piece) || availableAmount <= 0) {
				changed = -1;
				return piece;
			}
			there.setAmount(thereAmount + 1);
		}
		changed = 1;
		int pieceAmount = piece.getAmount();
		if (pieceAmount <= 1) {
			return null;
		}
		piece.setAmount(pieceAmount - 1);
		return piece;
	}

	public Piece placeTo(int slot, Piece piece) {
		if (!has(slot)) {
			content.set(slot, piece);
		} else {
			Piece there = content.get(slot);
			if (!there.getDefinition().areStackable(there, piece)) {
				changed = -1;
				return piece;
			}
			there.setAmount(there.getAmount() + piece.getAmount());
		}
		changed = 1;
		return null;
	}

	public Piece placeOneTo(int slot, Piece piece) {
		if (!has(slot)) {
			Piece copy = piece.clone();
			copy.setAmount(1);
			content.set(slot, copy);
		} else {
			Piece there = content.get(slot);
			if (!there.getDefinition().areStackable(there, piece)) {
				changed = -1;
				return piece;
			}
			there.setAmount(there.getAmount() + 1);
		}
		changed = 1;
		int amount = piece.getAmount();
		if (amount <= 1) {
			return null;
		}
		piece.setAmount(amount - 1);
		return piece;
	}

	public Piece placeSomeTo(int slot, Piece piece) {
		if (!has(slot)) {
			content.set(slot, piece);
			changed = 1;
			return null;
		} else {
			Piece there = content.get(slot);
			int thereAmount = there.getAmount();
			int availableAmount = 64 - thereAmount;
			if (!there.getDefinition().areStackable(there, piece) || availableAmount <= 0) {
				changed = -1;
				return piece;
			}
			int amountToAdd = piece.getAmount();
			if (amountToAdd > availableAmount) {
				there.setAmount(64);
				piece.setAmount(amountToAdd - availableAmount);
			} else {
				there.setAmount(thereAmount + amountToAdd);
				piece = null;
			}
			changed = 1;
			return piece;
		}
	}

	public void print() {
		for (int i = 0; i < content.size(); i++) {
			Logger.info(i + ": " + content.get(i));
		}
	}
}
