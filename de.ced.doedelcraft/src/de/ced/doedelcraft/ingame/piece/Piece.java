package de.ced.doedelcraft.ingame.piece;

import de.ced.doedelcraft.ingame.piece.definition.PieceDefinition;

public class Piece {

	private final PieceDefinition definition;
	private int amount = 1;
	private Object data = null;

	public Piece(PieceDefinition definition) {
		this.definition = definition;
	}

	private Piece(Piece piece) {
		definition = piece.definition;
		amount = piece.amount;
		data = definition.getDataCopy(piece);
	}

	public PieceDefinition getDefinition() {
		return definition;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Override
	public Piece clone() {
		return new Piece(this);
	}

	@Override
	public String toString() {
		return amount + "x" + definition.getName() + "(" + definition.getId() + ")";
	}
}
