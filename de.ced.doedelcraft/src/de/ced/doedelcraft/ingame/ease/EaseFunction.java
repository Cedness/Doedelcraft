package de.ced.doedelcraft.ingame.ease;

public interface EaseFunction {

	double function(double v);
}
