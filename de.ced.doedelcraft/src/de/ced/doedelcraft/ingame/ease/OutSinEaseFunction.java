package de.ced.doedelcraft.ingame.ease;

public class OutSinEaseFunction implements EaseFunction {

	@Override
	public double function(double v) {
		return Math.sin(0.5 * Math.PI * v);
	}
}
