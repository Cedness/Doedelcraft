package de.ced.doedelcraft.util;

import de.ced.doedelcraft.Doedelcraft;
import de.ced.doedelcraft.account.AccountManager;
import de.ced.doedelcraft.menu.MenuManager;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerCommandSendEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.event.server.ServerListPingEvent;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;

import de.ced.doedelcraft.ingame.chaa.CharManager;
import de.ced.doedelcraft.ingame.chaa.NPManager;
import de.ced.doedelcraft.ingame.chaa.UserManager;

/**
 * This class handles (most) of the Bukkit-Events and forwards them to the
 * corresponding manager to keep other classes more clean.
 * 
 * If you want to add your own manager here, make sure to add a field and a line
 * in {@link ready()} and {@link done()}.
 * If an event is already used by a manager, mind that event data may be changed
 * by any manager. The manager which gets called last usually has the last say.
 * To get the initial data, put your manager before all which change what you
 * want to read.
 * 
 * @author Ced
 */
public class ListenerClass implements Listener {

	private Doedelcraft doedelcraft;
	private AccountManager accountManager;
	private MenuManager menuManager;
	private CharManager charManager;
	private UserManager userManager;
	@SuppressWarnings("unused")
	private NPManager npManager;

	public ListenerClass(Doedelcraft doedelcraft) {
		this.doedelcraft = doedelcraft;
	}

	public void ready() {
		accountManager = doedelcraft.getAccountManager();
		menuManager = doedelcraft.getMenuManager();
		charManager = doedelcraft.getCharManager();
		userManager = charManager.getUserManager();
		npManager = charManager.getNPManager();
		Bukkit.getPluginManager().registerEvents(this, doedelcraft);
		ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(doedelcraft, PacketType.Play.Server.ENTITY_METADATA) {
			@Override
			public void onPacketSending(PacketEvent event) {
				onPacket(event);
			}
		});
	}

	public void done() {
		accountManager = null;
		menuManager = null;
		charManager = null;
		userManager = null;
		npManager = null;
	}

	public void onPacket(PacketEvent e) {
		userManager.onPacket(e);
	}

	@EventHandler
	public void onPing(ServerListPingEvent e) {
		accountManager.onPing(e);
	}

	@EventHandler
	public void onLogin(PlayerLoginEvent e) {
		accountManager.onLogin(e);
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		accountManager.onJoin(e);
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		accountManager.onQuit(e);
	}

	@EventHandler
	public void onPlayerCommand(PlayerCommandPreprocessEvent e) {
		accountManager.getCommandManager().onPlayerCommand(e);
	}

	@EventHandler
	public void onCommandListSend(PlayerCommandSendEvent e) {
		accountManager.getCommandManager().onCommandListSend(e);
	}

	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		userManager.onMove(e);
	}

	@EventHandler
	public void onTeleport(PlayerTeleportEvent e) {
		userManager.onTeleport(e);
	}

	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		charManager.onDamage(e);
	}

	@EventHandler
	public void onFood(FoodLevelChangeEvent e) {
		userManager.onFoodLevelChange(e);
	}

	@EventHandler
	public void onExperienceChange(PlayerExpChangeEvent e) {
		userManager.onExperienceChange(e);
	}

	@EventHandler
	public void onLevelChange(PlayerLevelChangeEvent e) {
		userManager.onLevelChange(e);
	}

	@EventHandler
	public void onRegainHealth(EntityRegainHealthEvent e) {
		charManager.onRegainHealth(e);
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		userManager.onInteract(e);
	}

	@EventHandler
	public void onItemSwap(PlayerSwapHandItemsEvent e) {
		userManager.onSwapHandItems(e);
	}

	@EventHandler
	public void onAnimation(PlayerAnimationEvent e) {
		userManager.onAnimation(e);
	}

	@EventHandler
	public void onScroll(PlayerItemHeldEvent e) {
		userManager.onHeldItemChange(e);
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		userManager.onDrop(e);
	}

	@EventHandler
	public void onSneak(PlayerToggleSneakEvent e) {
		userManager.onSneak(e);
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		userManager.onInventoryClick(e);
		menuManager.onClick(e);
	}

	@EventHandler
	public void onInventoryDrag(InventoryDragEvent e) {
		userManager.onInventoryDrag(e);
	}

	@EventHandler
	public void onInventoryMove(InventoryMoveItemEvent e) {
		userManager.onInventoryMove(e);
	}

	@EventHandler
	public void onInventoryOpen(InventoryOpenEvent e) {
		menuManager.onMenuOpen(e);
		userManager.onInventoryOpen(e);
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		userManager.onInventoryClose(e);
		menuManager.onMenuClose(e);
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		menuManager.onChat(e);
		userManager.onChat(e);
	}
}
