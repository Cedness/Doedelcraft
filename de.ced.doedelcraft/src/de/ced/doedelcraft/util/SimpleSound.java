package de.ced.doedelcraft.util;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/**
 * Simple sounds not using the sound-chain-system, which therefore also work in
 * the main-menu or while otherwise not logged in as a user.
 * 
 * @author Ced
 */
public class SimpleSound {

	private final Sound sound;
	private final float volume;
	private final float pitch;
	private final boolean ambient;

	public SimpleSound() {
		this(null, 1, 1, true);
	}

	public SimpleSound(Sound sound) {
		this(sound, 1, 1, true);
	}

	public SimpleSound(Sound sound, boolean ambient) {
		this(sound, 1, 1, ambient);
	}

	public SimpleSound(Sound sound, double volume, double pitch) {
		this(sound, volume, pitch, true);
	}

	public SimpleSound(Sound sound, double volume, double pitch, boolean ambient) {
		this.sound = sound;
		this.volume = (float) volume;
		this.pitch = (float) pitch;
		this.ambient = ambient;
	}

	public void autoplay(Entity entity) {
		if (entity instanceof Player) {
			autoplay((Player) entity);
			return;
		}
		if (ambient) {
			playAt(entity);
		}
	}

	public void autoplay(Player player) {
		if (ambient) {
			playAt(player);
		} else {
			playFor(player);
		}
	}

	public void playAt(Entity entity) {
		playAt(entity.getLocation());
	}

	public void playAt(double x, double y, double z) {
		playAt(new Location(Bukkit.getWorlds().get(0), x, y, z));
	}

	public void playAt(Location location) {
		if (sound == null) {
			return;
		}
		location.getWorld().playSound(location, sound, SoundCategory.MASTER, volume, pitch);
	}

	public void playFor(Player player) {
		playFor(player, player.getLocation());
	}

	public void playFor(Player player, double x, double y, double z) {
		playFor(player, new Location(player.getWorld(), x, y, z));
	}

	public void playFor(Player player, Location location) {
		if (sound == null) {
			return;
		}
		player.playSound(location, sound, SoundCategory.MASTER, volume, pitch);
	}

	public void autostop(Player player) {
		if (ambient) {
			stopForAll();
		} else {
			stopFor(player);
		}
	}

	public void stopFor(Player player) {
		if (sound == null) {
			return;
		}
		player.stopSound(sound, SoundCategory.MASTER);
	}

	public void stopForAll() {
		if (sound == null) {
			return;
		}
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.stopSound(sound, SoundCategory.MASTER);
		}
	}

	public void stopAround(Player player) {
		stopAround(player.getLocation());
	}

	public void stopAround(Location location) {
		if (sound == null) {
			return;
		}
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (player.getLocation().distance(location) > 15) {
				continue;
			}
			player.stopSound(sound, SoundCategory.MASTER);
		}
	}

	public Sound getSound() {
		return sound;
	}

	public float getVolume() {
		return volume;
	}

	public float getPitch() {
		return pitch;
	}

	public boolean isAmbient() {
		return ambient;
	}
}
