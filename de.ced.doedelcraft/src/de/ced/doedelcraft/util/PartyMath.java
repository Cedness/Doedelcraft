package de.ced.doedelcraft.util;

import de.ced.doedelcraft.ingame.chaa.Char;
import de.ced.logic.vector.Vector2;
import de.ced.logic.vector.Vector3;
import de.ced.logic.vector.Vector5;

/**
 * Static methods to do some commonly used math.
 * 
 * @author Ced
 */
public class PartyMath {

	public static int square(int i) {
		return i * i;
	}

	public static double square(double d) {
		return d * d;
	}

	public static Vector2 getCircleXY(double percent, double radius) {
		percent = percent * 2.0 * Math.PI;
		return Vector2.from(radius * Math.cos(percent), radius * Math.sin(percent));
	}

	public static Vector3[] getArmPositions(Char chaa) {
		final double SIDE_OFFSET = 0.35;
		final double FRONT_OFFSET = 0.25;
		final double HEIGHT_OFFSET = 0.75;

		Vector5 location = chaa.getMinecraft().getLocation();
		double yaw = location.a() / 180 * Math.PI;
		double yawSin = Math.sin(yaw);
		double yawCos = Math.cos(yaw);
		double xOffset = SIDE_OFFSET * yawCos;
		double zOffset = SIDE_OFFSET * yawSin;
		double yOffset = HEIGHT_OFFSET;
		double xFrontOffset = -FRONT_OFFSET * yawSin;
		double zFrontOffset = FRONT_OFFSET * yawCos;
		Vector3[] result = new Vector3[2];

		Vector3 left = Vector3.from(xOffset + xFrontOffset, yOffset, zOffset + zFrontOffset);
		Vector3 right = Vector3.from(-xOffset + xFrontOffset, yOffset, -zOffset + zFrontOffset);
		result[0] = left.add(location);
		result[1] = right.add(location);
		return result;
	}

	public static Vector3 getRandoms(double multiplier) {
		Vector3 result = Vector3.empty();
		for (int i = 0; i < 3; i++) {
			result.set(i, Math.random() * multiplier);
		}
		return result;
	}
}
