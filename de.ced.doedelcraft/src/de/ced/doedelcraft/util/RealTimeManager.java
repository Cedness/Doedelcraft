package de.ced.doedelcraft.util;

import java.util.Calendar;
import java.util.Locale;

import de.ced.doedelcraft.account.Language;
import de.ced.doedelcraft.account.Dictionary;

public class RealTimeManager {

	private static final Calendar calendar = Calendar.getInstance();

	public static void refresh() {
		calendar.setTimeInMillis(System.currentTimeMillis());
	}

	public static long currentTimeTick() {
		long now = calendar.getTimeInMillis();
		now += calendar.getTimeZone().getOffset(now);
		return now / 50L;
	}

	public static int getHour() {
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	public static String getHourFix() {
		int value = getHour();
		return (value < 10 ? "0" : "") + value;
	}

	public static int getMinute() {
		return calendar.get(Calendar.MINUTE);
	}

	public static String getMinuteFix() {
		int value = getMinute();
		return (value < 10 ? "0" : "") + value;
	}

	public static int getSecond() {
		return calendar.get(Calendar.SECOND);
	}

	public static String getSecondFix() {
		int value = getSecond();
		return (value < 10 ? "0" : "") + value;
	}

	public static int getMillisecond() {
		return calendar.get(Calendar.MILLISECOND);
	}

	public static String getMillisecondFix() {
		int value = getMillisecond();
		return (value < 10 ? "0" : "") + value;
	}

	public static int getDay() {
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	public static String getDayFix() {
		int value = getDay();
		return (value < 10 ? "0" : "") + value;
	}

	public static int getMonth() {
		return calendar.get(Calendar.MONTH) + 1;
	}

	public static String getMonthFix() {
		int value = getMonth();
		return (value < 10 ? "0" : "") + value;
	}

	public static String getMonthName() {
		return calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
	}

	public static int getYear() {
		return calendar.get(Calendar.YEAR);
	}

	public static int getWeekDay() {
		return calendar.get(Calendar.DAY_OF_WEEK);
	}

	public static String getWeekDayName() {
		return calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.ENGLISH);
	}

	public static Timespan getTimespan(long value) {
		return new Timespan(value);
	}

	public static Timespan getTimespanToNow(long value) {
		return new Timespan(value - calendar.getTimeInMillis());
	}

	public static class Timespan {

		private static final String LANG = "time";

		private final long value;

		private Timespan(long value) {
			this.value = value;
		}

		public long getValue() {
			return value;
		}

		public long getSeconds() {
			return value / 1000;
		}

		public long getMinutes() {
			return getSeconds() / 60;
		}

		public long getHours() {
			return getMinutes() / 60;
		}

		public long getDays() {
			return getHours() / 24;
		}

		public long getWeeks() {
			return getDays() / 7;
		}

		public long getMonths() {
			return getDays() / 30;
		}

		public long getYears() {
			return getDays() / 365;
		}

		public String humanize(Language language) {
			Dictionary dictionary = language.getD(LANG);
			Value value = new Value();
			String text = dictionary.get("more_than") + " ";

			if (value.set(getYears())) {
				text += value.absolute() + " " + dictionary.get("year" + value.getPlural());
			} else if (value.set(getMonths())) {
				text += value.absolute() + " " + dictionary.get("month" + value.getPlural());
			} else if (value.set(getWeeks())) {
				text += value.absolute() + " " + dictionary.get("week" + value.getPlural());
			} else if (value.set(getDays())) {
				text += value.absolute() + " " + dictionary.get("day" + value.getPlural());
			} else if (value.set(getHours())) {
				text += value.absolute() + " " + dictionary.get("hour" + value.getPlural());
			} else if (value.set(getMinutes())) {
				text = value.absolute() + " " + dictionary.get("minute" + value.getPlural());
			} else {
				value.set(getSeconds());
				text = value.absolute() + " " + dictionary.get("second" + value.getPlural());
			}
			return text + " " + dictionary.get(this.value > 0 ? "future" : "past");
		}

		private class Value {

			private long absolute;
			private boolean multiple;

			public boolean set(long value) {
				absolute = Math.abs(value);
				multiple = absolute != 1;
				return absolute > 0;
			}

			public long absolute() {
				return absolute;
			}

			public String getPlural() {
				return multiple ? "s" : "";
			}
		}
	}
}
