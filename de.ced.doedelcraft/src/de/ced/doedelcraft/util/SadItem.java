package de.ced.doedelcraft.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SadItem extends ItemStack {

	private static final Material DEFAULT_ITEM = Material.BARRIER;

	private ItemMeta meta = null;

	public SadItem() {
		this(DEFAULT_ITEM);
	}

	public SadItem(Material item) {
		super(item);
		loadMeta();
		meta.addItemFlags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_POTION_EFFECTS, ItemFlag.HIDE_ATTRIBUTES);
		meta.setUnbreakable(true);
		saveMeta();
	}

	public SadItem(String name) {
		this(DEFAULT_ITEM, name);
	}

	public SadItem(Material item, String name) {
		this(item);
		setName(name);
	}

	public SadItem(String... text) {
		this(DEFAULT_ITEM, text);
	}

	public SadItem(Material item, String... text) {
		this(item);
		setText(text);
	}

	public SadItem(List<String> text) {
		this(DEFAULT_ITEM, text);
	}

	public SadItem(Material item, List<String> text) {
		this(item);
		setText(text);
	}

	public SadItem(ItemStack itemStack) {
		super(itemStack);
	}

	private void loadMeta() {
		if (meta != null) {
			return;
		}
		meta = getItemMeta();
	}

	private void saveMeta() {
		setItemMeta(meta);
		meta = null;
	}

	public Material getItem() {
		return getType();
	}

	public SadItem setItem(Material item) {
		setItem0(item);
		return this;
	}

	private void setItem0(Material item) {
		setType(item);
	}

	public List<String> getText() {
		loadMeta();
		return getText0();
	}

	private List<String> getText0() {
		List<String> text = new ArrayList<>();
		text.add(getName0());
		text.addAll(getLore0());
		return text;
	}

	public SadItem setText(String... text) {
		return setText(Arrays.asList(text));
	}

	public SadItem setText(List<String> text) {
		loadMeta();
		setText0(text);
		saveMeta();
		return this;
	}

	private void setText0(List<String> text) {
		String name = null;
		List<String> lore = null;
		if (text != null && !text.isEmpty()) {
			name = text.get(0);
			lore = text.subList(1, text.size());
		}
		setName0(name);
		setLore0(lore);
	}

	public SadItem modifyText(String... text) {
		return modifyText(Arrays.asList(text));
	}

	public SadItem modifyText(List<String> text) {
		loadMeta();
		modifyText0(text);
		saveMeta();
		return this;
	}

	private void modifyText0(List<String> text) {
		modifyText0(text, (currentLine, line) -> {
			return line;
		});
	}

	public SadItem extendText(String... text) {
		return extendText(Arrays.asList(text));
	}

	public SadItem extendText(List<String> text) {
		loadMeta();
		extendText0(text);
		saveMeta();
		return this;
	}

	private void extendText0(List<String> text) {
		modifyText0(text, (currentLine, line) -> {
			return currentLine + line;
		});
	}

	public SadItem shiftText(String... text) {
		return shiftText(Arrays.asList(text));
	}

	public SadItem shiftText(List<String> text) {
		loadMeta();
		shiftText0(text);
		saveMeta();
		return this;
	}

	private void shiftText0(List<String> text) {
		modifyText0(text, (currentLine, line) -> {
			return line + currentLine;
		});
	}

	private void modifyText0(List<String> text, Modifier modifier) {
		List<String> currentText = getText0();
		int i;
		for (i = 0; i < currentText.size() && i < text.size(); i++) {
			String line = text.get(i);
			if (line != null) {
				currentText.set(i, modifier.modify(currentText.get(i), line));
			}
		}
		currentText.addAll(text.subList(i, text.size()));
		setText0(currentText);
	}

	private interface Modifier {
		public abstract String modify(String currentLine, String line);
	}

	public String getName() {
		loadMeta();
		return getName0();
	}

	private String getName0() {
		return meta.hasDisplayName() ? meta.getDisplayName() : "";
	}

	/**
	 * Use only for items without lore. Use {@link setText(List<String> text)} for
	 * other items.
	 */
	public SadItem setName(String name) {
		loadMeta();
		setName0(name);
		saveMeta();
		return this;
	}

	private void setName0(String name) {
		if (name == null) {
			name = "";
		}
		meta.setDisplayName(name);
	}

	@Deprecated
	public List<String> getLore() {
		loadMeta();
		return getLore0();
	}

	private List<String> getLore0() {
		return meta.hasLore() ? meta.getLore() : List.of();
	}

	@Deprecated
	public SadItem setLore(String... lore) {
		return setLore(Arrays.asList(lore));
	}

	@Deprecated
	public SadItem setLore(List<String> lore) {
		loadMeta();
		setLore0(lore);
		saveMeta();
		return this;
	}

	private void setLore0(List<String> lore) {
		meta.setLore(lore);
	}

	public int getCount() {
		return getAmount();
	}

	public SadItem setCount(int amount) {
		super.setAmount(amount);
		return this;
	}

	public boolean isEnchanted() {
		loadMeta();
		return isEnchanted0();
	}

	private boolean isEnchanted0() {
		return meta.hasEnchants();
	}

	public SadItem enchant(boolean enchant) {
		loadMeta();
		enchant0(enchant);
		saveMeta();
		return this;
	}

	private void enchant0(boolean enchant) {
		if (enchant) {
			meta.addEnchant(Enchantment.DURABILITY, 1, true);
		} else {
			meta.removeEnchant(Enchantment.DURABILITY);
		}
	}

	@Override
	public SadItem clone() {
		return new SadItem(this);
	}

	public static Inventory cloneInventory(Inventory inventory, Inventory clone) {
		ItemStack[] contents = inventory.getContents();
		for (int i = 0; i < contents.length; i++) {
			ItemStack item = contents[i];
			if (item == null)
				continue;
			clone.setItem(i, item);
		}
		return clone;
	}

	public static List<String> parseText(String text, int maxLength) {
		String[] paragraphs = text.split("\n");
		String[][] words = new String[paragraphs.length][];
		for (int i = 0; i < paragraphs.length; i++) {
			words[i] = paragraphs[i].split(" ");
		}
		List<String> lines = new ArrayList<>();
		for (int paragraphId = 0; paragraphId < words.length; paragraphId++) {
			StringBuilder builder = new StringBuilder();
			int length = 0;
			for (int wordId = 0; wordId < words[paragraphId].length; wordId++) {
				String word = words[paragraphId][wordId];
				int wordLength = word.length();
				if (length + wordLength > maxLength) {
					applyLine(builder, lines, length);
					builder = new StringBuilder();
					length = 0;
				}
				length += wordLength + 1;
				builder.append(word).append(' ');
			}
			applyLine(builder, lines, length);
		}
		return lines;
	}

	private static void applyLine(StringBuilder builder, List<String> lines, int length) {
		lines.add(builder.deleteCharAt(length - 1).toString());
	}
}
