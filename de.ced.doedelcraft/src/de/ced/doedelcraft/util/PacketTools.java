package de.ced.doedelcraft.util;

import java.lang.reflect.Field;

import de.ced.doedelcraft.Doedelcraft;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.mojang.authlib.GameProfile;

import net.minecraft.server.v1_16_R2.EntityPlayer;
import net.minecraft.server.v1_16_R2.Packet;
import net.minecraft.server.v1_16_R2.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_16_R2.PacketPlayOutPlayerInfo;

/**
 * Useful methods to do some stuff not covered by Bukkit.
 * 
 * @author Ced
 */
public class PacketTools {

	private static Field nameField;

	static {
		try {
			nameField = GameProfile.class.getDeclaredField("name");
			nameField.setAccessible(true);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
	}

	public static void setName(Player player, String name, Doedelcraft doedelcraft) {
		CraftPlayer craftPlayer = (CraftPlayer) player;
		GameProfile gameProfile = craftPlayer.getProfile();
		try {
			nameField.set(gameProfile, name);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		int entityId = craftPlayer.getEntityId();
		sendPacketToAllExcept(player, new PacketPlayOutEntityDestroy(entityId));
		EntityPlayer handle = craftPlayer.getHandle();
		sendPacketToAll(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, handle));
		Bukkit.getScheduler().runTaskLater(doedelcraft, () -> {
			sendPacketToAllExcept(player, new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, handle));
			sendPacketToAll(new PacketPlayOutEntityDestroy(entityId));
		}, 4);
	}

	public static void sendPacketToAllExcept(Player player, Packet<?> packet) {
		for (Player receiver : Bukkit.getOnlinePlayers()) {
			if (receiver.equals(player)) {
				continue;
			}
			sendPacketTo(receiver, packet);
		}
	}

	public static void sendPacketToAll(Packet<?> packet) {
		for (Player receiver : Bukkit.getOnlinePlayers()) {
			sendPacketTo(receiver, packet);
		}
	}

	public static void sendPacketTo(Player player, Packet<?> packet) {
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
	}
}
