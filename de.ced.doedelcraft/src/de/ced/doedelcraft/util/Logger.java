package de.ced.doedelcraft.util;

import org.bukkit.Bukkit;

/**
 * Prints strings to the servers stdout and log.
 * 
 * @author Ced
 */
public class Logger {

	private static final java.util.logging.Logger logger = Bukkit.getLogger();
	public static final String PREFIX = "[Doedelcraft] ";

	private static String processMessage(String msg) {
		return PREFIX + msg;
	}

	public static void info(String msg) {
		logger.info(processMessage(msg));
	}

	public static void warning(String msg) {
		logger.warning(processMessage(msg));
	}

	public static void severe(String msg) {
		logger.severe(processMessage(msg));
	}

	public static void fatal(String msg) {
		logger.severe("[FATAL] " + processMessage(msg));
	}
}
