name: logout
permission_level: 2
needs_user: false
results:
- message: logout
  color: 4
  sound: BLOCK_BEACON_DEACTIVATE
- message: logout_fail
  color: c
  sound: BLOCK_ANVIL_FALL