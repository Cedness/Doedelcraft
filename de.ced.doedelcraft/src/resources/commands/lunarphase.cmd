name: lunarphase
permission_level: 1
needs_user: false
results:
- message: lunarphase_fail
  color: c
- message: lunarphase_display
  color: 7
- message: lunarphase_set
  color: a