name: refresh
permission_level: 1
results:
- message: cheat_fail
  color: c
- message: refresh_fail
  color: c
- message: cannot_find
  color: c
- message: refresh
  color: a