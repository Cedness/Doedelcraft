module de.ced.doedelcraft {
    exports de.ced.doedelcraft.game;
    requires spigot;
    requires de.ced.logic;
    requires ProtocolLib;
    requires java.logging;
}