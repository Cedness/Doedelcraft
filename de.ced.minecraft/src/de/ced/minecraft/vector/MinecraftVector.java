package de.ced.minecraft.vector;

import de.ced.logic.vector.Vector3;
import org.bukkit.Location;
import org.bukkit.World;

import static de.ced.minecraft.MinecraftWorldManager.getWorld;

public interface MinecraftVector {

    default Location toLocation() {
        return toLocation(getWorld());
    }

    default Location toLocation(World world) {
        return LocationFactory.toLocationFallback(getVectorForConversion(), world);
    }

    default org.bukkit.util.Vector toBukkitVector() {
        return LocationFactory.toBukkitVectorFallback(getVectorForConversion());
    }

    Vector3 getVectorForConversion();
}
