package de.ced.minecraft.vector;

import de.ced.logic.vector.NonArrayVector3;
import de.ced.logic.vector.Vector3;
import org.bukkit.Location;

public abstract class LocationVector3 extends NonArrayVector3 implements MinecraftVector3, LocationHolder {

	protected LocationVector3() {}

	public abstract Vector3 replace(Location location);

	@Override
	public LocationVector3 location() {
		return this;
	}

	@Override
	public double x() {
		return toLocation().getX();
	}

	@Override
	public Vector3 setX(double x) {
		toLocation().setX(x);
		return this;
	}

	@Override
	public double y() {
		return toLocation().getY();
	}

	@Override
	public Vector3 setY(double y) {
		toLocation().setY(y);
		return this;
	}

	@Override
	public double z() {
		return toLocation().getZ();
	}

	@Override
	public Vector3 setZ(double z) {
		toLocation().setZ(z);
		return this;
	}

	@Override
	public abstract Location toLocation();

	@Override
	public org.bukkit.util.Vector toBukkitVector() {
		return toLocation().toVector();
	}
}
