package de.ced.minecraft.vector;

import de.ced.logic.vector.Vector3;
import org.bukkit.Location;

public class IndependentLocationVector3 extends LocationVector3 {

	private Location location;

	public IndependentLocationVector3() {}

	public IndependentLocationVector3(Location location) {
		this.location = location;
	}

	@Override
	public Vector3 replace(Location location) {
		this.location = location;
		return this;
	}

	@Override
	public Location toLocation() {
		return location;
	}
}
