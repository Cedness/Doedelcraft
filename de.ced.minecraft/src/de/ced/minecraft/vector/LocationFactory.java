package de.ced.minecraft.vector;

import de.ced.logic.vector.Vector3;
import de.ced.logic.vector.Vector5;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.function.Function;

import static de.ced.minecraft.MinecraftWorldManager.getWorld;

public class LocationFactory {

    public static Location toLocation(Vector3 vector) {
        return toLocation(vector, getWorld());
    }

    public static Location toLocation(Vector3 vector, World world) {
        return convertByMinecraft(vector,
                MinecraftVector::toLocation,
                vector3 -> toLocationFallback(vector3, world)
        );
    }

    public static Location toLocationFallback(Vector3 vector, World world) {
        return convertByDimension(vector,
                vector5 -> new Location(world, vector5.x(), vector5.y(), vector5.z(), (float) vector5.a(), (float) vector5.b()),
                vector3 -> new Location(world, vector3.x(), vector3.y(), vector3.z())
        );
    }

    public static Location toLocation(Vector3 vector, Location location) {
        Function<Vector3, Location> vector3Function = vector3 -> {
            location.setX(vector3.x());
            location.setY(vector3.y());
            location.setZ(vector3.z());
            return location;
        };
        return convertByDimension(vector,
                vector5 -> {
                    vector3Function.apply(vector5);
                    location.setYaw((float) vector5.a());
                    location.setPitch((float) vector5.b());
                    return location;
                },
                vector3Function
        );
    }

    public static org.bukkit.util.Vector toBukkitVector(Vector3 vector) {
        return convertByMinecraft(vector,
                MinecraftVector::toBukkitVector,
                LocationFactory::toBukkitVectorFallback
        );
    }

    public static org.bukkit.util.Vector toBukkitVectorFallback(Vector3 vector) {
        return new org.bukkit.util.Vector(vector.x(), vector.y(), vector.z());
    }

    public static org.bukkit.util.Vector toBukkitVector(Vector3 vector, org.bukkit.util.Vector bukkitVector) {
        return bukkitVector.setX(vector.x()).setY(vector.y()).setZ(vector.z());
    }

    private static <T> T convertByMinecraftAndDimension(Vector3 vector,
                                                        Function<MinecraftVector, T> minecraftVectorFunction,
                                                        Function<Vector5, T> vector5Function,
                                                        Function<Vector3, T> vector3Function) {
        return convertByMinecraft(vector, minecraftVectorFunction, vector0 -> convertByDimension(vector0, vector5Function, vector3Function));
    }

    private static <T> T convertByMinecraft(Vector3 vector,
                                            Function<MinecraftVector, T> minecraftVectorFunction,
                                            Function<Vector3, T> vectorFunction) {
        if (vector instanceof MinecraftVector) {
            return minecraftVectorFunction.apply((MinecraftVector) vector);
        } else {
            return vectorFunction.apply(vector);
        }
    }

    private static <T> T convertByDimension(Vector3 vector,
                                            Function<Vector5, T> vector5Function,
                                            Function<Vector3, T> vector3Function) {
        if (vector instanceof Vector5) {
            return vector5Function.apply((Vector5) vector);
        } else {
            return vector3Function.apply(vector);
        }
    }
}
