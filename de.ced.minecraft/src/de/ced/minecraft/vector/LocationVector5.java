package de.ced.minecraft.vector;

import de.ced.logic.vector.NonArrayVector5;
import de.ced.logic.vector.Vector5;
import org.bukkit.Location;

public class LocationVector5 extends NonArrayVector5 implements MinecraftVector3, LocationHolder, YawPitchHolder {

	private Location location;
	private final LocationVector3 locationPart = new LocationPart3(this);
	private final YawPitchPart2 yawPitchPart = new YawPitchPart2(this);

	public LocationVector5(Location location) {
		replace(location);
	}

	public Vector5 replace(Location location) {
		this.location = location;
		return this;
	}

	@Override
	public LocationVector3 location() {
		return locationPart;
	}

	@Override
	public YawPitchPart2 yawPitch() {
		return yawPitchPart;
	}

	@Override
	public double x() {
		return location.getX();
	}

	@Override
	public Vector5 setX(double x) {
		location.setX(x);
		return this;
	}

	@Override
	public double y() {
		return location.getY();
	}

	@Override
	public Vector5 setY(double y) {
		location.setY(y);
		return this;
	}

	@Override
	public double z() {
		return location.getZ();
	}

	@Override
	public Vector5 setZ(double z) {
		location.setZ(z);
		return this;
	}

	@Override
	public double a() {
		return location.getYaw();
	}

	@Override
	public Vector5 setA(double a) {
		location.setYaw((float) a);
		return this;
	}

	@Override
	public double b() {
		return location.getPitch();
	}

	@Override
	public Vector5 setB(double b) {
		location.setPitch((float) b);
		return this;
	}

	@Override
	public Location toLocation() {
		return location;
	}

	@Override
	public org.bukkit.util.Vector toBukkitVector() {
		return location.toVector();
	}
}
