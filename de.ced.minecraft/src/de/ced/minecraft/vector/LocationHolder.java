package de.ced.minecraft.vector;

public interface LocationHolder {
	LocationVector3 location();
}
