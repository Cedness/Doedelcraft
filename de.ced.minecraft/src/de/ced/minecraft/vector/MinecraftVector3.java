package de.ced.minecraft.vector;

import de.ced.logic.vector.Vector3;

public interface MinecraftVector3 extends Vector3, MinecraftVector {

    @Override
    default Vector3 getVectorForConversion() {
        return this;
    }
}
