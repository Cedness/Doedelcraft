package de.ced.minecraft.vector;

import de.ced.logic.vector.Vector3;
import org.bukkit.Location;

public class LocationPart3 extends LocationVector3 {

	private final LocationVector5 vector;

	public LocationPart3(LocationVector5 vector) {
		this.vector = vector;
	}

	@Override
	public Vector3 replace(Location location) {
		vector.replace(location);
		return this;
	}

	@Override
	public Location toLocation() {
		return vector.toLocation();
	}
}
