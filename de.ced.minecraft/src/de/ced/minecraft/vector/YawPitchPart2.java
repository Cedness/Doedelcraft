package de.ced.minecraft.vector;

import de.ced.logic.vector.NonArrayVector2;
import de.ced.logic.vector.Vector2;
import de.ced.logic.vector.Vector3;
import org.bukkit.Location;

public class YawPitchPart2 extends NonArrayVector2 implements MinecraftVector, YawPitchHolder {

	private final LocationVector5 vector;

	public YawPitchPart2(LocationVector5 parent) {
		vector = parent;
	}

	public Vector2 replace(Location location) {
		vector.replace(location);
		return this;
	}

	@Override
	public YawPitchPart2 yawPitch() {
		return this;
	}

	@Override
	public double x() {
		return vector.toLocation().getYaw();
	}

	@Override
	public Vector2 setX(double yaw) {
		vector.toLocation().setYaw((float) yaw);
		return this;
	}

	@Override
	public double y() {
		return vector.toLocation().getPitch();
	}

	@Override
	public Vector2 setY(double pitch) {
		vector.toLocation().setPitch((float) pitch);
		return this;
	}

	@Override
	public Location toLocation() {
		return vector.toLocation();
	}

	@Override
	public org.bukkit.util.Vector toBukkitVector() {
		return vector.toLocation().toVector();
	}

	@Override
	public Vector3 getVectorForConversion() {
		return vector;
	}
}
