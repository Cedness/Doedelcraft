package de.ced.minecraft.vector;

import de.ced.logic.vector.NonArrayVector3;
import de.ced.logic.vector.Vector3;
import org.bukkit.Location;
import org.bukkit.World;

public class BukkitVector3 extends NonArrayVector3 implements MinecraftVector3 {

	private org.bukkit.util.Vector bukkitVector;

	public BukkitVector3(org.bukkit.util.Vector bukkitVector) {
		this.bukkitVector = bukkitVector;
	}

	public void replace(org.bukkit.util.Vector bukkitVector) {
		this.bukkitVector = bukkitVector;
	}

	@Override
	public double x() { return bukkitVector.getX(); }

	@Override
	public Vector3 setX(double x) {
		bukkitVector.setX(x);
		return this;
	}

	@Override
	public double y() { return bukkitVector.getY(); }

	@Override
	public Vector3 setY(double y) {
		bukkitVector.setY(y);
		return this;
	}

	@Override
	public double z() { return bukkitVector.getZ(); }

	@Override
	public Vector3 setZ(double z) {
		bukkitVector.setZ(z);
		return this;
	}

	@Override
	public Location toLocation(World world) {
		return bukkitVector.toLocation(world);
	}

	@Override
	public org.bukkit.util.Vector toBukkitVector() {
		return bukkitVector;
	}
}
