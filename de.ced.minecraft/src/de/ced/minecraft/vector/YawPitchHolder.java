package de.ced.minecraft.vector;

import de.ced.logic.vector.Vector2;
import de.ced.logic.vector.Vector3;

public interface YawPitchHolder {

	YawPitchPart2 yawPitch();

	default Vector3 toRay() {
		return toRay(Vector3.empty());
	}

	default Vector3 toRay(Vector3 target) {
		Vector2 yawPitch = yawPitch();
		double yaw = Math.toRadians(yawPitch.x() + 180.0);
		return target.set(Math.sin(yaw), Math.cos(Math.toRadians(yawPitch.y() + 90.0)), -Math.cos(yaw));
	}
}
