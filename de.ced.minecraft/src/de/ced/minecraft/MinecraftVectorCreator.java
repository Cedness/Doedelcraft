package de.ced.minecraft;

import de.ced.doedelcraft.game.GameVectorCreator;
import de.ced.minecraft.vector.IndependentLocationVector3;

public class MinecraftVectorCreator implements GameVectorCreator {

    private final MinecraftWorldManager worldManager;

    public MinecraftVectorCreator(MinecraftWorldManager worldManager) {
        this.worldManager = worldManager;
    }

    @Override
    public IndependentLocationVector3 createLocationVector() {
        return new IndependentLocationVector3();
    }
}
