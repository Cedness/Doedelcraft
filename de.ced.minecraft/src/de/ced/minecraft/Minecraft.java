package de.ced.minecraft;

public class Minecraft {

    private final MinecraftPlugin plugin;
    private final MinecraftWorldManager worldManager;
    private final MinecraftVectorCreator vectorCreator;

    public Minecraft(MinecraftPlugin plugin) {
        this.plugin = plugin;
        worldManager = new MinecraftWorldManager();
        vectorCreator = new MinecraftVectorCreator(worldManager);
    }
}
