package de.ced.minecraft;

import org.bukkit.Bukkit;
import org.bukkit.World;

public class MinecraftWorldManager {

    private static final World WORLD = Bukkit.getWorlds().stream().findFirst().orElseThrow(() -> new IllegalStateException("No world loaded"));

    public static World getWorld() {
        return WORLD;
    }
}
