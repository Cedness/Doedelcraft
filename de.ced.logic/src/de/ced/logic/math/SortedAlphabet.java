package de.ced.logic.math;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class SortedAlphabet {

	private List<Integer> valueToChar;
	private Map<Integer, Integer> charToValue;

	public SortedAlphabet(String symbols) {
		valueToChar = new ArrayList<>(symbols.length());
		for (int i = 0; i < symbols.length(); i++) {
		}
	}

	public SortedAlphabet(Integer... symbols) {
		this(Arrays.asList(symbols));
	}

	public SortedAlphabet(Collection<Integer> symbols) {
		valueToChar = new ArrayList<>(symbols);
		init();
	}

	private void init() {
		for (int value = 0; value < valueToChar.size(); value++) {
			charToValue.put(valueToChar.get(value), value);
		}
	}

	public int getSymbol(int value) {
		return valueToChar.get(value);
	}

	public int getValue(int symbol) {
		return charToValue.get(symbol);
	}

	public int getSize() {
		return valueToChar.size();
	}
}
