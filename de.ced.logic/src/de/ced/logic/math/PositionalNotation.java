package de.ced.logic.math;

public class PositionalNotation {

	private final SortedAlphabet alphabet;

	public PositionalNotation(SortedAlphabet alphabet) {
		this.alphabet = alphabet;
	}

	public int getBase() {
		return alphabet.getSize();
	}
}
