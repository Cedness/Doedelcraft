package de.ced.logic.math;

import java.util.function.DoubleBinaryOperator;
import java.util.function.Supplier;

public class SadMatrix {

	double[][] values;

	public SadMatrix(int rowCount, int columnCount) {
		values = new double[rowCount][columnCount];
	}

	public double get(int row, int column) {
		return values[row][column];
	}

	public void set(int row, int column, double value) {
		values[row][column] = value;
	}

	public int rowCount() {
		return values.length;
	}

	public int columnCount() {
		return values[0].length;
	}

	public boolean isSquareMatrix() {
		return rowCount() == columnCount();
	}

	public boolean isRowMatrix() {
		return rowCount() == 1;
	}

	public boolean isColumnMatrix() {
		return columnCount() == 1;
	}

	public boolean isScalar() {
		return isRowMatrix() && isColumnMatrix();
	}

	public boolean isSymmetric() {
		if (!isSquareMatrix()) {
			return false;
		}
		final boolean[] symmetric = { true };
		iterateHalf((row, column) -> {
			if (get(row, column) != get(column, row))
				symmetric[0] = false;
		});
		return symmetric[0];
	}

	public boolean isSkewSymmetric() {
		if (!isSquareMatrix()) {
			return false;
		}
		final boolean[] skewSymmetric = { true };
		iterateHalf((row, column) -> {
			if (get(row, column) != -get(column, row))
				skewSymmetric[0] = false;
		});
		return skewSymmetric[0];
	}

	private void requireRCequalsRC(SadMatrix matrix, Supplier<String> name) {
		if (rowCount() != matrix.rowCount())
			throw new ArithmeticException("Equal row-count in " + name.get() + " is required.");
	}

	private void requireCCequalsCC(SadMatrix matrix, Supplier<String> name) {
		if (columnCount() != matrix.columnCount())
			throw new ArithmeticException("Equal column-count in " + name.get() + " is required.");
	}

	private void requireRCequalsCC(SadMatrix matrix, Supplier<String> name) {
		if (rowCount() != matrix.columnCount())
			throw new ArithmeticException("Column-count of " + name.get() + " has to match row-count.");
	}

	private void requireCCequalsRC(SadMatrix matrix, Supplier<String> name) {
		if (columnCount() != matrix.rowCount())
			throw new ArithmeticException("Row-count of " + name.get() + " has to match column-count.");
	}

	private void requireEqualCount(SadMatrix matrix, Supplier<String> name) {
		requireRCequalsRC(matrix, name);
		requireCCequalsCC(matrix, name);
	}

	public SadMatrix createEqualSizeMatrix() {
		return new SadMatrix(rowCount(), columnCount());
	}

	public SadMatrix add(SadMatrix matrix) {
		return add0(matrix, () -> new SadMatrix(rowCount(), columnCount()));
	}

	public SadMatrix add(SadMatrix matrix, SadMatrix target) {
		requireEqualCount(target, () -> "target");
		return add0(matrix, () -> target);
	}

	private SadMatrix add0(SadMatrix matrix, Supplier<SadMatrix> targetSupplier) {
		requireEqualCount(matrix, () -> "argument");
		SadMatrix target = targetSupplier.get();
		iterate(new ComponentWiseBinaryOperation(matrix, target, (a, b) -> a + b));
		return target;
	}

	public SadMatrix subtract(SadMatrix matrix) {
		return subtract0(matrix, () -> new SadMatrix(rowCount(), columnCount()));
	}

	public SadMatrix subtract(SadMatrix matrix, SadMatrix target) {
		requireEqualCount(target, () -> "target");
		return subtract0(matrix, () -> target);
	}

	private SadMatrix subtract0(SadMatrix matrix, Supplier<SadMatrix> targetSupplier) {
		requireEqualCount(matrix, () -> "argument");
		SadMatrix target = targetSupplier.get();
		iterate(new ComponentWiseBinaryOperation(matrix, target, (a, b) -> a - b));
		return target;
	}

	private class ComponentWiseBinaryOperation implements MatrixIterateAction {

		private SadMatrix matrix;
		private SadMatrix target;
		private DoubleBinaryOperator operator;

		public ComponentWiseBinaryOperation(SadMatrix matrix, SadMatrix target, DoubleBinaryOperator operator) {
			this.matrix = matrix;
			this.target = target;
			this.operator = operator;
		}

		@Override
		public void execute(int row, int column) {
			target.set(row, column, operator.applyAsDouble(matrix.get(row, column), target.get(row, column)));
		}
	}

	private static interface MatrixIterateAction {
		void execute(int row, int column);
	}

	private void iterate(MatrixIterateAction action) {
		for (int row = 0; row < rowCount(); row++) {
			for (int column = 0; column < columnCount(); column++) {
				action.execute(row, column);
			}
		}
	}

	private void iterateHalf(MatrixIterateAction action) {
		for (int row = 0; row < rowCount(); row++) {
			for (int column = 0; column < row; column++) {
				action.execute(row, column);
			}
		}
	}

	public static SadMatrix empty(int rowCount, int columnCount) {
		return new SadMatrix(rowCount, columnCount);
	}

	public static SadMatrix identity(int count) {
		SadMatrix matrix = empty(count, count);
		for (int field = 0; field < count; field++) {
			matrix.set(field, field, 1);
		}
		return matrix;
	}
}
