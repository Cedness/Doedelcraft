package de.ced.logic.math;

import java.util.Arrays;
import java.util.List;

import de.ced.logic.math.Meth.LogResult;

public class SadNumber {

	private int base;
	private List<Integer> value;

	public SadNumber(int base, int number) {
		this.base = base;
		LogResult logResult = Meth.log(base, number);
		int log = logResult.log;
		int exp = logResult.exp;
		value = Arrays.asList(new Integer[log]);
		while (exp >= 1) {
			int digit = number / exp * exp;
			number -= digit;
			value.set(--log, digit);
		}
	}

	public SadNumber(int base, SadNumber number) {
		this.base = base;
		LogResult logResult = Meth.log(base, number.value.size());
		int log = logResult.log;
		int exp = logResult.exp;
	}

	public int getBase() {
		return base;
	}
}
