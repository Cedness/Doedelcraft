package de.ced.logic.math;

public class Meth {

	public static int pow2(int i) {
		return i * i;
	}

	public static double pow2(double d) {
		return d * d;
	}

	public static int pow3(int i) {
		return i * i * i;
	}

	public static double pow3(double d) {
		return d * d * d;
	}

	public static int pow(int i, int power) {
		int res = 1;
		for (; power > 0; power--) {
			res *= i;
		}
		return res;
	}

	public static double pow(double d, int power) {
		double res = 1;
		for (; power > 0; power--) {
			res *= d;
		}
		return res;
	}

	public static class LogResult {
		public int log = 1;
		public int exp;
	}

	public static LogResult log(int base, int i) {
		LogResult result = new LogResult();
		for (int value = base; value < i; value *= base) {
			result.exp = value;
			result.log++;
		}
		return result;
	}
}
