package de.ced.logic.vector;

public class ArrayVector2 extends AbstractVector2 {

	private double[] values = new double[2];

	protected ArrayVector2() {}

	protected ArrayVector2(Vector2 vector) {
		set(vector);
	}

	protected ArrayVector2(double scalar) {
		set(scalar);
	}

	@Deprecated
	protected ArrayVector2(double[] values) {
		this(values[0], values[1]);
	}

	protected ArrayVector2(double x, double y) {
		set(x, y);
	}

	@Override
	protected void replaceContents(double[] values) {
		this.values = values;
	}

	@Override
	public double get(int i) {
		return values[i];
	}

	@Override
	public Vector2 set(int i, double v) {
		values[i] = v;
		return this;
	}

	@Override
	public double x() {
		return get(0);
	}

	@Override
	public Vector2 setX(double x) {
		return set(0, x);
	}

	@Override
	public double y() {
		return get(1);
	}

	@Override
	public Vector2 setY(double y) {
		return set(1, y);
	}
}
