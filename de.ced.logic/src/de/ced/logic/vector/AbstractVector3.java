package de.ced.logic.vector;

public abstract class AbstractVector3 extends AbstractVector2 implements Vector3 {

	public static final int SIZE = Vector3.SIZE;

	protected AbstractVector3() {}

	protected AbstractVector3(double scalar) {
		set(scalar);
	}

	protected AbstractVector3(Vector3 vector) {
		set(vector);
	}

	protected AbstractVector3(double[] values) {
		set(values[0], values[1], values[2]);
	}

	protected AbstractVector3(double x, double y, double z) {
		set(x, y, z);
	}

	@Override
	public int size() {
		return Vector3.super.size();
	}

	@Override
	public Vector3 clone() {
		return (Vector3) super.clone();
	}

	@Override
	public boolean resembles(Vector3 vector) {
		return resembles0(vector, SIZE);
	}

	@Override
	public boolean compare(Vector3 vector, VectorPredicate predicate) {
		return compare0(vector, predicate, SIZE);
	}

	@Override
	public boolean compare(Vector3 vector, VectorPredicate predicate, int... range) {
		return compare0(vector, predicate, range, SIZE);
	}

	@Override
	public boolean compare(Vector3 vector, VectorPredicate predicate, boolean... mask) {
		return compare0(vector, predicate, mask, SIZE);
	}

	@Override
	public boolean compareXZ(Vector3 vector, VectorPredicate predicate) {
		return predicate.test(x(), vector.x()) && predicate.test(z(), vector.z());
	}

	@Override
	public Vector3 identity() {
		return (Vector3) super.identity();
	}

	@Override
	public Vector3 invert() {
		return (Vector3) super.invert();
	}

	@Override
	public Vector3 invert(Vector3 target) {
		return (Vector3) super.invert(target);
	}

	@Override
	public Vector3 applyLength(double length) {
		return (Vector3) super.applyLength(length);
	}

	@Override
	public Vector3 applyLength(double length, double formerLength) {
		return (Vector3) super.applyLength(length, formerLength);
	}

	@Override
	public double distance(Vector3 vector) {
		return distance0(vector, SIZE);
	}

	@Override
	public double distanceSquared(Vector3 vector) {
		return distanceSquared0(vector, SIZE);
	}

	@Override
	public double dotProduct(Vector3 vector) {
		return dotProduct0(vector, SIZE);
	}

	@Override
	public Vector3 crossProduct(Vector3 vector) {
		return crossProduct(this, vector);
	}

	@Override
	public Vector3 crossProduct(Vector3 target, Vector3 vector) {
		for (ConcurrentIterator it = concurrentIterator(); it.valid(); it.advance()) {
			int i = it.i();
			int i1 = shift(i, 1);
			int i2 = shift(i, 2);
			it.set(get(i1) * vector.get(i2) - get(i2) * vector.get(i1));
		}
		return target;
	}

	private int shift(int i, int offset) {
		return (i + offset) % 3;
	}

	@Override
	public abstract double get(int i);

	@Override
	public abstract Vector3 set(int i, double v);

	@Override
	public Vector3 add(int i, double v) {
		return (Vector3) super.add(i, v);
	}

	@Override
	public Vector3 add(Vector3 target, int i, double v) {
		return (Vector3) super.add(target, i, v);
	}

	@Override
	public Vector3 mul(int i, double v) {
		return (Vector3) super.mul(i, v);
	}

	@Override
	public Vector3 mul(Vector3 target, int i, double v) {
		return (Vector3) super.mul(target, i, v);
	}

	@Override
	public Vector3 set(double scalar) {
		return (Vector3) super.set(scalar);
	}

	@Override
	public Vector3 add(double scalar) {
		return (Vector3) super.add(scalar);
	}

	@Override
	public Vector3 add(Vector3 target, double scalar) {
		return (Vector3) super.add(target, scalar);
	}

	@Override
	public Vector3 mul(double scalar) {
		return (Vector3) super.mul(scalar);
	}

	@Override
	public Vector3 mul(Vector3 target, double scalar) {
		return (Vector3) super.mul(target, scalar);
	}

	@Override
	public Vector3 set(Vector3 vector) {
		return (Vector3) set0(vector, SIZE);
	}

	@Override
	public Vector3 add(Vector3 summand) {
		return add(this, summand);
	}

	@Override
	public Vector3 add(Vector3 target, Vector3 summand) {
		return (Vector3) add0(target, summand, SIZE);
	}

	@Override
	public Vector3 mul(Vector3 factor) {
		return mul(this, factor);
	}

	@Override
	public Vector3 mul(Vector3 target, Vector3 factor) {
		return (Vector3) mul0(target, factor, SIZE);
	}

	// Literals

	@Override
	public Vector3 set(double x, double y, double z) {
		set(x, y);
		return setZ(z);
	}

	@Override
	public Vector3 add(double x, double y, double z) {
		return add(this, x, y, z);
	}

	@Override
	public Vector3 add(Vector3 target, double x, double y, double z) {
		add(target, x, y);
		return addZ(target, z);
	}

	@Override
	public Vector3 mul(double x, double y, double z) {
		return mul(this, x, y, z);
	}

	@Override
	public Vector3 mul(Vector3 target, double x, double y, double z) {
		mul(target, x, y);
		return mulZ(target, z);
	}

	// Per axis

	@Override
	public abstract double x();

	@Override
	public abstract Vector3 setX(double x);

	@Override
	public abstract double y();

	@Override
	public abstract Vector3 setY(double y);

	@Override
	public abstract double z();

	@Override
	public abstract Vector3 setZ(double z);

	@Override
	public Vector3 addX(double x) {
		return addX(this, x);
	}

	@Override
	public Vector3 addX(Vector3 target, double x) {
		return target.setX(x() + x);
	}

	@Override
	public Vector3 addY(double y) {
		return addY(this, y);
	}

	@Override
	public Vector3 addY(Vector3 target, double y) {
		return target.setY(y() + y);
	}

	@Override
	public Vector3 addZ(double z) {
		return addZ(this, z);
	}

	@Override
	public Vector3 addZ(Vector3 target, double z) {
		return target.setZ(z() + z);
	}

	@Override
	public Vector3 mulX(double x) {
		return mulX(this, x);
	}

	@Override
	public Vector3 mulX(Vector3 target, double x) {
		return target.setX(x() * x);
	}

	@Override
	public Vector3 mulY(double y) {
		return mulY(this, y);
	}

	@Override
	public Vector3 mulY(Vector3 target, double y) {
		return target.setY(y() * y);
	}

	@Override
	public Vector3 mulZ(double z) {
		return mulZ(this, z);
	}

	@Override
	public Vector3 mulZ(Vector3 target, double z) {
		return target.setZ(z() * z);
	}
}
