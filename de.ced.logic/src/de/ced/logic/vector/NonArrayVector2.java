package de.ced.logic.vector;

import static de.ced.logic.math.Meth.*;

public abstract class NonArrayVector2 extends AbstractVector2 {

	protected NonArrayVector2() {}

	protected NonArrayVector2(double scalar) {
		set(scalar);
	}

	protected NonArrayVector2(Vector2 vector) {
		set(vector);
	}

	protected NonArrayVector2(double[] values) {
		set(values[0], values[1]);
	}

	protected NonArrayVector2(double x, double y) {
		set(x, y);
	}

	@Override
	public Vector2 clone() {
		return Vector2.from(this);
	}

	@Override
	protected void replaceContents(double[] values) {
		set(values[0], values[1]);
	}

	@Override
	public double[] toArray(double[] values) {
		values[0] = x();
		values[1] = y();
		return values;
	}

	@Override
	public boolean resembles(Vector2 vector) {
		Boolean could = couldResemble(vector);
		return could != null ? could : x() == vector.x() && y() == vector.y();
	}

	@Override
	public boolean compare(Vector2 vector, VectorPredicate predicate) {
		return predicate.test(x(), vector.x()) && predicate.test(y(), vector.y());
	}

	@Override
	public boolean compare(Vector2 vector, VectorPredicate predicate, int... range) {
		boolean[] mask = new boolean[2];
		for (int i : range) {
			mask[i] = true;
		}
		return super.compare(vector, predicate, mask);
	}

	@Override
	public boolean compare(Vector2 vector, VectorPredicate predicate, boolean... mask) {
		return (!mask[0] || predicate.test(x(), vector.x()))
				&& (!mask[1] || predicate.test(y(), vector.y()));
	}

	@Override
	public double lengthSquared() {
		return pow2(x()) + pow2(y());
	}

	@Override
	public double distance(Vector2 vector) {
		return Math.sqrt(distanceSquared(vector));
	}

	@Override
	public double distanceSquared(Vector2 vector) {
		return pow2(x() - vector.x()) + pow2(y() - vector.y());
	}

	@Override
	public double dotProduct(Vector2 vector) {
		return x() * vector.x() + y() * vector.y();
	}

	@Override
	public double get(int i) {
		return switch (i) {
		case 0 -> x();
		case 1 -> y();
		default -> throw new IndexOutOfBoundsException(i);
		};
	}

	@Override
	public Vector2 set(int i, double v) {
		switch (i) {
		case 0 -> setX(v);
		case 1 -> setY(v);
		default -> throw new IndexOutOfBoundsException(i);
		}
		return this;
	}

	@Override
	public Vector2 set(double scalar) {
		return set(scalar, scalar);
	}

	@Override
	public Vector2 add(Vector2 target, double scalar) {
		return add(target, scalar);
	}

	@Override
	public Vector2 mul(Vector2 target, double scalar) {
		return mul(target, scalar);
	}

	@Override
	public Vector2 set(Vector2 vector) {
		return set(vector.x(), vector.y());
	}

	@Override
	public Vector2 add(Vector2 target, Vector2 summand) {
		return add(target, summand.x(), summand.y());
	}

	@Override
	public Vector2 mul(Vector2 target, Vector2 factor) {
		return mul(target, factor.x(), factor.y());
	}

	@Override
	public Vector2 set(double x, double y) {
		setX(x);
		return setY(y);
	}

	@Override
	public Vector2 add(Vector2 target, double x, double y) {
		addX(target, x);
		return addY(target, y);
	}

	@Override
	public Vector2 mul(Vector2 target, double x, double y) {
		mulX(target, x);
		return mulY(target, y);
	}

	// Per axis
}
