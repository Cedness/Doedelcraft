package de.ced.logic.vector;

public interface Vector4 extends Vector3 {

	int SIZE = 4;

	static Vector4 empty() {
		return new ArrayVector4();
	}

	static Vector4 from(double scalar) {
		return new ArrayVector4(scalar);
	}

	static Vector4 from(Vector4 vector) {
		return new ArrayVector4(vector);
	}

	@Deprecated
	static Vector4 from(double[] values) {
		return new ArrayVector4(values);
	}

	static Vector4 from(double x, double y, double z, double a) {
		return new ArrayVector4(x, y, z, a);
	}

	@Override
	default int size() {
		return 4;
	}

	@Override
	Vector4 clone();

	@Override
	double[] toArray();

	@Override
	double[] toArray(double[] values);

	@Override
	boolean equals(Object object);

	boolean resembles(Vector4 vector);

	boolean compare(Vector4 vector, VectorPredicate predicate);

	boolean compare(Vector4 vector, VectorPredicate predicate, int... range);

	boolean compare(Vector4 vector, VectorPredicate predicate, boolean... mask);

	@Override
	Vector4 identity();

	@Override
	Vector4 invert();

	Vector4 invert(Vector4 target);

	@Override
	Vector4 applyLength(double length);

	@Override
	Vector4 applyLength(double length, double formerLength);

	@Override
	double length();

	@Override
	double lengthSquared();

	double distance(Vector4 vector);

	double distanceSquared(Vector4 vector);

	double dotProduct(Vector4 vector);

	@Override
	double get(int i);

	@Override
	Vector4 set(int i, double v);

	@Override
	Vector4 add(int i, double v);

	Vector4 add(Vector4 target, int i, double v);

	@Override
	Vector4 mul(int i, double v);

	Vector4 mul(Vector4 target, int i, double v);

	@Override
	Vector4 set(double scalar);

	@Override
	Vector4 add(double scalar);

	Vector4 add(Vector4 target, double scalar);

	@Override
	Vector4 mul(double scalar);

	Vector4 mul(Vector4 target, double scalar);

	Vector4 set(Vector4 vector);

	Vector4 add(Vector4 summand);

	Vector4 add(Vector4 target, Vector4 summand);

	Vector4 mul(Vector4 factor);

	Vector4 mul(Vector4 target, Vector4 factor);

	Vector4 set(double x, double y, double z, double a);

	Vector4 add(double x, double y, double z, double a);

	Vector4 add(Vector4 target, double x, double y, double z, double a);

	Vector4 mul(double x, double y, double z, double a);

	Vector4 mul(Vector4 target, double x, double y, double z, double a);

	@Override
	double x();

	@Override
	Vector4 setX(double x);

	@Override
	double y();

	@Override
	Vector4 setY(double y);

	@Override
	double z();

	@Override
	Vector4 setZ(double z);

	double a();

	Vector4 setA(double a);

	@Override
	Vector4 addX(double x);

	Vector4 addX(Vector4 target, double x);

	@Override
	Vector4 addY(double y);

	Vector4 addY(Vector4 target, double y);

	@Override
	Vector4 addZ(double z);

	Vector4 addZ(Vector4 target, double z);

	Vector4 addA(double a);

	Vector4 addA(Vector4 target, double a);

	@Override
	Vector4 mulX(double x);

	Vector4 mulX(Vector4 target, double x);

	@Override
	Vector4 mulY(double y);

	Vector4 mulY(Vector4 target, double y);

	@Override
	Vector4 mulZ(double z);

	Vector4 mulZ(Vector4 target, double z);

	Vector4 mulA(double a);

	Vector4 mulA(Vector4 target, double a);
}
