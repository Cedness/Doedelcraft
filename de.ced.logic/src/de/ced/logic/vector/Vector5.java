package de.ced.logic.vector;

public interface Vector5 extends Vector4 {

	int SIZE = 5;

	static Vector5 empty() {
		return new ArrayVector5();
	}

	static Vector5 from(double scalar) {
		return new ArrayVector5(scalar);
	}

	static Vector5 from(Vector5 vector) {
		return new ArrayVector5(vector);
	}

	@Deprecated
	static Vector5 from(double[] values) {
		return new ArrayVector5(values);
	}

	static Vector5 from(double x, double y, double z, double a, double b) {
		return new ArrayVector5(x, y, z, a, b);
	}

	@Override
	default int size() {
		return 5;
	}

	@Override
	Vector5 clone();

	@Override
	double[] toArray();

	@Override
	double[] toArray(double[] values);

	@Override
	boolean equals(Object object);

	boolean resembles(Vector5 vector);

	boolean compare(Vector5 vector, VectorPredicate predicate);

	boolean compare(Vector5 vector, VectorPredicate predicate, int... range);

	boolean compare(Vector5 vector, VectorPredicate predicate, boolean... mask);

	@Override
	Vector5 identity();

	@Override
	Vector5 invert();

	Vector5 invert(Vector5 target);

	@Override
	Vector5 applyLength(double length);

	@Override
	Vector5 applyLength(double length, double formerLength);

	@Override
	double length();

	@Override
	double lengthSquared();

	double distance(Vector5 vector);

	double distanceSquared(Vector5 vector);

	double dotProduct(Vector5 vector);

	@Override
	double get(int i);

	@Override
	Vector5 set(int i, double v);

	@Override
	Vector5 add(int i, double v);

	Vector5 add(Vector5 target, int i, double v);

	@Override
	Vector5 mul(int i, double v);

	Vector5 mul(Vector5 target, int i, double v);

	@Override
	Vector5 set(double scalar);

	@Override
	Vector5 add(double scalar);

	Vector5 add(Vector5 target, double scalar);

	@Override
	Vector5 mul(double scalar);

	Vector5 mul(Vector5 target, double scalar);

	Vector5 set(Vector5 vector);

	Vector5 add(Vector5 summand);

	Vector5 add(Vector5 target, Vector5 summand);

	Vector5 mul(Vector5 factor);

	Vector5 mul(Vector5 target, Vector5 factor);

	Vector5 set(double x, double y, double z, double a, double b);

	Vector5 add(double x, double y, double z, double a, double b);

	Vector5 add(Vector5 target, double x, double y, double z, double a, double b);

	Vector5 mul(double x, double y, double z, double a, double b);

	Vector5 mul(Vector5 target, double x, double y, double z, double a, double b);

	@Override
	double x();

	@Override
	Vector5 setX(double x);

	@Override
	double y();

	@Override
	Vector5 setY(double y);

	@Override
	double z();

	@Override
	Vector5 setZ(double z);

	@Override
	double a();

	@Override
	Vector5 setA(double a);

	double b();

	Vector5 setB(double b);

	@Override
	Vector5 addX(double x);

	Vector5 addX(Vector5 target, double x);

	@Override
	Vector5 addY(double y);

	Vector5 addY(Vector5 target, double y);

	@Override
	Vector5 addZ(double z);

	Vector5 addZ(Vector5 target, double z);

	@Override
	Vector5 addA(double a);

	Vector5 addA(Vector5 target, double a);

	Vector5 addB(double b);

	Vector5 addB(Vector5 target, double b);

	@Override
	Vector5 mulX(double x);

	Vector5 mulX(Vector5 target, double x);

	@Override
	Vector5 mulY(double y);

	Vector5 mulY(Vector5 target, double y);

	@Override
	Vector5 mulZ(double z);

	Vector5 mulZ(Vector5 target, double z);

	@Override
	Vector5 mulA(double a);

	Vector5 mulA(Vector5 target, double a);

	Vector5 mulB(double b);

	Vector5 mulB(Vector5 target, double b);
}
