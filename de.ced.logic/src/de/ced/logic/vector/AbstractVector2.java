package de.ced.logic.vector;

public abstract class AbstractVector2 extends AbstractVector implements Vector2 {

	public static final int SIZE = Vector2.SIZE;

	protected AbstractVector2() {}

	protected AbstractVector2(double scalar) {
		set(scalar);
	}

	protected AbstractVector2(Vector2 vector) {
		set(vector);
	}

	protected AbstractVector2(double[] values) {
		set(values[0], values[1]);
	}

	protected AbstractVector2(double x, double y) {
		set(x, y);
	}

	@Override
	public int size() {
		return Vector2.super.size();
	}

	@Override
	public Vector2 clone() {
		return (Vector2) super.clone();
	}

	@Override
	public boolean resembles(Vector2 vector) {
		return resembles0(vector, SIZE);
	}

	@Override
	public boolean compare(Vector2 vector, VectorPredicate predicate) {
		return compare0(vector, predicate, SIZE);
	}

	@Override
	public boolean compare(Vector2 vector, VectorPredicate predicate, int... range) {
		return compare0(vector, predicate, range, SIZE);
	}

	@Override
	public boolean compare(Vector2 vector, VectorPredicate predicate, boolean... mask) {
		return compare0(vector, predicate, mask, SIZE);
	}

	@Override
	public Vector2 identity() {
		return (Vector2) super.identity();
	}

	@Override
	public Vector2 invert() {
		return (Vector2) super.invert();
	}

	@Override
	public Vector2 invert(Vector2 target) {
		return (Vector2) super.invert(target);
	}

	@Override
	public Vector2 applyLength(double length) {
		return (Vector2) super.applyLength(length);
	}

	@Override
	public Vector2 applyLength(double length, double formerLength) {
		return (Vector2) super.applyLength(length, formerLength);
	}

	@Override
	public double distance(Vector2 vector) {
		return distance0(vector, SIZE);
	}

	@Override
	public double distanceSquared(Vector2 vector) {
		return distanceSquared0(vector, SIZE);
	}

	@Override
	public double dotProduct(Vector2 vector) {
		return dotProduct0(vector, SIZE);
	}

	@Override
	public abstract double get(int i);

	@Override
	public abstract Vector2 set(int i, double v);

	@Override
	public Vector2 add(int i, double v) {
		return (Vector2) super.add(i, v);
	}

	@Override
	public Vector2 add(Vector2 target, int i, double v) {
		return (Vector2) super.add(target, i, v);
	}

	@Override
	public Vector2 mul(int i, double v) {
		return (Vector2) super.mul(i, v);
	}

	@Override
	public Vector2 mul(Vector2 target, int i, double v) {
		return (Vector2) super.mul(target, i, v);
	}

	@Override
	public Vector2 set(double scalar) {
		return (Vector2) super.set(scalar);
	}

	@Override
	public Vector2 add(double scalar) {
		return (Vector2) super.add(scalar);
	}

	@Override
	public Vector2 add(Vector2 target, double scalar) {
		return (Vector2) super.add(target, scalar);
	}

	@Override
	public Vector2 mul(double scalar) {
		return (Vector2) super.mul(scalar);
	}

	@Override
	public Vector2 mul(Vector2 target, double scalar) {
		return (Vector2) super.mul(target, scalar);
	}

	@Override
	public Vector2 set(Vector2 vector) {
		return (Vector2) set0(vector, SIZE);
	}

	@Override
	public Vector2 add(Vector2 summand) {
		return add(this, summand);
	}

	@Override
	public Vector2 add(Vector2 target, Vector2 summand) {
		return (Vector2) add0(target, summand, SIZE);
	}

	@Override
	public Vector2 mul(Vector2 factor) {
		return mul(this, factor);
	}

	@Override
	public Vector2 mul(Vector2 target, Vector2 factor) {
		return (Vector2) mul0(target, factor, SIZE);
	}

	// Literal values

	@Override
	public Vector2 set(double x, double y) {
		setX(x);
		return setY(y);
	}

	@Override
	public Vector2 add(double x, double y) {
		return add(this, x, y);
	}

	@Override
	public Vector2 add(Vector2 target, double x, double y) {
		addX(target, x);
		return addY(target, y);
	}

	@Override
	public Vector2 mul(double x, double y) {
		return mul(this, x, y);
	}

	@Override
	public Vector2 mul(Vector2 target, double x, double y) {
		mulX(target, x);
		return mulY(target, y);
	}

	// Per axis

	@Override
	public abstract double x();

	@Override
	public abstract Vector2 setX(double x);

	@Override
	public abstract double y();

	@Override
	public abstract Vector2 setY(double y);

	@Override
	public Vector2 addX(double x) {
		return addX(this, x);
	}

	@Override
	public Vector2 addX(Vector2 target, double x) {
		return target.setX(x() + x);
	}

	@Override
	public Vector2 addY(double y) {
		return addY(this, y);
	}

	@Override
	public Vector2 addY(Vector2 target, double y) {
		return target.setY(y() + y);
	}

	@Override
	public Vector2 mulX(double x) {
		return mulX(this, x);
	}

	@Override
	public Vector2 mulX(Vector2 target, double x) {
		return target.setX(x() * x);
	}

	@Override
	public Vector2 mulY(double y) {
		return mulY(this, y);
	}

	@Override
	public Vector2 mulY(Vector2 target, double y) {
		return target.setY(y() * y);
	}
}
