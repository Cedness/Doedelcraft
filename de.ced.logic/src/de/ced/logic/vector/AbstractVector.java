package de.ced.logic.vector;

import static de.ced.logic.math.Meth.*;

import java.util.Iterator;

public abstract class AbstractVector implements Vector {

	protected AbstractVector() {}

	@Override
	public int size() {
		return 0;
	}

	private int usableSize(Vector vector) {
		return Math.min(size(), vector.size());
	}

	@Override
	public Vector clone() {
		try {
			return (Vector) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Iterator<Double> iterator() {
		return new Iterator<Double>() {
			private int current;

			@Override
			public boolean hasNext() {
				return current < size();
			}

			@Override
			public Double next() {
				return get(current++);
			}
		};
	}

	@Override
	public VectorIterator vectorIterator() {
		return new VectorIterator();
	}

	public class VectorIterator implements Iterator<Double> {
		private int current;

		@Override
		public boolean hasNext() {
			return current < size();
		}

		@Override
		public Double next() {
			return get(current++);
		}

		public Double getCurrent() { return get(current); }

		public void advance() {
			current++;
		}

		public int i() {
			return current;
		}
	}

	public ConcurrentIterator concurrentIterator() {
		return new ConcurrentIterator();
	}

	public class ConcurrentIterator {
		private final double[] values = new double[size()];
		private int i;

		public boolean valid() {
			boolean valid = i < values.length;
			if (!valid) {
				replaceContents(values);
			}
			return valid;
		}

		public void advance() {
			i++;
		}

		public double get() {
			return AbstractVector.this.get(i);
		}

		public void set(double value) {
			values[i] = value;
		}

		public int i() {
			return i();
		}
	}

	protected abstract void replaceContents(double[] values);

	@Override
	public double[] toArray() {
		return toArray(new double[size()]);
	}

	@Override
	public double[] toArray(double[] values) {
		for (int i = 0; i < size(); i++) {
			values[i] = get(i);
		}
		return values;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("V").append(size()).append("(");
		VectorIterator i = vectorIterator();
		while (true) {
			builder.append(i.next());
			if (!i.hasNext()) {
				break;
			}
			builder.append(", ");
		}
		return builder.append(")").toString();
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Vector && equals((Vector) obj);
	}

	@Override
	public boolean equals(Vector vector) {
		return this == vector || vector != null && size() == vector.size() && compare0(vector, (v1, v2) -> v1 == v2, size());
	}

	@Override
	public boolean resembles(Vector vector) {
		return resembles0(vector, usableSize(vector));
	}

	protected boolean resembles0(Vector vector, int size) {
		Boolean could = couldResemble(vector);
		return could != null ? could : compare0(vector, (v1, v2) -> v1 == v2, size);
	}

	protected Boolean couldResemble(Vector vector) {
		if (this == vector) {
			return true;
		} else if (vector == null) {
			return false;
		}
		return null;
	}

	@Override
	public boolean compare(Vector vector, VectorPredicate predicate) {
		return compare0(vector, predicate, usableSize(vector));
	}

	protected boolean compare0(Vector vector, VectorPredicate predicate, int size) {
		for (int i = 0; i < size; i++) {
			if (!predicate.test(get(i), vector.get(i))) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean compare(Vector vector, VectorPredicate predicate, int... range) {
		return compare0(vector, predicate, range, usableSize(vector));
	}

	@Deprecated
	protected boolean compare0(Vector vector, VectorPredicate predicate, int[] range) {
		return compare0(vector, predicate, range, vector.size());
	}

	protected boolean compare0(Vector vector, VectorPredicate predicate, int[] range, int size) {
		for (int i : range) {
			if (!predicate.test(get(i), vector.get(i))) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean compare(Vector vector, VectorPredicate predicate, boolean... mask) {
		return compare0(vector, predicate, mask, usableSize(vector));
	}

	@Deprecated
	protected boolean compare0(Vector vector, VectorPredicate predicate, boolean[] mask) {
		return compare0(vector, predicate, mask, vector.size());
	}

	protected boolean compare0(Vector vector, VectorPredicate predicate, boolean[] mask, int size) {
		for (int i = 0; i < size; i++) {
			if (mask[i] && !predicate.test(get(i), vector.get(i))) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Vector identity() {
		return set(0);
	}

	@Override
	public Vector invert() {
		return invert(this);
	}

	@Override
	public Vector invert(Vector target) {
		return mul(target, -1);
	}

	@Override
	public Vector applyLength(double length) {
		return applyLength(length, length());
	}

	@Override
	public Vector applyLength(double length, double formerLength) {
		return mul(length / formerLength);
	}

	@Override
	public double length() {
		return Math.sqrt(lengthSquared());
	}

	@Override
	public double lengthSquared() {
		double result = 0;
		for (double value : this) {
			result += pow2(value);
		}
		return result;
	}

	@Override
	public double distance(Vector vector) {
		return distance0(vector, usableSize(vector));
	}

	@Deprecated
	protected double distance0(Vector vector) {
		return distance0(vector, vector.size());
	}

	protected double distance0(Vector vector, int size) {
		return Math.sqrt(distanceSquared0(vector, size));
	}

	@Override
	public double distanceSquared(Vector vector) {
		return 0;
	}

	@Deprecated
	protected double distanceSquared0(Vector vector) {
		return distanceSquared0(vector, vector.size());
	}

	protected double distanceSquared0(Vector vector, int size) {
		double result = 0;
		for (int i = 0; i < size; i++) {
			result += pow2(get(i) - vector.get(i));
		}
		return result;
	}

	@Override
	public double dotProduct(Vector vector) {
		return dotProduct0(vector, usableSize(vector));
	}

	@Deprecated
	protected double dotProduct0(Vector vector) {
		return dotProduct0(vector, vector.size());
	}

	protected double dotProduct0(Vector vector, int size) {
		double result = 0;
		for (int i = 0; i < size; i++) {
			result += get(i) * vector.get(i);
		}
		return result;
	}

	@Override
	public abstract double get(int i);

	@Override
	public abstract Vector set(int i, double v);

	@Override
	public Vector add(int i, double v) {
		return add(this, i, v);
	}

	@Override
	public Vector add(Vector target, int i, double v) {
		return target.set(i, get(i) + v);
	}

	@Override
	public Vector mul(int i, double v) {
		return mul(this, i, v);
	}

	@Override
	public Vector mul(Vector target, int i, double v) {
		return target.set(i, get(i) * v);
	}

	@Override
	public Vector set(double scalar) {
		for (int i = 0; i < size(); i++) {
			set(i, scalar);
		}
		return this;
	}

	@Override
	public Vector add(double scalar) {
		return add(this, scalar);
	}

	@Override
	public Vector add(Vector target, double scalar) {
		for (int i = 0; i < size(); i++) {
			target.set(i, get(i) + scalar);
		}
		return target;
	}

	@Override
	public Vector mul(double scalar) {
		return mul(this, scalar);
	}

	@Override
	public Vector mul(Vector target, double scalar) {
		for (int i = 0; i < size(); i++) {
			target.set(i, get(i) * scalar);
		}
		return target;
	}

	@Override
	public Vector set(Vector vector) {
		return set0(vector, usableSize(vector));
	}

	@Deprecated
	protected Vector set0(Vector vector) {
		return set0(vector, vector.size());
	}

	protected Vector set0(Vector vector, int size) {
		for (int i = 0; i < size; i++) {
			set(i, vector.get(i));
		}
		return this;
	}

	@Override
	public Vector add(Vector summand) {
		return add(this, summand);
	}

	@Override
	public Vector add(Vector target, Vector summand) {
		return add0(target, summand, usableSize(summand));
	}

	@Deprecated
	protected Vector add0(Vector target, Vector summand) {
		return add0(target, summand, summand.size());
	}

	protected Vector add0(Vector target, Vector summand, int size) {
		for (int i = 0; i < size; i++) {
			target.set(i, get(i) + summand.get(i));
		}
		return target;
	}

	@Override
	public Vector mul(Vector factor) {
		return mul(this, factor);
	}

	@Override
	public Vector mul(Vector target, Vector factor) {
		return mul0(target, factor, usableSize(factor));
	}

	@Deprecated
	protected Vector mul0(Vector target, Vector factor) {
		return mul0(target, factor, factor.size());
	}

	protected Vector mul0(Vector target, Vector factor, int size) {
		for (int i = 0; i < size; i++) {
			target.set(i, get(i) * factor.get(i));
		}
		return target;
	}

	// Per axis
}
