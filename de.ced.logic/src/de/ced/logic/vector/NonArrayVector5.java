package de.ced.logic.vector;

import static de.ced.logic.math.Meth.*;

public abstract class NonArrayVector5 extends AbstractVector5 {

	protected NonArrayVector5() {}

	protected NonArrayVector5(double scalar) {
		set(scalar);
	}

	protected NonArrayVector5(Vector5 vector) {
		set(vector);
	}

	protected NonArrayVector5(double[] values) {
		set(values[0], values[1], values[2], values[3], values[4]);
	}

	protected NonArrayVector5(double x, double y, double z, double a, double b) {
		set(x, y, z, a, b);
	}

	@Override
	public Vector5 clone() {
		return Vector5.from(this);
	}

	@Override
	protected void replaceContents(double[] values) {
		set(values[0], values[1], values[2], values[3], values[4]);
	}

	@Override
	public double[] toArray(double[] values) {
		values[0] = x();
		values[1] = y();
		values[2] = z();
		values[3] = a();
		values[4] = b();
		return values;
	}

	@Override
	public boolean resembles(Vector5 vector) {
		Boolean could = couldResemble(vector);
		return could != null ? could : x() == vector.x() && y() == vector.y() && z() == vector.z() && a() == vector.a() && b() == vector.b();
	}

	@Override
	public boolean compare(Vector5 vector, VectorPredicate predicate) {
		return predicate.test(x(), vector.x())
				&& predicate.test(y(), vector.y())
				&& predicate.test(z(), vector.z())
				&& predicate.test(a(), vector.a())
				&& predicate.test(b(), vector.b());
	}

	@Override
	public boolean compare(Vector5 vector, VectorPredicate predicate, int... range) {
		boolean[] mask = new boolean[5];
		for (int i : range) {
			mask[i] = true;
		}
		return super.compare(vector, predicate, mask);
	}

	@Override
	public boolean compare(Vector5 vector, VectorPredicate predicate, boolean... mask) {
		return (!mask[0] || predicate.test(x(), vector.x()))
				&& (!mask[1] || predicate.test(y(), vector.y()))
				&& (!mask[2] || predicate.test(z(), vector.z()))
				&& (!mask[3] || predicate.test(a(), vector.a()))
				&& (!mask[4] || predicate.test(b(), vector.b()));
	}

	@Override
	public double lengthSquared() {
		return pow2(x()) + pow2(y()) + pow2(z()) + pow2(a()) + pow2(b());
	}

	@Override
	public double distance(Vector5 vector) {
		return Math.sqrt(distanceSquared(vector));
	}

	@Override
	public double distanceSquared(Vector5 vector) {
		return pow2(x() - vector.x()) + pow2(y() - vector.y()) + pow2(z() - vector.z()) + pow2(a() - vector.a()) + pow2(b() - vector.b());
	}

	@Override
	public double dotProduct(Vector5 vector) {
		return x() * vector.x() + y() * vector.y() + z() * vector.z() + a() * vector.a() + b() * vector.b();
	}

	@Override
	public Vector3 crossProduct(Vector3 target, Vector3 vector) {
		double x = y() * vector.z() - z() * vector.y();
		double y = z() * vector.x() - x() * vector.z();
		double z = x() * vector.y() - y() * vector.x();
		return set(x, y, z);
	}

	@Override
	public double get(int i) {
		return switch (i) {
		case 0 -> x();
		case 1 -> y();
		case 2 -> z();
		case 3 -> a();
		case 4 -> b();
		default -> throw new IndexOutOfBoundsException(i);
		};
	}

	@Override
	public Vector5 set(int i, double v) {
		switch (i) {
		case 0 -> setX(v);
		case 1 -> setY(v);
		case 2 -> setZ(v);
		case 3 -> setA(v);
		case 4 -> setB(v);
		default -> throw new IndexOutOfBoundsException(i);
		}
		return this;
	}

	@Override
	public Vector5 set(double scalar) {
		return set(scalar, scalar, scalar, scalar, scalar);
	}

	@Override
	public Vector5 add(Vector5 target, double scalar) {
		return add(target, scalar, scalar, scalar, scalar, scalar);
	}

	@Override
	public Vector5 mul(Vector5 target, double scalar) {
		return mul(target, scalar, scalar, scalar, scalar, scalar);
	}

	@Override
	public Vector5 set(Vector5 vector) {
		return set(vector.x(), vector.y(), vector.z(), vector.a(), vector.b());
	}

	@Override
	public Vector5 add(Vector5 target, Vector5 summand) {
		return add(target, summand.x(), summand.y(), summand.z(), summand.a(), summand.b());
	}

	@Override
	public Vector5 mul(Vector5 target, Vector5 factor) {
		return mul(target, factor.x(), factor.y(), factor.z(), factor.a(), factor.b());
	}

	@Override
	public Vector5 set(double x, double y, double z, double a, double b) {
		setX(x);
		setY(y);
		setZ(z);
		setA(a);
		return setB(b);
	}

	@Override
	public Vector5 add(Vector5 target, double x, double y, double z, double a, double b) {
		addX(target, x);
		addY(target, y);
		addZ(target, z);
		addA(target, a);
		return addB(target, b);
	}

	@Override
	public Vector5 mul(Vector5 target, double x, double y, double z, double a, double b) {
		mulX(target, x);
		mulY(target, y);
		mulZ(target, z);
		mulA(target, a);
		return mulB(target, b);
	}

	// Per axis
}
