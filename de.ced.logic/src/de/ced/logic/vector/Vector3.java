package de.ced.logic.vector;

import java.util.Iterator;

public interface Vector3 extends Vector2 {

	int SIZE = 3;

	static Vector3 empty() {
		return new ArrayVector3();
	}

	static Vector3 from(double scalar) {
		return new ArrayVector3(scalar);
	}

	static Vector3 from(Vector3 vector) {
		return new ArrayVector3(vector);
	}

	@Deprecated
	static Vector3 from(double[] values) {
		return new ArrayVector3(values);
	}

	static Vector3 from(double x, double y, double z) {
		return new ArrayVector3(x, y, z);
	}

	@Override
	default int size() {
		return 3;
	}

	@Override
	Vector3 clone();

	@Override
	Iterator<Double> iterator();

	@Override
	double[] toArray();

	@Override
	double[] toArray(double[] values);

	@Override
	boolean equals(Object object);

	boolean resembles(Vector3 vector);

	boolean compare(Vector3 vector, VectorPredicate predicate);

	boolean compare(Vector3 vector, VectorPredicate predicate, int... range);

	boolean compare(Vector3 vector, VectorPredicate predicate, boolean... mask);

	boolean compareXZ(Vector3 vector, VectorPredicate predicate);

	@Override
	Vector3 identity();

	@Override
	Vector3 invert();

	Vector3 invert(Vector3 target);

	@Override
	Vector3 applyLength(double length);

	@Override
	Vector3 applyLength(double length, double formerLength);

	@Override
	double length();

	@Override
	double lengthSquared();

	double distance(Vector3 vector);

	double distanceSquared(Vector3 vector);

	double dotProduct(Vector3 vector);

	Vector3 crossProduct(Vector3 vector);

	Vector3 crossProduct(Vector3 target, Vector3 vector);

	@Override
	double get(int i);

	@Override
	Vector3 set(int i, double v);

	@Override
	Vector3 add(int i, double v);

	Vector3 add(Vector3 target, int i, double v);

	@Override
	Vector3 mul(int i, double v);

	Vector3 mul(Vector3 target, int i, double v);

	@Override
	Vector3 set(double scalar);

	@Override
	Vector3 add(double scalar);

	Vector3 add(Vector3 target, double scalar);

	@Override
	Vector3 mul(double scalar);

	Vector3 mul(Vector3 target, double scalar);

	Vector3 set(Vector3 vector);

	Vector3 add(Vector3 summand);

	Vector3 add(Vector3 target, Vector3 summand);

	Vector3 mul(Vector3 factor);

	Vector3 mul(Vector3 target, Vector3 factor);

	Vector3 set(double x, double y, double z);

	Vector3 add(double x, double y, double z);

	Vector3 add(Vector3 target, double x, double y, double z);

	Vector3 mul(double x, double y, double z);

	Vector3 mul(Vector3 target, double x, double y, double z);

	@Override
	double x();

	@Override
	Vector3 setX(double x);

	@Override
	double y();

	@Override
	Vector3 setY(double y);

	double z();

	Vector3 setZ(double z);

	@Override
	Vector3 addX(double x);

	Vector3 addX(Vector3 target, double x);

	@Override
	Vector3 addY(double y);

	Vector3 addY(Vector3 target, double y);

	Vector3 addZ(double z);

	Vector3 addZ(Vector3 target, double z);

	@Override
	Vector3 mulX(double x);

	Vector3 mulX(Vector3 target, double x);

	@Override
	Vector3 mulY(double y);

	Vector3 mulY(Vector3 target, double y);

	Vector3 mulZ(double z);

	Vector3 mulZ(Vector3 target, double z);
}
