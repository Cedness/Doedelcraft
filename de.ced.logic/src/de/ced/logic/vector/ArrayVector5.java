package de.ced.logic.vector;

public class ArrayVector5 extends AbstractVector5 {

	private double[] values = new double[5];

	protected ArrayVector5() {}

	protected ArrayVector5(Vector3 vector) {
		set(vector);
	}

	protected ArrayVector5(double scalar) {
		set(scalar);
	}

	@Deprecated
	protected ArrayVector5(double[] values) {
		this(values[0], values[1], values[2], values[3], values[4]);
	}

	protected ArrayVector5(double x, double y, double z, double a, double b) {
		set(x, y, z, a, b);
	}

	@Override
	protected void replaceContents(double[] values) {
		this.values = values;
	}

	@Override
	public double get(int i) {
		return values[i];
	}

	@Override
	public Vector5 set(int i, double v) {
		values[i] = v;
		return this;
	}

	@Override
	public double x() {
		return get(0);
	}

	@Override
	public Vector5 setX(double x) {
		return set(0, x);
	}

	@Override
	public double y() {
		return get(1);
	}

	@Override
	public Vector5 setY(double y) {
		return set(1, y);
	}

	@Override
	public double z() {
		return get(2);
	}

	@Override
	public Vector5 setZ(double z) {
		return set(2, z);
	}

	@Override
	public double a() {
		return get(3);
	}

	@Override
	public Vector5 setA(double a) {
		return set(3, a);
	}

	@Override
	public double b() {
		return get(3);
	}

	@Override
	public Vector5 setB(double b) {
		return set(4, b);
	}
}
