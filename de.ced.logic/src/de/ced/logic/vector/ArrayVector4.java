package de.ced.logic.vector;

public class ArrayVector4 extends AbstractVector4 {

	private double[] values = new double[4];

	protected ArrayVector4() {}

	protected ArrayVector4(Vector3 vector) {
		super(vector);
	}

	protected ArrayVector4(double scalar) {
		super(scalar);
	}

	@Deprecated
	protected ArrayVector4(double[] values) {
		this(values[0], values[1], values[2], values[3]);
	}

	protected ArrayVector4(double x, double y, double z, double a) {
		super(x, y, z, a);
	}

	@Override
	protected void replaceContents(double[] values) {
		this.values = values;
	}

	@Override
	public double get(int i) {
		return values[i];
	}

	@Override
	public Vector4 set(int i, double v) {
		values[i] = v;
		return this;
	}

	@Override
	public double x() {
		return get(0);
	}

	@Override
	public Vector4 setX(double x) {
		return set(0, x);
	}

	@Override
	public double y() {
		return get(1);
	}

	@Override
	public Vector4 setY(double y) {
		return set(1, y);
	}

	@Override
	public double z() {
		return get(2);
	}

	@Override
	public Vector4 setZ(double z) {
		return set(2, z);
	}

	@Override
	public double a() {
		return get(3);
	}

	@Override
	public Vector4 setA(double a) {
		return set(3, a);
	}
}
