package de.ced.logic.vector;

import java.util.Iterator;

public interface Vector extends Iterable<Double>, Cloneable {

	/**
	 * @return the size or dimension of the vector
	 */
	int size();

	Vector clone();

	@Override
	Iterator<Double> iterator();

	AbstractVector.VectorIterator vectorIterator();

	double[] toArray();

	double[] toArray(double[] values);

	@Override
	boolean equals(Object object);

	boolean equals(Vector vector);

	boolean resembles(Vector vector);

	boolean compare(Vector vector, VectorPredicate predicate);

	boolean compare(Vector vector, VectorPredicate predicate, int... range);

	boolean compare(Vector vector, VectorPredicate predicate, boolean... mask);

	Vector identity();

	Vector invert();

	Vector invert(Vector target);

	Vector applyLength(double length);

	Vector applyLength(double length, double formerLength);

	double length();

	double lengthSquared();

	double distance(Vector vector);

	double distanceSquared(Vector vector);

	double dotProduct(Vector vector);

	double get(int i);

	Vector set(int i, double v);

	Vector add(int i, double v);

	Vector add(Vector target, int i, double v);

	Vector mul(int i, double v);

	Vector mul(Vector target, int i, double v);

	Vector set(double scalar);

	Vector add(double scalar);

	Vector add(Vector target, double scalar);

	Vector mul(double scalar);

	Vector mul(Vector target, double scalar);

	Vector set(Vector vector);

	Vector add(Vector summand);

	Vector add(Vector target, Vector summand);

	Vector mul(Vector factor);

	Vector mul(Vector target, Vector factor);
}
