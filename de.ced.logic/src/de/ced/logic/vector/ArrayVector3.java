package de.ced.logic.vector;

public class ArrayVector3 extends AbstractVector3 {

	private double[] values = new double[3];

	protected ArrayVector3() {}

	protected ArrayVector3(Vector3 vector) {
		set(vector);
	}

	protected ArrayVector3(double scalar) {
		set(scalar);
	}

	@Deprecated
	protected ArrayVector3(double[] values) {
		this(values[0], values[1], values[2]);
	}

	protected ArrayVector3(double x, double y, double z) {
		set(x, y, z);
	}

	@Override
	protected void replaceContents(double[] values) {
		this.values = values;
	}

	@Override
	public double get(int i) {
		return values[i];
	}

	@Override
	public Vector3 set(int i, double v) {
		values[i] = v;
		return this;
	}

	@Override
	public double x() {
		return get(0);
	}

	@Override
	public Vector3 setX(double x) {
		return set(0, x);
	}

	@Override
	public double y() {
		return get(1);
	}

	@Override
	public Vector3 setY(double y) {
		return set(1, y);
	}

	@Override
	public double z() {
		return get(2);
	}

	@Override
	public Vector3 setZ(double z) {
		return set(2, z);
	}
}
