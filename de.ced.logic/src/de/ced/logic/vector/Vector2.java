package de.ced.logic.vector;

import java.util.Iterator;

public interface Vector2 extends Vector {

	int SIZE = 2;

	static Vector2 empty() {
		return new ArrayVector2();
	}

	static Vector2 from(double scalar) {
		return new ArrayVector2(scalar);
	}

	static Vector2 from(Vector2 vector) {
		return new ArrayVector2(vector);
	}

	@Deprecated
	static Vector2 from(double[] values) {
		return new ArrayVector2(values);
	}

	static Vector2 from(double x, double y) {
		return new ArrayVector2(x, y);
	}

	@Override
	default int size() {
		return 2;
	}

	@Override
	Vector2 clone();

	@Override
	Iterator<Double> iterator();

	@Override
	double[] toArray();

	@Override
	double[] toArray(double[] values);

	@Override
	boolean equals(Object object);

	boolean resembles(Vector2 vector);

	boolean compare(Vector2 vector, VectorPredicate predicate);

	boolean compare(Vector2 vector, VectorPredicate predicate, int... range);

	boolean compare(Vector2 vector, VectorPredicate predicate, boolean... mask);

	@Override
	Vector2 identity();

	@Override
	Vector2 invert();

	Vector2 invert(Vector2 target);

	@Override
	Vector2 applyLength(double length);

	@Override
	Vector2 applyLength(double length, double formerLength);

	@Override
	double length();

	@Override
	double lengthSquared();

	double distance(Vector2 vector);

	double distanceSquared(Vector2 vector);

	double dotProduct(Vector2 vector);

	@Override
	double get(int i);

	@Override
	Vector2 set(int i, double v);

	@Override
	Vector2 add(int i, double v);

	Vector2 add(Vector2 target, int i, double v);

	@Override
	Vector2 mul(int i, double v);

	Vector2 mul(Vector2 target, int i, double v);

	@Override
	Vector2 set(double scalar);

	@Override
	Vector2 add(double scalar);

	Vector2 add(Vector2 target, double scalar);

	@Override
	Vector2 mul(double scalar);

	Vector2 mul(Vector2 target, double scalar);

	Vector2 set(Vector2 vector);

	Vector2 add(Vector2 summand);

	Vector2 add(Vector2 target, Vector2 summand);

	Vector2 mul(Vector2 factor);

	Vector2 mul(Vector2 target, Vector2 factor);

	Vector2 set(double x, double y);

	Vector2 add(double x, double y);

	Vector2 add(Vector2 target, double x, double y);

	Vector2 mul(double x, double y);

	Vector2 mul(Vector2 target, double x, double y);

	double x();

	Vector2 setX(double x);

	double y();

	Vector2 setY(double y);

	Vector2 addX(double x);

	Vector2 addX(Vector2 target, double x);

	Vector2 addY(double y);

	Vector2 addY(Vector2 target, double y);

	Vector2 mulX(double x);

	Vector2 mulX(Vector2 target, double x);

	Vector2 mulY(double y);

	Vector2 mulY(Vector2 target, double y);
}
