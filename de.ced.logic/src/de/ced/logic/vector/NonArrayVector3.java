package de.ced.logic.vector;

import static de.ced.logic.math.Meth.*;

public abstract class NonArrayVector3 extends AbstractVector3 {

	protected NonArrayVector3() {}

	protected NonArrayVector3(double scalar) {
		set(scalar);
	}

	protected NonArrayVector3(Vector3 vector) {
		set(vector);
	}

	protected NonArrayVector3(double[] values) {
		set(values[0], values[1], values[2]);
	}

	protected NonArrayVector3(double x, double y, double z) {
		set(x, y, z);
	}

	@Override
	public Vector3 clone() {
		return Vector3.from(this);
	}

	@Override
	protected void replaceContents(double[] values) {
		set(values[0], values[1], values[2]);
	}

	@Override
	public double[] toArray(double[] values) {
		values[0] = x();
		values[1] = y();
		values[2] = z();
		return values;
	}

	@Override
	public boolean resembles(Vector3 vector) {
		Boolean could = couldResemble(vector);
		return could != null ? could : x() == vector.x() && y() == vector.y() && z() == vector.z();
	}

	@Override
	public boolean compare(Vector3 vector, VectorPredicate predicate) {
		return predicate.test(x(), vector.x()) && predicate.test(y(), vector.y()) && predicate.test(z(), vector.z());
	}

	@Override
	public boolean compare(Vector3 vector, VectorPredicate predicate, int... range) {
		boolean[] mask = new boolean[3];
		for (int i : range) {
			mask[i] = true;
		}
		return super.compare(vector, predicate, mask);
	}

	@Override
	public boolean compare(Vector3 vector, VectorPredicate predicate, boolean... mask) {
		return (!mask[0] || predicate.test(x(), vector.x()))
				&& (!mask[1] || predicate.test(y(), vector.y()))
				&& (!mask[2] || predicate.test(z(), vector.z()));
	}

	@Override
	public double lengthSquared() {
		return pow2(x()) + pow2(y()) + pow2(z());
	}

	@Override
	public double distance(Vector3 vector) {
		return Math.sqrt(distanceSquared(vector));
	}

	@Override
	public double distanceSquared(Vector3 vector) {
		return pow2(x() - vector.x()) + pow2(y() - vector.y()) + pow2(z() - vector.z());
	}

	@Override
	public double dotProduct(Vector3 vector) {
		return x() * vector.x() + y() * vector.y() + z() * vector.z();
	}

	@Override
	public Vector3 crossProduct(Vector3 target, Vector3 vector) {
		double x = y() * vector.z() - z() * vector.y();
		double y = z() * vector.x() - x() * vector.z();
		double z = x() * vector.y() - y() * vector.x();
		return set(x, y, z);
	}

	@Override
	public double get(int i) {
		return switch (i) {
		case 0 -> x();
		case 1 -> y();
		case 2 -> z();
		default -> throw new IndexOutOfBoundsException(i);
		};
	}

	@Override
	public Vector3 set(int i, double v) {
		switch (i) {
		case 0 -> setX(v);
		case 1 -> setY(v);
		case 2 -> setZ(v);
		default -> throw new IndexOutOfBoundsException(i);
		}
		return this;
	}

	@Override
	public Vector3 set(double scalar) {
		return set(scalar, scalar, scalar);
	}

	@Override
	public Vector3 add(Vector3 target, double scalar) {
		return add(target, scalar, scalar, scalar);
	}

	@Override
	public Vector3 mul(Vector3 target, double scalar) {
		return mul(target, scalar, scalar, scalar);
	}

	@Override
	public Vector3 set(Vector3 vector) {
		return set(vector.x(), vector.y(), vector.z());
	}

	@Override
	public Vector3 add(Vector3 target, Vector3 summand) {
		return add(target, summand.x(), summand.y(), summand.z());
	}

	@Override
	public Vector3 mul(Vector3 target, Vector3 factor) {
		return mul(target, factor.x(), factor.y(), factor.z());
	}

	@Override
	public Vector3 set(double x, double y, double z) {
		setX(x);
		setY(y);
		return setZ(z);
	}

	@Override
	public Vector3 add(Vector3 target, double x, double y, double z) {
		addX(target, x);
		addY(target, y);
		return addZ(target, z);
	}

	@Override
	public Vector3 mul(Vector3 target, double x, double y, double z) {
		mulX(target, x);
		mulY(target, y);
		return mulZ(target, z);
	}

	// Per axis
}
