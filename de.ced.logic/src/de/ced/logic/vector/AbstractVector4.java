package de.ced.logic.vector;

public abstract class AbstractVector4 extends AbstractVector3 implements Vector4 {

	public static final int SIZE = Vector4.SIZE;

	protected AbstractVector4() {}

	protected AbstractVector4(double scalar) {
		set(scalar);
	}

	protected AbstractVector4(Vector3 vector) {
		set(vector);
	}

	protected AbstractVector4(double[] values) {
		set(values[0], values[1], values[2], values[3]);
	}

	protected AbstractVector4(double x, double y, double z, double a) {
		set(x, y, z, a);
	}

	@Override
	public int size() {
		return Vector4.super.size();
	}

	@Override
	public Vector4 clone() {
		return (Vector4) super.clone();
	}

	@Override
	public boolean resembles(Vector4 vector) {
		return resembles0(vector, SIZE);
	}

	@Override
	public boolean compare(Vector4 vector, VectorPredicate predicate) {
		return compare0(vector, predicate, SIZE);
	}

	@Override
	public boolean compare(Vector4 vector, VectorPredicate predicate, int... range) {
		return compare0(vector, predicate, range, SIZE);
	}

	@Override
	public boolean compare(Vector4 vector, VectorPredicate predicate, boolean... mask) {
		return compare0(vector, predicate, mask, SIZE);
	}

	@Override
	public Vector4 identity() {
		return (Vector4) super.identity();
	}

	@Override
	public Vector4 invert() {
		return (Vector4) super.invert();
	}

	@Override
	public Vector4 invert(Vector4 target) {
		return (Vector4) super.invert(target);
	}

	@Override
	public Vector4 applyLength(double length) {
		return (Vector4) super.applyLength(length);
	}

	@Override
	public Vector4 applyLength(double length, double formerLength) {
		return (Vector4) super.applyLength(length, formerLength);
	}

	@Override
	public double distance(Vector4 vector) {
		return distance0(vector, SIZE);
	}

	@Override
	public double distanceSquared(Vector4 vector) {
		return distanceSquared0(vector, SIZE);
	}

	@Override
	public double dotProduct(Vector4 vector) {
		return dotProduct0(vector, SIZE);
	}

	@Override
	public abstract double get(int i);

	@Override
	public abstract Vector4 set(int i, double v);

	@Override
	public Vector4 add(int i, double v) {
		return (Vector4) super.add(i, v);
	}

	@Override
	public Vector4 add(Vector4 target, int i, double v) {
		return (Vector4) super.add(target, i, v);
	}

	@Override
	public Vector4 mul(int i, double v) {
		return (Vector4) super.mul(i, v);
	}

	@Override
	public Vector4 mul(Vector4 target, int i, double v) {
		return (Vector4) super.mul(target, i, v);
	}

	@Override
	public Vector4 set(double scalar) {
		return (Vector4) super.set(scalar);
	}

	@Override
	public Vector4 add(double scalar) {
		return (Vector4) super.add(scalar);
	}

	@Override
	public Vector4 add(Vector4 target, double scalar) {
		return (Vector4) super.add(target, scalar);
	}

	@Override
	public Vector4 mul(double scalar) {
		return (Vector4) super.mul(scalar);
	}

	@Override
	public Vector4 mul(Vector4 target, double scalar) {
		return (Vector4) super.mul(target, scalar);
	}

	@Override
	public Vector4 set(Vector4 vector) {
		return (Vector4) set0(vector, SIZE);
	}

	@Override
	public Vector4 add(Vector4 summand) {
		return add(this, summand);
	}

	@Override
	public Vector4 add(Vector4 target, Vector4 summand) {
		return (Vector4) add0(target, summand, SIZE);
	}

	@Override
	public Vector4 mul(Vector4 factor) {
		return mul(this, factor);
	}

	@Override
	public Vector4 mul(Vector4 target, Vector4 factor) {
		return (Vector4) mul0(target, factor, SIZE);
	}

	@Override
	public Vector4 set(double x, double y, double z, double a) {
		set(x, y, z);
		return setA(a);
	}

	@Override
	public Vector4 add(double x, double y, double z, double a) {
		return add(this, x, y, z, a);
	}

	@Override
	public Vector4 add(Vector4 target, double x, double y, double z, double a) {
		add(target, x, y, z);
		return addA(target, a);
	}

	@Override
	public Vector4 mul(double x, double y, double z, double a) {
		return mul(this, x, y, z, a);
	}

	@Override
	public Vector4 mul(Vector4 target, double x, double y, double z, double a) {
		mul(target, x, y, z);
		return mulA(target, a);
	}

	// Per axis

	@Override
	public abstract double x();

	@Override
	public abstract Vector4 setX(double x);

	@Override
	public abstract double y();

	@Override
	public abstract Vector4 setY(double y);

	@Override
	public abstract double z();

	@Override
	public abstract Vector4 setZ(double z);

	@Override
	public abstract double a();

	@Override
	public abstract Vector4 setA(double a);

	@Override
	public Vector4 addX(double x) {
		return addX(this, x);
	}

	@Override
	public Vector4 addX(Vector4 target, double x) {
		return target.setX(x() + x);
	}

	@Override
	public Vector4 addY(double y) {
		return addY(this, y);
	}

	@Override
	public Vector4 addY(Vector4 target, double y) {
		return target.setY(y() + y);
	}

	@Override
	public Vector4 addZ(double z) {
		return addZ(this, z);
	}

	@Override
	public Vector4 addZ(Vector4 target, double z) {
		return target.setZ(z() + z);
	}

	@Override
	public Vector4 addA(double a) {
		return addA(this, a);
	}

	@Override
	public Vector4 addA(Vector4 target, double a) {
		return target.setA(a() + a);
	}

	@Override
	public Vector4 mulX(double x) {
		return mulX(this, x);
	}

	@Override
	public Vector4 mulX(Vector4 target, double x) {
		return target.setX(x() * x);
	}

	@Override
	public Vector4 mulY(double y) {
		return mulY(this, y);
	}

	@Override
	public Vector4 mulY(Vector4 target, double y) {
		return target.setY(y() * y);
	}

	@Override
	public Vector4 mulZ(double z) {
		return mulZ(this, z);
	}

	@Override
	public Vector4 mulZ(Vector4 target, double z) {
		return target.setZ(z() * z);
	}

	@Override
	public Vector4 mulA(double a) {
		return mulA(this, a);
	}

	@Override
	public Vector4 mulA(Vector4 target, double a) {
		return target.setA(a() * a);
	}
}
