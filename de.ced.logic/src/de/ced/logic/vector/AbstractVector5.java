package de.ced.logic.vector;

public abstract class AbstractVector5 extends AbstractVector4 implements Vector5 {

	public static final int SIZE = Vector5.SIZE;

	protected AbstractVector5() {}

	protected AbstractVector5(double scalar) {
		set(scalar);
	}

	protected AbstractVector5(Vector3 vector) {
		set(vector);
	}

	protected AbstractVector5(double[] values) {
		set(values[0], values[1], values[2], values[3]);
	}

	protected AbstractVector5(double x, double y, double z, double a, double b) {
		set(x, y, z, a, b);
	}

	@Override
	public int size() {
		return Vector5.super.size();
	}

	@Override
	public Vector5 clone() {
		return (Vector5) super.clone();
	}

	@Override
	public boolean resembles(Vector5 vector) {
		return resembles0(vector, SIZE);
	}

	@Override
	public boolean compare(Vector5 vector, VectorPredicate predicate) {
		return compare0(vector, predicate, SIZE);
	}

	@Override
	public boolean compare(Vector5 vector, VectorPredicate predicate, int... range) {
		return compare0(vector, predicate, range, SIZE);
	}

	@Override
	public boolean compare(Vector5 vector, VectorPredicate predicate, boolean... mask) {
		return compare0(vector, predicate, mask, SIZE);
	}

	@Override
	public Vector5 identity() {
		return (Vector5) super.identity();
	}

	@Override
	public Vector5 invert() {
		return (Vector5) super.invert();
	}

	@Override
	public Vector5 invert(Vector5 target) {
		return (Vector5) super.invert(target);
	}

	@Override
	public Vector5 applyLength(double length) {
		return (Vector5) super.applyLength(length);
	}

	@Override
	public Vector5 applyLength(double length, double formerLength) {
		return (Vector5) super.applyLength(length, formerLength);
	}

	@Override
	public double distance(Vector5 vector) {
		return distance0(vector, SIZE);
	}

	@Override
	public double distanceSquared(Vector5 vector) {
		return distanceSquared0(vector, SIZE);
	}

	@Override
	public double dotProduct(Vector5 vector) {
		return dotProduct0(vector, SIZE);
	}

	@Override
	public abstract double get(int i);

	@Override
	public abstract Vector5 set(int i, double v);

	@Override
	public Vector5 add(int i, double v) {
		return (Vector5) super.add(i, v);
	}

	@Override
	public Vector5 add(Vector5 target, int i, double v) {
		return (Vector5) super.add(target, i, v);
	}

	@Override
	public Vector5 mul(int i, double v) {
		return (Vector5) super.mul(i, v);
	}

	@Override
	public Vector5 mul(Vector5 target, int i, double v) {
		return (Vector5) super.mul(target, i, v);
	}

	@Override
	public Vector5 set(double scalar) {
		return (Vector5) super.set(scalar);
	}

	@Override
	public Vector5 add(double scalar) {
		return (Vector5) super.add(scalar);
	}

	@Override
	public Vector5 add(Vector5 target, double scalar) {
		return (Vector5) super.add(target, scalar);
	}

	@Override
	public Vector5 mul(double scalar) {
		return (Vector5) super.mul(scalar);
	}

	@Override
	public Vector5 mul(Vector5 target, double scalar) {
		return (Vector5) super.mul(target, scalar);
	}

	@Override
	public Vector5 set(Vector5 vector) {
		return (Vector5) set0(vector, SIZE);
	}

	@Override
	public Vector5 add(Vector5 summand) {
		return add(this, summand);
	}

	@Override
	public Vector5 add(Vector5 target, Vector5 summand) {
		return (Vector5) add0(target, summand, SIZE);
	}

	@Override
	public Vector5 mul(Vector5 factor) {
		return mul(this, factor);
	}

	@Override
	public Vector5 mul(Vector5 target, Vector5 factor) {
		return (Vector5) mul0(target, factor, SIZE);
	}

	@Override
	public Vector5 set(double x, double y, double z, double a, double b) {
		set(x, y, z, a);
		return setB(b);
	}

	@Override
	public Vector5 add(double x, double y, double z, double a, double b) {
		return add(this, x, y, z, a, b);
	}

	@Override
	public Vector5 add(Vector5 target, double x, double y, double z, double a, double b) {
		add(target, x, y, z, a);
		return addB(target, b);
	}

	@Override
	public Vector5 mul(double x, double y, double z, double a, double b) {
		return mul(this, x, y, z, a, b);
	}

	@Override
	public Vector5 mul(Vector5 target, double x, double y, double z, double a, double b) {
		mul(target, x, y, z, b);
		return mulB(target, a);
	}

	// Per axis

	@Override
	public abstract double x();

	@Override
	public abstract Vector5 setX(double x);

	@Override
	public abstract double y();

	@Override
	public abstract Vector5 setY(double y);

	@Override
	public abstract double z();

	@Override
	public abstract Vector5 setZ(double z);

	@Override
	public abstract double a();

	@Override
	public abstract Vector5 setA(double a);

	@Override
	public abstract double b();

	@Override
	public abstract Vector5 setB(double b);

	@Override
	public Vector5 addX(double x) {
		return addX(this, x);
	}

	@Override
	public Vector5 addX(Vector5 target, double x) {
		return target.setX(x() + x);
	}

	@Override
	public Vector5 addY(double y) {
		return addY(this, y);
	}

	@Override
	public Vector5 addY(Vector5 target, double y) {
		return target.setY(y() + y);
	}

	@Override
	public Vector5 addZ(double z) {
		return addZ(this, z);
	}

	@Override
	public Vector5 addZ(Vector5 target, double z) {
		return target.setZ(z() + z);
	}

	@Override
	public Vector5 addA(double a) {
		return addA(this, a);
	}

	@Override
	public Vector5 addA(Vector5 target, double a) {
		return target.setA(a() + a);
	}

	@Override
	public Vector5 addB(double b) {
		return addB(this, b);
	}

	@Override
	public Vector5 addB(Vector5 target, double b) {
		return target.setB(b() + b);
	}

	@Override
	public Vector5 mulX(double x) {
		return mulX(this, x);
	}

	@Override
	public Vector5 mulX(Vector5 target, double x) {
		return target.setX(x() * x);
	}

	@Override
	public Vector5 mulY(double y) {
		return mulY(this, y);
	}

	@Override
	public Vector5 mulY(Vector5 target, double y) {
		return target.setY(y() * y);
	}

	@Override
	public Vector5 mulZ(double z) {
		return mulZ(this, z);
	}

	@Override
	public Vector5 mulZ(Vector5 target, double z) {
		return target.setZ(z() * z);
	}

	@Override
	public Vector5 mulA(double a) {
		return mulA(this, a);
	}

	@Override
	public Vector5 mulA(Vector5 target, double a) {
		return target.setA(a() * a);
	}

	@Override
	public Vector5 mulB(double b) {
		return mulB(this, b);
	}

	@Override
	public Vector5 mulB(Vector5 target, double b) {
		return target.setB(b() * b);
	}
}
