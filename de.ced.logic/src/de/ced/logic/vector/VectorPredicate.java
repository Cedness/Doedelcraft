package de.ced.logic.vector;

public interface VectorPredicate {
	boolean test(double v1, double v2);
}
