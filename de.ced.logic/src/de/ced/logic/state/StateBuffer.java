package de.ced.logic.state;

import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

public class StateBuffer<R> {

	protected BooleanSupplier upToDate;
	protected BooleanSupplier shouldSave;
	protected Supplier<R> supplier;
	protected R result;

	public StateBuffer(BooleanSupplier upToDate, BooleanSupplier shouldSave, Supplier<R> supplier) {
		this.upToDate = upToDate;
		this.shouldSave = shouldSave;
		this.supplier = supplier;
	}

	public R get() {
		if (upToDate.getAsBoolean()) {
			return result;
		}
		R result = supplier.get();
		if (shouldSave.getAsBoolean()) {
			this.result = result;
		}
		return result;
	}

	public R last() {
		return result;
	}
}
