package de.ced.logic.state;

import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

public class StampStateBuffer<R, S> extends StateBuffer<R> {

	protected BooleanSupplier shouldActuallySave;
	protected Supplier<S> stampSupplier;
	protected S stamp;

	public StampStateBuffer(Supplier<S> stampSupplier, BooleanSupplier shouldSave, Supplier<R> supplier) {
		super(null, null, supplier);
		this.stampSupplier = stampSupplier;
		shouldActuallySave = shouldSave;
		upToDate = this::upToDate;
		this.shouldSave = this::shouldSave;
	}

	protected boolean upToDate() {
		return stampSupplier.equals(stamp);
	}

	protected boolean shouldSave() {
		if (shouldActuallySave.getAsBoolean()) {
			stamp = stampSupplier.get();
			return true;
		}
		return false;
	}
}
