package de.ced.logic.collection;

import java.util.Arrays;
import java.util.List;

public class DimensionalLists {

	public static List<Object> getMultidimensionalList(int... sizes) {
		return getMultidimensionalListPart(0, sizes);
	}

	private static List<Object> getMultidimensionalListPart(int d, int... sizes) {
		List<Object> list = Arrays.asList(new Object[sizes[d]]);
		if (d < sizes.length - 1) {
			for (int i = 0; i < d; i++) {
				list.set(i, getMultidimensionalListPart(d + 1, sizes));
			}
		}
		return list;
	}
}
