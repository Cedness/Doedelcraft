package de.ced.logic.collection;

/**
 * <strong>Warning!</strong> An object using this implementation can only be
 * contained by one {@link IndexingCollection} at a time, but therefore the
 * overhead of a map can be avoided.
 * 
 * @author Ced
 */
public interface SingleIndexable extends Indexable {

	@Override
	default int getPIndex(IndexingCollection<? extends Indexable> collection) {
		return getPIndex();
	}

	@Override
	default void setPIndex(IndexingCollection<? extends Indexable> collection, int index) {
		setPIndex(index);
	}

	int getPIndex();

	void setPIndex(int index);
}
