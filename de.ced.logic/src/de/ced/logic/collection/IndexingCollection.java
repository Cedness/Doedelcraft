package de.ced.logic.collection;

import java.util.Collection;

public interface IndexingCollection<E extends Indexable> extends Collection<E> {}
