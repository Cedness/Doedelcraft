package de.ced.logic.collection;

import java.util.Map;

public interface MultiIndexable extends Indexable {

	@Override
	default int getPIndex(IndexingCollection<? extends Indexable> collection) {
		return getPIndexMap().getOrDefault(collection, -1);
	}

	@Override
	default void setPIndex(IndexingCollection<? extends Indexable> collection, int index) {
		Map<IndexingCollection<? extends Indexable>, Integer> indexMap = getPIndexMap();
		if (index < 0) {
			indexMap.remove(collection);
		} else {
			indexMap.put(collection, index);
		}
	}

	Map<IndexingCollection<? extends Indexable>, Integer> getPIndexMap();
}
