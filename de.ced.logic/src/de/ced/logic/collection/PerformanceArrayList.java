package de.ced.logic.collection;

import java.util.AbstractList;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Queue;

@SuppressWarnings("unchecked")
public class PerformanceArrayList<E> extends AbstractList<E> {

	private Object[] values;
	private int size = 0;
	private int firstUnusedSlot = 0;
	private final Queue<Integer> emptySlots = new LinkedList<>();

	public PerformanceArrayList() {
		this(16);
	}

	public PerformanceArrayList(int initialCapacity) {
		values = new Object[initialCapacity];
	}

	public PerformanceArrayList(Collection<E> collection) {
		this(collection.toArray());
	}

	public PerformanceArrayList(Collection<E> collection, int initialCapacity) {
		this(collection.toArray(new Object[initialCapacity]));
	}

	private PerformanceArrayList(Object[] values) {
		this.values = values;
		size = values.length;
		for (firstUnusedSlot = size; firstUnusedSlot > 0 && values[firstUnusedSlot - 1] == null; firstUnusedSlot--, size--);
		for (int i = 0; i < firstUnusedSlot - 1; i++) {
			if (values[i] == null) {
				emptySlots.add(i);
				size--;
			}
		}
	}

	@Override
	public E get(int index) {
		if (index < 0 || firstUnusedSlot <= index) {
			throw new IndexOutOfBoundsException(index);
		}
		E element = (E) values[index];
		if (element == null) {
			throw new IndexOutOfBoundsException(index);
		}
		return element;
	}

	@Override
	public int size() { return size; }

	public int addGetIndex(E element) {
		requireNonNull(element);
		int index;
		if (emptySlots.isEmpty()) {
			index = firstUnusedSlot;
			if (index == Integer.MAX_VALUE) {
				throw new IndexOutOfBoundsException("Reached maximum number of elements");
			}
			if (index == values.length) {
				int newCapacity = values.length * 2;
				if (newCapacity < values.length) {
					newCapacity = Integer.MAX_VALUE;
				}
				growTo(newCapacity);
			}
			firstUnusedSlot++;
		} else {
			index = emptySlots.remove();
		}
		values[index] = element;
		size++;
		return index;
	}

	@Override
	public boolean add(E element) {
		addGetIndex(element);
		return true;
	}

	@Override
	public E set(int index, E element) {
		requireNonNull(element);
		E formerElement = get(index);
		values[index] = element;
		return formerElement;
	}

	protected static void requireNonNull(Object o) {
		Objects.requireNonNull(o, "Element may not be null");
	}

	@Override
	public E remove(int index) {
		E formerElement = get(index);
		remove0(index);
		return formerElement;
	}

	protected void remove0(int index) {
		values[index] = null;
		emptySlots.add(index);
		size--;
	}

	public void ensureCapacity(int capacity) {
		if (capacity > values.length) {
			growTo(capacity);
		}
	}

	private void growTo(int capacity) {
		Object[] formerValues = values;
		values = new Object[capacity];
		System.arraycopy(formerValues, 0, values, 0, firstUnusedSlot);
	}

	@Override
	public Iterator<E> iterator() { return new Itr(); }

	private class Itr implements Iterator<E> {
		int cursor;
		int lastRet = -1;
		int expectedModCount = modCount;

		@Override
		public boolean hasNext() {
			do {
				if (++cursor >= firstUnusedSlot) {
					return false;
				}
			} while (values[cursor] == null);
			return true;
		}

		@Override
		public E next() {
			checkForComodification();
			try {
				E element = (E) values[cursor];
				lastRet = cursor;
				return element;
			} catch (IndexOutOfBoundsException ex) {
				checkForComodification();
				throw new NoSuchElementException();
			}
		}

		@Override
		public void remove() {
			if (lastRet < 0) {
				throw new IllegalStateException();
			}
			checkForComodification();

			try {
				PerformanceArrayList.this.remove(lastRet);
				lastRet = -1;
				expectedModCount = modCount;
			} catch (IndexOutOfBoundsException ex) {
				throw new ConcurrentModificationException();
			}
		}

		void checkForComodification() {
			if (modCount != expectedModCount) {
				throw new ConcurrentModificationException();
			}
		}
	}

	@Override
	public ListIterator<E> listIterator() { return null; }

	@Override
	public ListIterator<E> listIterator(int index) { return null; }
}
