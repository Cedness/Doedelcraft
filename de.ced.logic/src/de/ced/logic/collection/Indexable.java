package de.ced.logic.collection;

public interface Indexable {

	default boolean hasPIndex(IndexingCollection<? extends Indexable> collection) {
		return getPIndex(collection) >= 0;
	}

	int getPIndex(IndexingCollection<? extends Indexable> collection);

	void setPIndex(IndexingCollection<? extends Indexable> collection, int index);
}
