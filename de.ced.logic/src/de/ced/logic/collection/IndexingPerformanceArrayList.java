package de.ced.logic.collection;

import java.util.Collection;

public class IndexingPerformanceArrayList<E extends Indexable> extends PerformanceArrayList<E> implements IndexingCollection<E> {

	public IndexingPerformanceArrayList() { super(); }

	public IndexingPerformanceArrayList(int initialCapacity) { super(initialCapacity); }

	public IndexingPerformanceArrayList(Collection<E> collection) { super(collection); }

	public IndexingPerformanceArrayList(Collection<E> collection, int initialCapacity) { super(collection, initialCapacity); }

	@Override
	public int addGetIndex(E element) {
		requireNonNull(element);
		requireUnindexed(element);
		int index = super.addGetIndex(element);
		element.setPIndex(this, index);
		return index;
	}

	@Override
	public E set(int index, E element) {
		requireNonNull(element);
		requireUnindexed(element);
		E formerElement = super.set(index, element);
		formerElement.setPIndex(this, -1);
		element.setPIndex(this, index);
		return formerElement;
	}

	protected void requireUnindexed(Indexable element) {
		if (element.hasPIndex(this)) {
			throw new IllegalStateException("Element already has an index");
		}
	}

	@Override
	public E remove(int index) {
		E formerElement = super.remove(index);
		formerElement.setPIndex(this, -1);
		return formerElement;
	}

	@Override
	public boolean remove(Object o) {
		int index = indexOf(o);
		if (index >= 0) {
			remove0(index);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean contains(Object o) {
		return indexOf(o) >= 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public int indexOf(Object o) {
		requireNonNull(o);
		E element = (E) o;
		int index = element.getPIndex(this);
		if (index < 0) {
			return -1;
		}
		E sameElement = get(index);
		if (!sameElement.equals(element)) {
			return -1;
		}
		return index;
	}
}
